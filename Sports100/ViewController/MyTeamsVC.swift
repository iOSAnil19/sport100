//
//  MyTeamsVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 29/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class MyTeamsVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var barItem_Add:UIBarButtonItem!
    // @IBOutlet weak var barItem_Search:UIBarButtonItem!
    
    let refreshControl = UIRefreshControl()
    
    var pageNo = 1
    var totalPageCount = 1
   // var teams = [Team]()
    var sportTeams = [TeamSport]()
    var delegate:TeamsDelegate?
    var userId = ""
    var isBool = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Teams"
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(MyTeamsVC.refreshMethod), for: .valueChanged)
        showHud("Processing..")
       // self.getTeamListing()
        
        if self.delegate is ProfileMessageCell {
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.rightBarButtonItems = []
            barItem_Add = nil
            self.setCancelButton()
        }
        if isBool == true{
        let newBtn = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(anotherMethod))
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = newBtn
        }
        self.getGroupedSport()

        navigationItem.largeTitleDisplayMode = .never
       setNavigationBarImage()
    }
    @objc func anotherMethod(){
      self.dismiss(animated: true, completion: nil)
        
    }
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func refreshMethod() {
        if self != nil {
            pageNo = 1
            self.getGroupedSport()

          //  self.getTeamListing()
        }
    }
    
    
    func getGroupedSport() {
        let params = ["key": Api.key, "userID":userId]
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getGoupedSports, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    
                    if success == 200 {
                        
                        if let allGroupedTeams = result["sports"] as? [[String:Any]], allGroupedTeams.count > 0 {
                            
                            let groupedTeams = allGroupedTeams.map({ (sportInfo) -> TeamSport in
                                TeamSport(object: sportInfo)
                            })
                            self.sportTeams.removeAll()
                            for sportTeam in groupedTeams {
                                if let allTeams = sportTeam.teams, allTeams.count > 0 {
                                    let iAmTeamsMember = allTeams.filter({ (teamMember) -> Bool in
                                        teamMember.member == "Yes"
                                    })
                                    if iAmTeamsMember.count > 0 {
                                        self.sportTeams.append(sportTeam)
                                    }
                                }
                            }
                            if self.table_View != nil {
                                self.table_View.reloadData()
                            }
                        }
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
   /* func getTeamListing() {
        
        if userId == "" {
            userId = userInfo?.userId ?? ""
        }
        
        let params = ["key": Api.key, "userID":userId]
        var requestString = Api.getAllUserTeam
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 1 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result["success"] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allTeams = result["teams"] as? [[String:Any]], allTeams.count > 0 {
                            
                            let teamModels = allTeams.map ({ (teamInfo) -> Team in
                                Team.init(object: teamInfo)
                               
                            })
                            if self.pageNo == 1 {
                                self.teams = teamModels
                            } else {
                                for t in teamModels {
                                     self.teams.append(t)
                                }
                            }
                            if self.table_View != nil {
                                self.table_View.reloadData()
                            }
                        }
                    } else {
                        let error = "\(String(describing: result["message"] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MyTeamsVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sportTeams.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sport = self.sportTeams[section]
        if let teams = sport.teams, teams.count > 0 {
            
            let iAmTeamsMember = teams.filter({ (teamMember) -> Bool in
                teamMember.member == "Yes"
            })
            return iAmTeamsMember.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "TeamCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? TeamCell
        let sport = self.sportTeams[indexPath.section]
        if let teams = sport.teams, teams.count > 0 {
            let iAmTeamsMember = teams.filter({ (teamMember) -> Bool in
                teamMember.member == "Yes"
            })
            let team = iAmTeamsMember[indexPath.row]
            cell?.setUPTeamCell(team)
        }
        return cell ?? UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        
//        if editingStyle == .delete {
//            let sport = self.sportTeams[indexPath.section]
//            if var teams = sport.teams, teams.count > 0 {
//                let iAmTeamsMember = teams.filter({ (teamMember) -> Bool in
//                    teamMember.member == "Yes"
//                })
//                let team = iAmTeamsMember[indexPath.row]
//                self.removeTeam(team)
//                teams.remove(at: indexPath.row)
//                tableView.beginUpdates()
//                tableView.deleteRows(at: [indexPath], with: .fade)
//                tableView.endUpdates()
//            }
//        }
//    }
    
    func removeTeam(_ team:Team) {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.removeTeam, andParameter: params) { (response, error) in
        }
    }
}

extension MyTeamsVC : UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80 * getScaleFactor()
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let nibs = Bundle.main.loadNibNamed("TeamHeader", owner: self, options: nil)
//        let headerView = nibs?.last as? TeamHeader
//        let sport = self.sportTeams[section]
//        if let teams = sport.teams, teams.count > 0 {
//            if let sportId = sport.iD {
//                headerView?.setUpView(sportId)
//                return headerView
//            }
//        }
//        return nil
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
    
    /* func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     if indexPath.row == teams.count - 2 {
     if totalPageCount == pageNo {
     return
     }
     pageNo = pageNo + 1
     self.getTeamListing()
     }
     }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sport = self.sportTeams[indexPath.section]
        if let teams = sport.teams, teams.count > 0 {
            let iAmTeamsMember = teams.filter({ (teamMember) -> Bool in
                teamMember.member == "Yes"
            })
            let team = iAmTeamsMember[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: false)
            
            if let sender = self.delegate, sender is UITableViewCell {
                sender.teamSelected(team)
                self.dismiss(animated: true, completion: nil)
                return
            }
            
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.team = team
            vc.teamSport = sport
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc,animated:true)
        }
    }
}




