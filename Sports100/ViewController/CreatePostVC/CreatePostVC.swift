//
//  CreatePostVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 06/12/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class CreatePostVC: UIViewController {
    
    //MARK: ------------- VARIABLE/OUTLET ----------------
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var imagePost: UIImageView!
    
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    weak var rootVC:UIViewController?
    @IBOutlet weak var lblCharactersLeft: UILabel!
    let CharacterLimit = 240
    
    var videoURL: URL? {
        didSet {
            if videoURL != nil {
                imgPlay.isHidden = false
                btnPlay.isHidden = false
            } else {
                imgPlay.isHidden = true
                btnPlay.isHidden = true
            }
        }
    }
    
    var selectedImage: UIImage? {
        didSet {
            if let img = self.selectedImage {
                imagePost.isHidden = false
                imagePost.image = img
                btnPlay.isHidden = false
            } else {
                imagePost.isHidden = true
                imagePost.image = nil
            }
        }
    }
    
    //MARK: ------------- VIEW LIFE CYCLE ----------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        imgPlay.isHidden = true
        btnPlay.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
//        imgPlay.isHidden = true
//        btnPlay.isHidden = true
    }
    
    //MARK: ------------- UIBUTTON ACTION METHODS ----------------
    
    @IBAction func buttonClickedCross(_ sender: UIButton) {
        textView.resignFirstResponder()
        self.selectedImage = nil
        self.videoURL = nil
    }
    
    @IBAction func buttonClickedDone() {
        
        if self.selectedImage != nil || self.textView.text != "" || self.videoURL != nil {
            
            textView.resignFirstResponder()
            if (textView.text ?? "").trimmingCharacters(in: .whitespaces).isEmpty {
                textView.text = nil
            }
            
            var postAction: UIAlertAction?
            var alertMessage = ""
            
            if let img = self.selectedImage {
                
                if let urlExist = self.videoURL {
                    alertMessage = "Would you like to post this video?"
                    if let postText = textView.text, postText != "" {
                        // post with image + text
                        postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
                            self.makeVideoPost(urlExist, postText)
                        }
                        // videoTitle
                        // description
                    } else {
                        postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
                            self.makeVideoPost(urlExist, nil)
                        }
                        // post with video
                    }
                } else {
                    alertMessage = "Would you like to post this picture?"
                    if let postText = textView.text, postText != "" {
                        // post with image + text
                        postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
                            self.makeImagePost(img, postText)
                        }
                        // imageTitle
                        // description
                    } else {
                        postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
                            self.makeImagePost(img, nil)
                        }
                        // post with image
                    }
                }
            } else {
                if let postText = textView.text, postText != "" {
                    // post with text
                    alertMessage = "Would you like to post?"
                    postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
                        self.makeTextPost(postText)
                    }
                }
            }
            
            showAlert(nil, message: alertMessage, withAction: postAction, with: true, andTitle: "Cancel", onView: self)
        }
    }
    
    @IBAction func buttonClickedCamera() {
        textView.resignFirstResponder()
        if let cameraNavigationVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CameraNavigationVC) as? UINavigationController {
            let vcs = cameraNavigationVC.viewControllers
            if let viewController = vcs.first as? CustomCameraViewController {
                viewController.rootVC = self
            }
            self.present(cameraNavigationVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonClicked_Play(_ sender:UIButton) {
        if let url = self.videoURL {
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            
            if let photoPreviewVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.PhotoPreviewVC) as? PhotoPreviewVC {
                photoPreviewVC.selectedImage = selectedImage
                photoPreviewVC.rootVC = self.rootVC
                photoPreviewVC.isBool = true
                self.navigationController?.pushViewController(photoPreviewVC, animated: true)
            }
//
//            let image = selectedImage
//            let vc = PhotoPreviewVC()
//            vc.selectedImage = image
//            self.present(vc, animated: true)
        }
    }
    
    //MARK: ------------- Private Methods ----------------
    
    private func makeTextPost(_ text:String) {

        showHud("Processing..")
        let params = ["key": Api.key,
                      ApiConstant.userId: userInfo?.userId ?? "",
                      ApiConstant.achieveDescription: text,
                      ApiConstant.achieveDate: getDateFromDate(Date()),
                      "type":"PostText",
                      "video": "",
                      "post_image": "",] as [String : Any]
        print(params)
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.addFeedPost, andParameter: params) { (response, error) in
            self.handleErrorAndResponse(response, error)
        }
        
    }
    
    private func makeImagePost(_ image:UIImage,_ text: String?) {
        
        var desc = ""
        if let t = text { desc = t }
        let params = ["key":Api.key,
                      "userID":userInfo?.userId ?? "",
                      ApiConstant.achieveDate: getDateFromDate(Date()),
                      "title":"",
                      "type":"Image",
                      "description":desc,
                      "video": ""]
        
        
        let networkManager = NetworkManager()
        guard let imageData = UIImageJPEGRepresentation(image, 1) else { return }
        showHud("Processing..")
        networkManager.uploadImage(Api.addFeedPost, onPath: imageData, placeholderImage: nil,ApiConstant.post_photo, andParameter: params) { (response, error) in
                self.handleErrorAndResponse(response, error)
        }
    }
    
    func makeVideoPost(_ video_URL:URL,_ text: String?) {
        
        showHud("Processing..")
        var desc = ""
        if let t = text { desc = t }

        
        let params = ["key":"\(Api.key)",
                      "userID":userInfo?.userId ?? "",
                       "aDate": "\(getDateFromDate(Date()))",
                      "type":"Video",
                      
                      "description":"\(desc)",
        ]
        print(params)
        let networkManager = NetworkManager()
        
        var videoData: Data? = nil
        var imageData: Data? = nil
        if let thumbImage = getThumbnailFrom(path: video_URL) {
            imageData = UIImagePNGRepresentation(thumbImage)
        }
        
//            let uploadURL = URL(fileURLWithPath: URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("temporaryPreview.mov").absoluteString)
        do {
        
            
//            compressVideo(video_URL, outputURL: uploadURL, handler: { completion in
//                if completion?.status == .completed {
//                    let newDataForUpload = Data(contentsOf: uploadURL)
//                    print("Size of new Video after compression is (bytes):\(newDataForUpload?.count ?? 0)")
//                }
//            })
            videoData = try Data(contentsOf:video_URL, options: NSData.ReadingOptions.uncached)
            
            
        } catch {
            print(error.localizedDescription)
        }
        
        networkManager.uploadImage(Api.uploadVideo, onPath:videoData, placeholderImage: imageData, ApiConstant.post_Video,andParameter: params) { (response, error) in
            self.handleErrorAndResponse(response, error)
        }
        
        
        
        
        
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let timestamp = asset.duration
            print("Timestemp:   \(timestamp)")
            let cgImage = try imgGenerator.copyCGImage(at: timestamp, actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    private func handleErrorAndResponse(_ response: Any?, _ error: Error?) {
        hideHud()
        if error == nil {
            if let result = response as? [String:Any] {
                let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                if success == 200 {
                    textView.text = ""
                    selectedImage = nil
                    btnPlay.isHidden = true
                    self.tabBarController?.selectedIndex = 0
                    
                    if let profileNavVC = self.tabBarController?.viewControllers?.last as? UINavigationController, profileNavVC.viewControllers.count > 0 {
                        if let profileVC = profileNavVC.viewControllers[0] as? ProfileVC {
                            GetUser.getUserProfileInfo({ (user) in
                                if profileVC.table_View != nil {
        
                                    showAlert(ERROR, message:result[NetworkManager.kMESSAGE] as! String, onView: self)
                                    profileVC.table_View.reloadData()
                                }
                            })
                        }
                    }
                    
                    if let feedNavVC = self.tabBarController?.viewControllers?.first as? UINavigationController, feedNavVC.viewControllers.count > 0 {
                        if let feedVC = feedNavVC.viewControllers[0] as? FeedsVC {
                            feedVC.refreshMethod()
                        }
                    }
                    
                    // showAlert("", message: "Image Uploaded Succesfully", onView: self.tabBarController ?? self)
                    AppDelegate.getDelegate().previousIndex = 0
                    AppDelegate.getDelegate().currentIndex = 0
                    self.selectedImage = nil
                } else {
                    let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                    showAlert(ERROR, message: error.capitalized, onView: self)
                }
            } else {
                showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
            }
        } else {
            showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
        }
    }
}

//MARK: ------------- UITextView Delegate Method ----------------

extension CreatePostVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let size = CGSize(width: view.frame.width - 34, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        textView.constraints.forEach { (constraint) in
            if constraint.firstAttribute == .height {
                constraint.constant = estimatedSize.height
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        self.lblCharactersLeft.text = "\(CharacterLimit - updatedText.count) Characters"
        return updatedText.count < CharacterLimit
    }
    
    func compressVideo(_ inputURL: URL?, outputURL: URL?, handler completion: @escaping (AVAssetExportSession?) -> Void) {
        var urlAsset: AVURLAsset? = nil
        if let inputURL = inputURL {
            urlAsset = AVURLAsset(url: inputURL, options: nil)
        }
        var exportSession: AVAssetExportSession? = nil
        if let urlAsset = urlAsset {
            exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality)
        }
        exportSession?.outputURL = outputURL
        exportSession?.outputFileType = .mov
        exportSession?.shouldOptimizeForNetworkUse = true
        exportSession?.exportAsynchronously(completionHandler: {
            completion(exportSession)
        })
    }
   

}
