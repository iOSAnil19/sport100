//
//  CookiePolicyCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 06/08/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class CookiePolicyCell: UITableViewCell {
    
    @IBOutlet weak var btn_Necessary: UIButton!
    @IBOutlet weak var btn_Analytics: UIButton!
    @IBOutlet weak var btn_Functionalities: UIButton!
    @IBOutlet weak var btn_Targeting: UIButton!
    @IBOutlet weak var btn_SocialMedia: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonClicked_Necessary(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonClicked_Analytics(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonClicked_Functionalities(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonClicked_Targeting(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonClicked_SocialMedia(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func checkForSelection() {
        guard let parentVC = self.parentViewController as? TermsAndConditionVC else { return }
        if btn_Necessary.isSelected || btn_Analytics.isSelected || btn_Functionalities.isSelected || btn_Targeting.isSelected || btn_SocialMedia.isSelected {
            parentVC.signUpVC?.cookiePolicy = true
        } else {
            parentVC.signUpVC?.cookiePolicy = false
        }
    }
}
