//
//  PrivacyPolicyCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 06/08/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class PrivacyPolicyCell: UITableViewCell {
    
    @IBOutlet weak var btn_Yes: UIButton!
    @IBOutlet weak var btn_No: UIButton!
    
    var optionChoosen = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonClicked_Yes(_ sender: UIButton) {
        guard let parentVC = self.parentViewController as? TermsAndConditionVC else { return }
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            parentVC.privacyPolicy = true
        }
        parentVC.signUpVC?.privacyPolicy = sender.isSelected
        btn_No.isSelected = false
        optionChoosen = 1
    }
    
    @IBAction func buttonClicked_No(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        btn_Yes.isSelected = false
        optionChoosen = 2
        if let parentVC = self.parentViewController as? TermsAndConditionVC {
            parentVC.signUpVC?.cookiePolicy = false
            if sender.isSelected {
                parentVC.privacyPolicy = false
            }
            showAlert(AppTitle, message: "We're sorry, we can't proceed with your sign up. Agreement to our Privacy Policy is essential for us to fulfill our obligation to our users.", onView: parentVC)
        }
    }
}
