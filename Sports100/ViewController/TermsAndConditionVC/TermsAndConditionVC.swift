//
//  TermsAndConditionVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 05/08/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire

protocol TermsAndConditions {
    func checkTerms(isChecked:Bool)
}

class TermsAndConditionVC: UIViewController {
    
    @IBOutlet weak var table_View: UITableView!
    var signUpVC: SignUpVC?
    var tcArray = ["Terms and Conditions", "Privacy Policy", "Mailing List"]
    var idArray = ["TermsAndConditionCell", "PrivacyPolicyCell", "MailingCell"]
    var delegate: TermsAndConditions?
    var privacyPolicy = false
    var priviceyPolicy:Bool?
    var termsAndConditions = false
    var termsAndCondition :Bool?
  var param : [String: Any]?

    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func termsAndConditions(tap: UITapGestureRecognizer) {
        if let myLabel = tap.view as? UILabel {
            guard let range = myLabel.text?.range(of: "Terms and Conditions")?.nsRange else {
                return
            }
           // if tap.didTapAttributedTextInLabel(label: myLabel, inRange: range) {
                if let url = URL.init(string:termsConditionsURL) {
                    let svc = SFSafariViewController.init(url: url)
                    self.present(svc, animated: true, completion: nil)
                }
          //  }
        }
    }
    
    @objc func privacyPolicy(tap: UITapGestureRecognizer) {
        if let myLabel = tap.view as? UILabel {
            guard let range = myLabel.text?.range(of: "Privacy Policy")?.nsRange else {
                return
            }
            if let url = URL.init(string:privacyURL) {
                let svc = SFSafariViewController.init(url: url)
                self.present(svc, animated: true, completion: nil)
            }
            if tap.didTapAttributedTextInLabel(label: myLabel, inRange: range) {
                
            }
        }
    }
    
    @IBAction func buttonClickedDone(_ sender: UIButton) {
        if privacyPolicy == true && termsAndConditions ==  true{
          if let data = self.param {
          self.sendRequestToServer(data, forRequest: Api.userRegister)
      }
//        if let del = delegate{
//            del.checkTerms(isChecked: true)
//        }
//
//        self.navigationController?.popViewController(animated: true)
        }
        else{
                
        }
    }
  
  private func sendRequestToServer(_ parameters:[String:Any], forRequest request:String) {
          
          showHud("Processing..")
          
          let networkManager = NetworkManager()
  //        networkManager.getDataForRequest(request, andParameter: parameters) { (response, error) in
          Alamofire.request(url.userRegister, method: .post, parameters: parameters, encoding: URLEncoding.default , headers: nil).responseJSON { (responseData) in
                      
              hideHud()
              print(responseData.value ?? "No response")
              DispatchQueue.main.async {

              }
              if responseData.error == nil {
                  if let result = responseData.value as? [String:Any] {
                      let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                      print(result)
                      if success == 200 {
                          
                          if let userDetail = result["userdetails"] as? [String:Any] {
                              UserInfo.sharedInfo.setUserProfileWithInfo(userDetail)
                          }
                        self.goToCompletePage()
//                          if self.social_User == nil {
//                              self.goToCompletePage()
//                          } else {
//                              self.goToCompletePage()
//                              UserDefaults.standard.set("", forKey: CACHED_MAIL)
//                             // self.goToFeedView()
//                          }
                      } else {
                          let error = "\(String(describing: result[NetworkManager.Error] ?? INTERNAL_ERROR_MESSAGE))"
                          showAlert(ERROR, message: error.capitalized, onView: self)
                      }
                  } else {
                      showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                  }
              } else {
                  print(responseData.error?.code)
                  showAlert(ERROR, message: responseData.error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
              }
          }
      }
  
  private func goToCompletePage() {
      
      if let completeRegisterVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CompleteRegisterVC) {
          self.navigationController?.pushViewController(completeRegisterVC, animated: true)
      }
  }
  
  
}

extension TermsAndConditionVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tcArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = idArray[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if let tappableLabel = cell?.contentView.viewWithTag(101) as? UILabel {
            let tap = UITapGestureRecognizer(target: self, action: #selector(termsAndConditions(tap:)))
            tappableLabel.addGestureRecognizer(tap)
            if let cellExist = cell as? TermsAndConditionCell {
                if self.termsAndConditions == true || cellExist.optionChoosen == 1 {
                    cellExist.btn_Yes.isSelected = true
                    cellExist.btn_No.isSelected = false
                } else {
                    cellExist.btn_Yes.isSelected = false
                    if cellExist.optionChoosen == 2 {
                        cellExist.btn_No.isSelected = true
                    }
                }
                if cellExist.btn_Yes.isSelected == true{
                    termsAndCondition = true
                }else{
                    termsAndCondition = false
                }
            }
        }
        
        if let newLabel = cell?.contentView.viewWithTag(102) as? UILabel {
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(privacyPolicy(tap:)))
            newLabel.addGestureRecognizer(tap1)
            if let cellExist = cell as? PrivacyPolicyCell {
                if self.privacyPolicy == true || cellExist.optionChoosen == 1 {
                    cellExist.btn_Yes.isSelected = true
                    cellExist.btn_No.isSelected = false
                } else {
                    cellExist.btn_Yes.isSelected = false
                    if cellExist.optionChoosen == 2 {
                        cellExist.btn_No.isSelected = true
                    }
                }
                if cellExist.btn_Yes.isSelected == true{
                    priviceyPolicy = true
                }else{
                    priviceyPolicy = false
                }
            }
        }
        
        if let cellExist = cell as? MailingCell {
            if cellExist.optionChoosen == 1 {
                cellExist.btn_Yes.isSelected = true
                cellExist.btn_No.isSelected = false
            } else {
                cellExist.btn_Yes.isSelected = false
                if cellExist.optionChoosen == 2 {
                    cellExist.btn_No.isSelected = true
                }
            }
        }
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension TermsAndConditionVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height = 0.0
        switch indexPath.section {
        case 0:
            height = 177.0
            break
        case 1:
            height = 220.0
            break
        case 2:
            height = 158.0
            break
        default:
            height = 0.0
        }
        return CGFloat(height) * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = tcArray[section]
        return title
    }
}
