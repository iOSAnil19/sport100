//
//  TabBarVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
      //  tabBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1) //UIColor.lightGray
        //UITabBar.appearance().tintColor = UIColor.white
      //  tabBar.inActiveTintColor()
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        AppDelegate.getDelegate().previousIndex = AppDelegate.getDelegate().currentIndex
        AppDelegate.getDelegate().currentIndex = tabBarController.selectedIndex
        
//        if let navvc = viewController as? UINavigationController {
//            if let vc = navvc.visibleViewController as? PhotoPreviewVC {
//                if AppDelegate.getDelegate().previousIndex != 2 {
//                    vc.presentCamera()
//                }
//            }
//        }
    }
}

extension UITabBar{
    func inActiveTintColor() {
        if let items = items{
            for item in items{
                item.image =  item.image?.withRenderingMode(.alwaysOriginal)
                item.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.lightGray], for: .normal)
                item.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)], for: .selected)
                //UIColor.lightGray
            }
        }
    }
}
