//
//  AddMoreInfoVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
//import LocationPickerViewController
import CoreLocation
import MapKit
import IQKeyboardManagerSwift
import Alamofire
import Foundation
protocol AddMoreInfoDelegate {
    func profileIsUpDated()
}
import GooglePlaces
import GoogleMaps

class AddMoreInfoVC: UIViewController,didSelectdData{
    
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    @IBOutlet weak var Tf_standard: UITextField!
    @IBOutlet weak var tf_NickName:UITextField!
    @IBOutlet weak var tf_ChooseSport:UITextField!
    @IBOutlet weak var tf_Position:UITextField!
    @IBOutlet weak var tf_Nationality:UITextField!
    @IBOutlet weak var tf_AvalabilityStatus:UITextField!
    @IBOutlet weak var tf_Avalability:UITextField!
    @IBOutlet weak var tf_PrimaryHandFoot:UITextField!
    @IBOutlet weak var tf_Height:UITextField!
    @IBOutlet weak var tf_Weight:UITextField!
    @IBOutlet weak var tf_DBS:UITextField!
    @IBOutlet weak var tf_Location:UITextField!
    
    @IBOutlet weak var viewToolBar:UIView!
    
    @IBOutlet weak var picker:UIPickerView!
    @IBOutlet weak var activity_IndicatorSport:UIActivityIndicatorView!
    @IBOutlet weak var activity_IndicatorPosition:UIActivityIndicatorView!
    @IBOutlet weak var availabilityView:AvailabilityView!
    @IBOutlet weak var lbl_Alert:UILabel!
    @IBOutlet weak var img_User:UIImageView!
    @IBOutlet weak var btn_ImageRemove:UIButton!
    
    @IBOutlet weak var privacyBtn: UIButton!
    var checkBtn = 0
    var delegate:AddMoreInfoDelegate?
    var isBool = Bool()
    var fromPage = ""
    var standId : String!
    var positions:[Position]?
    var selectedPosition:Position?
    
    
    
    var sports:[Sport]? {
        didSet {
            if let allSports = sports {
                if !isBlankField(tf_ChooseSport) {
                    let sportExist = allSports.filter { (sport) -> Bool in
                        sport.name == tf_ChooseSport.text ?? ""
                    }
                    if sportExist.count > 0 {
                        selectedSport = sportExist.first
                    }
                }
                self.picker.reloadAllComponents()
            }
        }
    }
    var selectedSport:Sport? {
        didSet {
            if let sport = selectedSport {
                self.getAllThePositionFor(sport.id)
                
            }
//            tf_ChooseSport.text = selectedSport?.name ?? nil
        }
    }
    
    var imageEdited = false
    var selectedImage:UIImage?
    
    var imageManage:ImageManager?
    let allCountries = getAllCountries()
    
    let availabilityStatus = ["Available", "Not Available"]
    let footHandArray = ["Left footed",
                         "Right footed",
                         "Both footed",
                         "Left handed",
                         "Right handed",
                         "Ambidextrous"]
    
    var MonAM = false, TuesAM = false, WednesAM = false, ThursAM = false, FriAM = false, SatAM = false, SunAM = false
    var MonPM = false, TuesPM = false, WednesPM = false, ThursPM = false, FriPM = false, SatPM = false, SunPM = false
    
    var country = ""
    var city = ""
    var locationName = ""
    var lat, lng: Double?
    var cityGet = ""
    var stateGet = ""
    var countryGet = ""
    var streetGet = ""

    
    var weightArray = Array(10...250)
    var inchesArr = Array(60...120)
    var heightArray = {
        (60...120).map {
            ("\($0 / 12)' \($0 % 12)\"")
        }
    }()
    var selectedHeight = ""
    
    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        tf_Nationality.inputView = picker
        tf_AvalabilityStatus.inputView = picker
        tf_AvalabilityStatus.inputAccessoryView = viewToolBar
        tf_Position.inputView = picker
        tf_PrimaryHandFoot.inputView = picker
        tf_ChooseSport.inputView = picker
        
        tf_Weight.inputView = picker
        tf_Height.inputView = picker
        tf_Position.isUserInteractionEnabled = false
        
        
        if let sportsExist = Utility.shared.availableSports {
            
            let mySport = sportsExist.filter { (sport) -> Bool in
                sport.id == userInfo?.sportId ?? ""
            }
            if mySport.count > 0 {
                self.selectedSport = mySport.first
            }
            self.sports = sportsExist
            
            self.activity_IndicatorSport.stopAnimating()
            self.tf_ChooseSport.isUserInteractionEnabled = true
        } else {
            DispatchQueue.global().async {
                self.getAllTheSports()
                self.getAllThePositionFor(userInfo?.sportCode ?? "1")
            }
        }
        
        self.navigationController?.navigationBar.isHidden = false
        if fromPage == "LoginVC" {
            self.navigationItem.hidesBackButton = true
        }
        navigationItem.largeTitleDisplayMode = .never
        setBackViewColor()
        setNavigationBarImage()
        
        //self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        if isBool == false{
        self.setUpAllTheFields()
        }else{
            
        }
        
    }
    

       
        func extractFromGooglePlcae(place: GMSPlace) -> String {
    
            lat = place.coordinate.latitude
            lng = place.coordinate.longitude
            let latDouble = Double(lat ?? 0.0)
            let lngDouble = Double(lng ?? 0.0)
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude:latDouble , longitude:lngDouble) // <- New York

            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in

                placemarks?.forEach { (placemark) in

                    if let houseNo = placemark.subThoroughfare {
                                           print(houseNo)
                                           
                    }
                    
                    if let street = placemark.thoroughfare {
                        print(street)
                        
                    }
                    if let city = placemark.locality {
                        print(city)
                        self.cityGet = city
                      
                        
                    }
                    
                    if let country = placemark.country {
                        print(country)
                        self.countryGet = country
                    }
                    if let administrativeArea = placemark.administrativeArea {
                        print(administrativeArea)
                        self.stateGet = administrativeArea
                        if   self.cityGet  == "" {
                             self.cityGet = String(format:"%@",self.stateGet ?? "")
                            
                        }
                    }
                }
            })
                
            return ""
        }

  
    private func setUpAllTheFields() {
        
        img_User.layer.cornerRadius = 35
        img_User.clipsToBounds = true
        
        if let user = userInfo {
        
            if user.userImage != "" {
                let imgUrlString = imageUrl + "\(userInfo?.userImage ?? "")"
                self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.delayPlaceholder, completed: nil)
                btn_ImageRemove.isHidden = false
            }
            
            if user.nickName != "" {
                tf_NickName.text = user.nickName
            }
            if user.standardID != "" {
                self.standId = user.standardID
            }
            
            if user.standard != "" {
                Tf_standard.text = user.standard
            }
            
            if user.sportType != "" {
                selectedSport = Sport(name: user.sportType, id: user.sportCode)
                tf_ChooseSport.text = user.sportType
            }
            if user.position != "" {
                selectedPosition = Position(name: user.position, id: user.positionID)
                tf_Position.text = user.position
            }
            if user.nationality != "" {
                tf_Nationality.text = user.nationality
            }
            if user.availability != "" {
                tf_AvalabilityStatus.text = user.availability
            }
            if user.nickName != "" {
                tf_NickName.text = user.nickName
            }
            if user.height != "" {
                tf_Height.text = user.height
            }
            if user.weight != "" {
                tf_Weight.text = user.weight
            }
            if user.dbs != "" {
                tf_DBS.text = user.dbs
                //  tf_DBS.isUserInteractionEnabled = false
            }
            if user.street != "" {
                tf_Location.text = user.street
            }
            if user.city != "" {
               // tf_Location.text = (tf_Location.text ?? "") + " " + user.city
                self.cityGet = user.city
            }
            
            if user.country != ""{
                self.countryGet = user.country
            }
            self.locationName = tf_Location.text ?? ""
            
            if user.strongest != "" {
                tf_PrimaryHandFoot.text = user.strongest
            }
            if user.isHeight != "1"{
                if let height = Int(user.isHeight) {
                    checkBtn = height
                privacyBtn.isSelected = false
                }
            }else{
                checkBtn = Int(user.isHeight)!
                privacyBtn.isSelected = true
            }
            setUpAvaialability()
        }
    }
    
    private func setUpAvaialability() {
        
        guard let info = userInfo else { return }
        if info.availabilities.count > 0 {
            for availability in info.availabilities {
                if availability.day == "Mon" {
                    if availability.time == "AM" {
                        MonAM = true
                    } else if availability.time == "PM" {
                        MonPM = true
                    }
                    
                } else if availability.day == "Tue" {
                    if availability.time == "AM" {
                        TuesAM = true
                    } else if availability.time == "PM" {
                        TuesPM = true
                    }
                } else if availability.day == "Wed" {
                    if availability.time == "AM" {
                        WednesAM = true
                    } else if availability.time == "PM" {
                        WednesPM = true
                    }
                } else if availability.day == "Thu" {
                    if availability.time == "AM" {
                        ThursAM = true
                    } else if availability.time == "PM" {
                        ThursPM = true
                    }
                } else if availability.day == "Fri" {
                    if availability.time == "AM" {
                        FriAM = true
                    } else if availability.time == "PM" {
                        FriPM = true
                    }
                } else if availability.day == "Sat" {
                    if availability.time == "AM" {
                        SatAM = true
                    } else if availability.time == "PM" {
                        SatPM = true
                    }
                } else if availability.day == "Sun" {
                    if availability.time == "AM" {
                        SunAM = true
                    } else if availability.time == "PM" {
                        SunPM = true
                    }
                }
            }
            setAvailabilityField()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func privacyCheck(_ sender: UIButton) {
        if sender.isSelected == true {
            checkBtn = 0
            sender.isSelected = false
        }else if sender.isSelected == false {
            checkBtn = 1
            sender.isSelected = true
        }
    }
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        
//        guard isValid() == true else{return}
        let checkResult = checkAllTheFields()
        if checkResult.0 == true {
           
            
            showHud("Processing..")
            
            var params:[String:Any] = ["key":Api.key,
                                       "userID":userInfo?.userId ?? "1",
                                       "nickname":tf_NickName.text ?? "",
                                       "nationality":tf_Nationality.text ?? "",
                                       "height":tf_Height.text ?? "",
                                       "weight":tf_Weight.text ?? "",
                                       "dbs":tf_DBS.text ?? "",
                                       "positionID": selectedPosition?.id ?? "",
                                       "availability": "Available",
                                       "street":self.locationName,
                                       "town":self.cityGet,
                                       "country": self.countryGet,
                                       "post_code":"201301",
                                       "strongest":tf_PrimaryHandFoot.text ?? "",
                                       "sportID":selectedSport?.id ?? "",
                                       "isHideHeight":"\(checkBtn)",
                                       "isHideWeight":"\(checkBtn)" ,
                                       "standard": standId ?? "",
                                      // "primarySport":selectedSport?.id ?? ""
                
                                          ]
            params["monAM"] = "\(MonAM ?"1":"0")"
            params["monPM"] = "\(MonPM ?"1":"0")"
            params["tueAM"] = "\(TuesAM ?"1":"0")"
            params["tuePM"] = "\(TuesPM ?"1":"0")"
            params["wedAM"] = "\(WednesAM ?"1":"0")"
            params["wedPM"] = "\(WednesPM ?"1":"0")"
            params["thuAM"] = "\(ThursAM ?"1":"0")"
            params["thuPM"] = "\(ThursPM ?"1":"0")"
            params["friAM"] = "\(FriAM ?"1":"0")"
            params["friPM"] = "\(FriPM ?"1":"0")"
            params["satAM"] = "\(SatAM ?"1":"0")"
            params["satPM"] = "\(SatPM ?"1":"0")"
            params["sunAM"] = "\(SunAM ?"1":"0")"
            params["sunPM"] = "\(SunPM ?"1":"0")"
            
            let networkManager = NetworkManager()
            var imageData:Data?
            print(params)
            if let imgExist = selectedImage {
                imageData = UIImageJPEGRepresentation(imgExist, 1)
            }
            networkManager.uploadImage(Api.saveProfile, onPath:imageData, placeholderImage: nil,ApiConstant.profile_photo , andParameter: params) { (response, error) in
                
                print(response ?? "No response")
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                        if success == 200 {
                              hideHud()
                            if self.fromPage == "LoginVC" {
                              
                                self.goToFeedView()
                            } else {
                                GetUser.getUserProfileInfo({ (user) in
                                       hideHud()
                                    self.delegate?.profileIsUpDated(); self.navigationController?.popViewController(animated: true)
                                })
                            }
                        } else {
                            hideHud()
                            let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                            showAlert(ERROR, message: error.capitalized, onView: self)
                        }
                    } else {
                        hideHud()
                        showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                    }
                } else {
                    hideHud()
                    showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
                }
            }
            
        } else {
            lbl_Alert.text = checkResult.1
            lbl_Alert.flashView()
        }
        
        //profile_photo
    }
    func isValid() -> Bool {
       
        guard tf_ChooseSport.text != tf_ChooseSport.text  else {
            self.showAlertMessage("Enter Sport", andWithTitle: "Alert")
            return false}
        return true

    }
    @IBAction func buttonClicked_DBSInfo(_ sender:UIButton) {
        showAlert("DBS", message: "The Disclosure and Barring Service (DBS) helps employers make safer recruitment decisions and prevent unsuitable people from working with vulnerable groups, including children. It replaces the Criminal Records Bureau (CRB) and Independent Safeguarding Authority (ISA).", onView: self)
    }
    
    @IBAction func buttonClicked_Done(_ sender:UIButton) {
        tf_AvalabilityStatus.resignFirstResponder()
    }
    
    @IBAction func buttonClicked_ChangeProfileImage(_ sender:UIButton) {
        
        self.imageEdited = false
        imageManage = ImageManager()
        imageManage?.showImagePicker(self,sender) { image in
            
            DispatchQueue.main.async {
                if image != nil {
                    self.imageEdited = true
                    self.btn_ImageRemove.isHidden = false
                    self.img_User.image = image
                    self.selectedImage = image
                }
            }
        }
    }
    
    @IBAction func buttonClicked_RemoveProfileImage(_ sender:UIButton) {
        sender.isHidden = true
        self.imageEdited = false
        self.img_User.image = #imageLiteral(resourceName: "defult_user")
    }
    
    @IBAction func buttonClicked_SetAvailability(_ sender:UIButton) {
        
        MonAM = false; TuesAM = false; WednesAM = false; ThursAM = false; FriAM = false; SatAM = false; SunAM = false;
        MonPM = false; TuesPM = false; WednesPM = false; ThursPM = false; FriPM = false; SatPM = false; SunPM = false;
        tf_Avalability.text = ""
        availabilityView.showPopUp(self)
        if availabilityView.table_View != nil {
            availabilityView.table_View.reloadData()
        }
        availabilityView.delegate = self
        self.view.endEditing(true)
    }
    
    
    //MARK: ----------- UITextField Delegate METHODS ------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        if textField == tf_Nationality {
            picker.tag = 1000
            if isBlankField(tf_Nationality) {
                textField.text = allCountries[0]
            }
        } else if textField == tf_ChooseSport {
            picker.tag = 999
            if let allSports = self.sports {
                var index = 0
                if isBlankField(tf_ChooseSport) && allSports.count > 0 {
                    let mySport = allSports[0]
                    selectedSport = mySport
                    textField.text = mySport.name
                    index = 0
                } else if let mySport = selectedSport {
                    index = self.sports?.index(where: { $0.id == mySport.id }) ?? 0
                    textField.text = mySport.name
                }
                self.picker.selectRow(index, inComponent: 0, animated: false)
            } else {
                showAlert("", message: "Choose sport before selecting position", onView: self)
                textField.resignFirstResponder()
            }
        } else if textField == tf_AvalabilityStatus {
            IQKeyboardManager.shared.enableAutoToolbar = true
            picker.tag = 1001
            if isBlankField(tf_AvalabilityStatus) {
                textField.text = availabilityStatus[0]
                tf_Avalability.isHidden = false
            }
        } else if textField == tf_Avalability {
            availabilityView.showPopUp(self)
            textField.resignFirstResponder()
            tf_Avalability.resignFirstResponder()
            tf_PrimaryHandFoot.resignFirstResponder()
        } else if textField == tf_Location {
            showLocationPicker()
        } else if textField == tf_PrimaryHandFoot {
            picker.tag = 1002
            if isBlankField(tf_PrimaryHandFoot) {
                textField.text = footHandArray[0]
            }
        } else if textField == tf_Position {
            picker.tag = 1003
            if let allPositions = self.positions {
                var index = 0
                if isBlankField(tf_Position) && allPositions.count > 0 {
                    let myPosition = allPositions[0]
                    selectedPosition = myPosition
                    textField.text = myPosition.name
                    index = 0
                } else if let myPosition = selectedPosition {
                    textField.text = myPosition.name
                    index = self.positions?.index(where: { $0.id == myPosition.id }) ?? 0
                }
                self.picker.selectRow(index, inComponent: 0, animated: false)
            } else {
                showAlert("", message: "Choose sport before selecting position", onView: self)
                textField.resignFirstResponder()
            }
        } else if textField == tf_Weight {
            picker.tag = 1004
            if isBlankField(tf_Weight) {
                textField.text = "\(weightArray[0])"
            }
        }else if textField == tf_Height {
            picker.tag = 1005
            if isBlankField(tf_Height) {
                textField.text = "\(heightArray[0])"
            }
        }else if textField == Tf_standard {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.type = Type.standard
            vc.delegate = self
            self.present(vc,animated: true)
        }
        picker.reloadAllComponents()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tf_ChooseSport {
           // self.getAllThePositionFor(selectedSport?.id ?? "1")
//            if !isBlankField(tf_ChooseSport) {
//                let sportExist = self.sports?.filter { (sport) -> Bool in
//                    sport.name == tf_ChooseSport.text ?? ""
//                }
//                if let chooseSport = sportExist, chooseSport.count > 0 {
//                    selectedSport = chooseSport.first
//                }
//            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tf_AvalabilityStatus ||  textField == tf_Nationality || textField == tf_Location || textField == tf_Height ||  textField == tf_Weight {
            return false
        } else if textField == tf_Height ||  textField == tf_Weight {
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                return text.count < 3 ? true : false
            }
        }
        
        return true
    }
    
    func showLocationPicker() {
//        let locationPicker = LocationPicker()
//        locationPicker.pickCompletion = { (locationSelected) in
//
//            DispatchQueue.main.async {
//                self.tf_Location.text = locationSelected.name
//            }
//            if let locationDictionary = locationSelected.addressDictionary {
//                if let City = locationDictionary["City"] {
//                    print(City)
//                    self.city = "\(City)"
//                }
//            }
//            self.locationName = locationSelected.name
//        }
//        locationPicker.addBarButtons()
//
//        let navigationController = UINavigationController(rootViewController: locationPicker)
//      navigationController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
//
//        present(navigationController, animated: true, completion: nil)
        
        let placePickerController = GMSAutocompleteViewController()
                placePickerController.delegate = self as GMSAutocompleteViewControllerDelegate
                present(placePickerController, animated: true, completion: nil)
    }
}

extension AddMoreInfoVC {
    
    private func goToFeedView() {
        if let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier:StoryboardID.TabBarVC) {
            APPDELEGATE.window?.rootViewController = tabBarVC
            // self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
    
    /// fetch information of all the sports available
    fileprivate func getAllThePositionFor(_ id:String) {
        DispatchQueue.main.async {
            self.activity_IndicatorPosition.startAnimating()
        }
        let params = ["key": Api.key,"sportID":id]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getPosition, andParameter: params) { (response, error) in
            hideHud()
            DispatchQueue.main.async {
                self.activity_IndicatorPosition.stopAnimating()
                self.tf_Position.isUserInteractionEnabled = true
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    print(result)
                    if success == 200 {
                        if let positionsArray = result["Position"] as? [[String:Any]] {
                            let positionsModels = positionsArray.map({ (positionInfo) -> Position in
                                return Position.init(name:"\(String(describing: positionInfo["Position"] ?? ""))", id:"\(String(describing: positionInfo["ID"] ?? ""))")
                            })
                            
                            self.positions = positionsModels
//                            if let sportId = userInfo?.sportId, id == sportId {
//                               if let myPosition = self.positions?.filter({ (position) -> Bool in
//                                    position.id == userInfo?.positionID ?? ""
//                               }), myPosition.count > 0 {
//                                    self.selectedPosition = myPosition.first
//                                    self.tf_Position.text = myPosition.first?.name ?? ""
//                                }
//                            }
                            self.picker.reloadAllComponents()
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    /// fetch information of all the sports available
    fileprivate func getAllTheSports() {
        
        let params = ["key": Api.key]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getSport, andParameter: params) { (response, error) in
            hideHud()
            DispatchQueue.main.async {
                self.activity_IndicatorSport.stopAnimating()
                self.tf_ChooseSport.isUserInteractionEnabled = true
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.kMESSAGE] ?? "0"))")
                    if success == 200 {
                        if let sportsArray = result["sport"] as? [[String:Any]] {
                            let sportsModels = sportsArray.map({ (sportInfo) -> Sport in
                                return Sport(name: "\(String(describing: sportInfo["Name"] ?? ""))", id: "\(String(describing: sportInfo["id"] ?? ""))")
                            })
                            self.sports = sportsModels
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    
    
    func checkAllTheFields() -> (Bool,String) {
        // || isBlankField(tf_Avalability)
        if isBlankField(tf_ChooseSport) || isBlankField(tf_Position) {
            return (false,FILL_ALL_FIELDS)
        }
        
        if tf_Height.text != "" {
            if let height = Double(tf_Height.text ?? "0"), height <= 0.0 {
                return (false,"Please enter valid height")
            }
        }
        
        if tf_Weight.text != "" {
            if let weight = Double(tf_Weight.text ?? "0"), weight <= 0.0 {
                return (false,"Please enter valid weight")
            }
        }
        
        if let dbsText = tf_DBS.text, dbsText != "" {
            if (dbsText.count > 0){
                if (!(dbsText.count > 10 && dbsText.count < 14)){
                    return (false,"DBS number must be between 11 and 13 characters long")
                }
                
                let letters = CharacterSet.letters
                let digits = CharacterSet.decimalDigits
                
                if (!letters.contains(dbsText.unicodeScalars.first!)){
                    return (false,"DBS number must start with a letter")
                }
                let dbsNumbers = dbsText.unicodeScalars.dropFirst()
                for uni in dbsNumbers {
                    if !(digits.contains(uni)) {
                        return (false,"DBS number only contain numbers after the first letter")
                    }
                }
            }
        }
        return (true,"")
    }
    
}

struct Position {
    let name:String
    let id:String
}

extension AddMoreInfoVC:UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 999:
            return self.sports?.count ?? 0
        case 1000:
            return allCountries.count
        case 1001:
            return availabilityStatus.count
        case 1002:
            return footHandArray.count
        case 1003:
            return self.positions?.count ?? 0
        case 1004:
            return self.weightArray.count + 1
        case 1005:
            return self.heightArray.count
        default:
            return 0
        }
    }
}

extension AddMoreInfoVC:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        switch pickerView.tag {
        case 999:
            if let allSports = self.sports {
                let mySport = allSports[row]
                return  mySport.name
            }
            return ""
        case 1000:
            let country = allCountries[row]
            return country
        case 1001:
            let status = availabilityStatus[row]
            return status
        case 1002:
            let footHand = footHandArray[row]
            return footHand
        case 1003:
            if let allPositions = self.positions {
                let myPositions = allPositions[row]
                return  myPositions.name
            }
            return ""
        case 1004:
            var test = ""
            if row == 0 {
                test = "N/A"
            } else {
                let weight = weightArray[row]
                var poundWeight = Double(weight) * 2.20462
                poundWeight = poundWeight.rounded(toPlaces: 2)
                test = "\(weight) Kg (\(poundWeight) Pound)"
            }
            return test
        case 1005:
            let height = heightArray[row]
            let cmHeight = (Double(inchesArr[row]) * 2.54).rounded(toPlaces:1)
            return "\(height) (\(cmHeight)cm)"
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 999:
            if let allSports = self.sports {
                let mySport = allSports[row]
                if tf_ChooseSport.text != mySport.name {
                    selectedPosition = nil
                    tf_Position.text = nil
                }
                selectedSport = mySport
                tf_ChooseSport.text = mySport.name
                
                return
            }
        case 1000:
            let country = allCountries[row]
            tf_Nationality.text = country
        case 1001:
            let status = availabilityStatus[row]
            if status == "Available" {
                tf_Avalability.isHidden = false
            } else {
                tf_Avalability.isHidden = true
            }
            tf_AvalabilityStatus.text = status
        case 1002:
            let footHand = footHandArray[row]
            tf_PrimaryHandFoot.text = footHand
        case 1003:
            if let allPositions = self.positions {
                let myPositions = allPositions[row]
                selectedPosition = myPositions
                tf_Position.text = myPositions.name
                return
            }
        case 1004:
            var test = ""
            if row == 0 {
                test = "N/A"
            } else {
                let weight = weightArray[row]
                var poundWeight = Double(weight) * 2.20462
                poundWeight = poundWeight.rounded(toPlaces: 2)
                test = "\(weight)"
            }
            tf_Weight.text = test
        case 1005:
            let height = heightArray[row]
            tf_Height.text = "\(height)"
        default:
            break
        }
    }
}

extension AddMoreInfoVC:AvailabilityViewDelegate {
    func popHides() {
        setUpAvaialability()
        self.view.endEditing(true)
    }
    
    func setAMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            SunAM = available
        case "Monday":
            MonAM = available
        case "Tuesday":
            TuesAM = available
        case "Wednesday":
            WednesAM = available
        case "Thursday":
            ThursAM = available
        case "Friday":
            FriAM = available
        case "Saturday":
            SatAM = available
        default:
            break
        }
        self.setAvailabilityField()
    }
    
    func setPMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            SunPM = available
        case "Monday":
            MonPM = available
        case "Tuesday":
            TuesPM = available
        case "Wednesday":
            WednesPM = available
        case "Thursday":
            ThursPM = available
        case "Friday":
            FriPM = available
        case "Saturday":
            SatPM = available
        default:
            break
        }
        
        self.setAvailabilityField()
    }
    
    func setAvailabilityField() {
        
        var text = ""
        if SunAM || SunPM {
            text = "Sun, "
        }
        if MonAM || MonPM {
            text = text + "Mon, "
        }
        if TuesAM || TuesPM {
            text = text + "Tue, "
        }
        if WednesAM || WednesPM {
            text = text + "Wed, "
        }
        if ThursAM || ThursPM {
            text = text + "Thu, "
        }
        if FriAM || FriPM {
            text = text + "Fri, "
        }
        if SatAM || SatPM {
            text = text + "Sat"
        }
        
        tf_Avalability.text = text
    }
    
    
    
    func textField(textfield: String, type: Type, id: String) {
        if type == Type.standard {
        Tf_standard.text = textfield
            standId = id
    }
    
}

}


extension AddMoreInfoVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        tf_Location.text =  place.formattedAddress!
        self.locationName = place.formattedAddress!
        
        
         print(extractFromGooglePlcae(place: place))

        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
