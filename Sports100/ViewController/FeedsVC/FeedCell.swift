//
//  FeedCell.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AVKit
import Toast_Swift


class FeedCell: UITableViewCell {
    
    @IBOutlet weak var heightAchivment: NSLayoutConstraint!
    @IBOutlet weak var nameViewHeight:NSLayoutConstraint!
    @IBOutlet weak var likeViewHeight:NSLayoutConstraint!
    @IBOutlet weak var lbl_Comment:UILabel!
    @IBOutlet weak var lbl_Date:UILabel!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_LikeCount:UILabel!
    @IBOutlet weak var lbl_CommentCount:UILabel!

    @IBOutlet weak var img_User:UIImageView!
    @IBOutlet weak var img_Post:UIImageView!
    @IBOutlet weak var btn_More:UIButton!
    
    @IBOutlet weak var btn_Like:FaveButton!
    @IBOutlet weak var btn_Comment:FaveButton!
    @IBOutlet weak var btn_Play:UIButton!
    @IBOutlet weak var imgPlay:UIImageView!

    @IBOutlet weak var btn_Views:UIButton!
    
    var selectedIndex: Int?
    var selectedReason = ""
    var selectedAchivement: Feed?


    override func awakeFromNib() {
        super.awakeFromNib()
        btn_Comment.imageView?.contentMode = .scaleAspectFit
//        img_User.layer.cornerRadius = img_User.frame.width / 2
//        img_User.clipsToBounds = true
//        img_User.contentMode = .scaleAspectFit
//        img_User.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpFeedCell(_ feed:Feed) {
        
        guard let type = feed.type else { return }
       
        self.img_User.roundCorner()
        self.lbl_Date.text = geDateString(feed.created ?? "", " dd MMM yyyy hh:mm a")
        btn_Like.isSelected = feed.isLiked ?? false
        self.lbl_Name.text = "\(feed.user?.firstName ?? "") \(feed.user?.lastName ?? "")"
        let likeCount = feed.likeCount ?? ""
        if likeCount != "0" && likeCount != "-1"{
            self.lbl_LikeCount.text = likeCount
        }
        else {
            self.lbl_LikeCount.text = "0"
        }
       
        self.lbl_CommentCount.text = feed.commentCount ?? "0"
        
        if (type == "Achievement")   {
            self.lbl_Comment.text = feed.feed ?? ""
            
        } else if (type == "Image")  {
            let imgUrlString = imageUrl + "\(feed.feed ?? "")"
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.refreshCached, completed: nil)
//            img_Post.setupImageViewerWithCompletion(onOpen: {
//                self.hitSeviceToIncreaseViews(feed)
//            }) {
////
//            }
            img_Post.setupImageViewer()
            btn_Play.isHidden = true
            imgPlay.isHidden = true
            self.lbl_Comment.text = feed.desc ?? nil
        } else if (type == "Video") {
            btn_Play.isHidden = false
            imgPlay.isHidden = false
            btn_Play.tag = self.btn_Comment.tag
            
            let imgUrlString = imageUrl + "\(feed.videoThumb ?? "")"
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.refreshCached, completed: nil)
            self.lbl_Comment.text = feed.desc ?? nil
        }else if (type == "PostText") {
            self.lbl_Comment.text = feed.feed ?? nil
        } else if (type == "Team") {
            self.lbl_Comment.isHidden = false
            self.lbl_Comment.text = feed.desc ?? nil
            
          
        }
        
        let imgUrlString = imageUrl + "\(feed.user?.profilePic ?? "")"
        self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.refreshCached, completed: nil)
    }
    
    func setUpCommentFeedCell(_ feed:Feed) {
        
        guard let type = feed.type else { return }
        
        btn_Like.isSelected = feed.isLiked ?? false
        let likeCount = feed.likeCount ?? ""
        let count =  Int(likeCount) ?? 0

        if likeCount != "0" {
            self.lbl_LikeCount.text = likeCount
        }else if count <= 0 {
            self.lbl_LikeCount.text = "0"
        }
        else {
            self.lbl_LikeCount.text = "0"
        }
        let commentCount = feed.commentCount ?? "0"
        if commentCount != "0" {
            self.lbl_CommentCount.text = commentCount
        }
        else {
            self.lbl_CommentCount.text = "0"
        }

//      self.lbl_CommentCount.text = feed.commentCount ?? ""
        
        //  self.lbl_LikeCount.isHidden = likeCount == "" ? true:false
        //  self.lbl_CommentCount.text = feed.commentCount ?? ""
        
        if (type == "Achievement") || (type == "TextPost") {
            self.nameViewHeight.constant = 0
//            self.heightAchivment.constant = 0
            self.lbl_Comment.text = feed.feed ?? ""
        } else if (type == "Image")  {
            let imgUrlString = imageUrl + "\(feed.feed ?? "")"
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.refreshCached, completed: nil)
            img_Post.setupImageViewerWithCompletion(onOpen: {
                self.hitSeviceToIncreaseViews(feed)
            }) {
                
            }
            // img_Post.setupImageViewer()
            btn_Play.isHidden = true
            imgPlay.isHidden = true

                let comment = feed.desc ?? ""
                if comment != "" {
                    self.lbl_Comment.text = comment
                }
                else {
                    self.lbl_Comment.text = ""
                }

        } else if (type == "Video") {
            btn_Play.isHidden = false
            imgPlay.isHidden = false
            btn_Play.tag = self.btn_Views.tag
            
            let imgUrlString = imageUrl + "\(feed.videoThumb ?? "")"
            print(imgUrlString)
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.refreshCached, completed: nil)
            self.lbl_Comment.text = feed.desc ?? nil
        } else if (type == "PostText") {
            heightAchivment.constant = 0
            img_Post.isHidden = true
            btn_Play.isHidden = true
            imgPlay.isHidden = true
            self.lbl_Comment.text = feed.feed ?? nil
        }else if (type == "Team") {
            self.lbl_Comment.isHidden = false
            self.lbl_Comment.text = feed.desc ?? nil
            self.heightAchivment.constant = 0
            img_Post.isHidden = true
            btn_Play.isHidden = true
            imgPlay.isHidden = true
            
        }
    }
    func hitSeviceToIncreaseViews(_ feed:Feed) {
        let networkManager = NetworkManager()
        let params = ["key": Api.key, "userId":userInfo?.userId ?? "", "postId": feed.feedID ?? ""]
        networkManager.getDataForRequest(Api.isView, andParameter: params) { (response, error) in
        }
    }
    
    

    
    //MARK: ------------- UIBUTTON ACTION METHODS ----------------
    
    @IBAction func buttonClicked_User(_ sender:UIButton) {

        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        let feed = parentVC.feeds[btn_More.tag]
        
        if let profileVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
            profileVC.otherUserId = feed.user?.iD ?? ""
            profileVC.hidesBottomBarWhenPushed = true
            parentVC.navigationController?.pushViewController(profileVC, animated: true)
        }

    }
    
    func openImageViewer(_ feed:Feed)  {
        
        guard let parentVC = self.parentViewController else { return }
        if parentVC is FeedsVC {
        }
        if parentVC is CommentViewController {
        }
        
        self.hitSeviceToIncreaseViews(feed)
        let imgUrlString = imageUrl + "\(feed.feed ?? "")"
        var imageArray = Array<Any>()
        imageArray.append(imgUrlString)
        _ = ImageZoomManager.sharedimageZoomManager.openPicker(parentVC, imageArray)
        
    }
    
    @IBAction func buttonClicked_Play(_ sender:UIButton) {
        guard let parentVC = self.parentViewController else { return }
        var selectedFeed: Feed?
        if let parent = parentVC as? FeedsVC {
            selectedFeed = parent.feeds[sender.tag]
        }
        if let parent = parentVC as? CommentViewController {
            selectedFeed = parent.feed
        }
        guard let type = selectedFeed!.type else { return }
        if(type == "Image") {
            self.openImageViewer(selectedFeed!)
        }else if(type == "Video") {
        guard let feed = selectedFeed else { return }
        self.hitSeviceToIncreaseViews(feed)
        
        let imgUrlString = imageUrl + "\(feed.feed ?? "")"
        if let url = URL.init(string: imgUrlString) {
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            parentVC.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        }
    }
    
    @IBAction func buttonClicked_Like(_ sender:UIButton) {
        
        var feed: Feed!
        
        if let parentVC = self.parentViewController as? FeedsVC {
            feed = parentVC.feeds[sender.tag]
        } else if let parentVC = self.parentViewController as? CommentViewController {
            feed = parentVC.feed
        } else {
            return
        }
        
        feed.isLiked = sender.isSelected == true
        var likeCount = Int("\(String(describing: feed.likeCount ?? "0"))") ?? 0

        if sender.isSelected == true {
            likeCount = likeCount + 1
        } else {
            likeCount = likeCount - 1
        }
        if likeCount != 0 && likeCount != -1{
        self.lbl_LikeCount.text = "\(likeCount)"
        }
        feed.likeCount = "\(likeCount)"
       self.requestFor(Api.likeUnlikePost,feed)
    }
    
    @IBAction func buttonClicked_Comment(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        let feed = parentVC.feeds[sender.tag]

        if let commentVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CommentViewController) as? CommentViewController {
            commentVC.feed = feed
            commentVC.delegate = self
            parentVC.navigationController?.pushViewController(commentVC, animated: true)
        }
    }
    
    @IBAction func buttonClicked_Views(_ sender:UIButton) {
        var feed: Feed!
        guard let parent = self.parentViewController else { return }
        if let parentVC = self.parentViewController as? FeedsVC {
            feed = parentVC.feeds[sender.tag]
        } else if let parentVC = self.parentViewController as? CommentViewController {
            feed = parentVC.feed
        } else {
            return
        }

        if let viewsVC = parent.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ViewsVC) as? ViewsVC {
            viewsVC.selectedFeed = feed
            parent.navigationController?.pushViewController(viewsVC, animated: true)
        }
    }
    
    
    @IBAction func buttonClicked_More(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        
        let feed = parentVC.feeds[sender.tag]
        
        let titleIs = (feed.user?.iD ?? "") == userInfo?.userId ? "Delete":"Report"
        
        let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        var shareAction: UIAlertAction?
        if (feed.user?.iD ?? "") == userInfo?.userId {
            shareAction = UIAlertAction.init(title: "Edit", style: .default) { (acrion) in
                
                guard let parentVC = self.parentViewController else { return }
                
                let nibs = Bundle.main.loadNibNamed("AddAchievementView", owner: self, options: nil)
                let addAchievementView = nibs?.first as? AddAchievementView
                addAchievementView?.text_View.text = feed.feed ?? ""
                let date = getLocaleDate(feed.created ?? "")
                addAchievementView?.tf_Date.text = getStringFromDate(date)
                addAchievementView?.delegate = self
                addAchievementView?.dobSelected = getDateFromDate(date)
                if let id = feed.feedID {
                    addAchievementView?.selectedID = Int(id) ?? 0
                    self.selectedAchivement = feed
                }
                addAchievementView?.showPopUp(parentVC)
            }
            
            if let type = feed.type, type == "Achievement" {
                alertController.addAction(shareAction!)
            }
        }
        
        
        let deleteAction = UIAlertAction.init(title:titleIs, style: .destructive) { (action) in
            if feed.user?.iD == userInfo?.userId {
                self.removeOrReportFrom(sender.tag)
            } else {
                self.showReportScreen(sender.tag)
            }
            
            //self.removeOrReportFrom(sender.tag)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (acrion) in
            
        }
        
     //   alertController.addAction(shareAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        parentVC.present(alertController, animated: true, completion: nil)
    }
    
    private func showReportScreen(_ index:Int) {
        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        selectedIndex = index
        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ReportVC) as? ReportVC {
            vc.delegate = self
            let newNavigation = UINavigationController.init(rootViewController: vc)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            parentVC.present(newNavigation, animated: true, completion: nil)
        }
    }
    
    
    
    private func removeOrReportFrom(_ index:Int) {
        
        guard let parentVC = self.parentViewController as? FeedsVC else { return }        
        let feed = parentVC.feeds[index]
       
        
        var request = ""
        if feed.user?.iD == userInfo?.userId {
            request = Api.deletePost
            parentVC.feeds.remove(at: index)
            let indexPath = IndexPath.init(row: index, section: 0)
            parentVC.table_View.deleteRows(at: [indexPath], with: .bottom)
        } else {
            request = Api.reportPost
        }
        self.requestForRemoveOrReportFrom(request,feed)
    }
    
    
    private func requestForRemoveOrReportFrom(_ requestString:String, _ feed:Feed) {
        
        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        
        var params = ["key":Api.key,
                      "FeedID":feed.feedID ?? "",
                      "userID":userInfo?.userId ?? ""]
        if requestString == Api.reportPost {
            params["reason"] = selectedReason
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {

                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        let msg = "\(String(describing: result[NetworkManager.kMESSAGE] ?? ""))"
                        parentVC.showToast(message: msg.firstUppercased)

                        //showAlert("", message: msg, onView: parentVC)
                        if requestString != Api.likeUnlikePost {
                            parentVC.table_View.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    
    private func requestFor(_ requestString:String, _ feed:Feed) {
        guard let parent = self.parentViewController else { return }
        
        var params = ["key":Api.key,
                      "FeedID":feed.feedID ?? "",
                      "userID":userInfo?.userId ?? ""]
        if requestString == Api.reportPost {
            params["reason"] = selectedReason
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        let msg = "\(String(describing: result["like"] ?? ""))"
                        //showAlert("", message: msg, onView: parentVC)
   //                     parent.showToast(message: msg.firstUppercased)
                        if requestString != Api.likeUnlikePost {
                            guard let parentVC = self.parentViewController else { return }
                            if let navigationVC = parentVC.navigationController {
                                let vcs = navigationVC.viewControllers
                                for currentViewController in vcs {
                                    if let vc = currentViewController as? FeedsVC {
                                        vc.table_View.reloadData()
                                        break;
                                    }
                                }
                            }
                        } else {
                            guard let parentVC = self.parentViewController else { return }
                            if let navigationVC = parentVC.navigationController {
                                let vcs = navigationVC.viewControllers
                                for currentViewController in vcs {
                                    if let vc = currentViewController as? FeedsVC {
                                        vc.table_View.reloadData()
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        if requestString == Api.likeUnlikePost {
                            self.btn_Like.isSelected = !self.btn_Like.isSelected
                            feed.isLiked = self.btn_Like.isSelected

                            var likeCount = Int("\(String(describing: self.lbl_LikeCount.text ?? "0"))") ?? 0
                            if self.btn_Like.isSelected == true {
                                likeCount = likeCount + 1
                            } else {
                                likeCount = likeCount - 1
                            }
                            self.lbl_LikeCount.text = "\(likeCount)"
                            feed.likeCount = "\(likeCount)"
                        }
                    }
                } else {
                    if requestString == Api.likeUnlikePost {
                        self.btn_Like.isSelected = !self.btn_Like.isSelected
                        feed.isLiked = self.btn_Like.isSelected
                        
                        var likeCount = Int("\(String(describing: self.lbl_LikeCount.text ?? "0"))") ?? 0
                        if self.btn_Like.isSelected == true {
                            likeCount = likeCount + 1
                        } else {
                            likeCount = likeCount - 1
                        }
                        self.lbl_LikeCount.text = "\(likeCount)"
                        feed.likeCount = "\(likeCount)"
                    }
                }
            } else {
                if requestString == Api.likeUnlikePost {
                    self.btn_Like.isSelected = !self.btn_Like.isSelected
                    feed.isLiked = self.btn_Like.isSelected == true
                    var likeCount = Int("\(String(describing: self.lbl_LikeCount.text ?? "0"))") ?? 0
                    if self.btn_Like.isSelected == true {
                        likeCount = likeCount + 1
                    } else {
                        likeCount = likeCount - 1
                    }
                    self.lbl_LikeCount.text = "\(likeCount)"
                    feed.likeCount = "\(likeCount)"
                }
            }
        }
    }
}

extension FeedCell:FeedDelegate {
    func feedIsUpdated(_ feed:Feed) {
        //    guard let parentVC = self.parentViewController as? FeedsVC else { return }
        var commentCount = Int("\(String(describing: feed.commentCount ?? "0"))") ?? 0
        commentCount = commentCount + 1
        self.lbl_CommentCount.text = "\(commentCount)"
        feed.likeCount = "\(commentCount)"
    }
}

extension FeedCell: ReportDelegate {
    func reportReason(_ reason:String) {
        self.selectedReason = reason
        if let index = selectedIndex {
            self.removeOrReportFrom(index)
        }
    }
}

extension FeedCell:EditBioDelegate {
    func bioUpdated(_ text:String) {
        
        guard let feed = self.selectedAchivement else { return }
        guard let feedId = feed.feedID else { return }
        feed.feed = text
        self.lbl_Comment.text = text
        
        guard let parentVC = self.parentViewController as? FeedsVC else { return }
        if let index = parentVC.feeds.index(where: { (myFeed) -> Bool in
            myFeed.feedID == feedId
        }) {
            parentVC.feeds[index] = feed
            let indexPath = IndexPath(row: index, section: 0)
            parentVC.table_View.reloadRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
            
            if let nav = parentVC.navigationController {
                if let tabbarVC = nav.tabBarController, tabbarVC.viewControllers?.count != 0 {
                    if let profileNavVC = tabbarVC.viewControllers?.last as? UINavigationController, profileNavVC.viewControllers.count > 0 {
                        if let profileVC = profileNavVC.viewControllers[0] as? ProfileVC {
                            profileVC.getProfileInfo()
                        }
                    }
                }
            }
        }
        
    }
}
