//
//  FeedsVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol FeedDelegate {
    func feedIsUpdated(_ feed: Feed)
}

class FeedsVC: UIViewController {
    
    @IBOutlet weak var notificationBtn: UIBarButtonItem!
    @IBOutlet weak var table_View:UITableView!
    let refreshControl = UIRefreshControl()
    var feeds = [Feed]()
    
    var pageNo = 1
    var totalPageCount = 1
    var notificationCount : Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
       
        setNavigationBarImage()
        table_View.estimatedRowHeight = 280
        table_View.rowHeight = UITableViewAutomaticDimension
         self.getNotificationCount()
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(FeedsVC.refreshMethod), for: .valueChanged)
       
  //  self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        
        
        DispatchQueue.global().async { GetUser.getUserProfileInfo { (userDetail) in }
            
            self.getFeedsForRequest()
            self.getNotificationCount()
            showHud("Processing..")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        self.getNotificationCount()
        if notificationCount == nil{
        self.notificationBtn?.removeBadge()
        }else if notificationCount <= 0{
        self.notificationBtn?.removeBadge()
        } else{
        let point = CGPoint(x: 1, y: 2)
        self.notificationBtn?.addBadge(number: notificationCount, withOffset: point, andColor: #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), andFilled: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getNotificationCount()
        self.getFeedsForRequest()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    } 
    
    @IBAction func buttonClicked_Notification(_ sender: UIButton) {
        
        if let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.NotificationVC) as? NotificationVC {
            //   addMoreInfoVC.delegate = self
            notificationVC.delegate = self
            self.navigationController?.pushViewController(notificationVC, animated: true)
        }
    }
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getFeedsForRequest()
        self.getNotificationCount()
    }
    func getNotificationCount(){
        guard let userID = userInfo?.userId else{return}
        let params = ["key": Api.key, "toID":userID ,"fromID":userID ]
            print("------params----",params)
        ApiLocationManager.shared.apiManeger(api: url.notificationCount, params,URLEncoding.default) { (response, err) in
              if let result = response as? [String:Any] {
            let success = Int("\(String(describing: result[NetworkManager.Success] ?? "0"))")
                if success == 1{
                    self.notificationCount = Int("\(String(describing: result["messagesCount"] ?? "0"))")!
                }else{
                    self.notificationBtn.removeBadge()
                }
                self.table_View.reloadData()
                print(result["success"] as Any)
            }
        }
    }
    @objc func getFeedsForRequest() {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        print("------params----",params)
        var requestString = Api.getAllFeed
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
//                    self.notificationCount = Int("\(String(describing: result["notificationcount"] ?? ""))")!
                    
                    if success == 200 {
                        
                        if let allFeeds = result["UserFeed"] as? [[String:Any]], allFeeds.count > 0 {
                            print(JSON(allFeeds))
                            let feedModels = allFeeds.map({ (feedInfo) -> Feed in
                                Feed.init(object: feedInfo)
                            })
                            if self.pageNo == 1 {
                                self.feeds.removeAll()
                                self.feeds = feedModels
                            } else {
                                for f in feedModels {
                                    self.feeds.append(f)
                                }
                            }
                            self.table_View.reloadData()
                            self.view.layoutSubviews()
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension FeedsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        var cellId = "FeedCell"
        let feed = self.feeds[indexPath.row]
        
        if let type = feed.type {
            if (type == "Achievement") {
                cellId = "FeedAchieveCell"
            } else if (type == "Image") || (type == "Video"){
                cellId = "FeedCell"
            }
            else if (type == "TextPost") {
                cellId = "FeedTextCell"
            } else if (type == "PostText"){
                cellId = "FeedTextCell"
            }else if (type == "Team") {
                cellId = "FeedTextCell"
            } 

            let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? FeedCell
            cell?.btn_Like.tag = indexPath.row
            cell?.btn_More.tag = indexPath.row
            cell?.btn_Comment.tag = indexPath.row
            cell?.btn_Views.tag = indexPath.row
            cell?.setUpFeedCell(feed)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
}

extension FeedsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == feeds.count - 2 {
            if totalPageCount == pageNo {
                return
            }
            pageNo = pageNo + 1
            self.getFeedsForRequest()
            self.getNotificationCount()
            
        }
    }
}
extension FeedsVC : notificationCount{
    func getCountNotification(count: Int) {
        notificationCount = count
    }
    
}
