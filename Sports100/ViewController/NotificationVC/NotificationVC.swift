//
//  NotificationVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 18/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol notificationCount {
    func getCountNotification(count:Int)
}

class NotificationVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    let refreshControl = UIRefreshControl()
    
    var pageNo = 1
    var totalPageCount = 1
    
    var notifications = [EventNotification]()
    var delegate : notificationCount?
    //MARK: --------- View Life Cycle -----------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never
        self.title = "Notifications"
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(NotificationVC.refreshMethod), for: .valueChanged)
        table_View.tableFooterView = UIView.init(frame: .zero)
        
        //showHud("Processing..")
        refreshControl.beginRefreshing()
        self.getAllNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getReadNotificationCount()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    func getReadNotificationCount(){
        guard let userID = userInfo?.userId else{return}
        let params = ["key": Api.key, "toID":userID]
        print("------params----",params)
        ApiLocationManager.shared.apiManeger(api: url.readNotificationCount, params,URLEncoding.default) { (response, err) in
            if let result = response as? [String:Any] {
                let success = Int("\(String(describing: result[NetworkManager.Success] ?? "0"))")
                let msgCount = Int("\(String(describing: result["messagesCount"] ?? "0"))")
                if success == 1{
                    if let del = self.delegate{
                        del.getCountNotification(count: msgCount ?? 0)
                    }
                }
                self.table_View.reloadData()
                print(result["success"] as Any)
            }
        }
    }
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getAllNotification()
    }
    
    func getAllNotification() {
        //12
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        var requestString = Api.teamNotification
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allNotification = result["notification"] as? [[String:Any]], allNotification.count > 0 {
                            
                            print(JSON(allNotification))
                            
                            let notificationModels = allNotification.map({ (info) -> EventNotification in
                                EventNotification.init(object: info)
                            })
                            if self.pageNo == 1 {
                                self.notifications = notificationModels
                            } else {
                                for n in notificationModels {
                                    self.notifications.append(n)
                                }
                            }
                            self.table_View.reloadData()
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    @objc func connectedTo_Profile(sender: UIButton){
        let buttonTag = sender.tag
        let notification = self.notifications[buttonTag]
        if let fromId = notification.fromID, fromId != "", fromId != "0" {
            if let profileVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
                profileVC.otherUserId = fromId
                profileVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
    }
}

extension NotificationVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notification = self.notifications[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell {
            
            cell.lbl_NotificationText.text = notification.type
            let longString = notification.type
            let longestWord = notification.otheruserName
            if longString != nil && longestWord != nil {
                let range = (longString! as NSString).range(of: longestWord!)
                         let attributedString = NSMutableAttributedString(string:longString ?? "")
                         attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1) , range: range)
                         cell.lbl_NotificationText.attributedText = attributedString
                cell.btn_Profile.isHidden = false
            }
            else{
                cell.btn_Profile.isHidden = true
            }
         
           
            cell.lbl_Date.text = geDateString(notification.created ?? "", "dd MMM yyyy hh:mm a")
            cell.btn_Profile.tag = indexPath.row
            cell.btn_Profile.addTarget(self, action: #selector(connectedTo_Profile(sender:)), for: .touchUpInside)

            cell.btn_Accept.tag = indexPath.row
            cell.btn_Decline.tag = indexPath.row
            if notification.title == "Team request" || notification.title == "Contact request" {
                cell.btn_Accept.isHidden = false
                cell.btn_Decline.isHidden = false
            } else {
                cell.btn_Accept.isHidden = true
                cell.btn_Decline.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
}

extension NotificationVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
                let notification = self.notifications[indexPath.row]

        if notification.title == "Send Message"{
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"ChatMessageViewController", storyboardName:"Main") as! ChatMessageViewController
                           vc.otherUserId = notification.fromID
                           vc.otherUserName = notification.otheruserName
                           self.navigationController?.pushViewController(vc, animated:false)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == notifications.count - 2 {
            if totalPageCount == pageNo {
                return
            }
            pageNo = pageNo + 1
            self.getAllNotification()
        }
    }
}
