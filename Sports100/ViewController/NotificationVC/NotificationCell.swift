//
//  NotificationCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 18/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lbl_NotificationText:UILabel!
    @IBOutlet weak var lbl_Date:UILabel!
    @IBOutlet weak var btn_Accept:UIButton!
    @IBOutlet weak var btn_Decline:UIButton!
    @IBOutlet weak var btn_Profile:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonClicked_Accept(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? NotificationVC else { return }
        let notification = parentVC.notifications[sender.tag]
        
        if notification.title == "Contact request" {
            self.hitContactRequestService(Api.acceptContactRequest, mytag: sender.tag)
        } else {
            self.hitTeamRequestService(Api.acceptTeamRequest, mytag: sender.tag)
        }
    }
    
    @IBAction func buttonClicked_Decline(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? NotificationVC else { return }
        let notification = parentVC.notifications[sender.tag]
        
        if notification.title == "Contact request" {
            self.hitContactRequestService(Api.declineContactRequest, mytag: sender.tag)
            
        } else {
            self.hitTeamRequestService(Api.declineTeamRequest, mytag: sender.tag)
        }
    }
    
    private func hitContactRequestService(_ request:String, mytag:Int) {
        
        guard let parentVC = self.parentViewController as? NotificationVC else { return }
        
        let notification = parentVC.notifications[mytag]
        
        var params = ["key": Api.key,"eventID":notification.eventID ?? ""]
        if request == Api.acceptContactRequest {
            params["fromID"] = userInfo?.userId ?? ""
        }
        
        let indexPath = IndexPath.init(row: mytag, section: 0)
        parentVC.notifications.remove(at: indexPath.row)
        parentVC.table_View.deleteRows(at: [indexPath], with: .fade)
         parentVC.table_View.reloadData()
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(request, andParameter: params) { (result, error) in
        }
    }
    
    private func hitTeamRequestService(_ request:String, mytag:Int) {
        
        guard let parentVC = self.parentViewController as? NotificationVC else { return }
        
        let notification = parentVC.notifications[mytag]

        let params = ["key": Api.key, "userID":userInfo?.userId ?? "", "teamID":notification.eventID ?? "", "notifyID":notification.iD ?? ""]
        
        let indexPath = IndexPath.init(row: mytag, section: 0)
        parentVC.notifications.remove(at: indexPath.row)
        parentVC.table_View.deleteRows(at: [indexPath], with: .fade)
        parentVC.table_View.reloadData()
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(request, andParameter: params) { (result, error) in
            if request == Api.acceptTeamRequest {
                if let nav = parentVC.navigationController {
                    if let tabbarVC = nav.tabBarController, tabbarVC.viewControllers?.count != 0 {
                        if let teamNavVC = tabbarVC.viewControllers?[3] as? UINavigationController, teamNavVC.viewControllers.count > 0 {
                            if let teamVC = teamNavVC.viewControllers[0] as? TeamSportsVC {
                                teamVC.refreshMethod()
                            }
                        }
                    }
                }
            }
            
        }
    }

}
