//
//  MyFeedCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 03/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AVKit

class MyFeedCell: UITableViewCell {
    
    @IBOutlet weak var nameViewHeight:NSLayoutConstraint!
    @IBOutlet weak var likeViewHeight:NSLayoutConstraint!
    @IBOutlet weak var lbl_Comment:UILabel!
    @IBOutlet weak var lbl_Date:UILabel!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_LikeCount:UILabel!
    @IBOutlet weak var lbl_CommentCount:UILabel!
    
    @IBOutlet weak var img_User:UIImageView!
    @IBOutlet weak var img_Post:UIImageView!
    @IBOutlet weak var btn_More:UIButton!
    
    @IBOutlet weak var btn_Like:FaveButton!
    @IBOutlet weak var btn_Comment:FaveButton!
    @IBOutlet weak var btn_Play:UIButton!
    @IBOutlet weak var imgPlay:UIImageView!
     var selectedReason = ""
    @IBOutlet weak var btn_Views:UIButton!
    var selectedIndex: Int?
    var myParent: MyFeedVC?
    
    var selectedAchivement: Feed?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpMyFeedCell(_ feed:Feed, _ parentView: MyFeedVC) {
        
        guard let type = feed.type else { return }
        
        self.img_User.roundCorner()
        self.lbl_Date.text = getLocaleDate(feed.created ?? "", format: "dd MMM yyyy hh:mm a")
        btn_Like.isSelected = feed.isLiked ?? false
        self.lbl_Name.text = "\(feed.user?.firstName ?? "") \(feed.user?.lastName ?? "")"
        let likeCount = feed.likeCount ?? ""
        let myInt2 = Int(likeCount) ?? 0

         if myInt2 < 0  {
            self.lbl_LikeCount.text = "0"
        } else {
            self.lbl_LikeCount.text = likeCount
        }
        
        self.lbl_CommentCount.text = feed.commentCount ?? "0"
        
        if (type == "Achievement") || (type == "TextPost") {
            self.lbl_Comment.text = feed.feed ?? ""
            let imgUrlString = imageUrl + "\(feed.profilePic ?? "")"
            self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.continueInBackground, completed: nil)
            
        } else if (type == "Image") {
            let imgUrlString = imageUrl + "\(feed.feed ?? "")"
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
//            img_Post.setupImageViewerWithCompletion(onOpen: {
//                self.hitSeviceToIncreaseViews(feed)
//            }) {
//
//            }
            
            // img_Post.setupImageViewer()
            btn_Play.isHidden = true
            imgPlay.isHidden = true
            self.lbl_Comment.text = feed.desc ?? nil
        } else if (type == "Video") {
            btn_Play.isHidden = false
            imgPlay.isHidden = false
            btn_Play.tag = self.btn_Comment.tag
            
            let imgUrlString = imageUrl + "\(feed.videoThumb ?? "")"
            self.img_Post.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.refreshCached, completed: nil)
            self.lbl_Comment.text = feed.desc ?? nil
        }
        if let imagePath = feed.user?.profilePic {
            let imgUrlString = imageUrl + imagePath
            self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.refreshCached, completed: nil)
        } else {
            let imgUrlString = imageUrl + "\(feed.profilePic ?? "")"
            self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.continueInBackground, completed: nil)
        }
    }
    
    func openImageViewer(_ feed:Feed)  {
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        self.hitSeviceToIncreaseViews(feed)
        let imgUrlString = imageUrl + "\(feed.feed ?? "")"
        var imageArray = Array<Any>()
        imageArray.append(imgUrlString)
        _ = ImageZoomManager.sharedimageZoomManager.openPicker(parentVC, imageArray)
        
    }
    
    @IBAction func buttonClicked_Play(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        
        let feed = parentVC.feeds[sender.tag]
        guard let type = feed.type else { return }
        if(type == "Image") {
            self.openImageViewer(feed)
        }else {
            self.hitSeviceToIncreaseViews(feed)
            let imgUrlString = imageUrl + "\(feed.feed ?? "")"
            if let url = URL.init(string: imgUrlString) {
                let player = AVPlayer(url: url)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                parentVC.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
        
    }
    
    @IBAction func buttonClicked_Like(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        let feed = parentVC.feeds[sender.tag]
        
        feed.isLiked = sender.isSelected
        self.requestFor(Api.likeUnlikePost,feed)
    }
    
    @IBAction func buttonClicked_Comment(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        let feed = parentVC.feeds[sender.tag]
        
        if let commentVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CommentViewController) as? CommentViewController {
            commentVC.feed = feed
            parentVC.navigationController?.pushViewController(commentVC, animated: true)
        }
    }
    
    @IBAction func buttonClicked_Views(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        let feed = parentVC.feeds[sender.tag]
        
        if let viewsVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ViewsVC) as? ViewsVC {
            viewsVC.selectedFeed = feed
            parentVC.navigationController?.pushViewController(viewsVC, animated: true)
        }
    }
    
    func hitSeviceToIncreaseViews(_ feed:Feed) {
        let networkManager = NetworkManager()
        let params = ["key": Api.key, "userId":userInfo?.userId ?? "", "postId": feed.feedID ?? ""]
        networkManager.getDataForRequest(Api.isView, andParameter: params) { (response, error) in
        }
    }
    
    private func requestFor(_ requestString:String, _ feed:Feed) {
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        var params = [String:Any]()
        if requestString == Api.deletePost {
             params = ["key":Api.key,
                          "FeedID":feed.feedID ?? "",
                          "userID":userInfo?.userId ?? ""]
        }else{
             params = ["key":Api.key,
                          "FeedID":feed.feedID ?? "",
                          "userID":userInfo?.userId ?? "",
                          "reason": selectedReason]
        }
        
      
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            self.updateFeedListing()
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        let msg = "\(String(describing: result[NetworkManager.kMESSAGE] ?? ""))"
                        parentVC.showToast(message: msg.firstUppercased)
                        if requestString == Api.deletePost {
                            GetUser.getUserProfileInfo({ (user) in
                                self.myParent?.delegate?.profileIsUpDated()
                            })
                        }
                        
                    } else {
                        if requestString == Api.likeUnlikePost {
                            self.btn_Like.isSelected = !self.btn_Like.isSelected
                            feed.isLiked = self.btn_Like.isSelected
                        }
                    }
                } else {
                    if requestString == Api.likeUnlikePost {
                        self.btn_Like.isSelected = !self.btn_Like.isSelected
                        feed.isLiked = self.btn_Like.isSelected
                    }
                }
            } else {
                if requestString == Api.likeUnlikePost {
                    self.btn_Like.isSelected = !self.btn_Like.isSelected
                    feed.isLiked = self.btn_Like.isSelected
                }
            }
        }
    }
    
    @IBAction func buttonClicked_More(_ sender:UIButton) {
        
        
        if let parentVC = self.parentViewController {
            if let parentVC = parentVC as? MyFeedVC {
                // parentVC is someViewControllemyParentr
                print(parentVC)
            
       
//        guard let parentVC = self.myParent else { return }
        
         // let alertController: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let feed = parentVC.feeds[sender.tag]

        let titleIs = parentVC.userModel?.userId  == userInfo?.userId ? "Delete":"Report"
        
        
        let shareAction = UIAlertAction.init(title: "Edit", style: .default) { (acrion) in
                
            guard let parentVC = self.parentViewController else { return }
            
            let nibs = Bundle.main.loadNibNamed("AddAchievementView", owner: self, options: nil)
            let addAchievementView = nibs?.first as? AddAchievementView
            addAchievementView?.text_View.text = feed.feed ?? ""
            let date = getLocaleDate(feed.created ?? "")
            addAchievementView?.tf_Date.text = getStringFromDate(date)
            addAchievementView?.delegate = self
            addAchievementView?.dobSelected = getDateFromDate(date)
            if let id = feed.feedID {
                addAchievementView?.selectedID = Int(id) ?? 0
                self.selectedAchivement = feed
            }
            addAchievementView?.showPopUp(parentVC)
        }
        
        if let type = feed.type, type == "Achievement" {
            alertController.addAction(shareAction)
        }
        
        let deleteAction = UIAlertAction.init(title:titleIs, style: .destructive) { (action) in
            if feed.userID == userInfo?.userId {
                self.removeOrReportFrom(sender.tag)
            } else {
                self.showReportScreen(sender.tag)
            }
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (acrion) in
            
        }
        
   //     alertController.addAction(shareAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        parentVC.present(alertController, animated: true, completion: nil)
            }
    }
}
    private func showReportScreen(_ index:Int) {
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        selectedIndex = index
        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ReportVC) as? ReportVC {
            vc.delegate = self
            let newNavigation = UINavigationController.init(rootViewController: vc)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            parentVC.present(newNavigation, animated: true, completion: nil)
        }
    }
    private func removeOrReportFrom(_ index:Int) {
        
        
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        let feed = parentVC.feeds[index]
       
        var request = ""
        if feed.userID == userInfo?.userId {
            parentVC.feeds.remove(at: index)
            let indexPath = IndexPath.init(row: index, section: 0)
            parentVC.table_View.deleteRows(at: [indexPath], with: .bottom)
            request = Api.deletePost
        } else {
            request = Api.reportPost
        }
        self.requestFor(request,feed)
    }
    
    private func updateFeedListing() {
        guard let parentVC = self.parentViewController as? MyFeedVC else { return }
        let nav = parentVC.navigationController
        guard let tabBar = nav?.tabBarController as? TabBarVC else { return }
        
        if let feedNav = tabBar.viewControllers?[0] as? FeedsVC {
            feedNav.refreshMethod()
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        // CELLS STILL FREEZE EVEN WHEN THE FOLLOWING LINE IS COMMENTED OUT?!?!
        super.prepareForReuse()
      
        if let type = selectedAchivement?.type {
            if type == "Achievement" {
                imageView?.image = nil
                
                
            }else if type == "Video" {
            
                imageView?.image = nil
            }else{
                
                imageView?.image = nil
            }
        }
    }
    
    


}


extension MyFeedCell: EditBioDelegate {
    func bioUpdated(_ text:String) {
        
        guard let feed = self.selectedAchivement else { return }
        guard let feedId = feed.feedID else { return }
        feed.feed = text
        self.lbl_Comment.text = text
        
        guard let parentVC = self.myParent else { return }
        if let index = parentVC.feeds.index(where: { (myFeed) -> Bool in
            myFeed.feedID == feedId
        }) {
            parentVC.feeds[index] = feed
            let indexPath = IndexPath(row: index, section: 0)
            parentVC.table_View.reloadRows(at: [indexPath], with: UITableViewRowAnimation.bottom)
            
            if let nav = parentVC.navigationController {
                
                if let profileVC = nav.viewControllers.first as? ProfileVC {
                    profileVC.getProfileInfo()
                }
                
                if let tabbarVC = nav.tabBarController, tabbarVC.viewControllers?.count != 0 {
                    if let feedsNavVC = tabbarVC.viewControllers?.first as? UINavigationController, feedsNavVC.viewControllers.count > 0 {
                        if let feedsVC = feedsNavVC.viewControllers[0] as? FeedsVC {
                            feedsVC.refreshMethod()
                        }
                    }
                }
            }
        }
    }
}
extension MyFeedCell: ReportDelegate {
    func reportReason(_ reason:String) {
        self.selectedReason = reason
        if let index = selectedIndex {
            self.removeOrReportFrom(index)
        }
    }
}
