//
//  MyFeedVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 03/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

enum FEEDTYPE:String {
    case ACHIEVEMENT = "Achievement"
    case IMAGE = "Image"
    case VIDEO = "Video"

}

class MyFeedVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    var feedType:FEEDTYPE = FEEDTYPE.IMAGE
    
    var userModel:UserData?
    
    let refreshControl = UIRefreshControl()
    var feeds = [Feed]()
    var pageNo = 1
    var delegate:AddMoreInfoDelegate?
    var openFromprofile = false
    var isBool : Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.estimatedRowHeight = 280
        table_View.rowHeight = UITableViewAutomaticDimension
        
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(FeedsVC.getFeedsForRequest), for: .valueChanged)
        
      //  self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        
        if feedType == FEEDTYPE.ACHIEVEMENT {
            self.title = "Achievements"
            
        } else {
            self.title = "Images and Videos"
        }
        
        DispatchQueue.global().async { GetUser.getUserProfileInfo { (userDetail) in }
            
            self.getFeedsForRequest()
            showHud("Processing..")
        }
        
        setBackViewColor()
        navigationItem.largeTitleDisplayMode = .never
        table_View.tableFooterView = UIView.init(frame: .zero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    } // http://apogeesoftwares.com/sports100/posts/getUserFeed/page:1
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getFeedsForRequest()
    }
    
    @objc func getFeedsForRequest() {
        let params = ["key": Api.key, "userID":userModel?.userId ?? "", "feedType":feedType.rawValue]
        var requestString = Api.getAllUserFeed
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        requestString = requestString + "/page:\(pageNo)"
    
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        
                        if let allFeeds = result["UserFeed"] as? [[String:Any]], allFeeds.count > 0 {
                            
                            let feedModels = allFeeds.map({ (feedInfo) -> Feed in
                                Feed.init(object: feedInfo)
                            })
                            if self.pageNo == 1 {
                                self.feeds.removeAll()
                                self.feeds = feedModels
                            } else {
                                for f in feedModels {
                                    self.feeds.append(f)
                                }
                            }
                            DispatchQueue.main.async {
                                self.table_View.reloadData()
                            }
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
               // showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
}

extension MyFeedVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellId = "FeedCell"
        let feed = self.feeds[indexPath.row]
        
            if (feedType == FEEDTYPE.ACHIEVEMENT) {
                cellId = "MyFeedAchieveCell"
                
            } else if (feedType == FEEDTYPE.IMAGE) {
                cellId = "MyFeedCell"
            } else if (feedType == FEEDTYPE.VIDEO) {
                cellId = "MyFeedCell"
            }
            let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? MyFeedCell
        if isBool == true{
            cell?.btn_More.isHidden = true
        }
            cell?.btn_Like.tag = indexPath.row
            cell?.btn_More.tag = indexPath.row
            cell?.btn_Comment.tag = indexPath.row
            cell?.btn_Views.tag = indexPath.row
            cell?.setUpMyFeedCell(feed, self)
            cell?.selectionStyle = .none
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
    }
}

extension MyFeedVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == feeds.count - 2 {
            // pageNo = pageNo + 1
            //  self.getFeedsForRequest()
        }
    }
}
