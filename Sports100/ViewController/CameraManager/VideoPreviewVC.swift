//
//  VideoPreviewVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 25/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//
import UIKit
import AVFoundation
import AVKit

class VideoPreviewVC: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    var videoURL: URL?
    var thumbnailImage: UIImage?
    
    var player: AVPlayer?
    weak var rootVC: UIViewController?
    var playerController : AVPlayerViewController?
    var videoData: Data? = nil
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var viewSave: UIViewX!
    
    //MARK: ----------- VIEW LIFE CYCLE ------------

    override func viewDidLoad() {
        super.viewDidLoad()
      
        showHud("Processing..")
        
        setNavigationBarImage()

        player = AVPlayer(url: videoURL!)
        
        guard player != nil else { return }
        
        let layer = AVPlayerLayer.init(player: self.player!)
        layer.frame = self.view.bounds
        self.view.layer.addSublayer(layer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        if let url = self.videoURL {
            DispatchQueue.global().async {
                self.thumbnailImage = getThumbnailImage(forUrl: url)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        
        super.viewWillAppear(animated)
        let playPauseButton = UIButton.init(frame: self.view.bounds)
        playPauseButton.addTarget(self, action: #selector(VideoPreviewVC.buttonClicked_PausePlay), for: .touchUpInside)
        self.view.addSubview(playPauseButton)
        self.view.bringSubview(toFront: playPauseButton)
        self.view.bringSubview(toFront: viewSave)
        self.view.backgroundColor = .black
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideHud()
        player?.play()
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func buttonClicked_Camera(_ sender:UIButton) {
//        AppDelegate.getDelegate().previousIndex = AppDelegate.getDelegate().currentIndex
//        AppDelegate.getDelegate().currentIndex = tabBarController?.selectedIndex ?? 0
      //  self.playerController = nil
        self.player = nil
        self.presentCamera()
    }
    
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        let confirmAction = UIAlertAction.init(title: "Confirm", style: .default) { (action) in
            if let url = self.videoURL {
                SaveImageInGallery.sharedInstance.saveVideoIngallery(url)
            }
        }
        showAlert(APP_TITLE, message: "Save to camera roll?", withAction: confirmAction, with: true, andTitle: "Cancel", onView: self)
    }

    
    func presentCamera() {
        
        if let vc = rootVC as? PhotoPreviewVC {
            if let cameraVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CameraVC) as? CustomCameraViewController {
                cameraVC.rootVC = vc
                self.navigationController?.dismiss(animated: true) {
                    
                }
                vc.present(cameraVC, animated: true, completion: {
                    
                })
            }
        }
    }
    
    @IBAction func buttonClicked_Post(_ sender:UIButton) {
        
        guard let time = player?.currentItem?.duration else { return }
        print(time)
        player?.pause()
        let duration = Double(CMTimeGetSeconds(time))
        if duration > 16.0 {
            showAlert(nil, message: "Videos length exceeded. Videos can only be 15 seconds max", onView: self)
            return
        }
        if let vc = self.rootVC as? CreatePostVC {
            vc.videoURL = self.videoURL
            vc.selectedImage = self.thumbnailImage
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
        }
    }
    
    @objc func buttonClicked_PausePlay() {
        if let playerExist = self.player {
            if playerExist.isPlaying {
                playerExist.pause()
            } else {
                playerExist.play()
            }
        }
    }
}
