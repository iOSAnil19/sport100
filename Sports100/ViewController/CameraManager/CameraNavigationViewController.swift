//
//  CameraNavigationViewController.swift
//  GP Society
//
//  Created by Vivan Raghuvanshi on 29/01/18.
//  Copyright © 2018 Nripendra Hudda. All rights reserved.
//

import UIKit

class CameraNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.getDelegate().openCamera = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBar.isHidden = true
        if  AppDelegate.getDelegate().openCamera == true {
            if let naVC = self.storyboard?.instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController {
                if let cameraVC = naVC.visibleViewController as? CustomCameraViewController {
                    cameraVC.rootVC = self
                }
                self.present(naVC, animated: true, completion: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}

