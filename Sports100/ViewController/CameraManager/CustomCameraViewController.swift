//
//  CustomCameraViewController.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 25/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Photos
import AVKit
import MobileCoreServices

class CustomCameraViewController: SwiftyCamViewController, SwiftyCamViewControllerDelegate {
    
    @IBOutlet weak var captureButton: SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var labelTimer: UILabel!
    weak var rootVC:UIViewController?
    var imagePicker:UIImagePickerController?
    var countTimer  = Timer()
    var counter     = 0
    var isPushed = false
    
    //MARK: View Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameraDelegate = self
        captureButton.delegate = self
        shouldUseDeviceOrientation = true
        allowAutoRotate = true
        audioEnabled = true
        self.title = " "
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        labelTimer.text = ""
        counter     = 0
        isPushed = false
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureButton.delegate = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        countTimer.invalidate()
    }
    
    //MARK: Camera View Delegate
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        //        let newVC = PhotoViewController(image: photo)
        //        self.present(newVC, animated: true, completion: nil)
        self.goToNextViewWithImage(photo)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did Begin Recording")
        isPushed = false
        captureButton.growButton()
        countTimer = Timer.scheduledTimer(timeInterval:1, target: self, selector:#selector(updateTimer) , userInfo: nil, repeats: true) // Start timer
        
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController,didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        print("Did finish Recording")
        captureButton.shrinkButton()
        UIView.animate(withDuration: 0.25, animations: {
            self.flashButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        //        let main = instantiateViewControllerWithIdentifier(identifier:"VideoViewController", storyboardName:"Main") as! VideoViewController
        //         main.videoURL = url
        //       // let newVC = VideoPreviewVC(videoURL: url)
        //        self.present(main, animated: true, completion: nil)
        if isPushed == false {
            isPushed = true
            countTimer.invalidate()
            self.goToNextViewWithVideo(url)
        }
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }, completion: { (success) in
                focusView.removeFromSuperview()
            })
        })
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        print(zoom)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        print(camera)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print(error)
    }
    
    @IBAction func cameraSwitchTapped(_ sender: Any) {
        switchCamera()
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        flashEnabled = !flashEnabled
        
        if flashEnabled == true {
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
        } else {
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
        }
    }
    
    fileprivate func goToNextViewWithImage(_ image:UIImage) {
        
        if let photoPreviewVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.PhotoPreviewVC) as? PhotoPreviewVC {
            photoPreviewVC.selectedImage = image
            photoPreviewVC.rootVC = self.rootVC
            self.navigationController?.pushViewController(photoPreviewVC, animated: true)
        }
    }
    
    fileprivate func goToNextViewWithVideo(_ videoURL:URL) {
        if let videoPreviewVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.VideoPreviewVC) as? VideoPreviewVC {
            videoPreviewVC.videoURL = videoURL
            videoPreviewVC.rootVC = self.rootVC
        self.navigationController?.pushViewController(videoPreviewVC, animated: true)
        }
    }
    
    
    //MARK: Button Action
    
    @IBAction func buttonClicked_Back(_ sender:UIButton) {
        //        AppDelegate.getDelegate().openCamera = false
        if let vc = rootVC as? PhotoPreviewVC {
            vc.tabBarController?.selectedIndex = AppDelegate.getDelegate().previousIndex
            AppDelegate.getDelegate().previousIndex = AppDelegate.getDelegate().currentIndex
            AppDelegate.getDelegate().currentIndex = vc.tabBarController?.selectedIndex ?? 0
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: Back Button  Action
    
    @IBAction func buttonClicked_Gallary(_ sender: UIButton) {
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            
            guard let _self = self else { return }
            
            switch status {
            case .authorized:
                _self.openGallery()
                break
            case .denied:
                
                let alertController = UIAlertController(title: "Alert", message: "User doesn't have permission to use the library, please change privacy settings", preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title:"Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                
                // Add the actions
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                
                // Present the controller
                _self.present(alertController, animated: true, completion: nil)
                
                break
            case .notDetermined:
                break
            case .restricted:
                break
            }
            
        }
    }
}


//MARK: Image Picker View ----

extension CustomCameraViewController:UIImagePickerControllerDelegate {
    
    
    private func openGallery() {
        imagePicker = UIImagePickerController.init()
        imagePicker?.delegate = self
        imagePicker?.mediaTypes = [(kUTTypeImage as String), (kUTTypeMovie as String)]
        imagePicker?.sourceType = .photoLibrary
        if let picker = imagePicker {
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true) {
            if let pic = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.goToNextViewWithImage(pic)
                return
            }
            
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                //                let main = instantiateViewControllerWithIdentifier(identifier:"VideoPreviewVC", storyboardName:"Main") as! VideoPreviewVC
                //                main.videoURL = videoURL as URL
                //                // let newVC = VideoPreviewVC(videoURL: url)
                //                self.present(main, animated: true, completion: nil)
                
                self.goToNextViewWithVideo(videoURL as URL)
            }
            
            
        }
    }
}

extension CustomCameraViewController {
    
    @objc func updateTimer() {
        
        let secondLeft  = counter%3600
        if secondLeft <= 16 {
            self.labelTimer.text = self.seconds2Timestamp(intSeconds:secondLeft)
            counter += 1
        } else {
            countTimer.invalidate()
            self.session.stopRunning()
        }
        
        
    }
    func seconds2Timestamp(intSeconds:Int)->String {
        let mins:Int = intSeconds/60
        let hours:Int = mins/60
        let secs:Int = intSeconds%60
        //((hours<10) ? "0" : "") + String(hours) + ":" +
        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
        return strTimestamp
    }
}

extension CustomCameraViewController:UINavigationControllerDelegate {
    
}

