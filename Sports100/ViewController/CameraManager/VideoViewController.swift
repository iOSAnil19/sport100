//
//  VideoViewController.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 25/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//
import UIKit
import AVFoundation
import AVKit

class VideoViewController: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    var videoURL: URL?
    var player: AVPlayer?
    weak var rootVC:UIViewController?
    var playerController : AVPlayerViewController?
    var videoData        : Data? = nil
    
    @IBOutlet weak var btnSave: UIButton!
    
//    init(videoURL: URL) {
//        self.videoURL = videoURL
//        super.init(nibName: nil, bundle: nil)
//    }
    
//required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    //MARK: ----------- VIEW LIFE CYCLE ------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
        showHud("Processing..")
        
        setNavigationBarImage()
        self.view.backgroundColor = UIColor.gray
        player = AVPlayer(url: videoURL!)
       // playerController = AVPlayerViewController()
        
        guard player != nil// && playerController != nil
            else {
            return
        }
        
        let layer = AVPlayerLayer.init(player: self.player!)
        layer.frame = self.view.bounds
        self.view.layer.addSublayer(layer)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let playPauseButton = UIButton.init(frame: self.view.bounds)
        playPauseButton.addTarget(self, action: #selector(VideoViewController.buttonClicked_PausePlay), for: .touchUpInside)
        self.view.addSubview(playPauseButton)
        self.view.bringSubview(toFront: playPauseButton)
        self.view.bringSubview(toFront: btnSave)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideHud()
        player?.play()
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func buttonClicked_Camera(_ sender:UIButton) {
//        AppDelegate.getDelegate().previousIndex = AppDelegate.getDelegate().currentIndex
//        AppDelegate.getDelegate().currentIndex = tabBarController?.selectedIndex ?? 0
      //  self.playerController = nil
        self.player = nil
        self.presentCamera()
    }
    
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        let confirmAction = UIAlertAction.init(title: "Confirm", style: .default) { (action) in
            if let url = self.videoURL {
                SaveImageInGallery.sharedInstance.saveVideoIngallery(url)
            }
        }
        showAlert(APP_TITLE, message: "Save to camera roll?", withAction: confirmAction, with: true, andTitle: "Cancel", onView: self)
    }

    
    func presentCamera() {
        
        if let vc = rootVC as? PhotoPreviewVC {
            if let cameraVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CameraVC) as? CustomCameraViewController {
                cameraVC.rootVC = vc
                self.navigationController?.dismiss(animated: true) {
                    
                }
                vc.present(cameraVC, animated: true, completion: {
                    
                })
            }
        }
    }
    
    @IBAction func buttonClicked_Post(_ sender:UIButton) {
        
        guard let time = player?.currentItem?.duration else { return }
        print(time)
        player?.pause()
        let duration = Double(CMTimeGetSeconds(time))
        if duration > 17.0 {
            showAlert(nil, message: "Maximum length of video is 16 seconds. ", onView: self)
            return
        }
        
        let postAction = UIAlertAction.init(title: "Post", style: .default) { (action) in
            self.makeVideoPost(self.videoURL!)
        }
        
        showAlert(nil, message: "Would you like to post this video?", withAction: postAction, with: true, andTitle: "Cancel", onView: self)
       
    }
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
            //self.player!.pause()
        }
    }
    
    @objc func buttonClicked_PausePlay() {
        if let playerExist = self.player {
            if playerExist.isPlaying {
                playerExist.pause()
            } else {
                playerExist.play()
            }
        }
    }
    
    
    func makeVideoPost(_ video_URL:URL) {
        
        showHud("Processing..")
        
        let params = ["key":Api.key,
                      "userID":userInfo?.userId ?? "",
                      "videoTitle":"",
                      "videoDescription":""]
        
        let networkManager = NetworkManager()
        do {
            self.videoData = try Data(contentsOf:video_URL, options: NSData.ReadingOptions.uncached)
            
            //print("self.videoData",self.videoData as Any)
            //print("self.videoData?.count",self.videoData?.count as Any)
            
            // let thumbnailIm age = self.videoPreviewUIImage(moviePath:asset.url)
            //self.videoUrl = asset.url
        }catch {}
        
        
        
        networkManager.uploadImage(Api.uploadVideo, onPath:self.videoData, andParameter: params) { (response, error) in
            hideHud()
            print(response ?? "No response")
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        guard let photoPreviewVC = self.rootVC as? PhotoPreviewVC else { return }
                        photoPreviewVC.tabBarController?.selectedIndex = 0
                        
                        if let profileNavVC = photoPreviewVC.tabBarController?.viewControllers?.last as? UINavigationController, profileNavVC.viewControllers.count > 0 {
                            if let profileVC = profileNavVC.viewControllers[0] as? ProfileVC {
                                GetUser.getUserProfileInfo({ (user) in
                                    if profileVC.table_View != nil {
                                        profileVC.table_View.reloadData()
                                    }
                                })
                            }
                        }
                        
                        if let feedNavVC = photoPreviewVC.tabBarController?.viewControllers?.first as? UINavigationController, feedNavVC.viewControllers.count > 0 {
                            if let feedVC = feedNavVC.viewControllers[0] as? FeedsVC {
                                feedVC.refreshMethod()
                            }
                        }
                        
                        self.navigationController?.dismiss(animated: true) {
                            showAlert("", message: "Video Uploaded Succesfully", onView: self.tabBarController ?? self)
                        }
                        
                        AppDelegate.getDelegate().previousIndex = 0
                        AppDelegate.getDelegate().currentIndex = 0
                      //  self.playerController = nil
                        self.player = nil
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

