//
//  PhotoPreviewVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 25/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class PhotoPreviewVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var image: UIImageView!
    var selectedImage:UIImage?
    var isBool = Bool()
    
    var rootVC: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        if let img = selectedImage {
            self.image.image = img
        }
        setNavigationBarImage()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Post(_ sender:UIButton) {
        if let vc = self.rootVC as? CreatePostVC {
            vc.selectedImage = self.selectedImage
        }
        
        if isBool == true{
        self.navigationController?.popViewController(animated: true)
        }else{
        self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        if let img = selectedImage {
            SaveImageInGallery.sharedInstance.saveImageInGallery(image:img)
        }
    }

}
