//
//  TeamViewController.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 27/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

protocol selectTeamListProtocol:class  {
    func selectTeamMemberUser(_ team:Team)
}

class TeamViewController: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var search_Bar:UISearchBar!
    weak var selectTeamDelegate: selectTeamListProtocol?  = nil
    var parentNavigationController : UINavigationController?
    
    let refreshControl = UIRefreshControl()
    
    var pageNo = 1
    var totalPageCount = 1
    var teams = [Team]()
    var allFilteredTeam = [Team]()
    
    var selectedTeam:Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        table_View.refreshControl = refreshControl
//        refreshControl.addTarget(self, action: #selector(TeamsVC.refreshMethod), for: .valueChanged)
//        showHud("Processing..")
//        self.getTeamListing()
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        UITextField.appearance(whenContainedInInstancesOf: [type(of: search_Bar)]).tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
    }
    
    @objc func refreshMethod() {
        pageNo = 1
      //  self.getTeamListing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func searchText(_ text:String) {
        
        if teams.count > 0 {
            
            let filterTeams = self.teams.filter({ (team) -> Bool in
                if let name = team.name {
                    let lowerName = name.lowercased()
                    let searchText = text.lowercased()
                    return lowerName.hasPrefix(searchText)
                }
                return false
            })
            self.filterFromAllTeamListing(filterTeams)
            return
        }
        
        
        let params = ["key": Api.key, "userID":userInfo?.userId ?? "", "searchType":"Team", "keyword":"\(text)"]
        
        var requestString = Api.searchResult
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allTeams = result["teams"] as? [[String:Any]], allTeams.count > 0 {
                            print(JSON(allTeams))

                            let teamModels = allTeams.map({ (teamInfo) -> Team in
                                Team.init(object: teamInfo)
                            })
                            var allTeams = [Team]()
                            
                            if self.pageNo == 1 {
                                allTeams = teamModels
                            } else {
                                for t in teamModels {
                                    allTeams.append(t)
                                }
                            }
                            self.filterFromAllTeamListing(allTeams)
                        }
                    } else {
                        let error = "\(String(describing: result["team"] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    private func filterFromAllTeamListing(_ t:[Team]) {
        var allTeams = t
        if let teamMenuVC = self.selectTeamDelegate as? TeamMenuViewController, let selectedSport = teamMenuVC.selectedTeamSport {
            
            
            let sportViseFilterTeams = allTeams.filter({ (team) -> Bool in
                if let sport = team.sport {
                    if let id = sport.iD {
                        print(id)
                        return id == selectedSport.id
                    }
                }
                return false
            })
            allTeams = sportViseFilterTeams
        }
        allTeams = self.filterTeamsWithGameDay(allTeams)
        allTeams = self.filterTeamsWithTrainningTime(allTeams)
        self.allFilteredTeam = allTeams
        self.table_View.reloadData()
    }
    
    private func filterTeamsWithGameDay(_ teams:[Team]) -> [Team] {
        var filteredTeam = [Team]()
        filteredTeam = teams
        guard let teamMenuVC = self.selectTeamDelegate as? TeamMenuViewController else { return filteredTeam }
        
        if teamMenuVC.GameSunAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sun", "AM")
        }
        if teamMenuVC.GameSunPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sun", "PM")
        }
        if teamMenuVC.GameMonAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Mon", "AM")
        }
        if teamMenuVC.GameMonPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Mon", "PM")
        }
        if teamMenuVC.GameTuesAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Tue", "AM")
        }
        if teamMenuVC.GameTuesPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Tue", "PM")
        }
        if teamMenuVC.GameWednesAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Wed", "AM")
        }
        if teamMenuVC.GameWednesPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Wed", "PM")
        }
        if teamMenuVC.GameThursAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Thu", "AM")
        }
        if teamMenuVC.GameThursPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Thu", "PM")
        }
        if teamMenuVC.GameFriAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Fri", "AM")
        }
        if teamMenuVC.GameFriPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Fri", "PM")
        }
        if teamMenuVC.GameSatAM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sat", "AM")
        }
        if teamMenuVC.GameSatPM {
            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sat", "PM")
        }
        return filteredTeam
    }
    
    private func checkAvailabilityOfGameDays(_ teams:[Team],_ day:String,_ time:String) -> [Team] {
        let availableTeams = teams.filter { (team) -> Bool in
            if let gameDays = team.gameDay, gameDays.count > 0 {
                let gameDaysAvailable = gameDays.filter({ (gameDay) -> Bool in
                    if let day = gameDay.day {
                        if let time = gameDay.time {
                            return day == day && time == time
                        }
                    }
                    return false
                })
                if gameDaysAvailable.count > 0 {
                    return true
                }
            }
            return false
        }
        return availableTeams.count > 0 ? availableTeams : teams
    }
    
    private func filterTeamsWithTrainningTime(_ teams:[Team]) -> [Team] {
        var filteredTeam = [Team]()
        filteredTeam = teams
        guard let teamMenuVC = self.selectTeamDelegate as? TeamMenuViewController else { return filteredTeam }
        
        if teamMenuVC.TrainingSunAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sun", "AM")
        }
        if teamMenuVC.TrainingSunPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sun", "PM")
        }
        if teamMenuVC.TrainingMonAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Mon", "AM")
        }
        if teamMenuVC.TrainingMonPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Mon", "PM")
        }
        if teamMenuVC.TrainingTuesAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Tue", "AM")
        }
        if teamMenuVC.TrainingTuesPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Tue", "PM")
        }

        if teamMenuVC.TrainingWednesAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Wed", "AM")
        }
        if teamMenuVC.TrainingWednesPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Wed", "PM")
        }
        if teamMenuVC.TrainingThursAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Thu", "AM")
        }
        if teamMenuVC.TrainingThursPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Thu", "PM")
        }

        if teamMenuVC.TrainingFriAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Fri", "AM")
        }
        if teamMenuVC.TrainingFriPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Fri", "PM")
        }
        if teamMenuVC.TrainingSatAM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sat", "AM")
        }
        if teamMenuVC.TrainingSatPM {
            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sat", "PM")
        }

        return filteredTeam
    }
    
    private func checkAvailabilityOfTrainningDays(_ teams:[Team],_ day:String,_ time:String) -> [Team] {
        let availableTeams = teams.filter { (team) -> Bool in
            if let trainningDays = team.traingTime, trainningDays.count > 0 {
                let trainningDaysAvailable = trainningDays.filter({ (trainningDayAvailable) -> Bool in
                    if let day = trainningDayAvailable.day {
                        if let time = trainningDayAvailable.time {
                            return day == day && time == time
                        }
                    }
                    return false
                })
                if trainningDaysAvailable.count > 0 {
                    return true
                }
            }
            return false
        }
        return availableTeams.count > 0 ? availableTeams : teams
    }
    
}

extension TeamViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allFilteredTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "TeamCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? TeamCell
        
        let team = self.allFilteredTeam[indexPath.row]
        cell?.setUPTeamCell(team)
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let team = self.allFilteredTeam[indexPath.row]
            self.removeTeam(team)
            self.teams.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    func removeTeam(_ team:Team) {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.removeTeam, andParameter: params) { (response, error) in
        }
    }
}

extension TeamViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = searchBar.text
        let isEmpty = text?.trimmingCharacters(in: .whitespaces).isEmpty
        if text == nil || text == "" || isEmpty == true {
            self.allFilteredTeam.removeAll()
            self.table_View.reloadData()
            self.view.endEditing(true)
        } else {
            self.searchText(text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
}

extension TeamViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let team = self.allFilteredTeam[indexPath.row]
        self.selectTeamDelegate?.selectTeamMemberUser(team)
    }
}
