//
//  TeamMenuViewController.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 27/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TeamMenuViewController: UIViewController, CAPSPageMenuDelegate, selectTeamListProtocol, selectPlayerListProtocol {
    var parentNavigationController : UINavigationController?
    var pageMenu : CAPSPageMenu?
    
    var allTeams:[Team]?
    
    var navigationBarheight :CGFloat = 64.0
    
    var selectedFiterType = 0
   
    var MonAM = false, TuesAM = false, WednesAM = false, ThursAM = false, FriAM = false, SatAM = false, SunAM = false
    var MonPM = false, TuesPM = false, WednesPM = false, ThursPM = false, FriPM = false, SatPM = false, SunPM = false
    
    var selectedPlayerSport:Sport?
    var selectedTeamSport:Sport?
    
    var GameMonAM = false, GameTuesAM = false, GameWednesAM = false, GameThursAM = false, GameFriAM = false, GameSatAM = false, GameSunAM = false
    var GameMonPM = false, GameTuesPM = false, GameWednesPM = false, GameThursPM = false, GameFriPM = false, GameSatPM = false, GameSunPM = false
    
    var TrainingMonAM = false, TrainingTuesAM = false, TrainingWednesAM = false, TrainingThursAM = false, TrainingFriAM = false, TrainingSatAM = false, TrainingSunAM = false
    var TrainingMonPM = false, TrainingTuesPM = false, TrainingWednesPM = false, TrainingThursPM = false, TrainingFriPM = false, TrainingSatPM = false, TrainingSunPM = false
    
    var searchTeamByPlayer = ""
    
    func resetAllValues() {
        MonAM = false; TuesAM = false; WednesAM = false; ThursAM = false; FriAM = false; SatAM = false; SunAM = false
        MonPM = false; TuesPM = false; WednesPM = false; ThursPM = false; FriPM = false; SatPM = false; SunPM = false
        
         selectedPlayerSport = nil
         selectedTeamSport = nil
        
        GameMonAM = false; GameTuesAM = false; GameWednesAM = false; GameThursAM = false; GameFriAM = false; GameSatAM = false; GameSunAM = false
        GameMonPM = false; GameTuesPM = false; GameWednesPM = false; GameThursPM = false; GameFriPM = false; GameSatPM = false; GameSunPM = false
        
        TrainingMonAM = false; TrainingTuesAM = false; TrainingWednesAM = false; TrainingThursAM = false; TrainingFriAM = false; TrainingSatAM = false; TrainingSunAM = false
        TrainingMonPM = false; TrainingTuesPM = false; TrainingWednesPM = false; TrainingThursPM = false; TrainingFriPM = false; TrainingSatPM = false; TrainingSunPM = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        createMenu()
        self.setFilterButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(self.searchTeamByPlayer)
    }
    private func setFilterButton() {
        
        let filterButton = UIButton(type: .custom)
        filterButton.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
        filterButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        filterButton.addTarget(self, action: #selector(filterPressed), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView:filterButton)
    }
    
    @objc func filterPressed() {
        if selectedFiterType == 0 {
            if let playerFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayerFilterVC") as? PlayerFilterVC {
                 playerFilterVC.delegate = self
                self.resetAllValues()
                self.navigationController?.pushViewController(playerFilterVC, animated: true)
            }
        } else {
            if let teamFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "TeamFilterVC") as? TeamFilterVC {
                teamFilterVC.delegate = self
                self.resetAllValues()
                self.navigationController?.pushViewController(teamFilterVC, animated: true)
            }
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func createMenu() {
        
        // Initialize view controllers to display and place in array
        self.navigationItem.title = "Search"
        
        // Initialize view controllers to display and place in array
        
        var controllerArray : [UIViewController] = []
        
        let playerViewController : PlayerViewController = Utility.instantiateViewControllerWithIdentifier(identifier: "PlayerViewController", storyboardName:"Main") as! PlayerViewController
        playerViewController.parentNavigationController = self.navigationController
        playerViewController.title = "Player"
        playerViewController.selectPlayerDelegate = self
        controllerArray.append(playerViewController)
        
        
        let teamViewController : TeamViewController = Utility.instantiateViewControllerWithIdentifier(identifier: "TeamViewController", storyboardName:"Main") as! TeamViewController
        teamViewController.parentNavigationController = self.navigationController
        teamViewController.title = "Teams"
        teamViewController.selectTeamDelegate = self
        controllerArray.append(teamViewController)
        
        // Customize menu (Optional)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0.0),// Remove Separator
            .ScrollMenuBackgroundColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)),
            .ViewBackgroundColor(UIColor.white),
            .BottomMenuHairlineColor(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)),
            .SelectionIndicatorColor(#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)),
            .MenuMargin(22.0*SCALE_FACTOR),
            .MenuHeight(44.0*SCALE_FACTOR),
            .SelectedMenuItemLabelColor(#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)),
            .UnselectedMenuItemLabelColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)),
            
            .SelectedMenuBackgroundItemLabelColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
            .UnselectedMenuBackgroundItemLabelColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
            
            .MenuItemFont(UIFont(name: "Avenir-Medium", size: 14.0 * SCALE_FACTOR)!),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorRoundEdges(true),
            .SelectionIndicatorHeight(3.0*SCALE_FACTOR),
            .MenuItemSeparatorPercentageHeight(0.0), // Remove Separator
            .EnableHorizontalBounce(false)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0.0, y:navigationBarheight, width:self.view.frame.width, height:self.view.frame.height), pageMenuOptions: parameters)
        
        //pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0.0, y:64.0, width:self.view.frame.width, height:self.view.frame.height), pageMenuOptions: parameters)
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
        
    }
    
    // MARK: -
    // MARK: - Move left Right Delegate method
    
    func didMoveToPage(controller: UIViewController, index: Int) {
        if let vc = controller as? TeamViewController {
            if let teamsExist = self.allTeams {
                vc.teams = teamsExist
            }
        }
        selectedFiterType = index
    }
    
    func willMoveToPage(controller: UIViewController, index: Int) {

    }
    
    
    // MARK: - Select PlayerMember Method
    func selectPlayerMemberUser(_ contact:Contact) {
        if let profileVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
            profileVC.otherUserId = contact.id
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    // MARK: - Select TeamMemberUser Method
     func selectTeamMemberUser(_ team:Team) {
        let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
        vc.teamId = team.iD
        vc.fromController = self
        self.navigationController?.pushViewController(vc,animated:true)
     }
}



