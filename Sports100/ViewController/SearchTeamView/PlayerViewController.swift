//
//  PlayerViewController.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 27/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift

protocol selectPlayerListProtocol:class  {
    func selectPlayerMemberUser(_ contact:Contact)
}

class PlayerViewController: UIViewController {
    
    var parentNavigationController : UINavigationController?
    weak var selectPlayerDelegate: selectPlayerListProtocol?  = nil
    
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var search_Bar:UISearchBar!
    var contacts = [Contact]()
    var pageNo = 1
    var totalPageCount = 1
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Players"
        table_View.tableFooterView = UIView.init(frame: .zero)
        self.setCancelButton()
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        UITextField.appearance(whenContainedInInstancesOf: [type(of: search_Bar)]).tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension PlayerViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = searchBar.text
        let isEmpty = text?.trimmingCharacters(in: .whitespaces).isEmpty
        if text == nil || text == "" || isEmpty == true {
            self.contacts.removeAll()
            self.table_View.reloadData()
            self.view.endEditing(true)
        } else {
            self.searchText(text!)
        }
    }
    
    
    func searchText(_ text:String) {
        let params = ["key": Api.key, "searchType":"Friend", "keyword":"\(text)", "userID":userInfo?.userId ?? ""]
        
        var requestString = Api.searchResult
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
           
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        if let allRecentMsg = result["friend"] as? [[String:Any]], allRecentMsg.count > 0 {
                            print(JSON(allRecentMsg))

                            var userModels = [Contact]()
                            
                            for msgInfo in allRecentMsg {
                                let newContact = Contact.init(name: "\(String(describing: msgInfo["Name"] ?? ""))", image: "\(String(describing: msgInfo["Image"] ?? ""))", id: "\(String(describing: msgInfo["ID"] ?? ""))")
                                if newContact.id != (userInfo?.userId ?? "") {
                                    userModels.append(newContact)
                                }
                            }
                            
                            if self.pageNo == 1 {
                                self.contacts = userModels
                            } else {
                                for user in userModels {
                                    self.contacts.append(user)
                                }
                            }
                            
                            /*if let teamMenuVC = self.selectPlayerDelegate as? TeamMenuViewController, let selectedSport = teamMenuVC.selectedPlayerSport {
                                let sportViseFilterPlayers = self.contacts.filter({ (player) -> Bool in
                                    //team.sport?.iD ?? "" == selectedSport.id
                                })
                                self.contacts = sportViseFilterPlayers
                            }*/
                            
                            self.table_View.reloadData()
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                       // showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension PlayerViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "ContactsCell"
        var cell = tableView.dequeueReusableCell(withIdentifier:cellId)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier:cellId)
        }
        let user = self.contacts[indexPath.row]
        if let label = cell?.contentView.viewWithTag(100) as? UILabel {
            label.text = "\(user.name)"
        }
        if let img = cell?.contentView.viewWithTag(101) as? UIImageView {
            let imgUrlString = imageUrl + "\(String(describing: user.image))"
            img.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        }
        
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
}

extension PlayerViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = self.contacts[indexPath.row]
        self.selectPlayerDelegate?.selectPlayerMemberUser(user)
    }
}
