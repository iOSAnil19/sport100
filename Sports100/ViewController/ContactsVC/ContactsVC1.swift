//
//  ContactsVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ContactsVC1: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    let refreshControl = UIRefreshControl()
    var pageNo = 1
    var totalPageCount = 1
    
    var usersList = [Contact]()
    
    var presentingController:UIViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never

        //  self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        
        self.title = "Contacts"
        //   self.navigationController?.navigationBar.prefersLargeTitles = true
        //  let search = UISearchController(searchResultsController: nil)
        // search.searchResultsUpdater = self

        if self.navigationController?.viewControllers.count == 1 {
            setCancelButton()
        } else {
            // setNavigationBarImage()
            //         search.searchBar.tintColor = .white
            //        search.searchBar.barTintColor = .white
        }
        //    self.navigationItem.searchController = search
        //    navigationItem.hidesSearchBarWhenScrolling  = false
        self.navigationController?.navigationBar.isTranslucent = false
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(ContactsVC1.refreshMethod), for: .valueChanged)
        table_View.tableFooterView = UIView.init(frame: .zero)
        showHud("Processing..")
        self.getAllUsers()
    }
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getAllUsers()
    }
    
    func getAllUsers() {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""] //
        var requestString = Api.contactList
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allRecentMsg = result["friend"] as? [[String:Any]], allRecentMsg.count > 0 {
                            
                            var userModels = [Contact]()
                            
                            for msgInfo in allRecentMsg {
                                let newContact = Contact.init(name: "\(String(describing: msgInfo["Name"] ?? ""))", image: "\(String(describing: msgInfo["Image"] ?? ""))", id: "\(String(describing: msgInfo["ID"] ?? ""))")
                                if newContact.id != (userInfo?.userId ?? "") {
                                    userModels.append(newContact)
                                }
                            }
                            
                            if self.pageNo == 1 {
                                self.usersList = userModels
                            } else {
                                for user in userModels {
                                    self.usersList.append(user)
                                }
                            }
                            self.table_View.reloadData()
                        }
                        
                    } else {
                        if let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "TeamMenuViewController") as? TeamMenuViewController {
                            searchVC.hidesBottomBarWhenPushed = true
                             searchVC.navigationBarheight = 0.0
                            self.navigationController?.pushViewController(searchVC, animated: true)
                        }
                        //let error = "\(String(describing: result["message"] ?? INTERNAL_ERROR_MESSAGE))"
                        //showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
        
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ContactsVC1 : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "ContactsCell"
        var cell = tableView.dequeueReusableCell(withIdentifier:cellId)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier:cellId)
        }
        let user = self.usersList[indexPath.row]
        if let label = cell?.contentView.viewWithTag(100) as? UILabel {
            label.text = user.name
        }
        if let img = cell?.contentView.viewWithTag(101) as? UIImageView {
            let imgUrlString = imageUrl + "\(String(describing: user.image))"
            img.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        }
        
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
}

extension ContactsVC1 : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = self.usersList[indexPath.row]
        
        if let controller = presentingController as? MessagesVC {
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"ChatMessageViewController", storyboardName:"Main") as! ChatMessageViewController
            vc.otherUserId = user.id
            vc.otherUserName = user.name
            controller.navigationController?.pushViewController(vc, animated:false)
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        if let profileVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
            profileVC.otherUserId = user.id
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == usersList.count - 2 {
            if totalPageCount == pageNo {
                return
            }
            pageNo = pageNo + 1
            self.getAllUsers()
        }
    }
}

