//
//  ContactVC.swift
//  Sports100
//
//  Created by VeeraJain on 24/07/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ContactVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactCell
        let user = usersList[indexPath.row]
            cell.userName.text = user.name
        let imgUrlString = imageUrl + "\(user.image )"
        if let imgLogoUrl = URL(string:imgUrlString) {
            cell.userImage.sd_setImage(with: imgLogoUrl, placeholderImage:#imageLiteral(resourceName: "defult_user"))
        }
        
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.otherUserId = usersList[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBOutlet weak var userTable: UITableView!
    let refreshControl = UIRefreshControl()
    var pageNo = 1
    var totalPageCount = 1
    var presentingController:UIViewController?

    var usersList = [Contact]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never
        userTable.delegate = self
        userTable.dataSource = self
        //  self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        
        self.title = "Contacts"
        NavigationBarImage()
        //   self.navigationController?.navigationBar.prefersLargeTitles = true
        //  let search = UISearchController(searchResultsController: nil)
        // search.searchResultsUpdater = self
        
        if self.navigationController?.viewControllers.count == 1 {
            setCancelButton()
        } else {
            // setNavigationBarImage()
            //         search.searchBar.tintColor = .white
            //        search.searchBar.barTintColor = .white
        }
        //    self.navigationItem.searchController = search
        //    navigationItem.hidesSearchBarWhenScrolling  = false
        self.navigationController?.navigationBar.isTranslucent = false
        userTable.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(ContactVC.refreshMethod), for: .valueChanged)
        userTable.tableFooterView = UIView()
        showHud("Processing..")
        getContact()
        
    }
    func NavigationBarImage() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 92, height: 22))
        let logo = UIImage(named: "Navbar.png")
        imageView.image = logo
        self.navigationItem.titleView = imageView
    }
    @objc func refreshMethod() {
        pageNo = 1
        self.getContact()
    }
    func getContact(){
          let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        var requestString = Api.contactList
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allRecentMsg = result["friend"] as? [[String:Any]], allRecentMsg.count > 0 {
                            
                            var userModels = [Contact]()
                            
                            for msgInfo in allRecentMsg {
                                let newContact = Contact.init(name: "\(String(describing: msgInfo["Name"] ?? ""))", image: "\(String(describing: msgInfo["Image"] ?? ""))", id: "\(String(describing: msgInfo["ID"] ?? ""))")
                                if newContact.id != (userInfo?.userId ?? "") {
                                    userModels.append(newContact)
                                }
                            }
                            
                            if self.pageNo == 1 {
                                self.usersList = userModels
                            } else {
                                for user in userModels {
                                    self.usersList.append(user)
                                }
                            }
                            self.userTable.reloadData()
                        }
                        
                    } else {
                        
                        let error = "\(String(describing: result["message"] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
