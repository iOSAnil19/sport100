//
//  ContactModel.swift
//  Sports100
//
//  Created by VeeraJain on 24/07/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactModel: NSObject {
    
    var name:String?
    var image:String?
    var Id:String?
    
     convenience init(json:JSON) {
        self.init()
        name = json[Key.name].stringValue
        image = json[Key.image].stringValue
        Id = json[Key.id].stringValue
    }
    
    struct Key {
        static let name = "Name"
        static let id = "ID"
        static let image = "Image"
    }

}
