//
//  ChatMessageViewController.swift
//  Sports100
//
//  Created by Nripendra Hudda on 09/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol ChatMessageDelegate {
    func newMessageUpdated()
}


class ChatMessageViewController: UIViewController,UITextViewDelegate,THChatInputDelegate {
    
    @IBOutlet weak var chatMessageTableView : UITableView?
    @IBOutlet weak var sendButton : UIButton!
    @IBOutlet weak var  chatInput:THChatInput?
    
    let refreshControl = UIRefreshControl()
    var pageNo = 1
    var totalPageCount = 1
    
    //  var recentMsg:RecentMsg?
    var otherUserId:String?
    var otherUserName:String?
    var messages = [Message]()
    
    var delegate:ChatMessageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // *** Listen to keyboard show / hide ***
        
        
//        self.title = otherUserName
        let titleView = UILabel()
        titleView.text = otherUserName
        titleView.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        titleView.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        let width = titleView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)).width
        titleView.frame = CGRect(origin:CGPoint.zero, size:CGSize(width: width, height: 500))
        self.navigationItem.titleView = titleView

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(titleWasTapped))
        titleView.isUserInteractionEnabled = true
        titleView.addGestureRecognizer(recognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        
        // *** Hide keyboard when tapping outside ***
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        chatMessageTableView?.estimatedRowHeight = 10
        chatMessageTableView?.rowHeight = UITableViewAutomaticDimension
        
        self.view.backgroundColor = UIColor.white
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = false
        
        chatMessageTableView?.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(ChatMessageViewController.refreshMethod), for: .valueChanged)

        self.getAllMessagesOfUser()
    }
  
  
  
    @objc private func titleWasTapped() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.otherUserId = otherUserId!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func refreshMethod() {
        if totalPageCount == pageNo {
            return
        }
        pageNo = pageNo + 1
        self.getAllMessagesOfUser()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    
    func getAllMessagesOfUser() {
        
        guard let recentId = otherUserId else { return }
        
        let params = ["key": Api.key, "toID":recentId, "fromID":userInfo?.userId ?? ""]
        
        var requestString = Api.getAllMessage
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
               // hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allRecentMsg = result[NetworkManager.kMESSAGE] as? [[String:Any]], allRecentMsg.count > 0 {
                            var previousDate = ""
                            var todayDade = ""
                            let msgs = allRecentMsg.reversed()
                            let recentMsgModels = msgs.map({ (msgInfo) -> Message in
                                var newMsg =  Message.init(object: msgInfo)
                                
                               
                                previousDate = todayDade
                                todayDade = getDateStringFromString(newMsg.created ?? "")
                                if previousDate == "" || (previousDate != todayDade) {
                                    newMsg.datetimestamp = true
                                } else {
                                    newMsg.datetimestamp = false
                                }
                                return newMsg
                            })
                            if self.pageNo == 1 {
                                self.messages = recentMsgModels//.reversed()
                                
                                self.messages  = self.messages.sorted(by: { (item1, item2) -> Bool in
                                    let date1  = getLocaleDate(item1.created!)
                                    let date2  = getLocaleDate(item2.created!)
                                    return date1.compare(date2) == ComparisonResult.orderedAscending
                                })
                                
                            } else {
                                for recentMsg in recentMsgModels {
                                    self.messages.append(recentMsg)
                                    
                                }
                                
                                self.messages  = self.messages.sorted(by: { (item1, item2) -> Bool in
                                    let date1  = getLocaleDate(item1.created!)
                                    let date2  = getLocaleDate(item2.created!)
                                    return date1.compare(date2) == ComparisonResult.orderedAscending
                                })
                                
                              
                                
                            }
                            self.chatMessageTableView?.reloadData()
                            let indexPath = IndexPath.init(row:(self.messages.count - 1), section: 0)
                            self.chatMessageTableView?.scrollToRow(at:indexPath, at: .none , animated: true)
                        }
                        
                    } else {
                        if self.pageNo != 1 {
                            self.pageNo = self.pageNo -  1
                        }
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    if self.pageNo != 1 {
                        self.pageNo = self.pageNo - 1
                    }
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                if self.pageNo != 1 {
                    self.pageNo = self.pageNo - 1
                }
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
        //setCornerRadouius()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        //        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
        //
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    
    //MARK: - Keyboard method -
    var contentInset:UIEdgeInsets = UIEdgeInsets()
    @objc private func keyboardWillShow(notification:NSNotification){
        chatMessageTableView?.isScrollEnabled = true
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var info = notification.userInfo!
        var keyboardFrame:CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        contentInset = (chatMessageTableView?.contentInset)!
        contentInset.bottom = keyboardFrame.size.height
        chatMessageTableView?.contentInset = contentInset
    }
    
    @objc private func keyboardWillHide(notification:NSNotification){
        contentInset.bottom = 0
        chatMessageTableView?.contentInset = contentInset
        chatMessageTableView?.isScrollEnabled = true
    }
    
    
}
extension ChatMessageViewController : UITableViewDelegate, UITableViewDataSource {
    
    //MARK: TABLE DATASOURCE And Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    //MARK: *******************table view data source section
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //print("object.mediaFile?",object.mediaFile?.mediaType ?? "")
        let message = self.messages[indexPath.row]
        
        if message.sender?.iD == userInfo?.userId //if object.chtaId == "sender"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NIKMessageCellS", for: indexPath) as! NIKMessageCellS
            cell.selectionStyle = .none
            cell.object = message
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NIKMessageCellR", for: indexPath) as! NIKMessageCellR
            cell.selectionStyle = .none
            cell.object = message
            return cell
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        ///  self.selectRecentDelegate?.selectExitingUser(userObj: ChatUserObj)
        tableView.deselectRow(at:indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == messages.count - 2 {
//            if totalPageCount == pageNo {
//                return
//            }
//            pageNo = pageNo + 1
//            self.getAllMessagesOfUser()
//        }
    }
    
}


// MARK:-   Chat Input View

extension ChatMessageViewController
{
    // MARK: - THChatInputDelegate
    
    func chat(_ input: THChatInput?, sendWasPressed text: String?) {
        
        
        if text?.isEmpty == true
        {
            return
        }
        guard let recentId = otherUserId else { return }
        
        let date = getServerDateFromDate(Date())
        
        let sender = ["Name":userInfo?.firstName ?? "", "Image":"", "ID":userInfo?.userId ?? ""]
        let receiver = ["Name":"", "Image":"", "ID":recentId]
        let msgDict = ["MessageID":"0", "Message":text!, "Created":date, "To": receiver, "From":sender] as [String : Any]
        
        var msg = Message.init(object: msgDict)
        if self.messages.count > 0 {
            let previousMsg = self.messages.last
            let previousDate = getDateStringFromString(previousMsg?.created ?? "")
            let todayDade = getDateStringFromDate(Date())
            if previousDate == "" || (previousDate != todayDade) {
                msg.datetimestamp = true
            } else {
                msg.datetimestamp = false
            }
        } else {
            msg.datetimestamp = true
        }
        
        self.messages.append(msg)
        self.chatInput?.setText("")
        //self.view.endEditing(true)
        
        if(self.messages.count>0)
        {
            let indexPath = IndexPath.init(row:(self.messages.count - 1), section: 0)
            chatMessageTableView?.insertRows(at: [indexPath], with: .automatic)
            
            self.chatMessageTableView?.scrollToRow(at:indexPath, at: .top, animated: true)
        } else {
            chatMessageTableView?.reloadData()
        }
        self.sendMessageToUser(msg)
    }
    
    func chatKeyboardDidShow(_ cinput: THChatInput!) {
        
        if self.messages.count > 0 {
            let indexPath = IndexPath.init(row:(self.messages.count - 1), section: 0)
            self.chatMessageTableView?.scrollToRow(at:indexPath, at: .top, animated: true)
        }
       
    }
    
    func sendMessageToUser(_ msg:Message) {
        let params = ["key": Api.key, "toID":msg.receiver?.iD ?? "", "message":msg.message ?? "", "fromID":userInfo?.userId ?? ""] //fromID
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.sendMessage, andParameter: params) { (result, error) in
            if error == nil {
                if let msgNavVC = self.tabBarController?.viewControllers?[1] as? UINavigationController, msgNavVC.viewControllers.count > 0 {
                    if let messagesVC = msgNavVC.viewControllers[0] as? MessagesVC {
                        messagesVC.newMessageUpdated()
                    }
                }
            }
        }
    }
    
    func chatShowEmojiInput(_ input: THChatInput?) {
        // chatInput.textView.inputView = chatInput.textView.inputView == nil ? emojiInputView : nil
        //chatInput.textView.reloadInputViews()
    }
    
    func chatShowAttach(_ input: THChatInput?) {
        
    }
    
}

