//
//  MessagesVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class MessagesVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    let refreshControl = UIRefreshControl()
    var pageNo = 1
    var totalPageCount = 1
    
    var recentMsgs = [RecentMsg]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(MessagesVC.refreshMethod), for: .valueChanged)
        table_View.tableFooterView = UIView.init(frame: .zero)
        showHud("Processing..")
        self.getAllRecentMessage()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getAllRecentMessage()
    }
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getAllRecentMessage()
    }
    
    func getAllRecentMessage() {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""] //
        var requestString = Api.getMessageThred
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allRecentMsg = result["result"] as? [[String:Any]], allRecentMsg.count > 0 {
                            
                            let recentMsgModels = allRecentMsg.map({ (msgInfo) -> RecentMsg in
                                RecentMsg.init(object: msgInfo)
                            })
                            if self.pageNo == 1 {
                                self.recentMsgs = recentMsgModels
                            } else {
                                for recentMsg in recentMsgModels {
                                    self.recentMsgs.append(recentMsg)
                                }
                            }
                            if (self.table_View != nil) {
                                self.table_View.reloadData()
                            }
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? "No Recent Messages Found"))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
        
    }  //getMessageThred
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Edit(_ sender:UIBarButtonItem) {
        if(table_View.isEditing == true) {
            table_View.isEditing = false
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(buttonClicked_Edit))
        }
        else {
            table_View.isEditing = true
            self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(buttonClicked_Edit))
        }
    }
    
    @IBAction func buttonClicked_WriteNewMsg(_ sender:UIBarButtonItem) {
        
        if let contactsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactVC") as? ContactVC {
            contactsVC.presentingController = self
            let newNavigation = UINavigationController.init(rootViewController: contactsVC)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]

            self.present(newNavigation, animated: true, completion: nil)
        }
    }
}

extension MessagesVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentMsgs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let recentMsg = self.recentMsgs[indexPath.row]
        
        let cellId = "MessagesCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? MessagesCell
        cell?.lbl_Name.text = "\(recentMsg.name ?? "")"
        cell?.lbl_Message.text = recentMsg.message
        let imgUrlString = imageUrl + "\(recentMsg.image ?? "")"
        cell?.img_View.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.delayPlaceholder, completed: nil)
        
        if let time = recentMsg.created {
            let msgDate = getLocaleDate(time)
            cell?.lbl_Time.text = msgDate.getElapsedInterval()
        }
        if(DeviceType.IS_IPAD){
            cell?.img_View.layer.cornerRadius  = 24*SCALE_FACTOR
            cell?.img_View.clipsToBounds = true
        }
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let msg = self.recentMsgs[indexPath.row]
            self.removeRecentThread(msg)
            self.recentMsgs.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    
    func removeRecentThread(_ recentMsg:RecentMsg) {
        
        let params = ["key": Api.key, "toID":userInfo?.userId ?? "", "fromID":recentMsg.iD ?? ""]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.removeMessageThread, andParameter: params) { (response, error) in
        }
    }
}

extension MessagesVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"ChatMessageViewController", storyboardName:"Main") as! ChatMessageViewController
        let recentMsg = self.recentMsgs[indexPath.row]
        vc.otherUserId = recentMsg.iD ?? ""
        vc.otherUserName = recentMsg.name ?? ""
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == recentMsgs.count - 2 {
            if totalPageCount == pageNo {
                return
            }
            pageNo = pageNo + 1
            self.getAllRecentMessage()
        }
    }
}

extension MessagesVC : ChatMessageDelegate {
    func newMessageUpdated() {
        self.refreshMethod()
    }
}

