//
//  ChatMessageTableViewCell.swift
//  Sports100
//
//  Created by Nripendra Hudda on 09/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class NIKMessageCellS: UITableViewCell {
    
    @IBOutlet weak var lblMessage     : UILabel!
    @IBOutlet weak var lblTime        : UILabel!
    @IBOutlet weak var imgStatus      : UIImageView!
    @IBOutlet weak var viewBackground : UIView!
    @IBOutlet weak var lblDateTimeStamp : DateTimestampLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
    }
    
    func updateCellData(object:Message?) {
        viewBackground.drawBorderedView(cornerRadius: 5.0, borderColor: UIColor.clear)
        self.setDataForSentMessages()
    }
    
    var object:Message? {
        didSet{

             lblDateTimeStamp.setDate = object
            self.setDataForSentMessages()
            viewBackground.drawBorderedView(cornerRadius: 8.0, borderColor: UIColor.clear)
        }
    }
    
    func setDataForSentMessages()
    {
        self.lblMessage.text = object?.message ?? ""
         if(DeviceType.IS_IPAD){
           if((object?.message?.count ?? 0) < 3){
            self.lblTime.font = UIFont.init(name:"Avenir Next Regular", size: 13)
          }
        }
        if let receiverTime = object?.created {
            let date = getLocaleDate(receiverTime)
            self.lblTime.text = getTimeStringFromDate(date)
        } 
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}



class NIKMessageCellR: UITableViewCell {
    
    
    @IBOutlet weak var lblSenderName      : UILabel!
    @IBOutlet weak var lblMessage         : UILabel!
    @IBOutlet weak var lblTime            : UILabel!
    @IBOutlet weak var viewBG             : UIView!
    @IBOutlet weak var lblDateTimeStamp : DateTimestampLabel!

    @IBOutlet weak var groupBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var groupHeightConstant: NSLayoutConstraint!
    
    var isGroup = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        //viewBG.drawRecieverRect()
        viewBG.drawBorderedView(cornerRadius: 8.0, borderColor: UIColor.clear)
    }
    
    var object:Message?
    {
        didSet
        {
            //lblDatetimestamp.setDate = object
            lblDateTimeStamp.setDate = object
            self.setDataForReceivedMessages()
            viewBG.drawBorderedView(cornerRadius: 8.0, borderColor: UIColor.clear)
        }
    }
    
    
    func setDataForReceivedMessages()
    {
        
        if isGroup == true
        {
            groupBottomConstant.constant = 5
            groupHeightConstant.constant = 15
            
        }else
        {
            self.lblSenderName.text = ""
            groupBottomConstant.constant = 0
            groupHeightConstant.constant = 0
        }
        
        
        self.lblMessage.text = object?.message
        
        if let senderTime = object?.created
        {
            let date = getLocaleDate(senderTime)
            self.lblTime.text = getTimeStringFromDate(date)
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}

extension UIView
{
    func drawBorderedView(cornerRadius:CGFloat,borderColor:UIColor)
    {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func drawBorderedExtendedView(cornerRadius:CGFloat,borderColor:UIColor)
    {
        let aLayer = self.layer
        var aFrame = aLayer.frame
        aFrame.size.width = aFrame.size.width + 25
        aLayer.borderColor = borderColor.cgColor
        aLayer.borderWidth = 1
        aLayer.cornerRadius = cornerRadius
        aLayer.masksToBounds = true
        self.clipsToBounds = true
    }
    
}
class DateTimestampLabel:UILabel{
    
    @IBOutlet weak var heightConstant:NSLayoutConstraint!
    @IBOutlet weak var topConstant:NSLayoutConstraint!
    @IBOutlet weak var bottomConstant:NSLayoutConstraint!
    
    //    var orignalTop:CGFloat = 0.0
    //    var orignalBottom:CGFloat = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 6.0
        self.clipsToBounds = true
        
        self.backgroundColor = UIColor.clear //gray.withAlphaComponent(0.7)
        
        
        
    }
    
    var setDate:Message?
    {
        didSet
        {
            //            orignalTop = topConstant.constant
            //            orignalBottom = bottomConstant.constant
            
            if (setDate?.datetimestamp)! {
                self.isHidden = false
                self.text = getDateStringFromString(setDate?.created ?? "") //Date.dateInStringFromTimeStamp(timeStamp: Double((setDate?.timestamp)!)!, formate: "EEE, dd MMM, yyyy")
                self.text = "  " + self.text! + "  "
                
                if heightConstant != nil {
                    heightConstant.constant = 25
                    topConstant.constant = 8
                    bottomConstant.constant = 8
                }
            }
            else{
                self.isHidden = true
                if heightConstant != nil {
                    heightConstant.constant = 0
                    topConstant.constant = 5
                    bottomConstant.constant = 0
                }
            }
        }
    }
    
}

