//
//  OnboardVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 25/06/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyOnboard

class CellOnBoard : UICollectionViewCell {
    
    @IBOutlet weak var imageBoard: UIImageView!
}

class OnboardVC: UIViewController {
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var mPageController: UIPageControl!
    @IBOutlet weak var collectionOnboard: UICollectionView!
    var swiftyOnboard: SwiftyOnboard!
    let colors = [#colorLiteral(red: 0.9647058824, green: 0.9607843137, blue: 0.9960784314, alpha: 1),#colorLiteral(red: 0.9647058824, green: 0.9607843137, blue: 0.9960784314, alpha: 1),#colorLiteral(red: 0.9647058824, green: 0.9607843137, blue: 0.9960784314, alpha: 1),#colorLiteral(red: 0.9647058824, green: 0.9607843137, blue: 0.9960784314, alpha: 1)]
    var titleArray = ["Welcome to Confess!", "It’s completely anonymous", "Say something positive",""]
    var subTitleArray = ["Confess lets you anonymously\n send confessions to your friends\n and receive confessions from them.", "All confessions sent are\n anonymous. Your friends will only\n know that it came from one of\n their facebook friends.", "Be nice to your friends.\n Send them confessions that\n will make them smile :)",""]
    
    var gradiant: CAGradientLayer = {
        //Gradiant for the background view
        let blue = UIColor(red: 69/255, green: 127/255, blue: 202/255, alpha: 1.0).cgColor
        let purple = UIColor(red: 166/255, green: 172/255, blue: 236/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [purple, blue]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        gradient()
        self.navigationController?.navigationBar.isHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionOnboard.collectionViewLayout.invalidateLayout()
//        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
//
//        view.addSubview(swiftyOnboard)
//        swiftyOnboard.dataSource = self
//        swiftyOnboard.delegate = self
    }

    
    func gradient() {
        //Add the gradiant to the view:
        self.gradiant.frame = view.bounds
        view.layer.addSublayer(gradiant)
    }
    
    @IBAction func handleSkip() {
        //swiftyOnboard?.goToPage(index: 2, animated: true)
        self.goToLoginView()
    }
    
    @IBAction func handleContinue(sender: UIButton) {
        let index = mPageController.currentPage
        if index == 3 {
            self.goToLoginView()
            return
        }
        btnContinue.isHidden = true
        collectionOnboard.scrollToItem(at: IndexPath(item: 1, section: 0), at: .centeredHorizontally, animated: true)
//        swiftyOnboard?.goToPage(index: index + 1, animated: true)
    }
    
    private func goToLoginView() {
        if let navigationController = STORYBOARD.instantiateViewController(withIdentifier:"RootNavigation") as? UINavigationController {
            UserDefaults.standard.set(onboardSeen, forKey: onboardSeen)
            let loginVC = STORYBOARD.instantiateViewController(withIdentifier: StoryboardID.LoginVC)
            navigationController.viewControllers = [loginVC]
            APPDELEGATE.window?.rootViewController = navigationController
        }
    }
}

extension OnboardVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellOnBoard", for: indexPath) as! CellOnBoard
        cell.imageBoard.image = UIImage(named: "onboard\(indexPath.row)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        mPageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        btnSkip.isHidden = mPageController.currentPage != 0
        if (mPageController.currentPage == 0 || mPageController.currentPage == 3) {
        btnContinue.isHidden = false
        } else {
                btnContinue.isHidden = true
        }
        let text = mPageController.currentPage == 0 ? "Continue" : "Get Started!"
        btnContinue.setTitle(text, for: .normal)
        
    }
    
}

//extension OnboardVC: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
//
//    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
//        //Number of pages in the onboarding:
//        return 4
//    }
//
//    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
//        //Return the background color for the page at index:
//        return colors[index]
//    }
//
//    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
//        let view = SwiftyOnboardPage()
//        //Set the image on the page:
//        //view.frame = CGRect(x:0, y: 46, width:(APPDELEGATE.window?.frame.size.width)!, height: (APPDELEGATE.window?.frame.size.height)!-64)
//        DispatchQueue.main.asyncAfter(deadline: .now() ) {
////            view.imageView.frame = CGRect(x:0, y: 20, width:self.view.frame.size.width, height: self.view.frame.size.height-84)
//            view.imageView.image = UIImage(named: "onboard\(index)")
//            view.imageView.backgroundColor = UIColor.red
//
//
//
//        }
//
//
//        print(view.imageView.frame)
//        //Set the font and color for the labels:
//       // view.title.font = UIFont(name: "Lato-Heavy", size: 22)
//       // view.subTitle.font = UIFont(name: "Lato-Regular", size: 16)
//        view.imageView.isHidden = false
//        view.title.isHidden = true
//        view.subTitle.isHidden = true
//
//        //Set the text in the page:
//       // view.title.text = titleArray[index]
//       // view.subTitle.text = subTitleArray[index]
//
//        //Return the page for the given index:
//        return view
//    }
//    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
//
//        let overlay = SwiftyOnboardOverlay()
//        //Setup targets for the buttons on the overlay view:
//        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
//        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
//
//        //Setup for the overlay buttons:
//        DispatchQueue.main.asyncAfter(deadline: .now() ) {
//
//        overlay.continueButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16)
//        overlay.continueButton.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), for: .normal)
//        overlay.skipButton.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), for: .normal)
////        overlay.continueButton.titleLabel?.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
////        overlay.skipButton.titleLabel?.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
//        overlay.skipButton.titleLabel?.font = UIFont(name: "Lato-Heavy", size: 16)
//        overlay.skipButton.setTitleColor(.red, for: .normal)
//        }
//        //Return the overlay view:
//        return overlay
//    }
//
//    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
//        let currentPage = round(position)
//        overlay.pageControl.currentPage = Int(currentPage)
//        print(Int(currentPage))
//        overlay.continueButton.tag = Int(position)
////        overlay.continueButton.titleLabel?.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
////        overlay.skipButton.titleLabel?.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
//        if currentPage == 0.0 || currentPage == 1.0 || currentPage == 2.0 {
//            overlay.continueButton.setTitle("", for: .normal)
//            overlay.skipButton.setTitle("Skip", for: .normal)
//            overlay.skipButton.isHidden = false
//        } else {
//            overlay.continueButton.setTitle("Get Started!", for: .normal)
//            overlay.skipButton.isHidden = true
//        }
//    }
//}



