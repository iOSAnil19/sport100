//
//  TeamFilterVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 20/10/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TeamFilterVC: UIViewController {
    
    @IBOutlet weak var tableFilter: UITableView!
    @IBOutlet weak var picker:UIPickerView!
    @IBOutlet weak var availabilityView:AvailabilityView!
    
    let filterOptions = ["Sport", "Address", "Game Days", "Trainning Days"]

    var allSports = [Sport]()
    
    var delegate: UIViewController?

    var selectedSport:Sport?
    var gameText = ""
    var trainingText = ""
    
    var isGameSelected = false
    var isTrainingSelected = false
    
    var GameMonAM = false, GameTuesAM = false, GameWednesAM = false, GameThursAM = false, GameFriAM = false, GameSatAM = false, GameSunAM = false
    var GameMonPM = false, GameTuesPM = false, GameWednesPM = false, GameThursPM = false, GameFriPM = false, GameSatPM = false, GameSunPM = false
    
    var TrainingMonAM = false, TrainingTuesAM = false, TrainingWednesAM = false, TrainingThursAM = false, TrainingFriAM = false, TrainingSatAM = false, TrainingSunAM = false
    var TrainingMonPM = false, TrainingTuesPM = false, TrainingWednesPM = false, TrainingThursPM = false, TrainingFriPM = false, TrainingSatPM = false, TrainingSunPM = false

    //MARK: ------------- VIEW LIFE CYCLE ----------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let availableSports =  Utility.shared.availableSports, availableSports.count > 0 {
            allSports = availableSports
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClickedSave(_ sender: UIButton) {
        if let vc = delegate as? SearchVC {
//            vc.selectedTeamSport = self.selectedSport
//            vc.GameMonAM = GameMonAM; vc.GameTuesAM = GameTuesAM; vc.GameWednesAM = GameWednesAM; vc.GameThursAM = GameThursAM; vc.GameFriAM = GameFriAM; vc.GameSatAM = GameSatAM; vc.GameSunAM = GameSunAM;
//            vc.GameMonPM = GameMonPM; vc.GameTuesPM = GameTuesPM; vc.GameWednesPM = GameWednesPM; vc.GameThursPM = GameThursPM; vc.GameFriPM = GameFriPM; vc.GameSatPM = GameSatPM; vc.GameSunPM = GameSunPM;
//            vc.TrainingMonAM = TrainingMonAM; vc.TrainingTuesAM = TrainingTuesAM; vc.TrainingWednesAM = TrainingWednesAM; vc.TrainingThursAM = TrainingThursAM; vc.TrainingFriAM = TrainingFriAM; vc.TrainingSatAM = TrainingSatAM; vc.TrainingSunAM = TrainingSunAM;
//            vc.TrainingMonPM = TrainingMonPM; vc.TrainingTuesPM = TrainingTuesPM; vc.TrainingWednesPM = TrainingWednesPM; vc.TrainingThursPM = TrainingThursPM; vc.TrainingFriPM = TrainingFriPM; vc.TrainingSatPM = TrainingSatPM; vc.TrainingSunPM = TrainingSunPM;
//            vc.filterFromAllTeam()
        }
        self.navigationController?.popViewController(animated: true)
    }
 }

extension TeamFilterVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let filterCell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as? FilterCell {
            let filterText = filterOptions[indexPath.row]
            filterCell.lblFilterName.text = filterText
            filterCell.selectionStyle = .none
            switch (indexPath.row) {
            case 0:
                if let sportExist = self.selectedSport {
                    filterCell.lblFilterValue.text = sportExist.name
                }
                break;
            case 1:
                break;
            case 2:
                filterCell.lblFilterValue.text = gameText
                break;
            case 3:
                filterCell.lblFilterValue.text = trainingText
                break;
            default:
                break;
            }
            return filterCell
        }
        return UITableViewCell()
    }
}

extension TeamFilterVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isGameSelected = false
        isTrainingSelected = false
        switch (indexPath.row) {
        case 0:
            if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
                if !cell.isFirstResponder {
                    picker.tag = 999
                    picker.reloadAllComponents()
                    _ = cell.becomeFirstResponder()
                }
            }
            break;
        case 1:
            break;
        case 2:
            availabilityView.showPopUp(self)
            isGameSelected = true
            isTrainingSelected = false
            if availabilityView.table_View != nil {
                availabilityView.table_View.reloadData()
            }
            availabilityView.delegate = self
            break;
        case 3:
            availabilityView.showPopUp(self)
            isGameSelected = false
            isTrainingSelected = true
            if availabilityView.table_View != nil {
                availabilityView.table_View.reloadData()
            }
            availabilityView.delegate = self
            break;
        default:
            break;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0 * getScaleFactor()
    }
}

extension TeamFilterVC:UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 999:
            return self.allSports.count
        default:
            return 0
        }
    }
}

extension TeamFilterVC:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 999:
            let mySport = self.allSports[row]
            return mySport.name
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 999:
            let mySport = self.allSports[row]
            self.selectedSport = mySport
        default:
            break
        }
        tableFilter.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30 * getScaleFactor()
    }
}


extension TeamFilterVC:AvailabilityViewDelegate {
    func popHides() {
        self.view.endEditing(true)
    }
    
    func setAMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            if isGameSelected {
                GameSunAM = available
            } else if isTrainingSelected {
                TrainingSunAM = available
            } else {
                GameSunAM = false
                TrainingSunAM = false
            }
        case "Monday":
            if isGameSelected {
                GameMonAM = available
            } else if isTrainingSelected {
                TrainingMonAM = available
            } else {
                GameMonAM = false
                TrainingMonAM = false
            }
        case "Tuesday":
            if isGameSelected {
                GameTuesAM = available
            } else if isTrainingSelected {
                TrainingTuesAM = available
            } else {
                GameTuesAM = false
                TrainingTuesAM = false
            }
        case "Wednesday":
            if isGameSelected {
                GameWednesAM = available
            } else if isTrainingSelected {
                TrainingWednesAM = available
            } else {
                GameWednesAM = false
                TrainingWednesAM = false
            }
        case "Thursday":
            if isGameSelected {
                GameThursAM = available
            } else if isTrainingSelected {
                TrainingThursAM = available
            } else {
                GameThursAM = false
                TrainingThursAM = false
            }
        case "Friday":
            if isGameSelected {
                GameFriAM = available
            } else if isTrainingSelected {
                TrainingFriAM = available
            } else {
                GameFriAM = false
                TrainingFriAM = false
            }
        case "Saturday":
            if isGameSelected {
                GameSatAM = available
            } else if isTrainingSelected {
                TrainingSatAM = available
            } else {
                GameSatAM = false
                TrainingSatAM = false
            }
        default:
            break
        }
        self.setAvailabilityField()
    }
    
    func setPMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            if isGameSelected {
                GameSunPM = available
            } else if isTrainingSelected {
                TrainingSunPM = available
            } else {
                GameSunPM = false
                TrainingSunPM = false
            }
        case "Monday":
            if isGameSelected {
                GameMonPM = available
            } else if isTrainingSelected {
                TrainingMonPM = available
            } else {
                GameMonPM = false
                TrainingMonPM = false
            }
        case "Tuesday":
            if isGameSelected {
                GameTuesPM = available
            } else if isTrainingSelected {
                TrainingTuesPM = available
            } else {
                GameTuesPM = false
                TrainingTuesPM = false
            }
        case "Wednesday":
            if isGameSelected {
                GameWednesPM = available
            } else if isTrainingSelected {
                TrainingWednesPM = available
            } else {
                GameWednesPM = false
                TrainingWednesPM = false
            }
        case "Thursday":
            if isGameSelected {
                GameThursPM = available
            } else if isTrainingSelected {
                TrainingThursPM = available
            } else {
                GameThursPM = false
                TrainingThursPM = false
            }
        case "Friday":
            if isGameSelected {
                GameFriPM = available
            } else if isTrainingSelected {
                TrainingFriPM = available
            } else {
                GameFriPM = false
                TrainingFriPM = false
            }
        case "Saturday":
            if isGameSelected {
                GameSatPM = available
            } else if isTrainingSelected {
                TrainingSatPM = available
            } else {
                GameSatPM = false
                TrainingSatPM = false
            }
        default:
            break
        }
        self.setAvailabilityField()
    }
    
    func setAvailabilityField() {
        
        var text = ""
        if isGameSelected {
            if GameSunAM || GameSunPM {
                text = "Su, "
            }
            if GameMonAM || GameMonPM {
                text = text + "M, "
            }
            if GameTuesAM || GameTuesPM {
                text = text + "Tu, "
            }
            if GameWednesAM || GameWednesPM {
                text = text + "W, "
            }
            if GameThursAM || GameThursPM {
                text = text + "Th, "
            }
            if GameFriAM || GameFriPM {
                text = text + "F, "
            }
            if GameSatAM || GameSatPM {
                text = text + "Sa"
            }
            gameText = text
            tableFilter.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .automatic)
        } else if isTrainingSelected {
            if TrainingSunAM || TrainingSunPM {
                text = "Su, "
            }
            if TrainingMonAM || TrainingMonPM {
                text = text + "M, "
            }
            if TrainingTuesAM || TrainingTuesPM {
                text = text + "Tu, "
            }
            if TrainingWednesAM || TrainingWednesPM {
                text = text + "W, "
            }
            if TrainingThursAM || TrainingThursPM {
                text = text + "Th, "
            }
            if TrainingFriAM || TrainingFriPM {
                text = text + "F, "
            }
            if TrainingSatAM || TrainingSatPM {
                text = text + "Sa"
            }
            trainingText = text
             tableFilter.reloadRows(at: [IndexPath.init(row: 3, section: 0)], with: .automatic)
        } else {
            gameText = ""
            trainingText = ""
            tableFilter.reloadRows(at: [IndexPath.init(row: 2, section: 0),IndexPath.init(row: 3, section: 0)], with: .automatic)
        }
    }
}

