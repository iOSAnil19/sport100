//
//  MatchCell.swift
//  Sports100
//
//  Created by Depex Technologies Pvt. Ltd on 20/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class MatchCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Match:UILabel!
    @IBOutlet weak var lbl_MatchTime:UILabel!
    @IBOutlet weak var lbl_MatchScore:UILabel!
    @IBOutlet weak var lbl_MatchLocation:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateMatchCell(_ match:Matches, _ team:Team) {
        
        let teamName = team.name ?? ""
         self.lbl_Match.text = "\(teamName) vs \(match.matchName ?? "Match")"
        //let matchDate = getLocaleDate(match.matchDate ?? "")
        self.lbl_MatchTime.text = geDateString(match.matchDate ?? "", "dd MMM yyyy hh:mm a",isUTC: false)
        

        // self.lbl_MatchTime.text = getDateStringFromDate(matchDate)
         self.lbl_MatchScore.text = match.score  ?? ""
         self.lbl_MatchLocation.text = match.location ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
