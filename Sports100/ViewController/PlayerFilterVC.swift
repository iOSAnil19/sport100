//
//  PlayerFilterVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 20/10/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit


class PlayerFilterVC: UIViewController {
    
    @IBOutlet weak var tableFilter: UITableView!
    @IBOutlet weak var picker:UIPickerView!
    @IBOutlet weak var availabilityView:AvailabilityView!
    
    let filterOptions = ["Nick Name", "Sport", "Position", "Strongest", "Nationality", "Availability Times"] //"Location"
    let footHandArray = ["Left footed",
                         "Right footed",
                         "Both footed",
                         "Left handed",
                         "Right handed",
                         "Ambidextrous"]
    
    var allSports = [Sport]()
    var positions = [Position]()
    let allCountries = getAllCountries()
    var selectedPosition:Position?
    var selectedCountry = ""
    var selectedStrong = ""


    var selectedSport:Sport? {
        didSet {
            if let sport = selectedSport {
                self.getAllThePositionFor(sport.id)
            }
        }
    }
    
    var delegate: UIViewController?
    var availabilityText = ""
    
    // Filter option variables
    var isNickNameFilter = false
    var filterValueName = ""
    
    var MonAM = false, TuesAM = false, WednesAM = false, ThursAM = false, FriAM = false, SatAM = false, SunAM = false
    var MonPM = false, TuesPM = false, WednesPM = false, ThursPM = false, FriPM = false, SatPM = false, SunPM = false
    var trainingText = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let availableSports =  Utility.shared.availableSports, availableSports.count > 0 {
            allSports = availableSports
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClickedSave(_ sender: UIButton) {
        if let vc = delegate as? SearchVC {
//            vc.selectedPlayerSport = self.selectedSport
//            vc.searchTeamByPlayer = self.filterValueName
//            vc.selectedContry = self.selectedCountry
//            vc.selectedPlayerPosition = self.selectedPosition
//            vc.MonAM = MonAM;       vc.MonPM = MonPM;
//            vc.TuesAM = TuesAM;     vc.TuesPM = TuesPM;
//            vc.WednesAM = WednesAM; vc.WednesPM = WednesPM;
//            vc.ThursAM = ThursAM;   vc.ThursPM = ThursPM;
//            vc.FriAM = FriAM;       vc.FriPM = FriPM;
//            vc.SatAM = SatAM;       vc.SatPM = SatPM;
//            vc.SunAM = SunAM;       vc.SunPM = SunPM;
//            vc.selectedStrong = selectedStrong
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func getAllThePositionFor(_ id:String) {
        let params = ["key": Api.key,"sportID":id]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getPosition, andParameter: params) { (response, error) in
            hideHud()
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")

                    if success == 200 {
                        if let positionsArray = result["Position"] as? [[String:Any]] {
                            let positionsModels = positionsArray.map({ (positionInfo) -> Position in
                                return Position.init(name:"\(String(describing: positionInfo["Position"] ?? ""))", id:"\(String(describing: positionInfo["ID"] ?? ""))")
                            })
                            
                            self.positions = positionsModels
                            self.picker.reloadAllComponents()
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
               // showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
}

extension PlayerFilterVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let filterCell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as? FilterCell {
            let filterText = filterOptions[indexPath.row]
            filterCell.lblFilterName.text = filterText
            filterCell.selectionStyle = .none
            filterCell.lblFilterValue.text = nil
            switch (indexPath.row) {
            case 0:
                filterCell.lblFilterName.textColor = isNickNameFilter ? #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1): .black
                break;
            case 1:
                if let sportExist = self.selectedSport {
                    filterCell.lblFilterValue.text = sportExist.name
                    filterCell.becomeFirstResponder()
                }
                break;
            case 2:
                if let positionExist = self.selectedPosition {
                    filterCell.lblFilterValue.text = positionExist.name
                    filterCell.becomeFirstResponder()
                }
                break;
            case 3:
                
                if selectedStrong != "" {
                    filterCell.lblFilterValue.text = selectedStrong
                    filterCell.becomeFirstResponder()
                }
                
                break;
            case 4:
                filterCell.lblFilterValue.text = self.selectedCountry
                filterCell.becomeFirstResponder()
                break;
            case 5:
                filterCell.lblFilterValue.text = availabilityText
                break;
            default:
                break;
            }
            return filterCell
        }
        return UITableViewCell()
    }
}

extension PlayerFilterVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch (indexPath.row) {
        case 0:
            let filterText = filterOptions[indexPath.row]
            isNickNameFilter = !isNickNameFilter
            self.filterValueName = filterText
            tableView.reloadRows(at: [indexPath], with: .automatic)
            break;
        case 1:
            if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
                if !cell.isFirstResponder {
                    picker.tag = 999
                    picker.reloadAllComponents()
                    _ = cell.becomeFirstResponder()
                }
            }
            break;
        case 2:
            if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
                if !cell.isFirstResponder {
                    picker.tag = 1000
                    picker.reloadAllComponents()
                    _ = cell.becomeFirstResponder()
                }
            }
            break;
        case 3:
            
            if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
                if !cell.isFirstResponder {
                    picker.tag = 1002
                    picker.reloadAllComponents()
                    _ = cell.becomeFirstResponder()
                }
            }
            break;
        case 4:
            if let cell = tableView.cellForRow(at: indexPath) as? FilterCell {
                if !cell.isFirstResponder {
                    if self.positions.count > 0 {
                        picker.tag = 1001
                        picker.reloadAllComponents()
                        _ = cell.becomeFirstResponder()
                    }
                }
            }
            break;
        case 5:
            availabilityView.showPopUp(self)
            if availabilityView.table_View != nil {
                availabilityView.table_View.reloadData()
            }
            availabilityView.delegate = self
            break;
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0 * getScaleFactor()
    }
}


extension PlayerFilterVC:UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 999:
            return self.allSports.count
        case 1000:
            return self.positions.count
        case 1001:
            return self.allCountries.count
        case 1002:
            return self.footHandArray.count
        default:
            return 0
        }
    }
}

extension PlayerFilterVC:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 999:
            let mySport = self.allSports[row]
            return mySport.name
        case 1000:
            let myPosition = self.positions[row]
            return myPosition.name
        case 1001:
            let myCountry = self.allCountries[row]
            return myCountry
        case 1002:
            let strong = self.footHandArray[row]
            return strong
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 999:
            let mySport = self.allSports[row]
            self.selectedSport = mySport
            tableFilter.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
            break;
        case 1000:
            let myPosition = self.positions[row]
            self.selectedPosition = myPosition
            tableFilter.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
            break;
        case 1001:
            let myCountry = self.allCountries[row]
            self.selectedCountry = myCountry
            tableFilter.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            break;
        case 1002:
            let strong = self.footHandArray[row]
            self.selectedStrong = strong
            tableFilter.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
            break;
        default:
            break
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30 * getScaleFactor()
    }
}


extension PlayerFilterVC:AvailabilityViewDelegate {
    func popHides() {
        self.view.endEditing(true)
    }
    
    func setAMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            SunAM = available
        case "Monday":
            MonAM = available
        case "Tuesday":
            TuesAM = available
        case "Wednesday":
            WednesAM = available
        case "Thursday":
            ThursAM = available
        case "Friday":
            FriAM = available
        case "Saturday":
            SatAM = available
        default:
            break
        }
        self.setAvailabilityField()
    }
    
    func setPMAvailability(_ day:String, available:Bool) {
        switch day {
        case "Sunday":
            SunPM = available
        case "Monday":
            MonPM = available
        case "Tuesday":
            TuesPM = available
        case "Wednesday":
            WednesPM = available
        case "Thursday":
            ThursPM = available
        case "Friday":
            FriPM = available
        case "Saturday":
            SatPM = available
        default:
            break
        }
        
        self.setAvailabilityField()
    }
    
    func setAvailabilityField() {
        
        var text = ""
        if SunAM || SunPM {
            text = "Su, "
        }
        if MonAM || MonPM {
            text = text + "M, "
        }
        if TuesAM || TuesPM {
            text = text + "Tu, "
        }
        if WednesAM || WednesPM {
            text = text + "W, "
        }
        if ThursAM || ThursPM {
            text = text + "Th, "
        }
        if FriAM || FriPM {
            text = text + "F, "
        }
        if SatAM || SatPM {
            text = text + "Sa"
        }
        availabilityText = text
        tableFilter.reloadRows(at: [IndexPath.init(row: 5, section: 0)], with: .automatic)
    }
}


class FilterCell: UITableViewCell {
    
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var lblFilterValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    open override var canBecomeFirstResponder: Bool {
        return true
    }
    
    open override var canResignFirstResponder: Bool {
        return true
    }
    
    open override var inputView: UIView? {
        if let parentVC = self.parentViewController as? TeamFilterVC {
            return parentVC.picker
        }
        if let parentVC = self.parentViewController as? PlayerFilterVC {
            return parentVC.picker
        }
        return nil
    }
}


