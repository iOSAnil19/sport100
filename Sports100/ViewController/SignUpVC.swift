//
//  SignUpVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.

import UIKit
import SafariServices
import Alamofire

class SignUpVC: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    @IBOutlet weak var tf_FirstName:UITextField!
    @IBOutlet weak var tf_LastName:UITextField!
    @IBOutlet weak var tf_Email:UITextField!
    @IBOutlet weak var tf_Password:UITextField!
    @IBOutlet weak var tf_ConfirmPassword:UITextField!
    @IBOutlet weak var tf_Dob:UITextField!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var tf_Gender:UITextField!
    @IBOutlet weak var tf_ChooseSport:UITextField!
    @IBOutlet weak var date_Picker:UIDatePicker!
    @IBOutlet weak var picker:UIPickerView!
    @IBOutlet weak var lbl_Alert:UILabel!
    @IBOutlet weak var activity_Indicator:UIActivityIndicatorView!
    
    @IBOutlet weak var btn_AcceptTerms:UIButton!
    var isPriviceyDone :Bool?
    var privacyPolicy = false
    var termsAndConditions = false
    var cookiePolicy = false

    var sports:[Sport]?
    var social_User:SocialUser?
    let genders = ["Male", "Female", "Other"]
    var dobSelected:String?
    
    var selectedSport:Sport?
    
    var isDateConfirmed = false
    

    //MARK: ----------- VIEW LIFE CYCLE ------------

    override func viewDidLoad() {
        super.viewDidLoad()
        signupBtn.layer.cornerRadius = signupBtn.frame.height / 2
        signupBtn.clipsToBounds = true
        if let user = social_User {
            tf_FirstName.text = user.firstName ?? ""
            tf_LastName.text = user.lastName ?? ""
            tf_Gender.text = user.gender ?? ""
            tf_Email.text = user.email ?? ""
            tf_Password.isHidden = true
            tf_ConfirmPassword.isHidden = true
        }
        self.navigationController?.navigationBar.isHidden = false
        setBackViewColor()
        //setNavigationBarImage()
        self.title = "New User"
        
        let maxDate = Calendar.current.date(byAdding: .year, value: -16, to: Date())
        
        let minDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        
        date_Picker.maximumDate = maxDate
        date_Picker.minimumDate = minDate
        
        tf_Dob.inputView = date_Picker
        tf_Gender.inputView = picker
        tf_ChooseSport.inputView = picker
        
        tf_ChooseSport.isUserInteractionEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
  
  func moveToTerms(param:[String: Any]) {
    if let termsAndConditionVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.TermsAndConditionVC) as? TermsAndConditionVC {
        termsAndConditionVC.signUpVC = self
      termsAndConditionVC.param = param
        termsAndConditionVC.termsAndConditions = self.termsAndConditions
        termsAndConditionVC.privacyPolicy = self.privacyPolicy
        termsAndConditionVC.delegate = self
        self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
    }
  }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func buttonClicked_Register(_ sender:UIButton) {
        let checkResult = checkAllTheFields()
        if checkResult.0 == true {
        if isDateConfirmed == false {
            let confirmAction = UIAlertAction.init(title: "Confirm", style: .default) { (action) in
                self.isDateConfirmed = true
                self.hitApiForRegistration()
//              self.moveToTerms()
//                if let termsAndConditionVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.TermsAndConditionVC) as? TermsAndConditionVC {
//                    termsAndConditionVC.signUpVC = self
//                    termsAndConditionVC.termsAndConditions = self.termsAndConditions
//                    termsAndConditionVC.privacyPolicy = self.privacyPolicy
//                    self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
//                }
            }
            showAlert(APP_TITLE, message: "Please confirm your date of birth", withAction: confirmAction, with: true, andTitle: "Cancel", onView: self)
        } else {
            self.hitApiForRegistration()
//          self.moveToTerms()
        }
        } else {
            lbl_Alert.text = checkResult.1
            lbl_Alert.flashView()
        }
    }
    
    @IBAction func buttonClicked_TermsConditions(_ sender:UIButton) {
//        if let termsAndConditionVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.TermsAndConditionVC) as? TermsAndConditionVC {
//            termsAndConditionVC.signUpVC = self
//            termsAndConditionVC.termsAndConditions = self.termsAndConditions
//            termsAndConditionVC.privacyPolicy = self.privacyPolicy
//            termsAndConditionVC.delegate = self
//            self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
//        }
    }
    
    @IBAction func buttonClicked_AcceptTermsConditions(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func datePickerChanged(_ picker:UIDatePicker) {
        tf_Dob.text = getDateStringFromDate(picker.date)
        dobSelected = getDateFromDate(picker.date)
    }
    
    //MARK: ----------- PRIVATE METHODS ------------
    
    private func hitApiForRegistration() {
        
        let checkResult = checkAllTheFields()
        if checkResult.0 == true {
//        if isPriviceyDone == true {
          var params =   ["key": Api.key,
                                      ApiConstant.userFirstname: tf_FirstName.text ?? "",
                                      ApiConstant.userLastname: tf_LastName.text ?? "",
                                      ApiConstant.userEmail:tf_Email.text ?? "",
                                      ApiConstant.userPassword: tf_Password.text ?? "",
                                      ApiConstant.userGender: tf_Gender.text ?? "",
                                      ApiConstant.userDob:dobSelected ?? "",
                                      ApiConstant.sportTyppe: "1",
                                      ApiConstant.deviceType:"iOS", //iOS
                                      ApiConstant.standard : "1" ]
                
                      
                    
          //            print(params)
                      if social_User == nil {
                          params[ApiConstant.fBId] = ""
                      } else {
                          params[ApiConstant.fBId ] = social_User?.id ?? ""
                          
                      }
          print(params)
          self.moveToTerms(param: params)
            
//        }else{
//            if let termsAndConditionVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.TermsAndConditionVC) as? TermsAndConditionVC {
//                termsAndConditionVC.signUpVC = self
//                termsAndConditionVC.termsAndConditions = self.termsAndConditions
//                termsAndConditionVC.privacyPolicy = self.privacyPolicy
//                termsAndConditionVC.delegate = self
//                self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
//            }
//            }
        } else {
                        lbl_Alert.text = checkResult.1
                        lbl_Alert.flashView()
                       }
    }
    
    
    
    //MARK: ----------- UITextField Delegate METHODS ------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tf_Gender {
            picker.tag = 1000
            if isBlankField(tf_Gender) {
                 textField.text = genders[0]
            }
        } else if textField == tf_Dob {
            if isBlankField(tf_Dob) {
                textField.text = getDateStringFromDate(date_Picker.maximumDate!)
                dobSelected = getDateFromDate(date_Picker.maximumDate!)
                isDateConfirmed = true
            }
        }/* else if textField == tf_ChooseSport {
            picker.tag = 1001
            if let allSports = self.sports {
                if isBlankField(tf_ChooseSport) && allSports.count > 0 {
                    let mySport = allSports[0]
                    selectedSport = mySport
                    textField.text = mySport.name
                    return
                }
            } else {
                showAlert("No Sports found!", message: "", onView: self)
                textField.resignFirstResponder()
            }
        }*/
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tf_Gender ||  textField == tf_Dob || textField == tf_ChooseSport {
            return false
        }
        return true
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var returnValue = false
        
        switch textField {
        case tf_FirstName:
            tf_LastName.becomeFirstResponder()
        case tf_LastName:
            tf_Email.becomeFirstResponder()
        case tf_Email:
            tf_Password.becomeFirstResponder()
        case tf_Password:
            tf_ConfirmPassword.becomeFirstResponder()
        default:
            returnValue = true
            textField.resignFirstResponder()
        }
        return returnValue
    }
    
    private func goToCompletePage() {
        
        if let completeRegisterVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CompleteRegisterVC) {
            self.navigationController?.pushViewController(completeRegisterVC, animated: true)
        }
    }
    
    private func goToFeedView() {
        if let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier:StoryboardID.TabBarVC) {
            self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
}

extension SignUpVC {
    /// fetch information of all the sports available
    fileprivate func getAllTheSports() {
        
        let params = ["key": Api.key]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getSport, andParameter: params) { (response, error) in
            hideHud()
            DispatchQueue.main.async {
                self.activity_Indicator.stopAnimating()
                self.tf_ChooseSport.isUserInteractionEnabled = true
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        if let sportsArray = result["sport"] as? [[String:Any]] {
                            let sportsModels = sportsArray.map({ (sportInfo) -> Sport in
                               return Sport(name: "\(String(describing: sportInfo["Name"] ?? ""))", id: "\(String(describing: sportInfo["id"] ?? ""))")
                            })
                           self.sports = sportsModels
                        self.picker.reloadAllComponents()
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
               showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension SignUpVC {
    
    func checkAllTheFields() -> (Bool,String) {
        if isBlankField(tf_Email) || isBlankField(tf_FirstName) || isBlankField(tf_LastName) || isBlankField(tf_Dob) || isBlankField(tf_Gender) { //|| isBlankField(tf_ChooseSport)
            return (false,FILL_ALL_FIELDS)
        }
        
        if social_User == nil {
            if isBlankField(tf_Password) || isBlankField(tf_ConfirmPassword) {
                return (false,FILL_ALL_FIELDS)
            }
            
            if tf_Password.text != tf_ConfirmPassword.text {
                return (false,"*Passwords not matched")
            }
        }
        
        if !isValidEmail(tf_Email.text ?? "") {
            return (false,INVALID_EMAIL)
        }
        
        if (tf_FirstName.text ?? "").count < 3 {
            return (false,"*Please enter at least 3 characters for first name")
        }
        
        if (tf_LastName.text ?? "").count < 2 {
            return (false,"*Please enter at least 2 characters for last name")
        }
        
        if social_User == nil {
            if (tf_Password.text ?? "").count < 6 {
                return (false,"*Please enter at least 6 characters for password")
            }
        }
        
//        if termsAndConditions == false {
//            return (false,ACCEPT_TERM)
//        }
//
//        if privacyPolicy == false {
//            return (false,ACCEPT_PRIVACY)
//        }
        
//        if cookiePolicy == false {
//            return (false,ACCEPT_COOKIE) // Nripendra 11 july 
//        }
        
//        if btn_AcceptTerms.isSelected == false {
//            return (false,ACCEPT_TERM)
//        }
        
        return (true,"")
    }
}

extension SignUpVC:UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1000 {
            return genders.count
        } else {
            return self.sports?.count ?? 0
        }
    }
}

extension SignUpVC:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1000 {
            let title = genders[row]
            return title
        } else {
            if let allSports = self.sports {
                let mySport = allSports[row]
                return  mySport.name
            }
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1000 {
            let title = genders[row]
            tf_Gender.text = title
        } else {
            if let allSports = self.sports {
                let mySport = allSports[row]
                selectedSport = mySport
                tf_ChooseSport.text =   mySport.name
                return
            }
            tf_ChooseSport.text =  ""
        }
    }
}

struct Sport {
    let name:String
    let id:String
}

extension SignUpVC : TermsAndConditions{
    func checkTerms(isChecked: Bool) {
        if isChecked {
            isPriviceyDone = true
        }
    }
    
    
}
