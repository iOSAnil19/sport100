//
//  PlayersVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 18/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class PlayersVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    var contacts = [Player]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Players"
        table_View.tableFooterView = UIView.init(frame: .zero)
        self.setCancelButton()
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PlayersVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "ContactsCell"
        var cell = tableView.dequeueReusableCell(withIdentifier:cellId)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier:cellId)
        }
        let user = self.contacts[indexPath.row]
        if let label = cell?.contentView.viewWithTag(100) as? UILabel {
            label.text = "\(user.firstName ?? "") \(user.lastName ?? "")"
        }
        if let img = cell?.contentView.viewWithTag(101) as? UIImageView {
            let imgUrlString = imageUrl + "\(String(describing: user.image ?? ""))"
            img.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        }
        
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension PlayersVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let player = contacts[indexPath.row]
        
        if let profileVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
            profileVC.otherUserId = player.iD ?? ""
            profileVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
}
