//
//  ViewController.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//
import UIKit
import Fabric
import Crashlytics

class LoginVC: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    @IBOutlet weak var tf_Email:UITextField!
    @IBOutlet weak var tf_Password:UITextField!
    @IBOutlet weak var lbl_Alert:UILabel!
    @IBOutlet weak var stack_Container:UIStackView!
    
    let socialManager = SocialManager()

    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set view background color and navigation bar color
        setBackViewColor()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
        //self.tf_Email.text = "nrip@yopmail.com"
        //self.tf_Password.text = "123456"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
//        if let emailExist = UserDefaults.standard.value(forKey: CACHED_MAIL) as? String {
//            tf_Email.text = emailExist
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }
    
    // Login in button pressed
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        // Keyboard will hide on login button tap
        self.view.endEditing(true)
        
        //goToFeedView()
        //return
        
        // Check all fields are valid are not
        if allFieldsValid() {
            // users/signin
            //"Logging in..."
            let deviceToken  = Utility.getTempDeviceID()
            let parameters = ["key": Api.key,
                              ApiConstant.userEmail: tf_Email.text ?? "",
                              ApiConstant.userPassword: tf_Password.text ?? "",
                              ApiConstant.userToken:APPDELEGATE.appDeviceToken,
                              "deviceType":"iOS"]
            self.sendRequestToServer(parameters, forRequest: Api.signin)
        } else {
            // Show alert if fields are not valid
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
            
            lbl_Alert.text = FILL_ALL_FIELDS
            
            // Check blank email and password
            if (isBlankField(tf_Email)) || (isBlankField(tf_Password)) {
                stack_Container.jitter()
                lbl_Alert.flashView()
                return
            }
            // Check valid email
            if !isValidEmail(tf_Email.text ?? "") {
                lbl_Alert.text = INVALID_EMAIL
                stack_Container.jitter()
                lbl_Alert.flashView()
                return
            }
        }
    }
    
    // Login with facebook button tapped
    @IBAction func buttonClicked_Facebook(_ sender: Any) {
        
        socialManager.getFaceboookLoginInfoForView(self) { (response, error) in
            self.handleReponseAndError(response,error)
        }
    }
    
    // Sign Up button tapped
    @IBAction func buttonClicked_SignUp(_ sender:UIButton) {
        self.goToSignUpView(nil)
    }
    
    // If user forgot password show option to enter email and reset pwd
    @IBAction func buttonClicked_ForgotPassword(_ sender:UIButton) {
        self.view.endEditing(true)
        self.showAlertToResetPassword()
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tf_Email {
            tf_Password.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: ---------- Private Methods --------------
    
    private func allFieldsValid() -> Bool {
        
        if isBlankField(tf_Email) || isBlankField(tf_Password) || !isValidEmail(tf_Email.text ?? ""){
            return false
        }
        return true
    }
    
    // Handle the response after Facebook and Google login
    private func handleReponseAndError(_ result:Any?, _ error:Error?) {
        
        if error == nil {
            if let user = result as? SocialUser {
                self.loginWithSocialUser(user)
                
            } else {
                showAlert("", message: "\(String(describing: result ?? SOMETHING_WRONG))".capitalized, onView: self)
            }
        } else {
            if let code = error?.code, code == 1 {
                showAlert("", message:"The user canceled the sign-in flow", onView: self)
            }
            showAlert("", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
        }
    }
    
    
    private func showAlertToResetPassword() {
        let alertController = UIAlertController(title: "Forgot Password", message: "Please enter your registered email ID. We will send you an email to reset your password.", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .destructive, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            if isBlankField(firstTextField) {
                return
            }
            self.sendResetPasswordRequest(firstTextField.text ?? "")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Email Id"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func loginWithSocialUser(_ user:SocialUser) {
        
        showHud("Processing..")
        
        let parameters = ["key":Api.key,"FBID": user.id ?? ""]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.facebookLogin, andParameter: parameters) { (response, error) in
            hideHud()
            
            if error != nil {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            } else {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    print(result)
                    if success == 200 {
                        let registerstatus = "\(String(describing: result["registerstatus"] ?? "0"))"
                        if registerstatus == "register" {
                            self.goToSignUpView(user)
                        } else if registerstatus == "account deactivated" {
                            showAlert(ERROR, message:"Your account has been suspended. Please contact to Sports100.", onView: self)
                        } else {
                            self.handleSuccessResultForRequest(result, Api.facebookLogin)
                        }
                    } else {
                        showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            }
        }
    }
    
    private func sendResetPasswordRequest(_ emailId:String) {
        
         //Sending Reset Request...
        
       let parameters = ["key": Api.key, "email": emailId]
        self.sendRequestToServer(parameters, forRequest: Api.forgotPassword)
    }
    
    private func sendRequestToServer(_ parameters:[String:Any], forRequest request:String) {
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(request, andParameter: parameters) { (response, error) in
            hideHud()
            print(response ?? "No response")
            DispatchQueue.main.async {
                self.tf_Password.text = ""
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        self.handleSuccessResultForRequest(result, request)
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    private func handleSuccessResultForRequest(_ result:[String:Any], _ request:String) {
        if request == Api.forgotPassword {
            showAlert("", message: forgotPwdMessage, onView: self)
        } else {
            
            if let userDetail = result["userdetails"] as? [String:Any] {
                UserInfo.sharedInfo.setUserProfileWithInfo(userDetail)
            }
            
            let first = Int("\(String(describing: result["profile"] ?? "0"))")
            if (first == 0) {
                self.goToEditProfile()
//                self.goToFeedView()

            } else {
                self.goToFeedView()
            }
        }
    }
    
    //MARK: ---------- Go To Next View Controllers Methods ------------
    
    private func goToSignUpView(_ user:SocialUser?) {
        if let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.SignUpVC) as? SignUpVC {
            signUpVC.social_User = user
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    
    private func goToFeedView() {
        if let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier:StoryboardID.TabBarVC) {
            APPDELEGATE.window?.rootViewController = tabBarVC
           // self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
    
    private func goToEditProfile() {
        
        if let addMoreInfoVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.AddMoreInfoVC) as? AddMoreInfoVC {
            addMoreInfoVC.fromPage = "LoginVC"
            self.navigationController?.pushViewController(addMoreInfoVC, animated: true)
        }
    }

}
