//
//  TeamDetailViewController.swift
//  Sports100
//
//  Created by Nripendra Hudda on 10/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol TeamDetailDelegate {
    func matchAdded()
    func teamEdited(_ team:Team)
}

class TeamDetailViewController: UIViewController {
    
    @IBOutlet weak var table_View: UITableView!

    var team: Team?
    var teamId: String?
    var delegate: TeamDetailDelegate?
    var teamSport: TeamSport?
    var isBool : Bool?
    
    var fromController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarImage()
        
        table_View.estimatedRowHeight = 64;
        table_View.rowHeight = UITableViewAutomaticDimension
        navigationItem.largeTitleDisplayMode = .never
        if let teamExist = team {
            if teamExist.member == "Yes" && fromController == nil {
                self.makeRightEditButton()
                
            }
        } else {
            self.getTeamById()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func getTeamById() {
        
            showHud("Processing..")
            let params = ["key":Api.key,
                          "teamID":self.teamId ?? "",
                          "userID": userInfo?.userId ?? ""] as [String:Any]
        
        let networkManager = NetworkManager()
        
        networkManager.getDataForRequest(Api.getTeam, andParameter: params) { (response, error) in
            hideHud()
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        print(JSON(result))
                        
                        if let teamInfo = result["response"] as? [String:Any] {
                            let editedTeam = Team.init(object: teamInfo)
                            if editedTeam.member == "Yes" && self.fromController != nil  {
                                self.makeRightEditButton()
                                
                            }
                            self.team = editedTeam
                            self.table_View.reloadData()
                        }
                    } else {
                        hideHud()
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    hideHud()
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                hideHud()
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    private func makeRightEditButton() {
        let barButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(editButtonTapped))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func editButtonTapped() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CreateNewTeam) as? CreateNewTeamVC {
            vc.team = self.team
            vc.teamSport = self.teamSport
            vc.isBool  = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension TeamDetailViewController: AddTeamDelegate {
    func teamAdded() {}
    
    func teamEdited(_ team: Team) {
        team.member = "Yes"
        self.team = team
        self.delegate?.teamEdited(team)
        self.table_View.reloadData()
        let indexPath = IndexPath.init(row: 0, section: 0)
        self.table_View.scrollToRow(at: indexPath, at: .top, animated: false)
    }
}

extension TeamDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300 * getScaleFactor()
        }
        return UITableViewAutomaticDimension
    }
}

extension TeamDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let teamExist = team {
            if teamExist.member == "Yes" {
                if let traingninTime = teamExist.traingTime, traingninTime.count > 0 {
                    return 5
                }
                return 4
            }
            else {
                if let traingninTime = teamExist.traingTime, traingninTime.count > 0 {
                    return 4
                }
                return 4
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if team?.gameDay?.count == 0{
                return 0
            }else{
                return 1
            }
        }else if section == 2 {
            if team?.traingTime?.count == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingDayCell") as? TrainingDayCell
                cell?.isHidden = true
//                return 0
            }else{
                return 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let selectedTeam = self.team else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileCell") as? TeamProfileCell
        
            cell?.setAllValues(selectedTeam, teamSport)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GameDayCell") as? GameDayCell
                   cell?.setUpGameDayCell(selectedTeam)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
            
        } else if indexPath.section == 2 {
            if let traingninTime = selectedTeam.traingTime, traingninTime.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingDayCell") as? TrainingDayCell
                    cell?.setUpTraingTimeCell(selectedTeam)
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamBioCell") as? TeamBioCell
                cell?.setAllValues(selectedTeam)
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
            }
            
        } else if indexPath.section == 3 {
            if let traingninTime = selectedTeam.traingTime, traingninTime.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamBioCell") as? TeamBioCell
                cell?.setAllValues(selectedTeam)
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamMessageCell") as? TeamMessageCell
                if selectedTeam.member == "Yes" {
                    cell?.btnLeave.isHidden = false
                    cell?.btnMessage.isHidden = false
                    cell?.btnRequest.isHidden = true
                } else {
                    cell?.btnLeave.isHidden = true
                    cell?.btnMessage.isHidden = false
                    cell?.btnRequest.isHidden = false
                }
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
            }
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamMessageCell") as? TeamMessageCell
            if selectedTeam.member == "Yes" {
                cell?.btnLeave.isHidden = false
                cell?.btnMessage.isHidden = false
                cell?.btnRequest.isHidden = true
            } else {
                cell?.btnLeave.isHidden = true
                cell?.btnMessage.isHidden = false
                cell?.btnRequest.isHidden = false
            }
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
    
    
}
