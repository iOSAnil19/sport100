//
//  TeamSportsVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 06/07/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class TeamSportsVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    @IBOutlet weak var barItem_Add:UIBarButtonItem!
    
    let refreshControl = UIRefreshControl()
    
    var requestUrlString = ""
    
    var pageNo = 1
    var totalPageCount = 1
    var sportTeams = [TeamSport]() {
        didSet {
            allTeams.removeAll()
            for sportTeam in sportTeams {
                if let teamsExist = sportTeam.teams, teamsExist.count > 0 {
                    allTeams = allTeams + teamsExist
                }
            }
        }
    }
    
    var delegate:TeamsDelegate?
    var selectedTeam:Team?
    
    var allTeams = [Team]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Teams"
        table_View.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(TeamSportsVC.refreshMethod), for: .valueChanged)
        showHud("Processing..")
        
        if self.delegate is ProfileMessageCell {
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.rightBarButtonItems = []
            barItem_Add = nil
            requestUrlString = Api.getAllUserTeam
            //  barItem_Search = nil
            self.setCancelButton()
        } else {
            requestUrlString = Api.getAllTeam
            //setNavigationBarImage()
        }
        self.getGroupedSport()
    }
    
    func getGroupedSport() {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getGoupedSports, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    
                    if success == 200 {
                        
                        if let allGroupedTeams = result["sports"] as? [[String:Any]], allGroupedTeams.count > 0 {
                            let groupedTeams = allGroupedTeams.map({ (sportInfo) -> TeamSport in
                                TeamSport(object: sportInfo)
                            })
                            self.sportTeams.removeAll()
                            self.sportTeams = groupedTeams

                            if self.table_View != nil {
                                self.table_View.reloadData()
                            }
                        }
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func refreshMethod() {
        if self != nil {
            pageNo = 1
            self.getGroupedSport()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView(_ sportId:String) -> (String,UIImage) {
        var text = ""
        var img  = #imageLiteral(resourceName: "netball")
        
        switch sportId {
        case "1":
            text = "Football 5 a side"
            img = #imageLiteral(resourceName: "football5")
            break
        case "2":
            text = "Football 7 a side"
            img = #imageLiteral(resourceName: "football7")
            break
        case "3":
            text = "Football 11 a side"
            img = #imageLiteral(resourceName: "football11")
            break
        case "4":
            text = "Basketball"
            img = #imageLiteral(resourceName: "basketball")
            break
        case "5":
            text = "Netball"
            img = #imageLiteral(resourceName: "netball")
            break
        default:
            break
        }
        return (text, img)
    }
    
    //MARK: ------------- UIBUTTON ACTION METHODS ----------------
    
    @IBAction func buttonClicked_AddTeam(_ sender: UIButton) {
        if let addTeamVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.CreateNewTeam) as? CreateNewTeamVC {
            addTeamVC.delegate = self
            self.navigationController?.pushViewController(addTeamVC, animated: true)
        }
    }
    
    @IBAction func buttonClicked_Search(_ sender: UIButton) {
        if let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "TeamMenuViewController") as? TeamMenuViewController {
            searchVC.hidesBottomBarWhenPushed = true
            searchVC.navigationBarheight = 64.0
            searchVC.allTeams = self.allTeams
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
}

extension TeamSportsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sportTeams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "TeamSportCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId)
        let sport = self.sportTeams[indexPath.row]
        
        if let sportId = sport.iD {
           
            let (text, img) = self.setUpView(sportId)
            
            if let lbl = cell?.contentView.viewWithTag(1000) as? UILabel {
                lbl.text = text
            }
            if let imgView = cell?.contentView.viewWithTag(1001) as? UIImageView {
                imgView.image = img
            }
        }
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension TeamSportsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sport = self.sportTeams[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        
        let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamsVC", storyboardName:"Main") as! TeamsVC
        vc.selectedSport = sport
        vc.updateDelegate = self
        vc.delegate = self.delegate
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc,animated:true)
    }
}

extension TeamSportsVC: AddTeamDelegate {
    func teamEdited(_ team: Team) {
        
    }
    
    func teamAdded() {
        self.refreshMethod()
    }
}

extension TeamSportsVC: TeamUpdateDelegate {
    func matchAdded() {
        self.refreshMethod()
    }
    
    func teamEdited() {
        self.refreshMethod()
    }
    
    func teamRemoved() {
        self.refreshMethod()
    }
}


