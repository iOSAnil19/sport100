//
//  TeamHeader.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 30/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TeamHeader: UIView {
    
    @IBOutlet weak var lblSportName:UILabel!
    @IBOutlet weak var imgSport:UIImageView!

    func setUpView(_ sportId:String) {
        var text = ""
        var img  = #imageLiteral(resourceName: "netball")
        
        switch sportId {
        case "1":
            text = "Football 5 a side"
            img = #imageLiteral(resourceName: "football5")
            break
        case "2":
            text = "Football 7 a side"
            img = #imageLiteral(resourceName: "football7")
            break
        case "3":
            text = "Football 11 a side"
            img = #imageLiteral(resourceName: "football11")
            break
        case "4":
            text = "Basketball"
            img = #imageLiteral(resourceName: "basketball")
            break
        case "5":
            text = "Netball"
            img = #imageLiteral(resourceName: "netball")
            break
        default:
            break
        }
        self.lblSportName.text = text
        self.imgSport.image = img
    }

}
