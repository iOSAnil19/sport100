//
//  TeamsVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol TeamsDelegate {
    func teamSelected(_ team:Team)
}

protocol TeamUpdateDelegate {
    func matchAdded()
    func teamEdited()
    func teamRemoved()
}

class TeamsVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    var requestUrlString = ""
    
    var pageNo = 1
    var totalPageCount = 1
    var sportTeams = [TeamSport]()
    
    var delegate:TeamsDelegate?
    var updateDelegate:TeamUpdateDelegate?
    var selectedTeam:Team?
    var selectedSport: TeamSport?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = selectedSport?.name ?? ""
        
        if let teams = selectedSport?.teams, teams.count == 0 {
            showAlert(AppTitle, message:"No team found for \(String(describing: self.title))", onView: self)
            return
        }
        
        if self.delegate is ProfileMessageCell {
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.rightBarButtonItems = []

            requestUrlString = Api.getAllUserTeam
            self.setCancelButton()
        } else {
            requestUrlString = Api.getAllTeam

        }
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TeamsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if let teams = selectedSport?.teams, teams.count > 0 {
            return teams.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "TeamCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? TeamCell
        if let teams = selectedSport?.teams, teams.count > 0 {
            let team = teams[indexPath.row]
            cell?.setUPTeamCell(team)
        }
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            if var teams = selectedSport?.teams, teams.count > 0 {
                let team = teams[indexPath.row]
                self.removeTeam(team)
                
                let index = teams.index { (myTeam) -> Bool in
                    myTeam.iD == team.iD
                }
                if index != nil {
                    teams.remove(at: index!)
                    selectedSport?.teams = teams
                    table_View.beginUpdates()
                    table_View.deleteRows(at: [indexPath], with: .fade)
                    table_View.endUpdates()
                }
            }
        }
    }
    
    func removeTeam(_ team:Team) {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.removeTeam, andParameter: params) { (response, error) in
        }
    }
}

extension TeamsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if var teams = selectedSport?.teams, teams.count > 0 {
            let team = teams[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: false)
            
            if delegate is ProfileMessageCell {
                guard let isMember = team.member, isMember != "No" else {
                    showAlert("Sports100", message: "You can not invite someone untill you are a member of the team", onView: self)
                    return
                }
            }
            
            if let sender = self.delegate, sender is UITableViewCell {
                sender.teamSelected(team)
                self.dismiss(animated: true, completion: nil)
                return
            }
            selectedTeam = team
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.team = team
            vc.teamSport = selectedSport
            vc.delegate = self
            vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc,animated:true)
        }
    }
}

extension TeamsVC:TeamDetailDelegate {
    
    func matchAdded() {
        self.table_View.reloadData()
        self.updateDelegate?.matchAdded()
    }
    
    func teamEdited(_ team:Team) {
        
        guard let sportsExist = self.selectedSport else { return }
        
        if var teams = sportsExist.teams, teams.count > 0 {
            let index = teams.index { (myTeam) -> Bool in
                myTeam.iD == team.iD
            }
            if index != nil {
                teams.remove(at: index!)
                teams.insert(team, at: index!)
                self.selectedSport?.teams = teams
                self.table_View.reloadData()
            }
        }
        self.updateDelegate?.teamEdited()
    }
    
    func removeTeam() {
        
        guard let team = selectedTeam else { return }
        
        for (newIndex, sport) in self.sportTeams.enumerated() {
            if var teams = sport.teams, teams.count > 0 {
                let index = teams.index { (myTeam) -> Bool in
                    myTeam.iD == team.iD
                }
                if index != nil {
                    let indexPath = IndexPath.init(row: index!, section: newIndex)
                    teams.remove(at: index!)
                    table_View.beginUpdates()
                    table_View.deleteRows(at: [indexPath], with: .fade)
                    table_View.endUpdates()
                }
            }
        }
        self.updateDelegate?.teamRemoved()
    }
}


