//
//  TeamBioCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 26/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TeamBioCell: UITableViewCell {
    
    @IBOutlet weak var lblTeamBio: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setAllValues(_ team:Team) {
        print(team.profile ?? "dfbjhfb")
        lblTeamBio.text = team.profile ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
