//
//  TeamCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 14/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {
    
    @IBOutlet weak var lbl_TeamName:UILabel!
    @IBOutlet weak var lbl_TeamGround:UILabel!
    @IBOutlet weak var image_Team:UIImageView!
    @IBOutlet weak var image_Sport:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
      
        image_Team.layer.cornerRadius = image_Team.frame.height / 2
        image_Team.clipsToBounds = true
        image_Team.layer.masksToBounds = true
        image_Team.contentMode = .scaleAspectFill
        
    }
    
    func setUPTeamCell(_ team:Team) {
        self.lbl_TeamName.text = team.name ?? ""
        self.lbl_TeamGround.text = team.teamAddress ?? ""
        let imgUrlString = imageUrl + "\(String(describing: team.logo ?? ""))"
        self.image_Team.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        
      /*  if let sport = team.sport {
            if let id = sport.iD {
                switch id {
                case "1":
                    self.image_Sport.image = #imageLiteral(resourceName: "football5")
                    break
                case "2":
                    self.image_Sport.image = #imageLiteral(resourceName: "football7")
                    break
                case "3":
                    self.image_Sport.image = #imageLiteral(resourceName: "football11")
                    break
                case "4":
                    self.image_Sport.image = #imageLiteral(resourceName: "basketball")
                    break
                case "5":
                    self.image_Sport.image = #imageLiteral(resourceName: "netball")
                    break
                default:
                    break
                }
            }
        }*/
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
