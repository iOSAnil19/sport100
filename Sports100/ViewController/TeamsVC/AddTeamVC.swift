//
//  AddTeamVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 11/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import GoogleMaps
//import LocationPickerViewController
import GooglePlaces



class AddTeamVC: UIViewController {
    
    //MARK: ------------- IBOutlets/Variables --------------
    
    @IBOutlet weak var tf_TeamName:UITextField!
    @IBOutlet weak var tf_TeamAddress:UITextField!
    @IBOutlet weak var tf_Website:UITextField!
    @IBOutlet weak var tf_ChooseSport:UITextField!
    @IBOutlet weak var tf_GameTime:UITextField!
    @IBOutlet weak var tf_TrainingTime:UITextField!
    @IBOutlet weak var tv_Bio:UITextView!
    @IBOutlet weak var availabilityView:AvailabilityView!
    @IBOutlet weak var activity_IndicatorSport:UIActivityIndicatorView!
    @IBOutlet weak var lbl_Alert:UILabel!
    @IBOutlet weak var img_User:UIImageView!
    @IBOutlet weak var btn_ImageRemove:UIButton!
    @IBOutlet weak var picker:UIPickerView!
    
    var imageEdited = false
    var selectedImage:UIImage?
    var imageManage:ImageManager?
    var selectedSport:Sport?
    var sports:[Sport]? {
        didSet {
            self.picker.reloadAllComponents()
        }
    }
    
    var street = ""; var city = ""; var postCode = ""; var price = "";
    
    var GameMonAM = false, GameTuesAM = false, GameWednesAM = false, GameThursAM = false, GameFriAM = false, GameSatAM = false, GameSunAM = false
    
    var GameMonPM = false, GameTuesPM = false, GameWednesPM = false, GameThursPM = false, GameFriPM = false, GameSatPM = false, GameSunPM = false
    
    var TrainingMonAM = false, TrainingTuesAM = false, TrainingWednesAM = false, TrainingThursAM = false, TrainingFriAM = false, TrainingSatAM = false, TrainingSunAM = false
    
    var TrainingMonPM = false, TrainingTuesPM = false, TrainingWednesPM = false, TrainingThursPM = false, TrainingFriPM = false, TrainingSatPM = false, TrainingSunPM = false

    
    var team:Team?
    var teamSport:TeamSport?
    
    
    //MARK: ------------- View Life Cycle --------------

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Create New Team"
        tf_ChooseSport.inputView = picker
        activity_IndicatorSport.startAnimating()
        tf_ChooseSport.isUserInteractionEnabled = false
        img_User.layer.cornerRadius = img_User.frame.height / 2
        img_User.clipsToBounds = true
        DispatchQueue.global().async {
            self.getAllTheSports()
        }
        if let sportsExist = Utility.shared.availableSports {
            self.sports = sportsExist
            self.selectedSport = sportsExist.first
            self.activity_IndicatorSport.stopAnimating()
            self.tf_ChooseSport.isUserInteractionEnabled = true
        } else {
            DispatchQueue.global().async {
                self.getAllTheSports()
            }
        }
        
        if let teamExist = self.team {
             self.title = "Edit Team"
            self.setAllValuesWhenEdit(teamExist)
        }
    }
    
    //MARK: ------------- Button Action Methods --------------

    @IBAction func buttonClicked_ChangeImage(_ sender:UIButton) {
        imageManage = ImageManager()
        imageManage?.showImagePicker(self, sender) { image in
            DispatchQueue.main.async {
                if image != nil {
                    self.imageEdited = true
                    self.btn_ImageRemove.isHidden = false
                    self.img_User.image = image
                    self.selectedImage = image
                }
            }
        }
    }
    
    
    @IBAction func buttonClicked_Save(_ sender:UIButton) {
        
        let checkResult = checkAllTheFields()
        if checkResult.0 != true {
           
            showHud("Processing..")
            
            var params = ["street":street,
                          "city":city,
                          "postcode":postCode,
                          "sport":selectedSport?.id ?? "",
                          "name":tf_TeamName.text ?? "",
                          "profile":"\(tv_Bio.text!)",
                          "website":tf_Website.text ?? "",
                          "key": Api.key,
                          "Cost":"1",
                          "teamAddress":tf_TeamAddress.text ?? ""] as [String:Any]
            
            
            params["GameMonAM"] = "\(GameMonAM ?"1":"0")"
            params["GameMonPM"] = "\(GameMonPM ?"1":"0")"
            params["GameTueAM"] = "\(GameTuesAM ?"1":"0")"
            params["GameTuePM"] = "\(GameTuesPM ?"1":"0")"
            params["GameWedAM"] = "\(GameWednesAM ?"1":"0")"
            params["GameWedPM"] = "\(GameWednesPM ?"1":"0")"
            params["GameThuAM"] = "\(GameThursAM ?"1":"0")"
            params["GameThuPM"] = "\(GameThursPM ?"1":"0")"
            params["GameFriAM"] = "\(GameFriAM ?"1":"0")"
            params["GameFriPM"] = "\(GameFriPM ?"1":"0")"
            params["GameSatAM"] = "\(GameSatAM ?"1":"0")"
            params["GameSatPM"] = "\(GameSatPM ?"1":"0")"
            params["GameSunAM"] = "\(GameSunAM ?"1":"0")"
            params["GameSunPM"] = "\(GameSunPM ?"1":"0")"
            
            params["TrainingMonAM"] = "\(TrainingMonAM ?"1":"0")"
            params["TrainingMonPM"] = "\(TrainingMonPM ?"1":"0")"
            params["TrainingTueAM"] = "\(TrainingTuesAM ?"1":"0")"
            params["TrainingTuePM"] = "\(TrainingTuesPM ?"1":"0")"
            params["TrainingWedAM"] = "\(TrainingWednesAM ?"1":"0")"
            params["TrainingWedPM"] = "\(TrainingWednesPM ?"1":"0")"
            params["TrainingThuAM"] = "\(TrainingThursAM ?"1":"0")"
            params["TrainingThuPM"] = "\(TrainingThursPM ?"1":"0")"
            params["TrainingFriAM"] = "\(TrainingFriAM ?"1":"0")"
            params["TrainingFriPM"] = "\(TrainingFriPM ?"1":"0")"
            params["TrainingSatAM"] = "\(TrainingSatAM ?"1":"0")"
            params["TrainingSatPM"] = "\(TrainingSatPM ?"1":"0")"
            params["TrainingSunAM"] = "\(TrainingSunAM ?"1":"0")"
            params["TrainingSunPM"] = "\(TrainingSunPM ?"1":"0")"
            
            params["userID"] = "\(userInfo?.userId ?? "")"
            params["homeGroundName"] = "homeGroundName"

          
            let networkManager = NetworkManager()
            var imageData:Data?
            if let imgExist = selectedImage {
                imageData = UIImageJPEGRepresentation(imgExist, 1)
            }
            
            var request = Api.addTeam
            
            if let teamExist = self.team {
                request = Api.editTeam
                params["TeamId"] = teamExist.iD ?? ""
            }
            
            networkManager.uploadImage(request, onPath:imageData, placeholderImage: nil,ApiConstant.post_photo ,andParameter: params) { (response, error) in
                hideHud()
                if error == nil {
                    if let result = response as? [String:Any] {
                        let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                        if success == 200 {
                            print(result)
                            if self.team != nil {
                                if let teamInfo = result["Team"] as? [String:Any] {
                                    let editedTeam = Team.init(object: teamInfo)
//                                    self.delegate?.teamEdited(editedTeam)
                                }
                              
                            } else {
//                                self.delegate?.teamAdded()
                            }
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            hideHud()
                            let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                            showAlert(ERROR, message: error.capitalized, onView: self)
                        }
                    } else {
                        hideHud()
                        showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                    }
                } else {
                    hideHud()
                    showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
                }
            }
        } else {
            lbl_Alert.text = checkResult.1
            lbl_Alert.flashView()
        }
    }
    
    @IBAction func buttonClicked_GameAvailability(_ sender:UIButton) {
        GameMonAM = false; GameTuesAM = false; GameWednesAM = false; GameThursAM = false; GameFriAM = false; GameSatAM = false; GameSunAM = false;
        
        GameMonPM = false; GameTuesPM = false; GameWednesPM = false; GameThursPM = false; GameFriPM = false; GameSatPM = false; GameSunPM = false;
        tf_GameTime.text = ""
        availabilityView.tag = 1001
        self.showAvailabilityView()
    }
    
    @IBAction func buttonClicked_TrainingAvailability(_ sender:UIButton) {
        TrainingMonAM = false; TrainingTuesAM = false; TrainingWednesAM = false;
        TrainingThursAM = false; TrainingFriAM = false; TrainingSatAM = false;
        TrainingSunAM = false;
        
        TrainingMonPM = false; TrainingTuesPM = false; TrainingWednesPM = false;
        TrainingThursPM = false; TrainingFriPM = false; TrainingSatPM = false; TrainingSunPM = false;
        tf_TrainingTime.text = ""
        availabilityView.tag = 1002
        self.showAvailabilityView()
    }
    
    func showAvailabilityView() {
        availabilityView.showPopUp(self)
        if availabilityView.table_View != nil {
            availabilityView.table_View.reloadData()
        }
        availabilityView.delegate = self
        self.view.endEditing(true)
    }
    
    //MARK: ------------- UITextField Delegate Methods --------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == tf_TeamAddress {
            showLocationPicker(textField)
            textField.resignFirstResponder()
        }
        else if textField == tf_ChooseSport {
            picker.tag = 999
            if let allSports = self.sports {
                if isBlankField(tf_ChooseSport) && allSports.count > 0 {
                    let mySport = allSports[0]
                    selectedSport = mySport
                    textField.text = mySport.name
                }
            } else {
                showAlert("", message: "Choose sport before selecting position", onView: self)
                textField.resignFirstResponder()
            }
        }
    }
    

    //MARK: ------------- Private Methods --------------
    
    func showLocationPicker(_ textField:UITextField) {
        
        let placePickerController = GMSAutocompleteViewController()
             
        
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
        if textField == tf_TeamAddress {
            placePickerController.view.tag = 101
        } else {
            placePickerController.view.tag = 102
        }
         placePickerController.delegate = self
        present(placePickerController, animated: true, completion: nil)
    }
    
    func checkAllTheFields() -> (Bool,String) {
        if isBlankField(tf_TeamName) || isBlankField(tf_TeamAddress) || isBlankField(tf_ChooseSport) || isBlankField(tf_GameTime) {
            return (false,FILL_ALL_FIELDS)
        }
        
        if (tf_TeamName.text ?? "").count < 2  {
            return (false,"*Please enter at least 3 characters for team")
        }
        
        return (true,"")
    }
    
    fileprivate func getAllTheSports() {
        
        let params = ["key": Api.key]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getSport, andParameter: params) { (response, error) in
            hideHud()
            DispatchQueue.main.async {
                self.activity_IndicatorSport.stopAnimating()
                self.tf_ChooseSport.isUserInteractionEnabled = true
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        if let sportsArray = result["sport"] as? [[String:Any]] {
                            let sportsModels = sportsArray.map({ (sportInfo) -> Sport in
                               return Sport(name: "\(String(describing: sportInfo["Name"] ?? ""))", id: "\(String(describing: sportInfo["id"] ?? ""))")
                            })
                            self.sports = sportsModels
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    // Set All values when come for edit team
    func setAllValuesWhenEdit(_ editTeam:Team) {
        tf_ChooseSport.isUserInteractionEnabled = false
        selectedSport = Sport(name: teamSport?.name ?? "", id: teamSport?.iD ?? "")
        self.btn_ImageRemove.isHidden = false
        self.tf_TeamName.text = editTeam.name ?? ""
        self.tf_TeamAddress.text = editTeam.teamAddress ?? ""
        self.tv_Bio.text = editTeam.profile ?? ""
        self.tf_Website.text = editTeam.website ?? ""
        self.tf_ChooseSport.text = teamSport?.name ?? ""
       // self.tf_GameTime.text = editTeam.sport?.name ?? ""
       // self.tf_TrainingTime.text = editTeam.sport?.name ?? ""

        let imgUrlString = imageUrl + "\(String(describing: editTeam.logo ?? ""))"
        self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        
     //   street = editTeam.location?.street ?? ""
     //   city = editTeam.location?.town ?? ""
     //   postCode = editTeam.location?.postCode ?? ""
        self.setUpGameDay()
        self.setUpTrainingDay()
      
      self.tf_TeamName.isUserInteractionEnabled = true
      self.tf_TeamAddress.isUserInteractionEnabled = true
      self.tv_Bio.isUserInteractionEnabled = true
      self.tf_Website.isUserInteractionEnabled = true
      self.tf_ChooseSport.isUserInteractionEnabled = true
    }
    
    
    private func setUpGameDay() {
        availabilityView.tag = 1001
        guard let teamExist = team else { return }
        if let gameDays = teamExist.gameDay, gameDays.count > 0 {
            for gameDay in gameDays {
                if gameDay.day == "Mon" {
                    if gameDay.time == "AM" {
                        GameMonAM = true
                    } else if gameDay.time == "PM" {
                        GameMonPM = true
                    }
                    
                } else if gameDay.day == "Tue" {
                    if gameDay.time == "AM" {
                        GameTuesAM = true
                    } else if gameDay.time == "PM" {
                        GameTuesPM = true
                    }
                } else if gameDay.day == "Wed" {
                    if gameDay.time == "AM" {
                        GameWednesAM = true
                    } else if gameDay.time == "PM" {
                        GameWednesPM = true
                    }
                } else if gameDay.day == "Thu" {
                    if gameDay.time == "AM" {
                        GameThursAM = true
                    } else if gameDay.time == "PM" {
                        GameThursPM = true
                    }
                } else if gameDay.day == "Fri" {
                    if gameDay.time == "AM" {
                        GameFriAM = true
                    } else if gameDay.time == "PM" {
                        GameFriPM = true
                    }
                } else if gameDay.day == "Sat" {
                    if gameDay.time == "AM" {
                        GameSatAM = true
                    } else if gameDay.time == "PM" {
                        GameSatPM = true
                    }
                } else if gameDay.day == "Sun" {
                    if gameDay.time == "AM" {
                        GameSunAM = true
                    } else if gameDay.time == "PM" {
                        GameSunPM = true
                    }
                }
            }
            setAvailabilityField()
        }
    }
    
    
    private func setUpTrainingDay() {
        availabilityView.tag = 1002
        guard let teamExist = team else { return }
        if let traingDays = teamExist.traingTime, traingDays.count > 0 {
            for traingDay in traingDays {
                if traingDay.day == "Mon" {
                    if traingDay.time == "AM" {
                        TrainingMonAM = true
                    } else if traingDay.time == "PM" {
                        TrainingMonPM = true
                    }
                    
                } else if traingDay.day == "Tue" {
                    if traingDay.time == "AM" {
                        TrainingTuesAM = true
                    } else if traingDay.time == "PM" {
                        TrainingTuesPM = true
                    }
                } else if traingDay.day == "Wed" {
                    if traingDay.time == "AM" {
                        TrainingWednesAM = true
                    } else if traingDay.time == "PM" {
                        TrainingWednesPM = true
                    }
                } else if traingDay.day == "Thu" {
                    if traingDay.time == "AM" {
                        TrainingThursAM = true
                    } else if traingDay.time == "PM" {
                        TrainingThursPM = true
                    }
                } else if traingDay.day == "Fri" {
                    if traingDay.time == "AM" {
                        TrainingFriAM = true
                    } else if traingDay.time == "PM" {
                        TrainingFriPM = true
                    }
                } else if traingDay.day == "Sat" {
                    if traingDay.time == "AM" {
                        TrainingSatAM = true
                    } else if traingDay.time == "PM" {
                        TrainingSatPM = true
                    }
                } else if traingDay.day == "Sun" {
                    if traingDay.time == "AM" {
                        TrainingSunAM = true
                    } else if traingDay.time == "PM" {
                        TrainingSunPM = true
                    }
                }
            }
            setAvailabilityField()
        }
    }
    
}


extension AddTeamVC:UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sports?.count ?? 0
    }
}

extension AddTeamVC:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let allSports = self.sports {
            let mySport = allSports[row]
            return  mySport.name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if let allSports = self.sports {
            let mySport = allSports[row]
            selectedSport = mySport
            tf_ChooseSport.text =   mySport.name
            return
        }
        tf_ChooseSport.text =  ""
    }
}

extension AddTeamVC: GMSAutocompleteViewControllerDelegate {
//    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
//        street = ""
//        city = ""
//        postCode = ""
//        if let components = place.addressComponents, components.count > 0 {
//            for component in components {
//                if component.type == "postal_code" {
//                    postCode = component.name
//                }
//                if component.type == "locality" {
//                    city = component.name
//                }
//                if component.type == "administrative_area_level_2" && city == "" {
//                    city = component.name
//                }
//            }
//        }
//        tf_TeamAddress.text = place.formattedAddress ?? ""
//        viewController.dismiss(animated: true, completion: nil)
//    }
//
//    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//          self.searchBtn.setTitle( place.formattedAddress, for: .normal)
//          lat = "\(place.coordinate.latitude)"
//          long = "\(place.coordinate.longitude)"
//
//          let latitude : CLLocationDegrees = Double(lat!)!
//                         let longitude : CLLocationDegrees = Double(long!)!
//                         let camera = GMSCameraPosition.camera(withLatitude: latitude  ,
//                                                               longitude: longitude  , zoom: 13 )
//                         mapView.animate(to: camera)
//                         DispatchQueue.main.async { // Setting marker on mapview in main thread.
//                             self.marker.position = CLLocationCoordinate2DMake(latitude, longitude)
//                             self.marker.icon = #imageLiteral(resourceName: "pin")
//                             self.marker.map = self.mapView // Setting marker on Mapview
//                         }
//
          
          dismiss(animated: true, completion: nil)
      }
      
      func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
           print("Error: ", error.localizedDescription)
      }
      
      func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            dismiss(animated: true, completion: nil)
      }
      func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }
      
      func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
      }
}


extension AddTeamVC:AvailabilityViewDelegate {
    func popHides() {
       // setUpAvaialability()
        self.view.endEditing(true)
    }
    
    func setAMAvailability(_ day:String, available:Bool) {
        if availabilityView.tag == 1001 {
            switch day {
            case "Sunday":
                GameSunAM = available
            case "Monday":
                GameMonAM = available
            case "Tuesday":
                GameTuesAM = available
            case "Wednesday":
                GameWednesAM = available
            case "Thursday":
                GameThursAM = available
            case "Friday":
                GameFriAM = available
            case "Saturday":
                GameSatAM = available
            default:
                break
            }
        } else {
            switch day {
            case "Sunday":
                TrainingSunAM = available
            case "Monday":
                TrainingMonAM = available
            case "Tuesday":
                TrainingTuesAM = available
            case "Wednesday":
                TrainingWednesAM = available
            case "Thursday":
                TrainingThursAM = available
            case "Friday":
                TrainingFriAM = available
            case "Saturday":
                TrainingSatAM = available
            default:
                break
            }
        }
        self.setAvailabilityField()
    }
    
    func setPMAvailability(_ day:String, available:Bool) {
        
        if availabilityView.tag == 1001 {
            switch day {
            case "Sunday":
                GameSunPM = available
            case "Monday":
                GameMonPM = available
            case "Tuesday":
                GameTuesPM = available
            case "Wednesday":
                GameWednesPM = available
            case "Thursday":
                GameThursPM = available
            case "Friday":
                GameFriPM = available
            case "Saturday":
                GameSatPM = available
            default:
                break
            }
        } else {
            switch day {
            case "Sunday":
                TrainingSunPM = available
            case "Monday":
                TrainingMonPM = available
            case "Tuesday":
                TrainingTuesPM = available
            case "Wednesday":
                TrainingWednesPM = available
            case "Thursday":
                TrainingThursPM = available
            case "Friday":
                TrainingFriPM = available
            case "Saturday":
                TrainingSatPM = available
            default:
                break
            }
        }
        
        self.setAvailabilityField()
    }
    
    func setAvailabilityField() {
        if availabilityView.tag == 1001 {
            var text = ""
            if GameSunAM || GameSunPM {
                text = "Sun, "
            }
            if GameMonAM || GameMonPM {
                text = text + "Mon, "
            }
            if GameTuesAM || GameTuesPM {
                text = text + "Tue, "
            }
            if GameWednesAM || GameWednesPM {
                text = text + "Wed, "
            }
            if GameThursAM || GameThursPM {
                text = text + "Thu, "
            }
            if GameFriAM || GameFriPM {
                text = text + "Fri, "
            }
            if GameSatAM || GameSatPM {
                text = text + "Sat"
            }
            tf_GameTime.text = text
        } else {
            var text = ""
            if TrainingSunAM || TrainingSunPM {
                text = "Sun, "
            }
            if TrainingMonAM || TrainingMonPM {
                text = text + "Mon, "
            }
            if TrainingTuesAM || TrainingTuesPM {
                text = text + "Tue, "
            }
            if TrainingWednesAM || TrainingWednesPM {
                text = text + "Wed, "
            }
            if TrainingThursAM || TrainingThursPM {
                text = text + "Thu, "
            }
            if TrainingFriAM || TrainingFriPM {
                text = text + "Fri, "
            }
            if TrainingSatAM || TrainingSatPM {
                text = text + "Sat"
            }
            tf_TrainingTime.text = text
        }
    }
}

struct League {
    let leagueId:String
    let leagueName:String
}
