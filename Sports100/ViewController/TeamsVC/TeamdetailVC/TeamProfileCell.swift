//
//  TeamProfileCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 26/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TeamProfileCell: UITableViewCell {
    
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblTeamAddress: UILabel!
    @IBOutlet weak var lblTeamGame: UILabel!
    
    @IBOutlet weak var lblTeamPlayers: UILabel!
    @IBOutlet weak var lblTeamMatches: UILabel!

    @IBOutlet weak var imgTeam: UIImageView!
    @IBOutlet weak var imgGame: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setAllValues(_ team:Team, _ sport:TeamSport?) {
        self.lblTeamName.text = team.name ?? ""
        self.lblTeamAddress.text = team.teamAddress ?? ""
        if let mySport = sport {
            self.lblTeamGame.text = mySport.name ?? ""
        } else {
            self.lblTeamGame.text = team.sport?.name ?? ""
        }

        let imgUrlString = imageUrl + "\(team.logo ?? "")"
        self.imgTeam.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.delayPlaceholder, completed: nil)
     
        if let players = team.player, players.count > 0 {
            lblTeamPlayers.text = "  View Players (\(players.count))"
        }
        if let matches = team.matches, matches.count > 0 {
            lblTeamMatches.text = "  Matches (\(matches.count))"
        }
        self.imgTeam.setupImageViewer()
        
        var text = ""
        var img  = #imageLiteral(resourceName: "netball")
        
        var sportId = ""
        if let mySport = sport {
            sportId = mySport.iD ?? ""
        } else {
            sportId = team.sport?.iD ?? ""
        }
        
        switch sportId {
        case "1":
            text = "Football 5 a side"
            img = UIImage.init(named: "football_ic")!
            break
        case "2":
            text = "Football 7 a side"
            img = UIImage.init(named: "football_ic")!
            break
        case "3":
            text = "Football 11 a side"
            img = UIImage.init(named: "football_ic")!
            break
        case "4":
            text = "Basketball"
            img =  UIImage.init(named: "basketball_ic")!
            break
        case "5":
            text = "Netball"
            img = #imageLiteral(resourceName: "netball")
            break
        case "8":
            text = "Hockey"
            img = UIImage.init(named: "hockey_ic")!
            break
                     
        case "9":
            text = "Rugby"
            img = UIImage.init(named: "rugby_ic")!
            break
        case "11":
            text = "Polo"
            img = UIImage.init(named: "polo_ic")!
            break
            
            
        default:
            break
        }
        self.imgGame.image = img
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buttonClickedMatches(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }

        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MatchesVC) as? MatchesVC {
            vc.selectedTeam = parentVC.team
            vc.delegate = self
            let newNavigation = UINavigationController.init(rootViewController: vc)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            parentVC.present(newNavigation, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func buttonClickedPlayers(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }

        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.PlayersVC) as? PlayersVC {
            if let allPlayers = parentVC.team?.player, allPlayers.count > 0 {
                vc.contacts = allPlayers
                let newNavigation = UINavigationController.init(rootViewController: vc)
                newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
                newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
                parentVC.present(newNavigation, animated: true, completion: nil)
            }
        }
    }
}

extension TeamProfileCell: MatchesDelegate {
    
    func matchAddedDelegate(_ matches: [Matches]) {
        
        lblTeamMatches.text = "  Matches (\(matches.count))"
        
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }
        parentVC.team?.matches = matches
        parentVC.delegate?.matchAdded()
    }
}
