//
//  TeamMessageCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 26/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
protocol requestTeam {
    func requestTeamToJoin(team:Team)
}
class TeamMessageCell: UITableViewCell {
    
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnLeave: UIButton!
    var team :Team?
    var delegate : requestTeam?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonClickedMessage(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }

        if let teamMessageVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.TeamMessageVC) as? TeamMessageVC {
            teamMessageVC.team = parentVC.team
            parentVC.navigationController?.pushViewController(teamMessageVC, animated: true)
        }
    }
    
    @IBAction func buttonClickedRequestToJoinTeam(_ sender:UIButton) {
        
        showHud("")
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }
        
        guard let user = parentVC.team?.iD else { return }
        guard let teamid = parentVC.teamId else { return }
        
        let params = ["key": Api.key, "userID":userInfo?.userId ?? "","toID":user, "teamID": teamid ]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.requestTeamMember, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        hideHud()
                        showAlert("", message: "Your request has been sent", onView: parentVC)
                        sender.setTitle("invitation sent", for: .normal)
                    } else {
                        hideHud()
                        showAlert("", message: "Your request has been already sent", onView: parentVC)
                        sender.setTitle("request to join", for: .normal)
                    }
                }
                
            }else{
                hideHud()
                showAlert("", message: "\(error?.localizedDescription ?? "")", onView: parentVC)
            }
        }
    }
    
    
    @IBAction func buttonClickedLeaveTeam(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? TeamDetailViewController else { return }
        
        let leaveAction = UIAlertAction.init(title: "Leave", style: .destructive) { (action) in
            self.leaveTeam(parentVC)
        }
        
        showAlert("Leave Team", message: "Would you like to leave the team?", withAction: leaveAction, with: true, andTitle: "Cancel", onView: parentVC)
    }
    
    
    func leaveTeam(_ vc:TeamDetailViewController) {
        
        showHud("Processing..")
        
        let networkManager = NetworkManager()
        
        let params = ["key":Api.key,
                      "teamID":vc.team?.iD ?? "", "userID":userInfo?.userId ?? ""]
        networkManager.getDataForRequest(Api.leaveTeam, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                hideHud()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    
                    if success == 200 {
                        //self.delegate?.removeTeam()
                        vc.navigationController?.popViewController(animated: true)
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: vc)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: vc)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: vc)
            }
        }
    }
    
    func requestTojoinTeam(){
        
    }
    

}

