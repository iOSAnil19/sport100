//
//  GameDayCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 26/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class GameDayCell: UITableViewCell {
    
    @IBOutlet weak var btn_SunAM:UIButton!
    @IBOutlet weak var btn_MonAM:UIButton!
    @IBOutlet weak var btn_TueAM:UIButton!
    @IBOutlet weak var btn_WedAM:UIButton!
    @IBOutlet weak var btn_ThuAM:UIButton!
    @IBOutlet weak var btn_FriAM:UIButton!
    @IBOutlet weak var btn_SatAM:UIButton!
    
    @IBOutlet weak var btn_SunPM:UIButton!
    @IBOutlet weak var btn_MonPM:UIButton!
    @IBOutlet weak var btn_TuePM:UIButton!
    @IBOutlet weak var btn_WedPM:UIButton!
    @IBOutlet weak var btn_ThuPM:UIButton!
    @IBOutlet weak var btn_FriPM:UIButton!
    @IBOutlet weak var btn_SatPM:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setUpGameDayCell(_ team:Team) {
        btn_SunAM.isSelected = false
        btn_MonAM.isSelected = false
        btn_TueAM.isSelected = false
        btn_WedAM.isSelected = false
        btn_ThuAM.isSelected = false
        btn_FriAM.isSelected = false
        btn_SatAM.isSelected = false
        
        btn_SunPM.isSelected = false
        btn_MonPM.isSelected = false
        btn_TuePM.isSelected = false
        btn_WedPM.isSelected = false
        btn_ThuPM.isSelected = false
        btn_FriPM.isSelected = false
        btn_SatPM.isSelected = false
        
        guard let gameDays = team.gameDay else { return }
        
        if gameDays.count > 0 {
            for gameDay in gameDays {
                
                if gameDay.day == "Mon" {
                    if gameDay.time == "AM" {
                        btn_MonAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_MonPM.isSelected = true
                    }
                    
                } else if gameDay.day == "Tue" {
                    if gameDay.time == "AM" {
                        btn_TueAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_TuePM.isSelected = true
                    }
                } else if gameDay.day == "Wed" {
                    if gameDay.time == "AM" {
                        btn_WedAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_WedPM.isSelected = true
                    }
                } else if gameDay.day == "Thu" {
                    if gameDay.time == "AM" {
                        btn_ThuAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_ThuPM.isSelected = true
                    }
                } else if gameDay.day == "Fri" {
                    if gameDay.time == "AM" {
                        btn_FriAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_FriPM.isSelected = true
                    }
                } else if gameDay.day == "Sat" {
                    if gameDay.time == "AM" {
                        btn_SatAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_SatPM.isSelected = true
                    }
                } else if gameDay.day == "Sun" {
                    if gameDay.time == "AM" {
                        btn_SunAM.isSelected = true
                    } else if gameDay.time == "PM" {
                        btn_SunPM.isSelected = true
                    }
                }
                
            }
        }else{
            
            
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
