//
//  TrainingDayCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 26/04/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class TrainingDayCell: UITableViewCell {
    
    @IBOutlet weak var btn_SunAM:UIButton!
    @IBOutlet weak var btn_MonAM:UIButton!
    @IBOutlet weak var btn_TueAM:UIButton!
    @IBOutlet weak var btn_WedAM:UIButton!
    @IBOutlet weak var btn_ThuAM:UIButton!
    @IBOutlet weak var btn_FriAM:UIButton!
    @IBOutlet weak var btn_SatAM:UIButton!
    
    @IBOutlet weak var btn_SunPM:UIButton!
    @IBOutlet weak var btn_MonPM:UIButton!
    @IBOutlet weak var btn_TuePM:UIButton!
    @IBOutlet weak var btn_WedPM:UIButton!
    @IBOutlet weak var btn_ThuPM:UIButton!
    @IBOutlet weak var btn_FriPM:UIButton!
    @IBOutlet weak var btn_SatPM:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setUpTraingTimeCell(_ team:Team) {
        btn_SunAM.isSelected = false
        btn_MonAM.isSelected = false
        btn_TueAM.isSelected = false
        btn_WedAM.isSelected = false
        btn_ThuAM.isSelected = false
        btn_FriAM.isSelected = false
        btn_SatAM.isSelected = false
        
        btn_SunPM.isSelected = false
        btn_MonPM.isSelected = false
        btn_TuePM.isSelected = false
        btn_WedPM.isSelected = false
        btn_ThuPM.isSelected = false
        btn_FriPM.isSelected = false
        btn_SatPM.isSelected = false
        
        guard let traingTimes = team.traingTime else { return }
        
        if traingTimes.count > 0 {
            for traingTime in traingTimes {
                
                if traingTime.day == "Mon" {
                    if traingTime.time == "AM" {
                        btn_MonAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_MonPM.isSelected = true
                    }
                    
                } else if traingTime.day == "Tue" {
                    if traingTime.time == "AM" {
                        btn_TueAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_TuePM.isSelected = true
                    }
                } else if traingTime.day == "Wed" {
                    if traingTime.time == "AM" {
                        btn_WedAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_WedPM.isSelected = true
                    }
                } else if traingTime.day == "Thu" {
                    if traingTime.time == "AM" {
                        btn_ThuAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_ThuPM.isSelected = true
                    }
                } else if traingTime.day == "Fri" {
                    if traingTime.time == "AM" {
                        btn_FriAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_FriPM.isSelected = true
                    }
                } else if traingTime.day == "Sat" {
                    if traingTime.time == "AM" {
                        btn_SatAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_SatPM.isSelected = true
                    }
                } else if traingTime.day == "Sun" {
                    if traingTime.time == "AM" {
                        btn_SunAM.isSelected = true
                    } else if traingTime.time == "PM" {
                        btn_SunPM.isSelected = true
                    }
                }
                
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
