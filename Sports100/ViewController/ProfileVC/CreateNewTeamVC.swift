//
//  CreateNewTeamVC.swift
//  Sports100
//
//  Created by VeeraJain on 09/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import  SwiftyJSON
import GooglePlaces
import GoogleMaps
import Alamofire
//import LocationPickerViewController
import SVProgressHUD


protocol AddTeamDelegate:class {
    func teamAdded()
    func teamEdited(_ team:Team)
}

class CreateNewTeamVC: UIViewController,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource ,didSelectdData,UITextViewDelegate{
  
    
    func textField(textfield: String, type: Type, id: String) {
        model.id = id
        
        if tfStandard.isFirstResponder || type == Type.standard{
            tfStandard.text = textfield
        }else if tfAge.isFirstResponder || type == Type.age {
            tfAge.text = textfield
        }
    }
  
    @IBOutlet weak var lblTittel: UILabel!
    @IBOutlet var imgGesture: UITapGestureRecognizer!
    @IBOutlet weak var tfWebsite: UITextField!
    @IBOutlet weak var textviewDetails: UITextView!
    @IBOutlet weak var tfStandard: UITextField!
    @IBOutlet weak var tfGender: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfWeekDayTrain: UITextField!
    @IBOutlet weak var tfWeekDayPlay: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfChooseSport: UITextField!
    @IBOutlet weak var tfTeamName: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgCreatTeam: UIImageView!
    var delegate:AddTeamDelegate?
    var arrSport = [ChooseSport]()
    var arrStanderd = [StandardModel]()
    var picker = UIPickerView()
    var isBool : Bool?
    var team : Team?
    var teamSport : TeamSport?
     var selectedSport:Sport?
    let model = StandardModel()
    var chooseSportID : String?
    var day = DayModel()
    var teamTraining = TeamTraningModel()
    var arrDay = [String]()
    var arrTraining = [String]()
    var traningDay = TeamTraningModel()
    var street = ""; var city = ""; var postCode = ""; var price = "";
    var arrGender = ["Gender","Male","Female","Both"]
    var selectedImage:UIImage?
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isToolbarHidden = true
        self.navigationItem.hidesBackButton = true
         btnSave.layer.cornerRadius = btnSave.frame.height / 2
        
         picker.dataSource = self
         picker.delegate = self
         imagePicker.delegate = self
         imgGesture.delegate = self
         tfChooseSport.delegate = self
         tfStandard.delegate = self
         tfAge.delegate =  self
         tfWeekDayTrain.delegate = self
         tfWeekDayPlay.delegate = self
         textviewDetails.delegate  = self
         tfAddress.delegate = self
         
         tfChooseSport.inputView = picker
         tfGender.inputView  = picker
        tfGender.textColor = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        getSport()
        
        tfChooseSport.isUserInteractionEnabled = true
        
        
        tfChooseSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        
        if let teamExist = self.team {
            self.lblTittel.text = "Edit Team"
            self.setAllValuesWhenEdit(teamExist)
        }else {
//          self.enableAll()
             self.lblTittel.text = "Create New Team"
          
        }
    }
    @IBAction func backOnPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        imgCreatTeam.layer.cornerRadius = imgCreatTeam.frame.height / 2
        imgCreatTeam.clipsToBounds = true
        imgCreatTeam.layer.masksToBounds = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        if isBool != true{
        arrDay = day.arrName
        let stringRepresentation = arrDay.joined(separator: ",")
        tfWeekDayPlay.text = stringRepresentation

        arrTraining = traningDay.arrName
        let string = arrTraining.joined(separator: ",")
        tfWeekDayTrain.text = string
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
       
        textviewDetails.text = ""
        textviewDetails.textColor = .black
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView == textviewDetails {
        if textviewDetails.text == "" {
            textviewDetails.text = "Do you play for fun or play to win? Tell us more about your team."
            textviewDetails.textColor = .lightGray
        }
//        else {
//            textviewDetails.text = ""
//            textviewDetails.textColor = .black
//        }
        }
        return true
    }
//    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        if textviewDetails.text == "" {
//            textviewDetails.text = "Play for fun of play to win? Take us more about you team"
//            textviewDetails.textColor = .gray
//        }else {
//            textviewDetails.text = ""
//             textviewDetails.textColor = .black
//        }
//        return true
//    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfStandard {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.isStanderd = true
            vc.delegate = self
            vc.lblHeader = "Standard"
            vc.type = Type.standard
            self.present(vc,animated: true)
            self.view.endEditing(true)
        } else if textField == tfAge {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.isAge = true
            vc.delegate = self
            vc.type = Type.age
            vc.lblHeader = "Age"
            self.present(vc,animated: true)
            self.view.endEditing(true)

        }else if textField == tfWeekDayPlay {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
                vc.lblHeader = "Game Day"
                vc.type = Type.gameDay
            vc.delegate = self
            self.present(vc,animated: true)
            self.view.endEditing(true)
        }else if textField == tfWeekDayTrain {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.type = Type.traningDay
            vc.lblHeader = "Training Day"
            vc.delegate = self
            self.present(vc,animated: true)
            self.view.endEditing(true)

        }else if textField == tfAddress {
            let placePickerController = GMSAutocompleteViewController()
            placePickerController.delegate = self as GMSAutocompleteViewControllerDelegate
            present(placePickerController, animated: true, completion: nil)
        }

    }
  
  // Set All values when come for edit team
      func enableAll() {
          tfChooseSport.isUserInteractionEnabled = true
          self.tfTeamName.isUserInteractionEnabled = true
          self.tfAddress.isUserInteractionEnabled = true
          self.textviewDetails.isUserInteractionEnabled = true
          self.tfWebsite.isUserInteractionEnabled = true
          self.tfChooseSport.isUserInteractionEnabled = true
          
          self.tfStandard.isUserInteractionEnabled = true
          self.tfGender.isUserInteractionEnabled = true
          self.textviewDetails.isUserInteractionEnabled = true
          self.textviewDetails.isUserInteractionEnabled = true
          
          tfGender.isUserInteractionEnabled = true
          
//              self.tfWeekDayTrain.text = string
        
          self.tfAge.isUserInteractionEnabled = true
 
      }

    // Set All values when come for edit team
    func setAllValuesWhenEdit(_ editTeam:Team) {
        tfChooseSport.isUserInteractionEnabled = true
        selectedSport = Sport(name: teamSport?.name ?? "", id: teamSport?.iD ?? "")
//        self.btn_ImageRemove.isHidden = false
        self.tfTeamName.text = editTeam.name ?? ""
        self.tfAddress.text = editTeam.teamAddress ?? ""
        self.textviewDetails.text = editTeam.profile ?? ""
        self.tfWebsite.text = editTeam.website ?? ""
        self.tfChooseSport.text = editTeam.sport?.name ?? ""
        self.chooseSportID = editTeam.sport?.iD
        self.tfStandard.text = editTeam.standard ?? ""
        self.tfGender.text = editTeam.gender ?? ""
        self.textviewDetails.text = editTeam.profile ?? ""
        self.textviewDetails.textColor = .black
        if isBool == true{
        if let gameDay = editTeam.gameDay {
        let day = gameDay.map{$0.day ?? ""}
          
        let string = day.joined(separator: ",")
        self.tfWeekDayPlay.text = string
        }
        tfGender.textColor = .black
        if let trainTime = editTeam.traingTime {
            let day = trainTime.map{$0.day ?? ""}
            
            let string = day.joined(separator: ",")
            self.tfWeekDayTrain.text = string
        }
  }
//         self.tfWeekDayPlay.text = editTeam.sport?.name ?? ""
//         self.tfWeekDayTrain.text = editTeam.sport?.name ?? ""
        self.tfAge.text = editTeam.age
        
        
        let imgUrlString = imageUrl + "\(String(describing: editTeam.logo ?? ""))"
        self.imgCreatTeam.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
        
        //   street = editTeam.location?.street ?? ""
        //   city = editTeam.location?.town ?? ""
        //   postCode = editTeam.location?.postCode ?? ""
//        self.setUpGameDay()
//        self.setUpTrainingDay()
    }
    @IBAction func imageOnclick(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            //      imagePicker.mediaTypes = [kUTTypeImage];
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            //      imagePicker.mediaTypes = [kUTTypeImage];
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.imgCreatTeam
            alert.popoverPresentationController?.sourceRect = self.imgCreatTeam.bounds
            alert.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }


        self.present(alert, animated: true, completion: nil)
     
    }
    func checkAllTheFields() -> (Bool,String) {
        if isBlankField(tfTeamName) || isBlankField(tfAddress) || isBlankField(tfChooseSport)  {
            return (false,FILL_ALL_FIELDS)
        }
        
        if (tfTeamName.text ?? "").count < 2  {
            return (false,"*Please enter at least 3 characters for team")
        }
        
        return (true,"")
    }
    
    @IBAction func saveOnPress(_ sender: UIButton) {
        let  lat = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.latitude))"
        let log = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.longitude))"
        guard isValid() == true else{return}
        if isBool == true {
          
            guard let teamID = team?.iD, let standerdID = team?.standardID ,let chooseID = chooseSportID else{return}
        showHud("Processing")
        let param = [ "key": Api.key,
                          "userID" : "\(userInfo?.userId ?? "")",
                "street" : "",
                "city" : "",
                "postcode" : "",
                "name" : tfTeamName.text!,
                "teamAddress" : tfAddress.text!,
                "profile" : textviewDetails.text!,
                "website" : tfWebsite.text!,
                "sport" : "\(chooseID)",
                "Cost" : "1",
                "TrainingMonAM" :traningDay.monAM == true ? "1" :" 0",
                "TrainingMonPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingTueAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingTuePM" :traningDay.monAM == true ? "1" : "0",
                "TrainingWedAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingWedPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingThuAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingThuPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingFriAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingFriPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSatAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSatPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSunAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSunPM" :traningDay.monAM == true ? "1" : "0",
                "GameMonAM" :day.monAM == true ? "1" : "0",
                "GameMonPM" :day.monPM == true ? "1" : "0",
                "GameTueAM" :day.tusAM == true ? "1" : "0",
                "GameTuePM" :day.tusAM == true ? "1" : "0",
                "GameWedAM" :day.wedAM == true ? "1" : "0",
                "GameWedPM" :day.wedPM == true ? "1" : "0",
                "GameThuAM" :day.thrAM == true ? "1" : "0",
                "GameThuPM" :day.thrPM == true ? "1" : "0",
                "GameFriAM" :day.friAM == true ? "1" : "0",
                "GameFriPM" :day.friPM == true ? "1" : "0",
                "GameSatAM" :day.satAM == true ? "1" : "0",
                "GameSatPM" :day.satPM == true ? "1" : "0",
                "GameSunAM" :day.sunAM == true ? "1" : "0",
                "GameSunPM" :day.sunPM == true ? "1" : "0",
                "TeamId" : "\(teamID)"  ,
                "post_image" :"",
                "ageGroup" : tfAge.text!,
                "teamGender" :tfGender.text!,
                "standard" : "\(standerdID )",
                "latitude" : "",
                "longitude" : ""] as [String : Any]
            print(param)
            
            var imageData:Data?
            
//            if  imgCreatTeam.image != nil {
//                if let img = imgCreatTeam.image {
//                    imageData = UIImageJPEGRepresentation(img, 1)
//                }else {
                    if let imgExist = selectedImage  {
                    imageData = UIImageJPEGRepresentation(imgExist, 1)
                }
//                }
//            }
            
        
                
//            networkManager.uploadImage(Api.editTeam, onPath:imageData, andParameter: param) { (response, error) in
//            ApiLocationManager.shared.apiManeger(api: url.editSport, param, URLEncoding.default){ (response, error) in
            //                                ApiLocationManager.shared.apiManeger(api: url.addTeam, param, URLEncoding.default){ (response, error) in
            ApiLocationManager.shared.uploadImage(url.editSport, onPath: imageData, andParameter: param) { (response, error) in
                
                
                print(response ?? "No response")
                self.handleErrorAndResponse(response, error)
                //                if response != nil {
//                    if let result = response as? [String:Any] {
//                        let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
//                        if success == 200 {
//                            hideHud()
//                              self.navigationController?.popViewController(animated: true)
//                            self.dismiss(animated: true, completion: nil)
//                        }else{
//                            hideHud()
//                        }
//                        self.dismiss(animated: true, completion: nil)
//                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                        hideHud()
//                    }
//                                            self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion: nil)
//                }
                
            }
   
        }else {
                showHud("Processing")
            guard  let standerdID = model.id ,let chooseID = chooseSportID else{return}
            let param = [ "key": Api.key,
                              "userID" : "\(userInfo?.userId ?? "")",
                    "street" : "",
                    "city" : "",
                    "postcode" : "",
                    "name" : tfTeamName.text!,
                    "teamAddress" : tfAddress.text!,
                    "profile" : textviewDetails.text!,
                    "website" : tfWebsite.text!,
                    "sport" : chooseID,
                    "Cost" : "1",
                    
                    "TrainingMonAM" :traningDay.monAM == true ? "1" :" 0",
                    "TrainingMonPM" :traningDay.monAM == true ? "1" : "0",
                 "TrainingTueAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingTuePM" :traningDay.monAM == true ? "1" : "0",
                "TrainingWedAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingWedPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingThuAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingThuPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingFriAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingFriPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSatAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSatPM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSunAM" :traningDay.monAM == true ? "1" : "0",
                "TrainingSunPM" :traningDay.monAM == true ? "1" : "0",
                "GameMonAM" :day.monAM == true ? "1" : "0",
                "GameMonPM" :day.monPM == true ? "1" : "0",
                "GameTueAM" :day.tusAM == true ? "1" : "0",
                "GameTuePM" :day.tusAM == true ? "1" : "0",
                "GameWedAM" :day.wedAM == true ? "1" : "0",
                "GameWedPM" :day.wedPM == true ? "1" : "0",
                "GameThuAM" :day.thrAM == true ? "1" : "0",
                "GameThuPM" :day.thrPM == true ? "1" : "0",
                "GameFriAM" :day.friAM == true ? "1" : "0",
                "GameFriPM" :day.friPM == true ? "1" : "0",
                "GameSatAM" :day.satAM == true ? "1" : "0",
                "GameSatPM" :day.satPM == true ? "1" : "0",
                "GameSunAM" :day.sunAM == true ? "1" : "0",
                "GameSunPM" :day.sunPM == true ? "1" : "0",
                "TrainingDontAM" :traningDay.dontTrain == true ? "1" : "0",
                "TrainingDontPM" :traningDay.dontTrain == true ? "1" : "0",
                    "post_image" :"",
                    "ageGroup" : tfAge.text!,
                    "teamGender" :tfGender.text!,
                    "standard" : standerdID ,
                    "latitude" : "",
                    "longitude" : ""] as [String : Any]
                print(param)
                var imageData:Data?
//            if  imgCreatTeam.image != nil {
//                if let img =  {
//                    imageData = UIImageJPEGRepresentation(img, 1)
//                }else {
                    if let imgExist = selectedImage  {
                        imageData = UIImageJPEGRepresentation(imgExist, 1)
                    }
//                }
//            }
//                networkManager.uploadImage(Api.addTeam, onPath:imageData, andParameter: param) { (response, error) in
            ApiLocationManager.shared.uploadImage(url.addTeam, onPath: imageData, andParameter: param) { (response, error) in
          
//                                ApiLocationManager.shared.apiManeger(api: url.addTeam, param, URLEncoding.default){ (response, error) in
                    print(response ?? "No response")
                    self.handleErrorAndResponse(response, error)
//                    if error == nil {
//                        if let result = response as? [String:Any] {
//                            let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
//                            if success == 200 {
//                                hideHud()
//                                 self.navigationController?.popViewController(animated: true)
//                            }else{
//                               hideHud()
//                            }
//
//                            self.navigationController?.popViewController(animated: true)
//                        }else{
//                           hideHud()
//                        }
//                        self.navigationController?.popViewController(animated: true)
//                    }
                
                }
                
            }
        }
     func handleErrorAndResponse(_ response: Any?, _ error: Error?) {
        hideHud()
        if error == nil {
            if let result = response as? [String:Any] {
                let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                if success == 200 {
                    self.navigationController?.popViewController(animated: true)

                } else {
                    let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                    showAlert(ERROR, message: error.capitalized, onView: self)
                }
            } else {
                showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
            }
        } else {
            showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
        }
    }
    func isValid() -> Bool {
        guard imgCreatTeam != nil  else {
            self.showAlertMessage("Select Image", andWithTitle: "Alert")
            return false}
        guard tfTeamName.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Enter Team Name", andWithTitle: "Alert")
            return false}
        guard tfChooseSport.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Choose  Sport", andWithTitle: "Alert")
            return false}
        guard tfAddress.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Enter  Address", andWithTitle: "Alert")
            return false}
        guard tfWeekDayTrain.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Please complete training day schedule", andWithTitle: "Alert")
            return false}
        guard tfAge.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Enter  Age", andWithTitle: "Alert")
            return false}
        guard tfGender.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0, tfGender.text != arrGender[0] else {
            self.showAlertMessage("Enter Gender", andWithTitle: "Alert")
            return false}
        guard textviewDetails.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 , textviewDetails.text != "Do you play for fun or play to win? Tell us more about your team." else {
            self.showAlertMessage("Enter Bio Details ", andWithTitle: "Alert")
            return false}
        guard tfWeekDayPlay.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 else {
            self.showAlertMessage("Please complete training day schedule", andWithTitle: "Alert")
            return false}
        
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if tfChooseSport.isFirstResponder {
            return arrSport.count
        }
        else {
            return arrGender.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if tfChooseSport.isFirstResponder {
         return arrSport[row].name
        }else if tfGender.isFirstResponder {
            return arrGender[row]
        }
        return nil
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tfChooseSport.isFirstResponder {
            tfChooseSport.text = arrSport[row].name
             chooseSportID = arrSport[row].id
        }
        else if tfGender.isFirstResponder {
            if row == 0 {
                tfGender.textColor = UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
            } else {
                tfGender.textColor = .black
            }
            tfGender.text = arrGender[row]
            
        }
    }
    
    func getSport() {
        let networkManager = NetworkManager()
        let params = ["key":Api.key] as [String : Any]
        networkManager.getDataForRequest(Api.getSport, andParameter: params ){ (res, err) in
            if err == nil {
                let json = JSON(res!)
                let dic = json["Sport"].arrayObject
                
                for data in dic! {
                let model = ChooseSport.init(withDic: data as! [String : Any])
                self.arrSport.append(model)
                }
    }
}
    }

    
    
}
extension CreateNewTeamVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: UIimagePickerDelegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage  else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        
        // Set Selected UIImage to the User Profile UIImageView
        imgCreatTeam.image = selectedImage
        self.selectedImage = selectedImage
        // After that dismiss the Controller
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


extension CreateNewTeamVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        tfAddress.text =  place.formattedAddress!
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension CreateNewTeamVC : DayValue {
    func getDayModel(model: DayModel) {
        day = model
        arrDay = model.arrName
        
        arrDay = day.arrName
        let stringRepresentation = arrDay.joined(separator: ",")
        tfWeekDayPlay.text = stringRepresentation
    }
    func noTrainText(text: String) {
        tfWeekDayTrain.text = text
    }
    
    func getTainnigModel(model: TeamTraningModel) {
        traningDay = model
        arrTraining = model.arrName
        
        arrTraining = traningDay.arrName
        let stringRepresentation = arrTraining.joined(separator: ",")
        tfWeekDayTrain.text = stringRepresentation

        
    }
}
