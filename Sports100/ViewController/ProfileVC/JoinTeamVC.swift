//
//  JoinTeamVC.swift
//  Sports100
//
//  Created by VeeraJain on 15/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class JoinTeamVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,didSelectdData,GetIndexCount {
    func indexCount(index: Int, type: Type, id: String, name: String) {
        if tfTeamSport.isFirstResponder || type == .sports {
            tfTeamSport.text = " < \(index)  Entries >"
            sportsID = id
            
        }
    }
    
   

    
//    func textField(textfield: String, type: Type, id: String) {
//        if tfTeamStandard.isFirstResponder || type == Type.standard{
//            tfTeamStandard.text = textfield
//            standerdID = id
//        }
//    }
 
    

    
    @IBOutlet weak var tfTeamName: UITextField!
    @IBOutlet weak var tfTeamTrainingDay: UITextField!
    @IBOutlet weak var tfTeamGameDay: UITextField!
    @IBOutlet weak var tfTeamLocation: UITextField!
    @IBOutlet weak var tfTeamGender: UITextField!
    @IBOutlet weak var tfTeamAge: UITextField!
    @IBOutlet weak var tfTeamStandard: UITextField!
    @IBOutlet weak var tfTeamSport: UITextField!
    let  lat = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.latitude))"
    let log = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.longitude))"
    var picker = UIPickerView()
    var arrSearchTeam = [JoinTeamModel]()
    var teamTraining = TeamTraningModel()
    var arrGender = ["Select", "Male", "Female","Both"]
    var teamLocation = ["Within 1 mile","Within 5 mile","Within 10 mile","Within 20 mile","National"]
    var standerdID = ""
    var sportsID = ""
    var traningDay = TeamTraningModel()
    var day = DayModel()
    var arrDay = [String]()
    var arrTraining = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

         picker.delegate = self
        tfTeamSport.delegate = self
        tfTeamStandard.delegate = self
        tfTeamAge.delegate = self
        tfTeamLocation.delegate = self
        tfTeamGameDay.delegate = self
        tfTeamTrainingDay.delegate = self
    
        
        
        tfTeamGender.inputView = picker
        tfTeamLocation.inputView = picker

        
        tfTeamName.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamStandard.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamLocation.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamGameDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamTrainingDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        

    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        arrDay = day.arrName
        let stringRepresentation = arrDay.joined(separator: ",")
        tfTeamGameDay.text = stringRepresentation
        
        arrTraining = traningDay.arrName
        let string = arrTraining.joined(separator: ",")
        tfTeamTrainingDay.text = string
    }
    
    @IBAction func backOnPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneOnPress(_ sender: UIButton) {
        let params = ["key":Api.key, "sport_id":sportsID,
                      "name":tfTeamName.text ?? "",
                      "home_location":tfTeamLocation.text ?? "",
                      "team_gender": tfTeamGender.text ?? "",
                      "age_group": tfTeamAge.text ?? "",
                      "gameDay[monam]":day.monAM == true ? "AM" : "",
                      "gameDay[monpm]":day.monPM == true ? "PM" : "",
                      "gameDay[tueam]":day.tusAM == true ? "AM" : "",
                      "gameDay[tuepm]":day.tusAM == true ? "PM" : "",
                      "gameDay[wedam]":day.wedAM == true ? "AM" : "",
                      "gameDay[wedpm]":day.wedPM == true ? "PM" : "",
                      "gameDay[thuam]":day.thrAM == true ? "AM" : "",
                      "gameDay[thupm]":day.thrPM == true ? "PM" : "",
                      "gameDay[friam]":day.friAM == true ? "AM" : "",
                      "gameDay[fripm]":day.friPM == true ? "PM" : "",
                      "gameDay[satam]":day.satAM == true ? "AM" : "",
                      "gameDay[satpm]":day.satPM == true ? "PM" : "",
                      "gameDay[sunam]":day.sunAM == true ? "AM" : "",
                      "gameDay[sunpm]":day.sunPM == true ? "PM" : "",
                      "trainingTime[monam]":teamTraining.monAM == true ? "AM" : "",
                      "trainingTime[monpm]":teamTraining.monPM == true ? "PM" : "",
                      "trainingTime[tueam]":teamTraining.tusAM == true ? "AM" : "",
                      "trainingTime[tuepm]":teamTraining.tusAM == true ? "PM" : "",
                      "trainingTime[wedam]":teamTraining.wedAM == true ? "AM" : "",
                      "trainingTime[wedpm]":teamTraining.wedPM == true ? "PM" : "",
                      "trainingTime[thuam]":teamTraining.thrAM == true ? "AM" : "",
                      "trainingTime[thupm]":teamTraining.thrPM == true ? "PM" : "",
                      "trainingTime[friam]":teamTraining.friAM == true ? "AM" : "",
                      "trainingTime[fripm]":teamTraining.friPM == true ? "PM" : "",
                      "trainingTime[satam]":teamTraining.satAM == true ? "AM" : "",
                      "trainingTime[satpm]":teamTraining.satPM == true ? "PM" : "",
                      "trainingTime[sunam]":teamTraining.sunAM == true ? "AM" : "",
                      "trainingTime[sunpm]":teamTraining.sunPM == true ? "PM" : "",
                      "standard":tfTeamStandard.text ?? "",
                      "latitude":lat,
                      "longitude":log,
                      "distance":""] as [String : Any]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "JooinTeamTableView") as! JooinTeamTableView
        vc.param = params
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with VC1 at the root of the navigation stack.
        
        self.present(navController, animated:true, completion: nil)
//
//        self.navigationController!.present(vc,animated: true)
        
    }
    
    @IBAction func addOnPress(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier:  "CreateNewTeamVC") as! CreateNewTeamVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func refreshOnPress(_ sender: UIButton) {
        
            tfTeamTrainingDay.text = ""
            tfTeamGender.text = ""
            tfTeamAge.text = ""
            tfTeamName.text = ""
            tfTeamSport.text = ""
            tfTeamLocation.text = ""
            tfTeamStandard.text = ""
            tfTeamGameDay.text = ""
       
        
    }
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       if tfTeamGender.isFirstResponder {
            return arrGender.count
       }else{
        return teamLocation.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if tfTeamGender.isFirstResponder {
            tfTeamGender.text = arrGender[row]
    }else {
        tfTeamLocation.text = teamLocation[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       if tfTeamGender.isFirstResponder {
            return arrGender[row]
       }else{
        return teamLocation[row]
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfTeamSport {
            let vc = storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
            vc.delegate = self
            vc.type = Type.sports
            vc.tittle = "Sports"
            self.present(vc,animated: true)
        } else if textField == tfTeamStandard {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.type = Type.standard
            vc.delegate = self
            vc.lblHeader = "Standard"
            self.present(vc,animated: true)
        }else if textField == tfTeamGameDay {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.type = Type.gameDay
            vc.delegate = self
            vc.lblHeader = "Game Day"
            self.present(vc,animated: true)
        }else if textField == tfTeamTrainingDay {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.type = Type.traningDay
            vc.delegate = self
            vc.lblHeader = "Training Day"
            self.present(vc,animated: true)
        }else if textField == tfTeamAge {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.type = Type.age
            vc.delegate = self
            vc.lblHeader = "Age"
            self.present(vc,animated: true)
        }
        
    }
    func textField(textfield: String, type: Type, id: String) {
        if tfTeamAge.isFirstResponder || type == Type.age {
            tfTeamAge.text = textfield
        }else if tfTeamStandard.isFirstResponder || type == Type.standard {
            tfTeamStandard.text = textfield
               standerdID = id
        }else if tfTeamSport.isFirstResponder || type == Type.sports {
            tfTeamSport.text = textfield
        }
    }
    
   

}

extension JoinTeamVC  : DayValue{
    
        func getDayModel(model: DayModel) {
            day = model
            arrDay = model.arrName
        }
    func noTrainText(text: String) {
        tfTeamTrainingDay.text = text
    }
        
        func getTainnigModel(model: TeamTraningModel) {
            traningDay = model
            arrTraining = model.arrName
        }
    }


