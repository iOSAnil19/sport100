//
//  JooingTeamCell.swift
//  Sports100
//
//  Created by VeeraJain on 17/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class JooingTeamCell: UITableViewCell {

    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var lblTeamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgview?.layer.cornerRadius = (imgview?.frame.height)! / 2
        imgview.clipsToBounds = true
        imgview.contentMode = .scaleToFill
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
