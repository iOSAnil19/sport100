//
//  ImagePriviewVC.swift
//  Sports100
//
//  Created by VeeraJain on 22/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePriviewVC: UIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var scroolView: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    var imgUrl : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        scroolView.delegate = self
        
        scroolView.minimumZoomScale = 1.0
        scroolView.maximumZoomScale = 10.0
        scroolView.alwaysBounceVertical = false
        scroolView.alwaysBounceHorizontal = false
        scroolView.showsVerticalScrollIndicator = true
         imgView!.clipsToBounds = false
        imgView?.sd_setImage(with: URL.init(string: imgUrl!), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.continueInBackground, completed: nil)
        imgView!.layer.cornerRadius = 11.0
        imgView.isUserInteractionEnabled = true
        scroolView.isUserInteractionEnabled = true
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 2
    }
    
    @IBAction func doubleTap(_ sender: UITapGestureRecognizer) {
     handleDoubleTapScrollView(recognizer: sender)
    }
    
    @IBAction func doneOnPress(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
  
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return self.imgView
    }

    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scroolView.zoomScale == 1 {
            scroolView.zoom(to: zoomRectForScale(scale: 5.0, center: recognizer.location(in: recognizer.view)), animated: true)
        }
        else {
            scroolView.setZoomScale(1, animated: true)
        }
    }

    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imgView.frame.size.height / scale
        zoomRect.size.width  = imgView.frame.size.width  / scale
        let newCenter = imgView.convert(center, from: scroolView)
        zoomRect.origin.x = newCenter.x
        zoomRect.origin.y = newCenter.y
        return zoomRect
    }
}


