//
//  ProfileInfoCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 21/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AKMediaViewer



class ProfileInfoCell: UITableViewCell,EditBioDelegate {
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lbl_Header_Bio: UILabel!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_Country:UILabel!
    @IBOutlet weak var lbl_BioDescription:UILabel!
    @IBOutlet weak var lbl_NoBio:UILabel!
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var img_Flag:UIImageView!
    
    @IBOutlet weak var btn_Edit:UIButton!
    @IBOutlet weak var btn_Connect:UIButton!
    @IBOutlet weak var btn_Message:UIButton!
    @IBOutlet weak var btn_InviteToTeam:UIButton!
    var userdata : UserData?
    var imgUrlString : String?
    var img : UIImage?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_View.layer.cornerRadius = 50
        img_View.clipsToBounds = true
        img_View.layer.masksToBounds = true
    }
    
    func setAllValues(_ info:UserData?, isOther: Bool) {
        
        if var countryName = info?.nationality, countryName != "" {
            countryName = countryName.replacingOccurrences(of: " ", with: "-")
            img_Flag.image = UIImage.init(named: "\(countryName).png")
        }
        
        self.lbl_Name.text = "\(info?.firstName ?? "") " + "\(info?.lastName ?? "")"
        self.lbl_Country.text = "\(info?.nickName ?? "")"
        if let bio = info?.bio, bio != "" {
            lbl_NoBio.isHidden = true
            lbl_BioDescription.isHidden = false
            lbl_Header_Bio.isHidden = false
            self.lbl_BioDescription.text = "\(bio)"
        }
        else {
//
          if (!isOther) {
            lbl_NoBio.isHidden = false
          }
            
            lbl_Header_Bio.isHidden = false
            lbl_BioDescription.isHidden = true
            self.lbl_BioDescription.text = ""
        }
      
         imgUrlString = imageUrl + "\(info?.userImage ?? "")"
       
        self.img_View.sd_setImage(with: URL.init(string: imgUrlString!), placeholderImage: #imageLiteral(resourceName: "defult_user"), options:.delayPlaceholder, completed: nil)
//        self.img_View.setupImageViewer()
//        mediaViewerManager.startFocusingView(img_View)

    }
   
    
 
    //MARK: ------------- UIBUTTON ACTION METHODS ----------------
    
    @IBAction func buttonClicked_Connect(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        if sender.tag == 101 {
            let params = ["key": Api.key, "toID":parentVC.otherUserId , "userID":userInfo?.userId ?? ""]
            // https://apisports100.co.uk/index.php/friends/deleteContactRequest
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(Api.deleteContactRequest, andParameter: params) { (response, error) in
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        let msg = "\(String(describing: result["message"] ?? ""))"
                        showAlert("", message: msg.capitalized, onView: parentVC)
                        sender.setTitle("Connect", for: .normal)
                        
                    }
                } else {
                    showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                }
            }
        } else if sender.tag == 103 {
            
            guard let parentVC = self.parentViewController as? ProfileVC else { return }
            
            let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction.init(title:"Cancel Request", style: .destructive) { (action) in
                let params = ["key": Api.key, "toID":parentVC.otherUserId , "userID":userInfo?.userId ?? ""]
                
                let networkManager = NetworkManager()
                networkManager.getDataForRequest(Api.cancelReqst, andParameter: params) { (response, error) in
                    if error == nil {
                        if let result = response as? [String:Any] {
                            let msg = "\(String(describing: result["message"] ?? "0"))"
                            let responseCode = Int("\(String(describing: result["responsecode"] ?? "0"))")
                            if responseCode == 200 {
                            showAlert("", message: msg.capitalized, onView: parentVC)
                            sender.setTitle("Connect", for: .normal)
                            parentVC.userModel?.isPending = false
//                            sender.isUserInteractionEnabled = false
                            }
                        }
                    } else {
                        showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                    }
                }
            }
            
            let shareAction = UIAlertAction.init(title:"Resend Request", style: .default) { (action) in
                
                let params = ["key": Api.key, "toId":parentVC.otherUserId , "fromId":userInfo?.userId ?? "","eventId" : ""]
                
                let networkManager = NetworkManager()
                networkManager.getDataForRequest(Api.resendReqst, andParameter: params) { (response, error) in
                    if error == nil {
                        if let result = response as? [String:Any] {
                            let msg = "\(String(describing: result["message"] ?? "0"))"
                            let responseCode = Int("\(String(describing: result["responsecode"] ?? "0"))")
                            if responseCode == 200 {
                                showAlert("", message: msg.capitalized, onView: parentVC)
                                sender.setTitle("Pending", for: .normal)
                                parentVC.userModel?.isPending = true
                                //                            sender.isUserInteractionEnabled = false
                            }
                        }
                    } else {
                        showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                    }
                }
            }
            
            let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (acrion) in
                
            }
            alertController.addAction(deleteAction)
            alertController.addAction(shareAction)
            alertController.addAction(cancelAction)
            if let popoverController = alertController.popoverPresentationController {
                popoverController.sourceView = sender
                popoverController.sourceRect = sender.bounds
            }
            parentVC.present(alertController, animated: true, completion: nil)
        } else {
            let params = ["key": Api.key, "toID":parentVC.otherUserId , "fromID":userInfo?.userId ?? ""]
            
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(Api.sendRequest, andParameter: params) { (response, error) in
                if error == nil {
                    if let result = response as? [String:Any] {
                        let msg = "\(String(describing: result["message"] ?? "0"))"
                        showAlert("", message: msg.capitalized, onView: parentVC)
                        sender.setTitle("Pending", for: .normal)
                        parentVC.userModel?.isPending = true
//                        sender.isUserInteractionEnabled = false
                    }
                } else {
                    showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                }
            }
        }
        
    }
    
    @IBAction func btnImgView(_ sender: UIButton) {
         guard let parentVC = self.parentViewController as? ProfileVC else { return }
        let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: "ImagePriviewVC") as! ImagePriviewVC
        vc.imgUrl = imgUrlString
         parentVC.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func buttonClicked_EditBio(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController else { return }
        
        let nibs = Bundle.main.loadNibNamed("EditBioView", owner: self, options: nil)
        let editBioView = nibs?.last as? EditBioView
        editBioView?.text_View.text = self.lbl_BioDescription.text
        editBioView?.delegate = self
        editBioView?.showPopUp(parentVC)
    }
    
    @IBAction func buttonClicked_Message(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"ChatMessageViewController", storyboardName:"Main") as! ChatMessageViewController
        vc.otherUserId = parentVC.otherUserId
        if let otherUser = parentVC.userModel {
            vc.otherUserName = "\(otherUser.firstName) \(otherUser.lastName)"
        }
        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonClicked_Invite(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MyTeamsVC) as? MyTeamsVC {
            vc.delegate = self
            vc.isBool = true
            vc.userId = userInfo!.userId
            let newNavigation = UINavigationController.init(rootViewController: vc)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            parentVC.present(newNavigation, animated: true, completion: nil)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func bioUpdated(_ text:String) {
        //  self.lbl_BioDescription.text = text
        userInfo?.bio = text
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        parentVC.table_View.reloadSections([0], with: UITableViewRowAnimation.bottom)
        
    }
}


extension ProfileInfoCell:TeamsDelegate {
    
    func teamSelected(_ team:Team) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        guard let user = otherUserInfo else { return }
        guard let isMember = team.member, isMember != "No" else { return }
        
        let params = ["key": Api.key, "userID":userInfo?.userId ?? "","toID":user.userId, "teamID":team.iD ?? ""]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.requestTeamMember, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result["success"] ?? "0"))")
                    if success == 200 {
                        showAlert("", message: "Invitation has been sent successfully", onView: parentVC)
                    } else {
                        showAlert("", message: "Invitation has been already sent", onView: parentVC)
                    }
                }
                
            }
        }
    }
}

//extension ProfileInfoCell {
//
//    func parentViewControllerForMediaViewerManager(_ manager: AKMediaViewerManager) -> UIViewController {
//        return self
//    }
//
//    func mediaViewerManager(_ manager: AKMediaViewerManager, mediaURLForView view: UIView) -> URL {
//        //        let index: Int = view.tag
//        var url =  URL(fileURLWithPath: "")
//        if serviceData != nil
//        {
//            url =  URL(fileURLWithPath: "\(Constants.urls.imagePath)\(serviceData?.image)")
//        }else if serviceAd != nil
//        {
//            url =  URL(fileURLWithPath: "\(Constants.urls.imagePath)\(serviceAd?.service?.image)")
//        }
//        return url
//    }
//
////    func mediaViewerManager(_ manager: AKMediaViewerManager, titleForView view: UIView) -> String {
////        let url = mediaViewerManager(manager, mediaURLForView: view)
////        let fileExtension = url.pathExtension.lowercased()
////        let isVideo = fileExtension == "mp4" || fileExtension == "mov"
////
////        return (isVideo ? "Videos are also supported." : "")
////    }
//}
