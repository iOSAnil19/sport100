//
//  ProfileAchievmentCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 22/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfileAchievmentCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Time:UILabel!
    @IBOutlet weak var lbl_AchiText:UILabel!
    @IBOutlet weak var view_BottomConstraint:NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpAchievmentCell(_ achieve:Achievement) {
        self.lbl_Time.text = geDateString(achieve.aDate, "dd MMM yyyy hh:mm a")
        self.lbl_AchiText.text = achieve.description
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
