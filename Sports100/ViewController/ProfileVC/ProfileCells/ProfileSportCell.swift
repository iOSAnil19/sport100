//
//  ProfileSportCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 21/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfileSportCell: UITableViewCell {
    
    @IBOutlet weak var lbl_SportName:UILabel!
    @IBOutlet weak var lbl_Position:UILabel!
    @IBOutlet weak var img_Sport:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpSportCell(_ info:UserData?) {
        self.lbl_SportName.text = info?.sportType ?? ""
        self.lbl_Position.text = info?.position ?? ""
        if let sportType = info?.sportId {
            img_Sport.isHidden = false

            if sportType == "5" {
                img_Sport.image = #imageLiteral(resourceName: "netball")
            } else if sportType == "4" {
               // img_Sport.image = #imageLiteral(resourceName: "basketball")
                img_Sport.image = UIImage.init(named: "basketball_ic")
            } else if sportType == "1" {
                img_Sport.image = UIImage.init(named: "football_ic")

            } else if sportType == "2" {
                img_Sport.image = UIImage.init(named: "football_ic")

            } else if sportType == "3" {
                img_Sport.image = UIImage.init(named: "football_ic")
            }
            else if sportType == "8" {
                    img_Sport.image = UIImage.init(named: "hockey_ic")!
            }
            else if sportType == "9" {
                img_Sport.image = UIImage.init(named: "rugby_ic")
            }
            else if sportType == "11" {
                img_Sport.image = UIImage.init(named: "polo_ic")
            }
            else{
                img_Sport.isHidden = true
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
