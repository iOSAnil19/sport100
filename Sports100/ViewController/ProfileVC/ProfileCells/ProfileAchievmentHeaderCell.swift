//
//  ProfileAchievmentHeaderCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 24/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfileAchievmentHeaderCell: UITableViewCell {
    
    @IBOutlet weak var btn_Edit:UIButton!
    @IBOutlet weak var btn_SeeAll:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func buttonClicked_AddAchievement(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController else { return }
        
        let nibs = Bundle.main.loadNibNamed("AddAchievementView", owner: self, options: nil)
        let addAchievementView = nibs?.first as? AddAchievementView
        addAchievementView?.delegate = self
        addAchievementView?.showPopUp(parentVC)
    }
    
    @IBAction func buttonClicked_SeeAll(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        if let myFeedsVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MyFeedVC) as? MyFeedVC {
            myFeedsVC.feedType = FEEDTYPE.ACHIEVEMENT
            myFeedsVC.userModel = parentVC.userModel
            myFeedsVC.delegate = parentVC
            myFeedsVC.isBool = true
            parentVC.navigationController?.pushViewController(myFeedsVC, animated: true)
        }
    }
}

extension ProfileAchievmentHeaderCell:EditBioDelegate {
    func bioUpdated(_ text:String) {
        
        let achievement = Achievement.init(id: "0", description: text, aDate:getDateFromDate(Date()))
        if let info = userInfo,info.userAchievements.count > 1 {
            info.userAchievements.removeLast()
        }
        userInfo?.userAchievements.insert(achievement, at: 0)
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        parentVC.table_View.reloadSections([4], with: UITableViewRowAnimation.bottom)

    }
}
