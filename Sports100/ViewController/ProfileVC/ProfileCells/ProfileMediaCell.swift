//
//  ProfilePostCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 02/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AVKit

class ProfileMediaCell: UITableViewCell {
    
    @IBOutlet weak var img_View1:UIImageView!
    @IBOutlet weak var img_View2:UIImageView!
    @IBOutlet weak var img_View3:UIImageView!
    @IBOutlet weak var img_View4:UIImageView!
    
    @IBOutlet weak var btn_1:UIButton!
    @IBOutlet weak var btn_2:UIButton!
    @IBOutlet weak var btn_3:UIButton!
    @IBOutlet weak var btn_4:UIButton!

    
    @IBOutlet weak var stackView:UIStackView!
    @IBOutlet weak var lbl_NoPost:UILabel!
    @IBOutlet weak var btn_SeeAll:UIButton!
    


    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    func setUpPostCell(_ info:UserData?) {
        
        guard let posts = info?.userPosts else { return }
        
        btn_1.isHidden = true
        btn_2.isHidden = true
        btn_3.isHidden = true
        btn_4.isHidden = true

        
        if posts.count > 0 {
            lbl_NoPost.isHidden = true
            stackView.isHidden = false
            btn_SeeAll.isHidden = false

            let post = posts[0]
            
            if post.type == "Video" {
                btn_1.isHidden = false
                let imgUrlString = imageUrl + "\(post.postThumb)"
                print(imgUrlString)
                self.img_View1.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
        
            } else {
                let imgUrlString1 = imageUrl + "\(post.postImage)"
                self.img_View1.sd_setImage(with: URL.init(string: imgUrlString1), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                self.img_View1.setupImageViewer()
            }
            
            img_View2.isHidden = true
            img_View3.isHidden = true
            img_View4.isHidden = true
            
            if posts.count > 1 {
                img_View2.isHidden = false
                let post1 = posts[1]
                if post1.type == "Video" {
                    btn_2.isHidden = false
                    let imgUrlString = imageUrl + "\(post1.postThumb)"
                    print(imgUrlString)
                    self.img_View2.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    //DispatchQueue.main.async {
                      //  self.img_View2.image = getThumbnailImage(forUrl: URL.init(string: imgUrlString)!)
                    //}
                } else {
                    let imgUrlString2 = imageUrl + "\(post1.postImage)"
                    self.img_View2.sd_setImage(with: URL.init(string: imgUrlString2), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    self.img_View2.setupImageViewer()
                }
            }
            
            if posts.count > 2 {
                img_View3.isHidden = false
                let post2 = posts[2]
                if post2.type == "Video" {
                    btn_3.isHidden = false
                    let imgUrlString = imageUrl + "\(post2.postThumb)"
                     self.img_View3.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    //DispatchQueue.main.async {
                     //   self.img_View2.image = getThumbnailImage(forUrl: URL.init(string: imgUrlString)!)
                    //}
                } else {
                    let imgUrlString3 = imageUrl + "\(post2.postImage)"
                    self.img_View3.sd_setImage(with: URL.init(string: imgUrlString3), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    self.img_View3.setupImageViewer()
                }
            }
            
            if posts.count > 3 {
                img_View4.isHidden = false
                let post3 = posts[3]
                if post3.type == "Video" {
                    btn_4.isHidden = false
                    let imgUrlString = imageUrl + "\(post3.postThumb)"
                      self.img_View4.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    //DispatchQueue.main.async {
                     //   self.img_View4.image = getThumbnailImage(forUrl: URL.init(string: imgUrlString)!)
                    //}
                } else {
                    let imgUrlString4 = imageUrl + "\(post3.postImage)"
                    self.img_View4.sd_setImage(with: URL.init(string: imgUrlString4), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.delayPlaceholder, completed: nil)
                    self.img_View4.setupImageViewer()
                }
            }
        } else {
            stackView.isHidden = true
            lbl_NoPost.isHidden = false
            btn_SeeAll.isHidden = true
        }
    }
    
    @IBAction func buttonClicked_bt1(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let posts = parentVC.userModel?.userPosts else { return }
        if posts.count > 0 {
            let post = posts[0]
            if post.type == "Video" {

                let imgUrlString = imageUrl + "\(post.postImage)"
                print(imgUrlString)
                if let url = URL.init(string: imgUrlString) {
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    parentVC.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
        
    }
    
    @IBAction func buttonClicked_bt2(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let posts = parentVC.userModel?.userPosts else { return }
        if posts.count > 1 {
            let post = posts[1]
            if post.type == "Video" {
                
                let imgUrlString = imageUrl + "\(post.postImage)"
                if let url = URL.init(string: imgUrlString) {
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    parentVC.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
    }
    
    @IBAction func buttonClicked_bt3(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let posts = parentVC.userModel?.userPosts else { return }
        if posts.count > 2 {
            let post = posts[2]
            if post.type == "Video" {
                
                let imgUrlString = imageUrl + "\(post.postImage)"
                if let url = URL.init(string: imgUrlString) {
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    parentVC.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
    }
    
    @IBAction func buttonClicked_bt4(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let posts = parentVC.userModel?.userPosts else { return }
        if posts.count > 3 {
            let post = posts[0]
            if post.type == "Video" {
                
                let imgUrlString = imageUrl + "\(post.postImage)"
                if let url = URL.init(string: imgUrlString) {
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    parentVC.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            }
            
        }
    }
    
    @IBAction func buttonClicked_SeeAll(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        if let myFeedsVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MyFeedVC) as? MyFeedVC {
            myFeedsVC.feedType = FEEDTYPE.IMAGE
            myFeedsVC.userModel = parentVC.userModel
            myFeedsVC.delegate = parentVC
            parentVC.navigationController?.pushViewController(myFeedsVC, animated: true)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
