//
//  ProfilePhysicalCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 21/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfilePhysicalCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Height:UILabel!
    @IBOutlet weak var lbl_Weight:UILabel!
    @IBOutlet weak var lbl_Age:UILabel!
    @IBOutlet weak var lbl_Strong:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    func setUpPhysicalCell(_ info:UserData?) {
        self.lbl_Height.text = "\(info?.height ?? "")"
        if let weight = info?.weight, weight != "", weight != "N/A"  {
            self.lbl_Weight.text = "\(weight)KG"
        } else {
            self.lbl_Weight.text = "N/A"
        }
        self.lbl_Age.text = getDOBString(info?.dob ?? "")
        self.lbl_Strong.text = info?.strongest ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
