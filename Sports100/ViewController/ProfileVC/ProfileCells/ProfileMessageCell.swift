//
//  ProfileMessageCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 17/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfileMessageCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buttonClicked_Message(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"ChatMessageViewController", storyboardName:"Main") as! ChatMessageViewController
        vc.otherUserId = parentVC.otherUserId
        if let otherUser = parentVC.userModel {
            vc.otherUserName = "\(otherUser.firstName) \(otherUser.lastName)"
        }
        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonClicked_Invite(_ sender:UIButton) {
        
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        if let vc = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MyTeamsVC) as? MyTeamsVC {
            vc.delegate = self
            let newNavigation = UINavigationController.init(rootViewController: vc)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            parentVC.present(newNavigation, animated: true, completion: nil)
        }
        
    }
}

extension ProfileMessageCell:TeamsDelegate {
    
    func teamSelected(_ team:Team) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        
        guard let user = otherUserInfo else { return }
        guard let isMember = team.member, isMember != "No" else { return }
        
        let params = ["key": Api.key, "userID":userInfo?.userId ?? "","toID":user.userId, "teamID":team.iD ?? ""]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.requestTeamMember, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        showAlert("", message: "Invitation has been sent successfully", onView: parentVC)
                    } else {
                        showAlert("", message: "Invitation has been already sent", onView: parentVC)
                    }
                }
                
            }
        }
    }
}

