//
//  ProfileTeamCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 17/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
protocol SomeProtocol {
    func naviagate()
    func joinOnPress()
    
    
   
}


class ProfileTeamCell: UITableViewCell {
    
    @IBOutlet weak var teamView: UIView!
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var team_Image1:UIImageView!
    @IBOutlet weak var team_Image2:UIImageView!
    @IBOutlet weak var team_Image3:UIImageView!
    @IBOutlet weak var team_Image4: UIImageView!
    
    @IBOutlet weak var team_Name1:UILabel!
    @IBOutlet weak var team_Name2:UILabel!
    @IBOutlet weak var team_Name3:UILabel!
    @IBOutlet weak var team_Name4:UILabel!
    
    @IBOutlet weak var stackView:UIStackView!
    @IBOutlet weak var lbl_NoPost:UILabel!
    @IBOutlet weak var btn_SeeAll:UIButton!
    var delegate :SomeProtocol?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        team_Image1.layer.cornerRadius = team_Image1.frame.height / 2
        team_Image2.layer.cornerRadius = team_Image2.frame.height / 2
        team_Image3.layer.cornerRadius = team_Image3.frame.height / 2
        team_Image3.clipsToBounds = true
        team_Image3.layer.masksToBounds = true
        team_Image3.contentMode = .scaleAspectFill
              team_Image1.layer.masksToBounds = true
              team_Image2.layer.masksToBounds = true
              team_Image4.layer.masksToBounds = true
        team_Image4.layer.cornerRadius = team_Image4.frame.height / 2
        
        team_Image1.clipsToBounds = true
        team_Image2.clipsToBounds = true
        team_Image4.clipsToBounds = true
        team_Image1.contentMode = .scaleAspectFill
        team_Image2.contentMode = .scaleAspectFill
        team_Image4.contentMode = .scaleAspectFill
        
        
        
    }
    
    func setUpPostCell(_ info:UserData?) {
        
        guard let teams = info?.teams else { return }
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTeam1))
        team_Image1.addGestureRecognizer(tap1)
        team_Image1.isUserInteractionEnabled = true
        
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTeam2))
        team_Image2.addGestureRecognizer(tap2)
        team_Image2.isUserInteractionEnabled = true
        
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTeam3))
        team_Image3.addGestureRecognizer(tap3)
        team_Image3.isUserInteractionEnabled = true
        
        
        let tap4 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTeam4))
        team_Image4.addGestureRecognizer(tap4)
        team_Image4.isUserInteractionEnabled = true
        
        if teams.count > 0 {
            lbl_NoPost.isHidden = true
            stackView.isHidden = false
            btn_SeeAll.isHidden = false

            let team = teams[0]

            let imgUrlString1 = imageUrl + "\(team.teamImage)"
            self.team_Image1.sd_setImage(with: URL.init(string: imgUrlString1), placeholderImage: #imageLiteral(resourceName: "ph"), options:.delayPlaceholder, completed: nil)
          //  self.img_View1.setupImageViewer()
            team_Image2.isHidden = true
            team_Image3.isHidden = true

            self.team_Name1.text = team.teamName
            team_Name2.isHidden = true
            team_Name3.isHidden = true
            team_Image4.isHidden = true
            team_Name4.isHidden = true

            if teams.count > 1 {
                team_Image2.isHidden = false
                team_Name2.isHidden = false
                let team1 = teams[1]
                let imgUrlString2 = imageUrl + "\(team1.teamImage)"
                self.team_Image2.sd_setImage(with: URL.init(string: imgUrlString2), placeholderImage: #imageLiteral(resourceName: "ph"), options:.delayPlaceholder, completed: nil)
               // self.img_View2.setupImageViewer()
                self.team_Name2.text = team1.teamName
            }

            if teams.count > 2 {
                team_Image3.isHidden = false
                team_Name3.isHidden = false
                team_Image4.isHidden = true
                team_Name4.isHidden = true
                let team2 = teams[2]
                let imgUrlString3 = imageUrl + "\(team2.teamImage)"
                self.team_Image3.sd_setImage(with: URL.init(string: imgUrlString3), placeholderImage: #imageLiteral(resourceName: "ph"), options:.delayPlaceholder, completed: nil)
                self.team_Name3.text = team2.teamName

             //   self.team_Name3.setupImageViewer()
            }
        } else if teams.count > 3 {
            team_Image3.isHidden = false
            team_Name3.isHidden = false
            let team3 = teams[3]
            let imgUrlString4 = imageUrl + "\(team3.teamImage)"
            self.team_Image4.sd_setImage(with: URL.init(string: imgUrlString4), placeholderImage: #imageLiteral(resourceName: "ph"), options:.delayPlaceholder, completed: nil)
            self.team_Name4.text = team3.teamName
            
            //   self.team_Name3.setupImageViewer()
        }else {
            stackView.isHidden = true
            lbl_NoPost.isHidden = false
            btn_SeeAll.isHidden = true
            teamView.isHidden = true
            team_Name1.isHidden = true
            team_Name2.isHidden = true
            team_Name3.isHidden = true
            team_Name4.isHidden = true
        
        }
        
    }
    @IBAction func createOnPress(_ sender: UIButton) {
        if let del = delegate{
            del.naviagate()
        }
    }
   
  
    @IBAction func tapOnTeam3(_ sender: UITapGestureRecognizer) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let teams = parentVC.userModel?.teams else { return }
        
        if teams.count > 2 {
            let team = teams[2]
            //getTeam
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.teamId = team.teamId
            
            vc.hidesBottomBarWhenPushed = true
            vc.fromController = parentVC
           
            
            // vc.delegate = self
            parentVC.navigationController?.pushViewController(vc,animated:true)
            
        }
    }
    


    @IBAction func buttonClickedSeeAll(_ sender:UIButton) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }

        if let myTeamsVC = parentVC.storyboard?.instantiateViewController(withIdentifier: StoryboardID.MyTeamsVC) as? MyTeamsVC {
           // myTeamsVC.delegate = parentVC
            if parentVC.otherUserId == "" {
                myTeamsVC.userId = userInfo!.userId
            }else{
            myTeamsVC.userId = parentVC.otherUserId
            }
            
            myTeamsVC.hidesBottomBarWhenPushed = true
            parentVC.navigationController?.pushViewController(myTeamsVC, animated: true)
        }
    }

    @IBAction func tapOnTeam1(_ sender: UITapGestureRecognizer) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let teams = parentVC.userModel?.teams else { return }
        if teams.count > 0 {
            let team = teams[0]
            //getTeam
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.teamId = team.teamId
            
            vc.hidesBottomBarWhenPushed = true
            vc.fromController = parentVC
           // vc.delegate = self
            parentVC.navigationController?.pushViewController(vc,animated:true)

        }

    }

    @IBAction func tapOnTeam2(_ sender: UITapGestureRecognizer) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let teams = parentVC.userModel?.teams else { return }

        if teams.count > 1 {
            let team = teams[1]
            //getTeam
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.teamId = team.teamId
            vc.hidesBottomBarWhenPushed = true
            vc.fromController = parentVC

            // vc.delegate = self
            parentVC.navigationController?.pushViewController(vc,animated:true)

        }
    }
    @IBAction func tapOnTeam4(_ sender: UITapGestureRecognizer) {
        guard let parentVC = self.parentViewController as? ProfileVC else { return }
        guard let teams = parentVC.userModel?.teams else { return }
        
        if teams.count > 3 {
            let team = teams[3]
            //getTeam
            let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
            vc.teamId = team.teamId
            vc.hidesBottomBarWhenPushed = true
            vc.fromController = parentVC
            
            // vc.delegate = self
            parentVC.navigationController?.pushViewController(vc,animated:true)
            
        }
    }

    @IBAction func joinOnPress(_ sender: UIButton) {
        if let del = delegate {
            del.joinOnPress()
        }
        
    }
    

}
