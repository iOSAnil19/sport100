//
//  GameDayVC.swift
//  Sports100
//
//  Created by VeeraJain on 11/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
protocol DayValue {
    func getDayModel(model : DayModel)
    func getTainnigModel(model : TeamTraningModel)
    func noTrainText(text : String)
}
protocol SearchAvailability {
    func getAvalability(model : DayModel)
    
}
class GameDayVC: UIViewController {
    
    
    @IBOutlet weak var tableGameDay: UITableView!
    @IBOutlet weak var lblTittel: UILabel!
    var lblHeader = ""
    var index = 0
    var amValue = Int()
    var type : Type?
    var delegate : DayValue?
    var delegate1 : SearchAvailability?
    var arrDays = ["Monday","Tuseday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var string :String?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableGameDay.delegate = self
        tableGameDay.dataSource = self
        
        tableGameDay.tableFooterView = UIView()
        lblTittel.text = lblHeader
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if type == Type.traningDay{
            let indexPath = IndexPath(row: 0 , section: 1)
            let cell = tableGameDay.cellForRow(at: indexPath) as! NoTrainCell
            cell.noTraingView.isHidden = false
            //            cell.checkBtn.isHidden = false
        }else{
            let indexPath = IndexPath(row: 0 , section: 1)
            let cell = tableGameDay.cellForRow(at: indexPath) as! NoTrainCell
            cell.noTraingView.isHidden = true
            //            cell.checkBtn.isHidden = true
        }
    }
    @IBAction func doneOnPress(_ sender: UIButton) {
        
        
        if type == Type.gameDay || type == Type.searchPlayer {
            let model = DayModel()
            
            for index in 0..<arrDays.count {
                let indexPath = IndexPath(row: index , section: 0)
                let cell = tableGameDay.cellForRow(at: indexPath) as! DayCell //tableView(tableGameDay, cellForRowAt: indexPath) as! DayCell
                switch index {
                case 0 : model.monAM = cell.btnAM.isSelected
                model.monPM = cell.btnPM.isSelected
                if model.monAM || model.monPM {
                    model.arrName.append("Mon")
                    }
                    
                case 1 : model.tusAM = cell.btnAM.isSelected
                model.tusPM = cell.btnPM.isSelected
                if model.tusAM || model.tusPM {
                    model.arrName.append("Tue")
                    }
                    
                case 2 : model.wedAM = cell.btnAM.isSelected
                model.wedPM = cell.btnPM.isSelected
                if model.wedAM || model.wedPM{
                    model.arrName.append("Wed")
                    }
                    
                case 3 : model.thrAM = cell.btnAM.isSelected
                model.thrPM = cell.btnPM.isSelected
                if model.thrAM || model.thrPM{
                    model.arrName.append("Thr")
                    }
                    
                case 4 : model.friAM = cell.btnAM.isSelected
                model.friPM = cell.btnPM.isSelected
                if model.friAM || model.friPM {
                    model.arrName.append("Fri")
                    }
                    
                case 5 : model.satAM = cell.btnAM.isSelected
                model.satPM = cell.btnPM.isSelected
                if model.satAM || model.satPM{
                    model.arrName.append("Sat")
                    }
                    
                case 6 : model.sunAM = cell.btnAM.isSelected
                model.sunPM = cell.btnPM.isSelected
                if model.sunAM || model.sunPM {
                    model.arrName.append("Sun")
                    }
                    
                default:
                    print("Done")
                }
                
            }
            
            
            if let del = delegate {
                del.getDayModel(model: model)
                //
            }else if let del = delegate1 {
                del.getAvalability(model: model)
                //
            }
            
            
        }
        else if type == Type.traningDay {
            let model = TeamTraningModel()
            
            for index in 0..<arrDays.count {
                let indexPath = IndexPath(row: index , section: 0)
                let cell = tableGameDay.cellForRow(at: indexPath) as! DayCell //tableView(tableGameDay, cellForRowAt: indexPath) as! DayCell
                switch index {
                case 0 : model.monAM = cell.btnAM.isSelected
                model.monPM = cell.btnPM.isSelected
                if model.monAM || model.monPM {
                    model.arrName.append("Mon")
                    }
                    
                case 1 : model.tusAM = cell.btnAM.isSelected
                model.tusPM = cell.btnPM.isSelected
                if model.tusAM || model.tusPM {
                    model.arrName.append("Tue")
                    }
                    
                case 2 : model.wedAM = cell.btnAM.isSelected
                model.wedPM = cell.btnPM.isSelected
                if model.wedAM || model.wedPM{
                    model.arrName.append("Wed")
                    }
                    
                case 3 : model.thrAM = cell.btnAM.isSelected
                model.thrPM = cell.btnPM.isSelected
                if model.thrAM || model.thrPM{
                    model.arrName.append("Thr")
                    }
                    
                case 4 : model.friAM = cell.btnAM.isSelected
                model.friPM = cell.btnPM.isSelected
                if model.friAM || model.friPM {
                    model.arrName.append("Fri")
                    }
                    
                case 5 : model.satAM = cell.btnAM.isSelected
                model.satPM = cell.btnPM.isSelected
                if model.satAM || model.satPM{
                    model.arrName.append("Sat")
                    }
                    
                case 6 : model.sunAM = cell.btnAM.isSelected
                model.sunPM = cell.btnPM.isSelected
                if model.sunAM || model.sunPM {
                    model.arrName.append("Sun")
                    }
                    //        case 7 : model.dontTrain = cell.btnAM.isSelected
                    //        model.dontTrain = cell.btnPM.isSelected
                    //        if model.sunAM || model.sunPM {
                    //            model.arrName.append("We don’t train 🤷‍♂️")
                    //            }
                    
                default:
                    print("Done")
                }
                
                
            }
            if let del = delegate{
                del.getTainnigModel(model: model)
            }
            if let del = delegate{
                del.noTrainText(text: string!)
            }
            let indexPath = IndexPath(row: 0 , section: 1)
            let cell = tableGameDay.cellForRow(at: indexPath) as! NoTrainCell
            model.dontTrain = cell.checkBtn.isSelected
            if   model.dontTrain  {
                model.arrName.append("We don’t train 🤷‍♂️")
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelOnPress(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension GameDayVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return arrDays.count
        }
        else if section == 1{
            return 1
        }
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DayCell
            cell.lblDays.text = arrDays[indexPath.row]
            cell.btnAM.tag = indexPath.row
            cell.btnPM.tag = indexPath.row
            cell.btnAM.addTarget(self, action: #selector(amOnPress), for:  .touchUpInside)
            cell.btnPM.addTarget(self, action: #selector(pmOnPress), for:  .touchUpInside)
            return cell
        }else if indexPath.section == 1 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! NoTrainCell
            cell1.checkBtn.addTarget(self, action: #selector(noTrain), for:  .touchUpInside)
            string = cell1.lblWeDontTrain.text
            
            return cell1
            
        }
        return cell
        
    }
    
    @objc func amOnPress(sender:UIButton){
        print(sender.tag)
        sender.isSelected =  !sender.isSelected
    }
    
    @objc func pmOnPress(sender:UIButton){
        print(sender.tag)
        sender.isSelected =  !sender.isSelected
        
    }
    @objc func noTrain(sender:UIButton){
        print(sender.tag)
        sender.isSelected =  !sender.isSelected
        
    }
}


