//
//  JooinTeamTableView.swift
//  Sports100
//
//  Created by VeeraJain on 17/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import  SDWebImage

class JooinTeamTableView: UIViewController {

    @IBOutlet weak var teamTable: UITableView!
    var arrSearchTeam = [JoinTeamModel]()
    var model: JoinTeamModel?
    var param = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        teamTable.tableFooterView = UIView()
        teamTable.delegate = self
        teamTable.dataSource = self
        
        getTeamData(param: param)
    }
    

    @IBAction func refreshOnPres(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addOnPress(_ sender: UIButton) {
    }
    
    func getTeamData(param : [String:Any]) {
     
//        let params = ["key":Api.key, "sport_id":"",
//                      "name":"",
//                      "home_location":"",
//                      "team_gender":"",
//                      "age_group":"",
//                      "gameDay[monam]":"",
//                      "gameDay[monpm]":"",
//                      "gameDay[tueam]":"",
//                      "gameDay[tuepm]":"",
//                      "gameDay[wedam]":"",
//                      "gameDay[wedpm]":"",
//                      "gameDay[thuam]":"",
//                      "gameDay[thupm]":"",
//                      "gameDay[friam]":"",
//                      "gameDay[fripm]":"",
//                      "gameDay[satam]":"",
//                      "gameDay[satpm]":"",
//                      "gameDay[sunam]":"",
//                      "gameDay[sunpm]":"",
//                      "trainingTime[monam]":"",
//                      "trainingTime[monpm]":"",
//                      "trainingTime[tueam]":"",
//                      "trainingTime[tuepm]":"",
//                      "trainingTime[wedam]":"",
//                      "trainingTime[wedpm]":"",
//                      "trainingTime[thuam]":"",
//                      "trainingTime[thupm]":"",
//                      "trainingTime[friam]":"",
//                      "trainingTime[fripm]":"",
//                      "trainingTime[satam]":"",
//                      "trainingTime[satpm]":"",
//                      "trainingTime[sunam]":"",
//                      "trainingTime[sunpm]":"",
//                      "standard":"",
//                      "latitude":"",
//                      "longitude":"",
//                      "distance":""] as [String : Any]
        //        ApiLocationManager.shared.apiManeger(api: url.teamSearch, params, URLEncoding.default){ (response, err) in
        //            if err == nil {
        //                let json = JSON(response!)
        //                let dic = json["response"].arrayObject
        //
        //                for data in dic! {
        //                    let model = JoinTeamModel.init(withDic: data as! [String : Any])
        //                    self.arrSearchTeam.append(model)
        //                }
        //            }
        //        }
        Alamofire.request(url.teamSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.error == nil {
                self.arrSearchTeam.removeAll()
                let json = JSON(response.value as Any)
                let dic = json["response"].arrayObject
//                let array = controller.arrTeam.sorted(by: { $0.first_name! > $1.first_name! })
//                print(array)
//                controller.arrTeam = array
//                controller.playerTable.reloadData()
                
                for data in dic! {
                    let model = JoinTeamModel.init(withDic: data as! [String : Any])
                    self.arrSearchTeam.append(model)
                }
                let array = self.arrSearchTeam.sorted(by: { $0.team_name! < $1.team_name! })
                self.arrSearchTeam.removeAll()
                self.arrSearchTeam = array
                self.teamTable.reloadData()
            }
        }
    }
    
}

extension JooinTeamTableView : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! JooingTeamCell
        let image = arrSearchTeam[indexPath.row].team_logo
        let imgUrlString = imageUrl + "\(image ?? "")"
       cell.imgview?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
        cell.lblTeamName.text = arrSearchTeam[indexPath.row].team_name

      return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  vc = storyboard?.instantiateViewController(withIdentifier: "TeamDetailViewController") as! TeamDetailViewController
        vc.teamId = arrSearchTeam[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
}
