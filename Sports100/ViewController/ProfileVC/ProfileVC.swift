//
//  ProfileVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    //MARK: ------------- VARIABLE/OUTLET ----------------
    
    
    @IBOutlet weak var edit_Btn:UIBarButtonItem!
    @IBOutlet weak var table_View:UITableView!
    
    let refreshControl = UIRefreshControl()
    
    var otherUserId = ""
    var userModel:UserData? {
        didSet {
            table_View.reloadData()
        }
    }
    
    var isFriend = false
    
    //MARK: ------------- VIEW LIFE CYCLE ----------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(otherUserId)
        table_View.refreshControl = refreshControl
        table_View.estimatedRowHeight = 64;
        table_View.rowHeight = UITableViewAutomaticDimension
        
        refreshControl.addTarget(self, action: #selector(ProfileVC.getProfileInfo), for: .valueChanged)
        
        
        //setNavigationBarImage()
        // self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        getProfileInfo()
        if otherUserId != "" {
            
            if userModel != nil { return }
            
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.rightBarButtonItems = []
            self.navigationItem.hidesBackButton = false
            self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            // self.navigationController?.navigationBar.tintColor = UIColor.white
            
            if userModel == nil {
                showHud("Processing..")
                
                GetUser.getOtherUserProfileInfo(otherUserId, { (user) in
                    self.userModel = nil
                    if let otherUser = user {
                        otherUserInfo = otherUser
                        self.userModel = otherUserInfo
                        self.title = "\(otherUser.firstName) \(otherUser.lastName)"
                    }
                    self.table_View.reloadData()
                })
            }
        } else {
            navigationItem.leftBarButtonItems = [edit_Btn]
            userModel = userInfo
            //  setNavigationBarImage()
            self.title = "CV"
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationBar.prefersLargeTitles = true
                navigationItem.largeTitleDisplayMode = .automatic
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @objc func getProfileInfo() {
        refreshControl.beginRefreshing()
        
        if otherUserId == "" {
            GetUser.getUserProfileInfo({ (user) in
                self.refreshControl.endRefreshing()
                self.userModel = user
//                self.userModel?.userPosts = user!.userPosts
                
                self.table_View.reloadData()
                // self.table_View.layoutSubviews()
                self.table_View.setNeedsLayout()
            })
        } else {
            GetUser.getOtherUserProfileInfo(otherUserId, { (user) in
                self.refreshControl.endRefreshing()
                if let otherUser = user {
                    
                    otherUserInfo = otherUser
                    self.userModel = otherUserInfo
                    self.title = "\(otherUser.firstName) \(otherUser.lastName)"
                    self.table_View.reloadData()
                    self.table_View.layoutSubviews()
                }
            })
        }
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -------- BUTTON ACTION METHOD ------------
    
    @IBAction func buttonClicked_Settings(_ sender: UIButton) {
        if let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC"){
            settingsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    @IBAction func buttonClicked_Contacts(_ sender: UIButton) {
        
       let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ContactVC") as! ContactVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonClicked_Edit(_ sender: UIButton) {
        self.goToEditProfile()
    }
    
    private func goToEditProfile() {
        
        if let addMoreInfoVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.AddMoreInfoVC) as? AddMoreInfoVC {
            addMoreInfoVC.delegate = self
            self.navigationController?.pushViewController(addMoreInfoVC, animated: true)
        }
    }
}

extension ProfileVC:UITableViewDataSource,SomeProtocol {
    func joinOnPress() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "JoinTeamVC") as? JoinTeamVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func naviagate() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateNewTeamVC") as? CreateNewTeamVC
    self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.userModel == nil {
            return 0
        }
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 6 {
            if let info = userModel,info.userAchievements.count > 0 {
                return  info.userAchievements.count <= 3 ? info.userAchievements.count : 0
            }
            return 0
        }else if section == 1{
            if userModel?.isHeight == "1" {
               return 0
            }
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell") as? ProfileInfoCell
          cell?.setAllValues(userModel, isOther: otherUserId != "" )
            
            if otherUserId != "" {
                cell?.btn_Edit.isHidden = true
            }
            if userModel?.userId == userInfo?.userId {
                cell?.btn_Connect.isHidden = true
                cell?.btn_Message.isHidden = true
                cell?.btn_InviteToTeam.isHidden = true
            } else if userModel?.isFriend == true {
                cell?.btn_Connect.setTitle("Disconnect", for: .normal)
                cell?.btn_Connect.tag = 101
                //cell?.btn_Connect.isUserInteractionEnabled = true
            } else {
                if userModel?.isPending == true {
                    cell?.btn_Connect.setTitle("Pending", for: .normal)
                    cell?.btn_Connect.tag = 103
                } else {
                    cell?.btn_Connect.setTitle("Connect", for: .normal)
                    cell?.btn_Connect.tag = 102
                }
                
                //cell?.btn_Connect.isUserInteractionEnabled = true
            }
            cell?.lblLocation.text = (userModel?.city ?? "") + " " + (userModel?.country ?? "")
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSportCell") as? ProfileSportCell
            cell?.setUpSportCell(userModel)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePhysicalCell") as? ProfilePhysicalCell
          
//            if userModel?.isWeight == "1"  {
//                tableView.deleteSections([1], with: .fade)
//            }else{
                  cell?.setUpPhysicalCell(userModel)
//            }
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAvailabilityCell") as? ProfileAvailabilityCell
            cell?.setUpAvailabilityCell(userModel)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 5 {
            
                let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileAchievmentHeaderCell") as? ProfileAchievmentHeaderCell
                
                if otherUserId != "" {
                    cell?.btn_Edit.isHidden = true
                }
                if let info = userModel, info.userAchievements.count == 0 {
                    cell?.btn_SeeAll.isHidden = true
                } else {
                    cell?.btn_SeeAll.isHidden = false
                }
                
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
          
        }
        else if indexPath.section == 6 {

            let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileAchievmentCell") as? ProfileAchievmentCell
            if let info = userModel {
                
                let achieve = info.userAchievements[indexPath.row ]
                cell?.setUpAchievmentCell(achieve)
//                if indexPath.row == info.userAchievements.count {
//                    cell?.view_BottomConstraint.constant = 5
//                } else {
//                    cell?.view_BottomConstraint.constant = -5
//                }
            }
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()

          }
        else if indexPath.section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMediaCell") as? ProfileMediaCell
            cell?.setUpPostCell(userModel)
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 3 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTeamCell") as? ProfileTeamCell
            if userModel?.userId != userInfo?.userId {
                cell?.btnCreate.isHidden = true
            }
            cell?.setUpPostCell(userModel)
            cell?.delegate = self
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        } else if indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMessageCell") as? ProfileMessageCell
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
}

extension ProfileVC:AddMoreInfoDelegate {
    func profileIsUpDated() {
        self.table_View.reloadData()
    }
    
    
    
}



