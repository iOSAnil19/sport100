//
//  StanderVC.swift
//  Sports100
//
//  Created by VeeraJain on 11/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import AMPopTip

protocol didSelectdData {
    func textField(textfield : String,type : Type,id:String)
    
}
class StanderVC: UIViewController,UITableViewDataSource,UITableViewDelegate,PopUp{
    func popUpAbout(_ sender: UIButton) {
let _tag = sender.tag
        let indexPath = IndexPath(row:_tag, section: 0)
        let cell = tableView(tableStanderd, cellForRowAt: indexPath) as! StandardCell
//        if (arrStanderd[indexPath.row].name?.contains(find: "Recreational"))! {
//            popText = "You play for fun!"
//        } else if (arrStanderd[indexPath.row].name?.contains(find: "Performance"))! {
//            popText = "You play sport at a decent level and one day want to be Elite!"
//        }else  if (arrStanderd[indexPath.row].name?.contains(find: "Elite"))! {
//            popText = "You are semi pro/ professional and take sport seriously"
//        }else if (arrStanderd[indexPath.row].name?.contains(find: "Vets"))! {
//            popText = "You are over 35 years old and still got it 😉"
//        }else{
//            popTip.isHidden = true
//            popText = ""
//        }
        guard let info = arrStanderd[indexPath.row].info else{return}
        popText = info
      
        popTip.bubbleColor = #colorLiteral(red: 0, green: 0.4352941176, blue: 1, alpha: 1)
        popTip.textColor = .white
//        popTip.actionAnimation = .bounce(2)
        let buttonPostion = sender.convert(sender.bounds.origin, to: self.view)

          let here = CGRect(x: buttonPostion.x, y: buttonPostion.y, width: 200, height: 50)
        
        popTip.show(text: popText, direction: .left, maxWidth: 200, in: view, from: here, duration: 2)
    }

    
    @IBOutlet weak var lblTittel: UILabel!
    
    @IBOutlet weak var tableStanderd: UITableView!
    var lblHeader = ""
    var type : Type?
    var isStanderd : Bool?
    var isAge : Bool?
    var arrStanderd = [StandardModel]()
    var arrAge = [AgeModel]()
    var delegate : didSelectdData?
    var isBool = true
    var isJoinStanderd : Bool?
    var isJoinAge : Bool?
    var isJoinSport : Bool?
    var arrSport = [ChooseSport]()
    var isSports = true
    let popTip = PopTip()
    var popText = String()
    var buttonPostion = CGRect()
//    var imgojis = ["","😆","🏅","😆","🏅","😆","🏅","🏆","👴👵"]
    var aboutMSG = ["sxfdasdfasdfdsfsdfsdfsadfsafdsa","sdfsdfsdfsdf"]
    let arrStongest = ["Left Footed","Right Footed","Left Handed","Right Handed","Ambidextrous","Both Footed"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == Type.standard {
        getStanderd()
        }else if type == Type.age {
        getAge()
        
        }
        
        lblTittel.text = lblHeader
        tableStanderd.delegate = self
        tableStanderd.dataSource = self
        tableStanderd.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
         if type == Type.sports {
            getSport()
        }
    }
    func getStanderd() {
        let networkManager = NetworkManager()
        arrStanderd.removeAll()
        let params = ["key":Api.key] as [String : Any]
        networkManager.getDataForRequest(Api.getStanderd, andParameter: params ){ (res, err) in
            if err == nil {
                let json = JSON(res!)
                let dic = json["Sport"].arrayObject
                self.arrStanderd.removeAll()
                for data in dic! {
                    let model = StandardModel.init(withDic: data as! [String : Any])
                    self.arrStanderd.append(model)
                }
                self.tableStanderd.reloadData()
            }
        }
    }
    func getSport() {
        let networkManager = NetworkManager()
        arrSport.removeAll()
        let params = ["key":Api.key] as [String : Any]
        networkManager.getDataForRequest(Api.getSport, andParameter: params ){ (res, err) in
            if err == nil {
                let json = JSON(res!)
                let dic = json["Sport"].arrayObject
                
                for data in dic! {
                    let model = ChooseSport.init(withDic: data as! [String : Any])
                    self.arrSport.append(model)
                }
                self.tableStanderd.reloadData()
            }
        }
    }
    func getAge() {
        let networkManager = NetworkManager()
        arrAge.removeAll()
        let params = ["key":Api.key] as [String : Any]
        networkManager.getDataForRequest(Api.getAge, andParameter: params ){ (res, err) in
            if err == nil {
                self.arrAge.removeAll()
                let json = JSON(res!)
                let dic = json["result"].arrayObject
                
                for data in dic! {
                    let model = AgeModel.init(withDic: data as! [String : Any])
                    self.arrAge.append(model)
                }
                self.tableStanderd.reloadData()
            }
        }
    }
    @IBAction func cancelPopUpBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == Type.standard  {
        return arrStanderd.count
        }else if type == Type.age{
            return arrAge.count
        }else if type == Type.sports {
            return arrSport.count
        }else if type == Type.strongest{
            return arrStongest.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StandardCell
        
       if type == Type.standard  {
//        cell.lblStandard.text = arrStanderd[indexPath.row].name! + "\(imgojis[indexPath.row])"
        cell.aboutBtn.isHidden = false
        if indexPath.row == 0 {
            cell.aboutBtn.isHidden = true
            cell.lblStandard.text = arrStanderd[0].name
        }
        cell.lblStandard.text = arrStanderd[indexPath.row].name
        cell.delegate = self
        cell.aboutBtn.tag = indexPath.row
        
        
//
     }else if type == Type.age{
        cell.lblStandard.text = arrAge[indexPath.row].group
        }else if type == Type.sports {
            cell.lblStandard.text = arrSport[indexPath.row].name
       }else if type == Type.strongest{
        cell.lblStandard.text = arrStongest[indexPath.row]
        }
 
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
      if type == Type.standard  {
             let cell = arrStanderd[indexPath.row].name!
            let idStanderd = arrStanderd[indexPath.row].id!
        if let del = delegate{
        del.textField(textfield: cell,type: type!,id: idStanderd)
        }
        
        }else if type == Type.age{
              let cell = arrAge[indexPath.row].group!
            let idAge = arrAge[indexPath.row].id!
        if let del = delegate{
            del.textField(textfield: cell,type: type!, id: idAge)
            
        }
        }else if type == Type.sports {
            let cell = arrSport[indexPath.row].name!
            let idSport = arrSport[indexPath.row].id!
        if let del = delegate{
        del.textField(textfield: cell,type: type!,id: idSport)
        }
      }else if type == Type.strongest {
        let cell = arrStongest[indexPath.row]
//        let idSport = arrSport[indexPath.row].id!
        if let del = delegate{
            del.textField(textfield: cell,type: type!,id: "")
        }
        }
     
        self.dismiss(animated: true, completion: nil)
    }
   
}
