//
//  StandardCell.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import AMPopTip

protocol PopUp {
    func popUpAbout(_ sender : UIButton)
}

class StandardCell: UITableViewCell {
    @IBOutlet weak var aboutBtn: UIButton!
    
    var arrAbout = ["","You play for fun!","You play sport at a decent level and one day want to be Elite! ","You play for fun!","You play sport at a decent level and one day want to be Elite! ","You play for fun!","You play sport at a decent level and one day want to be Elite! ","You are semi pro/ professional and take sport seriously","You are over 35 years old and still got it 😉"]
    @IBOutlet weak var lblStandard: UILabel!
    
    var delegate : PopUp?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func aboutOnPress(_ sender: UIButton) {
        if let del = delegate {
            del.popUpAbout(sender)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
