//
//  SearchScrollerVC.swift
//  Sports100
//
//  Created by Mukesh Muteja on 24/08/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class SearchScrollerVC: UIViewController, UIScrollViewDelegate {

   
    @IBOutlet weak var scrollView: UIScrollView!
  
  @IBOutlet weak var btnPlayer: UIButton!
  @IBOutlet weak var btnTeam: UIButton!
  @IBOutlet weak var constHeaderX: NSLayoutConstraint!
    @IBOutlet var hideView: UIView!
    var editButton = UIBarButtonItem()
    var filterBtn = UIBarButtonItem()
    var refreshButton  =  UIBarButtonItem()
    let editImage    = #imageLiteral(resourceName: "add_team_ic")
    let refresh  = #imageLiteral(resourceName: "ic_reload")
    let filter  = #imageLiteral(resourceName: "ic-1")
  override func viewDidLoad() {
        super.viewDidLoad()
    self.scrollView.delegate = self
    hideView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height )
    
    view.addSubview(hideView)
    filterBtn.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    hideView.isHidden = true
    self.btnPlayer.isSelected = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.rightBarButtonItem = nil
        self.setUpbarButtonForPlayer()
        
    }
  @IBAction func actionButtonSelected(_ sender: UIButton) {
    
    if sender.tag == 0 {
      scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
         constHeaderX.constant = 0
        sender.layer.borderColor = UIColor.clear.cgColor
        if btnPlayer.isSelected == false {
            btnPlayer.isSelected = true
            btnTeam.isSelected = false
            btnPlayer.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnTeam.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        }else{
            btnPlayer.isSelected = true
            btnPlayer.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }
        setUpbarButtonForPlayer()
       
    } else {
        sender.layer.borderColor = UIColor.clear.cgColor
        if btnTeam.isSelected == false {
            btnTeam.isSelected = true
            btnPlayer.isSelected = false
            btnTeam.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnPlayer.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        }else{
            btnTeam.isSelected = true
            btnTeam.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }
      scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.size.width, y: 0), animated: true)
      constHeaderX.constant = self.scrollView.frame.size.width/2
        setUpBarButtonForTeam()
        
    }
    UIView.animate(withDuration: 0.2) {
      self.view.layoutIfNeeded()
    }
  }
    func setUpBarButtonForTeam(){
        editButton   = UIBarButtonItem(image: editImage,  style: .plain, target: self, action: #selector(addTapped))
        filterBtn   = UIBarButtonItem(image: filter,  style: .plain, target: self, action: #selector(filterTapped))
        refreshButton = UIBarButtonItem(image: refresh,  style: .plain, target: self, action: #selector(refreshTapped))
        navigationItem.leftBarButtonItems = [editButton, refreshButton]
    }
    func setUpbarButtonForPlayer(){
        refreshButton = UIBarButtonItem(image: refresh,  style: .plain, target: self, action: #selector(refreshTapped))
        filterBtn = UIBarButtonItem(image: filter,  style: .plain, target: self, action: #selector(filterTapped))
        navigationItem.leftBarButtonItems = [refreshButton]
    }

  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.x >= self.scrollView.frame.size.width {
      constHeaderX.constant = self.scrollView.frame.size.width/2
        if scrollView.contentOffset.x >= 0 {
            btnPlayer.isSelected = false
            btnTeam.isSelected = true
            btnTeam.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnPlayer.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            setUpBarButtonForTeam()
        }
    } else {
        constHeaderX.constant = 0
        if scrollView.contentOffset.x == 0 {
            btnTeam.isSelected = false
            btnPlayer.isSelected = true
            btnPlayer.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnTeam.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
           setUpbarButtonForPlayer()
        }
    }
    UIView.animate(withDuration: 0.2) {
        self.view.layoutIfNeeded()
    }
    
  }
    @objc func refreshTapped(){
        if scrollView.contentOffset.x == 0.0 {
            
            let controller = self.childViewControllers.first as! PlayerSearchVC
            self.navigationItem.rightBarButtonItem = nil
            controller.playerTable.isHidden = true
            controller.doneHeight.constant = 50
            controller.tfPlayerName.text = ""
            controller.tfPlayerSport.text = ""
            controller.tfPlayerPosition.text = ""
            controller.tfPlayerStrogest.text = ""
            controller.tfPlayerStandard.text = ""
            controller.tfPlayerNationality.text = ""
            controller.tfPlayerMinHeight.text = ""
            controller.tfPlayerMinAge.text = ""
            controller.tfPlayerMaxAge.text = ""
            controller.tfPlayerMaxHeight.text = ""
            controller.tfPlayerGender.text = ""
            controller.tfplayerAvailability.text = ""
            controller.tfPlayerLocation.text = ""
            controller.arrTeam.removeAll()
            controller.standerdID.removeAll()
            controller.sportsID.removeAll()
            controller.selectedPosition = ""
            print("done")
            
        }else if scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers[1] as! TeamSearchVC
            self.navigationItem.rightBarButtonItem = nil
            controller.teamTable.isHidden = true
            controller.doneHeight.constant = 50
            controller.tfTeamTrainingDay.text = ""
            controller.tfTeamGender.text = ""
            controller.tfTeamAge.text = ""
            controller.tfTeamName.text = ""
            controller.tfTeamSport.text = ""
            controller.tfTeamLocation.text = ""
            controller.tfTeamStandard.text = ""
            controller.tfTeamGameDay.text = ""
            
            print("perfect")
        }
        
    }
    @objc func addTapped(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateNewTeamVC") as! CreateNewTeamVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func filterTapped(){
        
        hideView.isHidden = false
    }
    
    @IBAction func nameAscendingOnPress(_ sender: UIButton) {
        nameAsending()
    }
    @IBAction func nameDesendingOnPress(_ sender: UIButton) {
        nameDesending()
    }
    
    @IBAction func distanceAsendingOnPress(_ sender: UIButton) {
        distanseAsending()
    }
    @IBAction func hideOnTapp(_ sender: Any) {
        self.hideView.isHidden = true
    }
    @IBAction func distanceDesendingOnPress(_ sender: UIButton) {
        distanceDesending()
    }
    func nameAsending() { // should probably be called sort and not filter
        if scrollView.contentOffset.x == 0.0  {
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.first_name! < $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
            
        }else if scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! < $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
            
        }
    }
    func nameDesending() { // should probably be called sort and not filter
        if scrollView.contentOffset.x == 0.0  {
            
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.first_name! > $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
            
        }else if scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! > $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
            
        }
    }
    func distanseAsending() { // should probably be called sort and not filter
        if scrollView.contentOffset.x == 0.0  {
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
            
        }
            else if scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
            
        }
    }
    func distanceDesending() { // should probably be called sort and not filter
        if scrollView.contentOffset.x == 0.0  {
            
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
            
        }
            else if scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
            
        }
    }
}
