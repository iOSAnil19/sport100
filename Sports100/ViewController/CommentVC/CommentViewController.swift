//
//  CommentViewController.swift
//  Sports100
//
//  Created by Nripendra Hudda on 04/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift



class CommentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,THChatInputDelegate {
   
    
    
    var feed:Feed?
    @IBOutlet weak var tblViewComments:UITableView!
   
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var  chatInput:THChatInput?
    var commentText:String? = ""
    @IBOutlet weak var activity_View:UIActivityIndicatorView!
    
    var commentObjNewsFeed:Comment?
    var arrDataSourceComment = Array<Comment>()
  
    
    var currentPage = 1
    var totalPageCount = 1
    var refreshControl = UIRefreshControl()
    
    private var isVisibleKeyboard = true
    
    var feed_id = String()
    var trend_id = String()
    var isComingFromNewsFeed = Bool()
    
    var delegate:FeedDelegate?
      
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewComments.delegate = self
        tblViewComments.dataSource = self
        
        self.title = "Comments"

        addPullUpRefreshControll()
        getNewsFeedCommentsList()
        
        refreshControl.beginRefreshing()
        
        tblViewComments.estimatedRowHeight = 62
        tblViewComments.rowHeight = UITableViewAutomaticDimension

        
       // self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
      //  self.navigationController?.navigationBar.tintColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.arrDataSourceComment.count>0)
        {
            let indexPath = IndexPath.init(row:(self.arrDataSourceComment.count - 1), section: 1)
            
            self.tblViewComments?.scrollToRow(at:indexPath, at: .bottom, animated: true)
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
       
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    //MARK: - Keyboard method -
    var contentInset:UIEdgeInsets = UIEdgeInsets()
    @objc private func keyboardWillShow(notification:NSNotification){
        
        tblViewComments?.isScrollEnabled = true
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var info = notification.userInfo!
        var keyboardFrame:CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        contentInset = (tblViewComments?.contentInset)!
        contentInset.bottom = keyboardFrame.size.height
        tblViewComments?.contentInset = contentInset
        if(self.arrDataSourceComment.count>0)
        {
            let indexPath = IndexPath.init(row:(self.arrDataSourceComment.count - 1), section: 1)
            
            self.tblViewComments?.scrollToRow(at:indexPath, at: .bottom, animated: true)
        }
    }
    
    @objc private func keyboardWillHide(notification:NSNotification){
        contentInset.bottom = 0
        tblViewComments?.contentInset = contentInset
        tblViewComments?.isScrollEnabled = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Refresh Controll -
    
    func addPullUpRefreshControll(){
        //Add refresh controller
        refreshControl.addTarget(self, action:#selector(refresh), for: UIControlEvents.valueChanged)
        tblViewComments.addSubview(refreshControl) // not required
        refreshControl.endRefreshing()
    }
    
    @objc func refresh(sender:AnyObject) {
        currentPage  = 1
        getNewsFeedCommentsList()
    }
    
    func endRefreshing() {
        // Code to refresh table view
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            
            self.refreshControl.endRefreshing()
            self.tblViewComments.setContentOffset(CGPoint.zero, animated: true)
        }
    }
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // MARK: - Table view data source / delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return arrDataSourceComment.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let feedSelected = self.feed else { return UITableViewCell()}
             var cellId = "FeedCell"
            if let type = feedSelected.type {
                if (type == "Achievement") {
                    cellId = "FeedAchieveCell"
                } else if (type == "Image") {
                    cellId = "FeedCell"
                }else if (type == "PostText") {
                    cellId = "FeedCell"
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? FeedCell
                cell?.btn_Like.tag = indexPath.row
                cell?.btn_Views.tag = indexPath.row
                cell?.setUpCommentFeedCell(feedSelected)
                cell?.selectionStyle = .none
                return cell ?? UITableViewCell()
            }
        }
        
        let cell:CommentsCell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentsCell
        cell.selectionStyle = .none
        let commentArray: [Comment] = self.arrDataSourceComment.reversed()
        let clData  = commentArray[indexPath.row]
        cell.updateNewsFeedCellData(clData: clData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            var lastElement = Int()
            
            lastElement = arrDataSourceComment.count - 1
            
            
            print("Index is: ",indexPath.row)
            if indexPath.row == lastElement {
                // handle your logic here to get more items, add it to dataSource and reload tableview
                // currentPage  = currentPage + 1
                // getNewsFeedCommentsList()
                
            }
        }
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendClicked(_ sender: Any) {
    
        if commentText?.isEmpty == false {
                addComment()

                let imgUrlString = "\(userInfo?.userImage ?? "")"

                let userDict = ["ID": userInfo?.userId ?? "",
                "FirstName": userInfo?.firstName ?? "",
                "LastName": userInfo?.lastName ?? "",
                "ProfilePic": imgUrlString]
                let commentDict = ["commentID": "0",
                                   "comment": commentText ?? "",
                                   "User":userDict] as [String:Any]
                let newComment = Comment.init(object: commentDict)
                arrDataSourceComment.append(newComment)
                commentText = ""
                tblViewComments.reloadData()
            }
    }
    
    func getNewsFeedCommentsList() {
        
        let params = ["key":Api.key,
                      "feedID":feed?.feedID ?? ""] as [String : Any]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getComment, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.endRefreshing()
            }
            
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                  //  self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allFeeds = result["UserComment"] as? [[String:Any]], allFeeds.count > 0 {
                            
                            let commentModels = allFeeds.map({ (commentInfo) -> Comment in
                                Comment.init(object: commentInfo)
                            })
                            if self.currentPage == 1 {
                                self.arrDataSourceComment.removeAll()
                                self.arrDataSourceComment = commentModels
                            } else {
                                for comment in commentModels {
                                    self.arrDataSourceComment.append(comment)
                                }
                            }
                            self.tblViewComments.reloadData()
                        }
                        
                    } else {
                    //    let error = "\(String(describing: result["error"] ?? INTERNAL_ERROR_MESSAGE))"
                    //    showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                  //  showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
               // showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func addComment() {
            
        let params = ["key":Api.key,
                      "feedID":feed?.feedID ?? "",
                      "userID":userInfo?.userId ?? "",
                      "comment":"\(commentText ?? "")"] as [String : Any]
            
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(Api.writeComment, andParameter: params) { (response, error) in
                DispatchQueue.main.async {
                }
                if error == nil {
                    if let result = response as? [String:Any] {
                        let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                        if success == 200 {
//                            if let commentCount = self.feed?.commentCount {
//                                let count = commentCount == "" ? 1:Int(commentCount)
//                                self.feed?.commentCount = "\(String(describing: count))"
//                            }
                            if let f = self.feed {
                                self.delegate?.feedIsUpdated(f)
                            }
                        }
                    }
                }
            }
        }
}


class CommentsCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCommentDescription: UILabel!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblCommentTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgViewUser.layer.cornerRadius = imgViewUser.frame.size.width/2
//        imageView?.layer.cornerRadius =  (imageView?.frame.size.width)! / 2
        imgViewUser.clipsToBounds = true
        imgViewUser.contentMode = .scaleAspectFill
        imgViewUser.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateNewsFeedCellData(clData:Comment) {
        lblName.text = "\(clData.user?.firstName ?? "") " + "\(clData.user?.lastName ?? "")"
        lblCommentDescription.text = clData.comment
        let imgUrlString = imageUrl + "\(clData.user?.profilePic ?? "")"

        if let imgLogoUrl = URL(string:imgUrlString) {
            imgViewUser.sd_setImage(with: imgLogoUrl, placeholderImage:#imageLiteral(resourceName: "defult_user"))
        }
    }
    
    func updateCommectCell(_ teamMessage:TeamMessage) {
        print(teamMessage.created)
        self.lblCommentTime.text = getLocaleDateString(teamMessage.created ?? "")
        lblName.text = "\(teamMessage.name ?? "")"
        lblCommentDescription.text = teamMessage.comment ?? ""
        let imgUrlString = imageUrl + "\(teamMessage.image ?? "")"
        
        if let imgLogoUrl = URL(string:imgUrlString) {
            imgViewUser.sd_setImage(with: imgLogoUrl, placeholderImage:#imageLiteral(resourceName: "defult_user"))
        }
    }
    
}
// MARK:-   Chat Input View

extension CommentViewController
{
    
    
    // MARK: - THChatInputDelegate
    
    func chat(_ input: THChatInput!, sendWasPressed text: String!) {
       
        commentText = text!.removingAllExtraNewLines
        
        if commentText?.isEmpty == false {
           
            addComment()
            
            let imgUrlString = "\(userInfo?.userImage ?? "")"
            
            let userDict = ["ID": userInfo?.userId ?? "",
                            "FirstName": userInfo?.firstName ?? "",
                            "LastName": userInfo?.lastName ?? "",
                            "ProfilePic": imgUrlString]
            let commentDict = ["commentID": "0",
                               "comment": commentText ?? "",
                               "User":userDict] as [String:Any]
            let newComment = Comment.init(object: commentDict)
            arrDataSourceComment.append(newComment)
            commentText = ""
            //tblViewComments.reloadData()
            
            if(self.arrDataSourceComment.count>0)
            {
                let indexPath = IndexPath.init(row:0, section: 1)
                tblViewComments?.insertRows(at: [indexPath], with: .top)
                
                self.tblViewComments?.scrollToRow(at:indexPath, at: .bottom, animated: true)
            }else{
                tblViewComments.reloadData()
            }
        }
        self.chatInput?.setText("")
    }
   
    
    func chatShowEmojiInput(_ input: THChatInput?)
    {
        // chatInput.textView.inputView = chatInput.textView.inputView == nil ? emojiInputView : nil
        //chatInput.textView.reloadInputViews()
    }
    
    func chatShowAttach(_ input: THChatInput?) {
    }
    
}
