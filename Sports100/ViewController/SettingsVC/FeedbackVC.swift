//
//  FeedbackVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 19/06/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class FeedbackVC: UIViewController {
    
    @IBOutlet weak var textViewFeedback: UITextView!
    var type: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = type ?? "Report"
        self.setNavigationBarButton()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        IQKeyboardManager.shared.enable = false
        textViewFeedback.autocorrectionType = .no
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        IQKeyboardManager.shared.enable = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func setNavigationBarButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .action, target: self, action: #selector(sendFeedBack))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func cancelPressed() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func sendFeedBack() {
        
//        guard let reportType = type else { return }
//        var feedType = 0
//        if reportType == 
        
        let networkManager = NetworkManager()
    

        let params = ["key": Api.key,
                      "userID":userInfo?.userId ?? "",
                      "FeedID":1,
        "reason":textViewFeedback.text ?? "" ] as [String :Any]
        print(params)
        networkManager.getDataForRequest(Api.sendReport, andParameter: params) { (response, error) in
             let result = response as! [String:Any]
         let response = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
            if response == 200 {
               
                let message = result["message"] as! String
                // Create the alert controller
                let alertController = UIAlertController(title: "Submit", message: message , preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                  self.navigationController?.dismiss(animated: true, completion: nil)                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
               
            } else {
                showAlert(APP_TITLE, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension FeedbackVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.navigationItem.rightBarButtonItem?.isEnabled = textView.text.count == 0 ? false : true
        return true
    }
}
