//
//  SettingsVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import MessageUI
import StoreKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    let array = ["Change Password", "Submit Bug Report", "Send Feedback", "Rate This App", "Share This App"]
    
    //MARK: ----------- VIEW LIFE CYCLE ------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
   // self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])

        
        setBackViewColor()
       // setNavigationBarImage()
        self.title = "Settings"
        navigationItem.largeTitleDisplayMode = .never
        table_View.tableFooterView = UIView.init(frame: .zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    
    @IBAction func buttonClicked_DeleteAccount(_ sender: Any) {
        let deleteAction = UIAlertAction.init(title: "Delete", style: .destructive) { (action) in
            self.deleteAccount()
        }
        
        showAlert("", message: "Would you like to delete your account?", withAction: deleteAction, with: true, andTitle: "Cancel", onView: self)
    }
    

    @IBAction func buttonClicked_Logout(_ sender: Any) {
        
        let leaveAction = UIAlertAction.init(title: "Logout", style: .destructive) { (action) in
            self.logout()
        }
        
        showAlert("", message: "Would you like to logout from Sports100?", withAction: leaveAction, with: true, andTitle: "Cancel", onView: self)
        
    }

    func deleteAccount() {
        let params = ["key": Api.key, "userId":userInfo?.userId ?? ""]
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.deactivateAccount, andParameter: params) { (response, error) in
            hideHud()
            print("deactivateAccount response \(response)")
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        if let navVC = self.storyboard?.instantiateViewController(withIdentifier: "RootNavigation") {
                            UserDefaults.standard.set(nil, forKey: CACHED_MAIL)
                            UserDefaults.standard.set(nil, forKey: "user")
                            APPDELEGATE.window?.rootViewController = navVC
                        }
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kRESPONSE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func logout() {
        let params = ["key": Api.key, "userId":userInfo?.userId ?? ""]
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.logout, andParameter: params) { (response, error) in
            hideHud()
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        if let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")  {
                            UserDefaults.standard.set(nil, forKey: "user")
                             let navVC = UINavigationController.init(rootViewController: navVC)
                            APPDELEGATE.window?.rootViewController = navVC
                        }
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kRESPONSE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    
    
    
    //MARK: ----------- PRIVATE METHODS ------------
    
    private func sendMailFor(_ subject:String,_ tag:Int) {
        
        
        if let feedbackVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.FeedbackVC) as? FeedbackVC {
            let newNavigation = UINavigationController.init(rootViewController: feedbackVC)
            newNavigation.navigationBar.tintColor = #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)
            newNavigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)]
            
            if tag == 101 {
                feedbackVC.type = "Report"
            } else {
                feedbackVC.type = "Feedback"
            }
            
            self.present(newNavigation, animated: true, completion: nil)
        }
        return
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            //mail.navigationBar
            mail.view.tag = tag
            mail.mailComposeDelegate = self
            mail.setToRecipients(["feedback@sports100.com"]) //
            mail.setSubject("Sports100 \(subject)")
            if tag == 101 {
                mail.setMessageBody("Please descibe the issue you have seen within the app", isHTML: true)
            } else {
                mail.setMessageBody("We are continuously working on improving the Sports100 App. We are always grateful for any feedback that we receive that will better your experience whist using the Sports100 App.", isHTML: true)
            }
            
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    private func showAlertToChangePassword() {
        let alertController = UIAlertController(title: "Change Password", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .destructive, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            let secondTextField = alertController.textFields![1] as UITextField
            
            let thirdTextField = alertController.textFields![2] as UITextField
            
            if isBlankField(firstTextField) {
                return
            }
            if isBlankField(secondTextField) {
                return
            }
            if isBlankField(thirdTextField) {
                return
            }
            if secondTextField.text != thirdTextField.text {
                showAlert(nil, message:"Passwords not matched!", onView: self)
                return;
            }
            self.changePassword((firstTextField.text ?? ""), (secondTextField.text ?? ""))
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Old Password*"
            textField.isSecureTextEntry = true
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "New Password*"
            textField.isSecureTextEntry = true
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Confirm New Password*"
            textField.isSecureTextEntry = true
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func changePassword(_ oldPassword:String, _ newPassword:String) {
        
        let params = ["key":Api.key,
                      ApiConstant.userId:userInfo?.userId ?? "",
                      ApiConstant.oldPassword:oldPassword,
                      ApiConstant.newPassword:newPassword,]
        showHud("Processing..")
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.changePassword, andParameter: params) { (response, error) in
            hideHud()
            print(response ?? "No response")
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        showAlert("", message:"Password changed successfully", onView: self)
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
}

extension SettingsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "CellId"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: cellId)
        }
        cell?.textLabel?.font = UIFont.init(name: "Avenir-Medium", size: 15.0)
        cell?.textLabel?.text = array[indexPath.row]
        cell?.accessoryType = .disclosureIndicator
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension SettingsVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.showAlertToChangePassword()
        } else if indexPath.row == 1 {
            self.sendMailFor("Bug report",101)
        } else if indexPath.row == 2 {
            self.sendMailFor("Feedback",102)
        } else if indexPath.row == 3 {
            SKStoreReviewController.requestReview()
//            if let url  = URL(string: "itms-apps://itunes.apple.com/app/id1024941703") {
//                if UIApplication.shared.canOpenURL(url) {
//                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                }
//            }
        } else {
            let indexPath = IndexPath(row:indexPath.row, section:0)
            let cell: UITableViewCell = table_View.cellForRow(at:indexPath)!
            
            showActivityViewToShare(self,cell)
        }
    }
}


extension SettingsVC:MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        var text = ""
        if controller.view.tag == 101 { text = "Bug report sent"} else { text = "Feedback sent" }
        
        if error == nil {
            if result == .sent {
                showAlert("", message: text.capitalized, onView: self)
            }
        } else {
            showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UIBarButtonItem {
    func setEnabled(_ enabled: Bool) {
        if customView != nil {
            if let allSubviews = customView?.subviews {
                for button in allSubviews {
                    if let b = button as? UIButton {
                        b.isEnabled = true
                    }
                }
            }
        }
    }
}
