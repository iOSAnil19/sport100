//
//  ReportVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 16/05/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

protocol ReportDelegate: class {
    func reportReason(_ reason:String)
}

class ReportVC: UIViewController {
    
    @IBOutlet weak var table_View:UITableView!
    
    var quesArr = ["Choose a reason for hiding this add",
                   "I find it offensive",
                   "It's spam",
                   "It's sexually inappropriate",
                   "It's scam or it's misleading",
                   "It's voilent or prohibited content"]
    
    weak var delegate: ReportDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Report"
        table_View.tableFooterView = UIView.init(frame: .zero)
        self.setCancelButton()

    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ReportVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.quesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "CellId")
        }
        cell?.textLabel?.text = self.quesArr[indexPath.row]
        if indexPath.row != 0 {
            cell?.textLabel?.font = UIFont.init(name: "Avenir-Medium", size: 15.0 * getScaleFactor())
            cell?.accessoryType = .disclosureIndicator
        } else {
            cell?.textLabel?.font = UIFont.init(name: "Avenir", size: 15.0 * getScaleFactor())
            cell?.accessoryType = .none
        }
        return cell ?? UITableViewCell()
    }
}

extension ReportVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let reason = self.quesArr[indexPath.row]
            self.delegate?.reportReason(reason)
            self.dismiss(animated: true) {
                
            }
        }
    }
}
