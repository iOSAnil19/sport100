//
//  CompleteRegisterVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class CompleteRegisterVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        self.title = "Welcome"
        setBackViewColor()
       // setNavigationBarImage()
        //self.navigationController?.navigationBar.applyNavigationGradient(colors:[#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1),#colorLiteral(red: 0.3019607843, green: 0.5215686275, blue: 0.8, alpha: 1)])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
