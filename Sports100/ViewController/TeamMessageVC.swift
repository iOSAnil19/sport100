//
//  TeamMessageVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 21/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift



class TeamMessageVC: UIViewController,UITableViewDelegate,UITableViewDataSource,THChatInputDelegate {
    
    var team:Team?
    @IBOutlet weak var tblViewComments:UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var  chatInput:THChatInput?
    var commentText:String? = ""
    @IBOutlet weak var activity_View:UIActivityIndicatorView!
    
    
    // lazy var arrNames = ["Rainer Zufall","Claire Grube","Armin Gips","Anna Bolika","Bill Yard","Klaus Thaler","Volker Putt","Mira Belle"]
    
    
    var commentObjNewsFeed:TeamMessage?
    var arrDataSourceComment = Array<TeamMessage>()
    
    
    var currentPage = 1
    var totalPageCount = 1
    var refreshControl = UIRefreshControl()
    
    private var isVisibleKeyboard = true
    
    var feed_id = String()
    var trend_id = String()
    var isComingFromNewsFeed = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewComments.delegate = self
        tblViewComments.dataSource = self
        
        self.title = team?.name ?? "Team Message"
        
        addPullUpRefreshControll()
        getTeamCommentsList()
        
        refreshControl.beginRefreshing()
        
        tblViewComments.estimatedRowHeight = 62
        tblViewComments.rowHeight = UITableViewAutomaticDimension
        
        
      //  self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
      //  self.navigationController?.navigationBar.tintColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.arrDataSourceComment.count>0)
        {
            let indexPath = IndexPath.init(row:(self.arrDataSourceComment.count - 1), section: 0)
            
            self.tblViewComments?.scrollToRow(at:indexPath, at: .bottom, animated: true)
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    //MARK: - Keyboard method -
    var contentInset:UIEdgeInsets = UIEdgeInsets()
    @objc private func keyboardWillShow(notification:NSNotification){
        
        tblViewComments?.isScrollEnabled = true
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        var info = notification.userInfo!
        var keyboardFrame:CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        contentInset = (tblViewComments?.contentInset)!
        contentInset.bottom = keyboardFrame.size.height
        tblViewComments?.contentInset = contentInset
        if(self.arrDataSourceComment.count>0)
        {
            let indexPath = IndexPath.init(row:(self.arrDataSourceComment.count - 1), section: 0)
            
            self.tblViewComments?.scrollToRow(at:indexPath, at: .bottom, animated: true)
        }
    }
    
    @objc private func keyboardWillHide(notification:NSNotification){
        contentInset.bottom = 0
        tblViewComments?.contentInset = contentInset
        tblViewComments?.isScrollEnabled = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Refresh Controll -
    
    func addPullUpRefreshControll(){
        //Add refresh controller
        refreshControl.addTarget(self, action:#selector(refresh), for: UIControlEvents.valueChanged)
        tblViewComments.addSubview(refreshControl) // not required
        refreshControl.endRefreshing()
        
    }
    
    @objc func refresh(sender:AnyObject) {
        currentPage  = 1
        getTeamCommentsList()
        
    }
    
    func endRefreshing() {
        // Code to refresh table view
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            
            self.refreshControl.endRefreshing()
            self.tblViewComments.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    
    
    // MARK: - Table view data source / delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataSourceComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CommentsCell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentsCell
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)

        cell.selectionStyle = .none
        let teamMsg  = arrDataSourceComment[indexPath.row]
        cell.updateCommectCell(teamMsg)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var lastElement = Int()
        
        lastElement = arrDataSourceComment.count - 1
        
        
        print("Index is: ",indexPath.row)
        if indexPath.row == lastElement {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            // currentPage  = currentPage + 1
            // getNewsFeedCommentsList()
            
        }
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendClicked(_ sender: Any) {
        
        if commentText?.isEmpty == false {
            addComment()
            
            let imgUrlString = "\(userInfo?.userImage ?? "")"
            
            
            let commentDict = ["CommentID": "0",
                               "UserID": userInfo?.userId ?? "",
                               "Name": "\(userInfo?.firstName ?? "") \(userInfo?.lastName ?? "")",
                               "Image": imgUrlString,
                               "Comment": commentText ?? "",
                               "Created": "\(Date())"] as [String:Any]
            let newComment = TeamMessage.init(object: commentDict)
            arrDataSourceComment.append(newComment)
            commentText = ""
            tblViewComments.reloadData()
        }
    }
    
    func getTeamCommentsList() {
        
        let params = ["key":Api.key,
                      "teamID":team?.iD ?? ""] as [String : Any]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getTeamMessage, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.endRefreshing()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    //  self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let all_Feed = result["teamComment"] as? [Any], all_Feed.count > 0 {
                            
//                            guard let allFeeds = all_Feed[0] as? [String:Any] else { return }
                            
                            let commentModels = all_Feed.map({ (commentInfo) -> TeamMessage in
                                TeamMessage.init(object: commentInfo)
                            })
//                            if self.currentPage == 1 {
//                                self.arrDataSourceComment.removeAll()
//                                self.arrDataSourceComment = commentModels
//                            } else {
                                for comment in commentModels {
                                    self.arrDataSourceComment.append(comment)
                                }
//                            }
                            self.tblViewComments.reloadData()
                        }
                    } else {
                            let error = "\(String(describing: result["message"] ?? INTERNAL_ERROR_MESSAGE))"
                            showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                      showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                 showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func addComment() {
        
        let params = ["key":Api.key,
                      "teamID":team?.iD ?? "",
                      "userID":userInfo?.userId ?? "",
                      "toID":team?.iD ?? "",
                      "comment":"\(commentText ?? "")"] as [String : Any]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.sendteamMessage, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
            }
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        //                            if let commentCount = self.feed?.commentCount {
                        //                                let count = commentCount == "" ? 1:Int(commentCount)
                        //                                self.feed?.commentCount = "\(String(describing: count))"
                        //                            }
                        
                    }
                }
            }
        }
    }
}

// MARK:-   Chat Input View

extension TeamMessageVC
{
    
    
    // MARK: - THChatInputDelegate
    
    func chat(_ input: THChatInput!, sendWasPressed text: String!) {
        
        commentText = text!
        
        if commentText?.isEmpty == false {
            
            addComment()
            
            let imgUrlString = "\(userInfo?.userImage ?? "")"

            let date = getServerDateFromDate(Date())
            
            let commentDict = ["CommentID": "0",
                               "UserID": userInfo?.userId ?? "",
                               "Name": "\(userInfo?.firstName ?? "") \(userInfo?.lastName ?? "")",
                "Image": imgUrlString,
                "Comment": commentText ?? "",
                "Created": "\(date)"] as [String:Any]
            let newComment = TeamMessage.init(object: commentDict)
            arrDataSourceComment.insert(newComment, at: 0)
            commentText = ""
            //tblViewComments.reloadData()
            
            if(self.arrDataSourceComment.count>0)
            {
                let indexPath = IndexPath.init(row:0, section: 0)
                tblViewComments?.insertRows(at: [indexPath], with: .top)
                
                self.tblViewComments?.scrollToRow(at:indexPath, at: .top, animated: true)
            }else{
                tblViewComments.reloadData()
            }
        }
        self.chatInput?.setText("")
    }
    
    
    func chatShowEmojiInput(_ input: THChatInput?)
    {
        // chatInput.textView.inputView = chatInput.textView.inputView == nil ? emojiInputView : nil
        //chatInput.textView.reloadInputViews()
    }
    
    func chatShowAttach(_ input: THChatInput?) {
    }
    
}

