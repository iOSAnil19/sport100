//
//  ViewsCell.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 05/08/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ViewsCell: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var img_User: UIImageView!
    @IBOutlet weak var btn_Connect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_User.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpCell(_ info: [String: Any]) {
    
        img_User.layer.cornerRadius = img_User.frame.width/2
        
        img_User.layer.masksToBounds = true
        if let name = info["Name"] as? String {
            self.lbl_Name.text = name
        }
        
        if let is_friend = info["is_friend"] as? String {
            var title = ""
            self.btn_Connect.isUserInteractionEnabled = true
            if is_friend == "No" {
                title = "  Connect  "
            } else if is_friend == "Yes" {
                title = "  Disconnect  "
            } else {
                title = "  Pending  "
                self.btn_Connect.isUserInteractionEnabled = false
            }
            self.btn_Connect.setTitle(title, for: UIControlState.normal)
        }
        
        if let img = info["Image"] as? String {
            let imgUrlString = imageUrl + "\(img)"
            self.img_User.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options: SDWebImageOptions.scaleDownLargeImages, completed:nil)
        }
        
        guard let id = info["ID"] as? String else { return }

        if (userInfo?.userId ?? "") == id {
            self.btn_Connect.isHidden = true
        } else {
            self.btn_Connect.isHidden = false
        }
    }
    
    @IBAction func buttonClicked_Connect(_ sender: UIButton) {
        
        guard let parentVC = self.parentViewController as? ViewsVC else { return }
        let info = parentVC.allLikesUser[sender.tag]
        guard let id = info["ID"] as? String else { return }
        guard let is_friend = info["is_friend"] as? String else { return }
        
        if is_friend != "No" {
            let params = ["key": Api.key, "toID":id, "userID":userInfo?.userId ?? ""]
            // https://apisports100.co.uk/index.php/friends/deleteContactRequest
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(Api.deleteContactRequest, andParameter: params) { (response, error) in
                
                if error == nil {
                    if let result = response as? [String:Any] {
                        let msg = "\(String(describing: result[NetworkManager.kMESSAGE] ?? ""))"
                        showAlert("", message: msg.capitalized, onView: parentVC)
                        sender.setTitle("  Connect  ", for: .normal)
                    }
                } else {
                    showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                }
            }
        } else {
            let params = ["key": Api.key, "toID":id, "fromID":userInfo?.userId ?? ""]
            let networkManager = NetworkManager()
            networkManager.getDataForRequest(Api.sendRequest, andParameter: params) { (response, error) in
                if error == nil {
                    if let result = response as? [String:Any] {
                        let msg = "\(String(describing: result[NetworkManager.kMESSAGE] ?? "0"))"
                        showAlert("", message: msg.capitalized, onView: parentVC)
                        sender.setTitle("  Pending  ", for: .normal)
                        sender.isUserInteractionEnabled = false
                    }
                } else {
                    showAlert("Error", message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView:parentVC)
                }
            }
        }
        
    }

}
