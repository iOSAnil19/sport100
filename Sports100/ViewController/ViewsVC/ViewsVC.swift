//
//  ViewsVC.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 05/08/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ViewsVC: UIViewController {
    
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var lbl_Views: UILabel!
    var selectedFeed: Feed?
    
    var allLikesUser = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Views and Likes"
        self.getDetail()
        table_View.tableFooterView = UIView(frame: .zero)
    }
    
    func getDetail() {
        guard let feed = selectedFeed else { return }
        showHud("Processing..")
        let networkManager = NetworkManager()
        let params = ["key": Api.key, "userID":userInfo?.userId ?? "", "postID": feed.feedID ?? ""]
        networkManager.getDataForRequest(Api.viewLikeUser, andParameter: params) { (response, error) in
            hideHud()
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    
                    if success == 200 {
                        print(result)
                        
                        if let totalViewed = result["totalViewed"] as? String {
                            self.lbl_Views.text = "\(totalViewed) Views"
                        }
                        
                        if let userList = result["userList"] as? [[String: Any]] {
                            self.allLikesUser = userList
                        }
                        self.table_View.reloadData()
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error.capitalized, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allLikesUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewsCell") as? ViewsCell
        let info = self.allLikesUser[indexPath.row]
        cell?.setUpCell(info)
        cell?.btn_Connect.tag = indexPath.row
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
}

extension ViewsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Liked by"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * getScaleFactor()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35 * getScaleFactor()
    }
}
