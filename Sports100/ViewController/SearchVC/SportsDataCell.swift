//
//  SportsDataCell.swift
//  Sports100
//
//  Created by VeeraJain on 24/08/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class SportsDataCell: UITableViewCell {
    
    @IBOutlet weak var lblsides: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = imgView.frame.height / 2
        imgView.clipsToBounds = true
        imgView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
