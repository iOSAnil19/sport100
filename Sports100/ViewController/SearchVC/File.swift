////
////  File.swift
////  Sports100
////
////  Created by VeeraJain on 21/05/19.
////  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
////
//
//import Foundation
//
//let  lat = "\(locationManeger.shared.locManager.location?.coordinate.latitude)"
//let log = "\(locationManeger.shared.locManager.location?.coordinate.longitude)"
//
//@IBOutlet weak var teamTable: UITableView!
//@IBOutlet weak var playerAndTeamTable: UITableView!
//@IBOutlet weak var playerTeamSegment: UISegmentedControl!
//
//@IBOutlet weak var scrollView: UIScrollView!
//@IBOutlet weak var btnAdd: UIButton!
//@IBOutlet weak var btnRefresh: UIButton!
//@IBOutlet weak var btnDone: UIButton!
//
//@IBOutlet weak var liveView: UIView!
//
//@IBOutlet weak var btnDone2Height: NSLayoutConstraint!
//@IBOutlet weak var btnDoneHeight: NSLayoutConstraint!
////// Player Textfield
//
//@IBOutlet weak var tfPlayerName: UITextField!
//@IBOutlet weak var tfPlayerSport: UITextField!
//@IBOutlet weak var tfPlayerPosition: UITextField!
//@IBOutlet weak var tfPlayerStandard: UITextField!
//@IBOutlet weak var tfPlayerNationality: UITextField!
//@IBOutlet weak var tfPlayerMinHeight: UITextField!
//@IBOutlet weak var tfPlayerMaxHeight: UITextField!
//@IBOutlet weak var tfPlayerMinAge: UITextField!
//@IBOutlet weak var tfPlayerLocation: UITextField!
//@IBOutlet weak var tfplayerAvailability: UITextField!
//@IBOutlet weak var tfPlayerGender: UITextField!
//@IBOutlet weak var tfPlayerMaxAge: UITextField!
//@IBOutlet weak var tfPlayerStrogest: UITextField!
//
////// Team Text Field
//
//@IBOutlet weak var btnDone2: UIButton!
//@IBOutlet weak var tfTeamName: UITextField!
//
//@IBOutlet weak var tfTeamTrainingDay: UITextField!
//@IBOutlet weak var tfTeamGameDay: UITextField!
//@IBOutlet weak var tfTeamLocation: UITextField!
//@IBOutlet weak var tfTeamGender: UITextField!
//@IBOutlet weak var tfTeamAge: UITextField!
//@IBOutlet weak var tfTeamStandard: UITextField!
//@IBOutlet weak var tfTeamSport: UITextField!
//
//var arrTeam = [SearchModel]()
//var arrSearchTeam = [JoinTeamModel]()
//var model: JoinTeamModel?
//var picker = UIPickerView()
//
//
//
//var teamLocation = ["Within 1 mile","Within 5 mile","Within 10 mile","Within 20 mile","National"]
//
//var minAge = ["3 ","4 ","5 ","6 ","7 ","8 ","9 ","10 ","11 ","12 ","13 "
//    ,"14 ","15 ","16 ","17 ","18 ","19 ","20 ","21 ","22 ","23 ","24 "
//    ,"25 ","26 ","27 ","28 ","29 ","30 ","31 ","32 ","33 ","34 ","35 "
//    ,"36 ","37 ","38 ","39 ","40 ","41 ","42 ","43 ","44 ","45 ","46 "
//    ,"47 ","48 ","49 ","50 ","51 ","52 ","53 ","54 ","55 ","56 ","57 "
//    ,"58 ","59 ","60 ","61 ","62 ","63 ","64 ","65 ","66 ","67 ","68 "
//    ,"69 ","70 ","71 ","72 ","73 ","74 ","75 ","76 ","77 ","78 ","79 "
//    ,"80 ","81 ","82 ","83 ","84 ","85 ","86 ","87 ","88 ","89 ","90 "
//    ,"91 ","92 ","93 ","94 ","95 ","96 ","97 ","98 ","99 ","100 "]
//var maxAge = ["3 ","4 ","5 ","6 ","7 ","8 ","9 ","10 ","11 ","12 ","13 "
//    ,"14 ","15 ","16 ","17 ","18 ","19 ","20 ","21 ","22 ","23 ","24 "
//    ,"25 ","26 ","27 ","28 ","29 ","30 ","31 ","32 ","33 ","34 ","35 "
//    ,"36 ","37 ","38 ","39 ","40 ","41 ","42 ","43 ","44 ","45 ","46 "
//    ,"47 ","48 ","49 ","50 ","51 ","52 ","53 ","54 ","55 ","56 ","57 "
//    ,"58 ","59 ","60 ","61 ","62 ","63 ","64 ","65 ","66 ","67 ","68 "
//    ,"69 ","70 ","71 ","72 ","73 ","74 ","75 ","76 ","77 ","78 ","79 "
//    ,"80 ","81 ","82 ","83 ","84 ","85 ","86 ","87 ","88 ","89 ","90 "
//    ,"91 ","92 ","93 ","94 ","95 ","96 ","97 ","98 ","99 ","100 "]
//var minHeight = ["2\' 0\"","2\' 1\"", "2\' 2\"", "2\' 3\"", "2\' 4\"", "2\' 5\"", "2\' 6\"", "2\' 7\"", "2\' 8\"",
//                 "2\' 9\"", "2\' 10\"", "2\' 11\"", "3\' 0\"", "3\' 1\"", "3\' 2\"", "3\' 3\"", "3\' 4\"", "3\' 5\"", "3\' 6\"", "3\' 7\"", "3\' 8\"",
//                 "3\' 9\"", "3\' 10\"", "3\' 11\"", "4\' 0\"", "4\' 1\"", "4\' 2\"", "4\' 3\"", "4\' 4\"", "4\' 5\"", "4\' 6\"", "4\' 7\"", "4\' 8\"",
//                 "4\' 9\"", "4\' 10\"", "4\' 11\"",
//                 "5\' 0\"", "5\' 1\"", "5\' 2\"", "5\' 3\"", "5\' 4\"", "5\' 5\"", "5\' 6\"", "5\' 7\"", "5\' 8\"",
//                 "5\' 9\"", "5\' 10\"", "5\' 11\"", "6\' 0\"", "6\' 1\"", "6\' 2\"", "6\' 3\"", "6\' 4\"", "6\' 5\"", "6\' 6\"", "6\' 7\"", "6\' 8\"",
//                 "6\' 9\"", "6\' 10\"", "6\' 11\"", "7\' 0\"", "7\' 1\"", "7\' 2\"", "7\' 3\"", "7\' 4\"", "7\' 5\"", "7\' 6\"", "7\' 7\"", "7\' 8\"",
//                 "7\' 9\"", "7\' 10\"", "7\' 11\"", "8\' 0\"", "8\' 1\"", "8\' 2\"", "8\' 3\"", "8\' 4\"", "8\' 5\"", "8\' 6\"", "8\' 7\"", "8\' 8\"",
//                 "8\' 9\"", "8\' 10\"", "8\' 11\"", "9\' 0\""]
//var maxHeight = ["2\' 0\"","2\' 1\"", "2\' 2\"", "2\' 3\"", "2\' 4\"", "2\' 5\"", "2\' 6\"", "2\' 7\"", "2\' 8\"",
//                 "2\' 9\"", "2\' 10\"", "2\' 11\"", "3\' 0\"", "3\' 1\"", "3\' 2\"", "3\' 3\"", "3\' 4\"", "3\' 5\"", "3\' 6\"", "3\' 7\"", "3\' 8\"",
//                 "3\' 9\"", "3\' 10\"", "3\' 11\"", "4\' 0\"", "4\' 1\"", "4\' 2\"", "4\' 3\"", "4\' 4\"", "4\' 5\"", "4\' 6\"", "4\' 7\"", "4\' 8\"",
//                 "4\' 9\"", "4\' 10\"", "4\' 11\"",
//                 "5\' 0\"", "5\' 1\"", "5\' 2\"", "5\' 3\"", "5\' 4\"", "5\' 5\"", "5\' 6\"", "5\' 7\"", "5\' 8\"",
//                 "5\' 9\"", "5\' 10\"", "5\' 11\"", "6\' 0\"", "6\' 1\"", "6\' 2\"", "6\' 3\"", "6\' 4\"", "6\' 5\"", "6\' 6\"", "6\' 7\"", "6\' 8\"",
//                 "6\' 9\"", "6\' 10\"", "6\' 11\"", "7\' 0\"", "7\' 1\"", "7\' 2\"", "7\' 3\"", "7\' 4\"", "7\' 5\"", "7\' 6\"", "7\' 7\"", "7\' 8\"",
//                 "7\' 9\"", "7\' 10\"", "7\' 11\"", "8\' 0\"", "8\' 1\"", "8\' 2\"", "8\' 3\"", "8\' 4\"", "8\' 5\"", "8\' 6\"", "8\' 7\"", "8\' 8\"",
//                 "8\' 9\"", "8\' 10\"", "8\' 11\"", "9\' 0\""]
//var arrGender = ["Select", "Male", "Female","Both"]
//
//
//playerAndTeamTable.isHidden = true
//teamTable.isHidden = true
//
//scrollView.delegate = self
//btnAdd.isHidden = true
//picker.delegate = self
//tfPlayerSport.delegate = self
//tfPlayerPosition.delegate = self
//tfPlayerStrogest.delegate = self
//tfPlayerNationality.delegate = self
//tfPlayerStandard.delegate = self
//
//
//
//playerAndTeamTable.frame.width = view.frame.width
//playerAndTeamTable.delegate = self
//playerAndTeamTable.dataSource = self
//teamTable.delegate = self
//teamTable.dataSource = self
//
//
//tfPlayerGender.inputView = picker
//tfTeamGender.inputView = picker
//tfPlayerMinAge.inputView = picker
//tfPlayerMaxAge.inputView = picker
//tfPlayerMinHeight.inputView = picker
//tfPlayerMaxHeight.inputView = picker
//tfTeamLocation.inputView = picker
//tfPlayerLocation.inputView = picker
//
//btnDone2.isHidden = true
//
//
//
//
//tfPlayerName.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerPosition.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerStrogest.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerStandard.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerNationality.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerMinHeight.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerMaxHeight.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerMinAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerMaxAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfplayerAvailability.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfPlayerLocation.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//
//
//
//tfTeamName.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamStandard.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamLocation.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamGameDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//tfTeamTrainingDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
//
//
//
//tfPlayerMaxAge.isUserInteractionEnabled = true
//
//
//
//@IBAction func segmentChangeOnPress(_ sender: UISegmentedControl) {
//    if playerTeamSegment.selectedSegmentIndex == 0 {
//        scrollView.contentOffset.x = 0
//    }else if playerTeamSegment.selectedSegmentIndex == 1{
//        scrollView.contentOffset.x = view.frame.width
//    }
//}
//
//@IBAction func btnDone2(_ sender: UIButton) {
//    if scrollView.tag != 45 {return}
//    teamTable.isHidden = false
//    //            btnDone2.isHidden = true
//    getTeam()
//
//    btnDone2Height.constant = 0
//
//
//
//}
//@IBAction func doneOnPress(_ sender: UIButton) {
//    if scrollView.tag != 45 {return}
//    playerAndTeamTable.isHidden = false
//    //            btnDone.isHidden = true
//    btnDoneHeight.constant = 0
//    getUser()
//
//
//}
//
//@IBAction func addOnPress(_ sender: UIButton) {
//    let vc = storyboard?.instantiateViewController(withIdentifier: "CreateNewTeamVC") as! CreateNewTeamVC
//    self.navigationController?.pushViewController(vc, animated: true)
//}
//
//@IBAction func refreshOnPress(_ sender: UIButton) {
//    if scrollView.contentOffset.x > 0{
//
//        teamTable.isHidden = true
//        //            btnDone2.isHidden  = false
//        btnDone2Height.constant = 50
//        tfTeamTrainingDay.text = ""
//        tfTeamGender.text = ""
//        tfTeamAge.text = ""
//        tfTeamName.text = ""
//        tfTeamSport.text = ""
//        tfTeamLocation.text = ""
//        tfTeamStandard.text = ""
//        tfTeamGameDay.text = ""
//    } else if scrollView.contentOffset.x == 0 {
//        playerAndTeamTable.isHidden = true
//        //            btnDone.isHidden = false
//        btnDoneHeight.constant = 50
//        tfPlayerName.text = ""
//        tfPlayerSport.text = ""
//        tfPlayerPosition.text = ""
//        tfPlayerStrogest.text = ""
//        tfPlayerStandard.text = ""
//        tfPlayerNationality.text = ""
//        tfPlayerMinHeight.text = ""
//        tfPlayerMinAge.text = ""
//        tfPlayerMaxAge.text = ""
//        tfPlayerMaxHeight.text = ""
//        tfPlayerGender.text = ""
//        tfplayerAvailability.text = ""
//        tfPlayerLocation.text = ""
//    }
//}
//
//func textFieldDidBeginEditing(_ textField: UITextField) {
//    if textField == tfPlayerSport {
//
//    }else if textField == tfPlayerPosition {
//
//    }else if textField == tfPlayerStandard {
//        //             let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
//        //            vc.title = "Standard"
//        //
//        //            self.present(vc,animated: true)
//
//    }else if textField == tfPlayerNationality{
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
//
//        self.present(vc,animated: true)
//
//    }else if textField == tfPlayerStrogest{
//
//    }
//}
//
//func scrollViewDidScroll(_ scrollView: UIScrollView) {
//    if scrollView.tag != 45 {return}
//    if scrollView.contentOffset.x > view.frame.width/2 {
//        btnAdd.isHidden = false
//        btnDone.isHidden = true
//        if teamTable.isHidden == true{
//            btnDone2.isHidden = false
//        }
//
//        playerTeamSegment.selectedSegmentIndex = 1
//    }else{
//        playerTeamSegment.selectedSegmentIndex = 0
//        btnDone2.isHidden = true
//        if playerAndTeamTable.isHidden == true {
//            btnDone.isHidden = false
//
//        }
//        btnAdd.isHidden = true
//    }
//}
//func numberOfComponents(in pickerView: UIPickerView) -> Int {
//    return 1
//}
//
//func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//    if tfPlayerMinHeight.isFirstResponder {
//        return minHeight.count
//    }else if tfPlayerMinAge.isFirstResponder {
//        return minAge.count
//    }else if tfPlayerMaxHeight.isFirstResponder {
//        return maxHeight.count
//    }else if tfPlayerMaxAge.isFirstResponder {
//        return maxAge.count
//    }else if tfTeamGender.isFirstResponder {
//        return arrGender.count
//    }else if tfPlayerGender.isFirstResponder {
//        return arrGender.count
//    }else if tfTeamLocation.isFirstResponder{
//        return teamLocation.count
//    }else if tfPlayerLocation.isFirstResponder{
//        return teamLocation.count
//    }
//    return 0
//}
//func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//    if tfPlayerMinHeight.isFirstResponder {
//        tfPlayerMinHeight.text = minHeight[row]
//    }else if tfPlayerMinAge.isFirstResponder {
//        tfPlayerMinAge.text =  minAge[row]
//    }else if tfPlayerMaxHeight.isFirstResponder {
//        tfPlayerMaxHeight.text =  maxHeight[row]
//    }else if tfPlayerMaxAge.isFirstResponder {
//        tfPlayerMaxAge.text = maxAge[row]
//    }else if tfTeamGender.isFirstResponder {
//        tfTeamGender.text = arrGender[row]
//    }else if tfPlayerGender.isFirstResponder {
//        tfPlayerGender.text = arrGender[row]
//    }else if tfTeamLocation.isFirstResponder {
//        tfTeamLocation.text = teamLocation[row]
//    }else if tfPlayerLocation.isFirstResponder {
//        tfPlayerLocation.text = teamLocation[row]
//    }
//}
//func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//    if tfPlayerMinHeight.isFirstResponder {
//        return minHeight[row]
//    }else if tfPlayerMinAge.isFirstResponder {
//        return minAge[row]
//    }else if tfPlayerMaxHeight.isFirstResponder {
//        return maxHeight[row]
//    }else if tfPlayerMaxAge.isFirstResponder {
//        return maxAge[row]
//    }else if tfTeamGender.isFirstResponder {
//        return arrGender[row]
//    }else if tfPlayerGender.isFirstResponder {
//        return arrGender[row]
//    }else if tfTeamLocation.isFirstResponder{
//        return teamLocation[row]
//    }else if tfPlayerLocation.isFirstResponder{
//        return teamLocation[row]
//    }
//    return nil
//
//}
//
//func getUser(){
//
//    let param = ["key" : Api.key,
//                 "sport_id" : "",
//                 "nickname" : tfPlayerName.text!,
//                 "post_code" : "",
//                 "availability" : "",
//                 "strongest" : "",
//                 "position_id" : "",
//                 "town" : "",
//                 "avaliablityTime[monam]" : "",
//                 "avaliablityTime[monpm]" : "",
//                 "avaliablityTime[tueam]" : "",
//                 "avaliablityTime[tuepm]" : "",
//                 "avaliablityTime[wedam]" : "",
//                 "avaliablityTime[wedpm]" : "",
//                 "avaliablityTime[thuam]" : "",
//                 "avaliablityTime[thupm]" : "",
//                 "avaliablityTime[friam]" : "",
//                 "avaliablityTime[fripm]" : "",
//                 "avaliablityTime[satam]" : "",
//                 "avaliablityTime[satpm]" : "",
//                 "avaliablityTime[sunam]" : "",
//                 "avaliablityTime[sunpm]" : "",
//                 "latitude" : lat,
//                 "longitude" : log,
//                 "distance" : "",
//                 "standard" : "",
//                 "fullName" : "",
//                 "nationality" : "",]
//    Alamofire.request(url.userSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//        if response.error == nil {
//            self.arrTeam.removeAll()
//            let json = JSON(response.value)
//            let dic = json["response"].arrayObject
//
//            for data in dic! {
//                let model = SearchModel.init(withDic: data as! [String : Any])
//                self.arrTeam.append(model)
//            }
//        }
//        self.playerAndTeamTable.reloadData()
//    }
//}
//
//func getTeam(){
//
//    let param = ["key":Api.key, "sport_id":"",
//                 "name":"",
//                 "home_location":"",
//                 "team_gender":"",
//                 "age_group":"",
//                 "gameDay[monam]":"",
//                 "gameDay[monpm]":"",
//                 "gameDay[tueam]":"",
//                 "gameDay[tuepm]":"",
//                 "gameDay[wedam]":"",
//                 "gameDay[wedpm]":"",
//                 "gameDay[thuam]":"",
//                 "gameDay[thupm]":"",
//                 "gameDay[friam]":"",
//                 "gameDay[fripm]":"",
//                 "gameDay[satam]":"",
//                 "gameDay[satpm]":"",
//                 "gameDay[sunam]":"",
//                 "gameDay[sunpm]":"",
//                 "trainingTime[monam]":"",
//                 "trainingTime[monpm]":"",
//                 "trainingTime[tueam]":"",
//                 "trainingTime[tuepm]":"",
//                 "trainingTime[wedam]":"",
//                 "trainingTime[wedpm]":"",
//                 "trainingTime[thuam]":"",
//                 "trainingTime[thupm]":"",
//                 "trainingTime[friam]":"",
//                 "trainingTime[fripm]":"",
//                 "trainingTime[satam]":"",
//                 "trainingTime[satpm]":"",
//                 "trainingTime[sunam]":"",
//                 "trainingTime[sunpm]":"",
//                 "standard":"",
//                 "latitude":lat,
//                 "longitude":log,
//                 "distance":""] as [String : Any]
//    Alamofire.request(url.teamSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//        if response.error == nil {
//            self.arrSearchTeam.removeAll()
//            let json = JSON(response.value as Any)
//            let dic = json["response"].arrayObject
//
//            for data in dic! {
//                let model = JoinTeamModel.init(withDic: data as! [String : Any] )
//                self.arrSearchTeam.append(model)
//            }
//            self.teamTable.reloadData()
//        }
//    }
//}
//
//
//}
//
//extension SearchVC : UITableViewDelegate,UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if scrollView.contentOffset.x > 0 {
//            return arrSearchTeam.count
//        }else {
//            return arrTeam.count
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        if scrollView.contentOffset.x == 0 && tableView == playerAndTeamTable{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! playerAndTeamCell
//
//            cell.lblName.text = arrTeam[indexPath.row].first_name
//            cell.lblPosition.text = arrTeam[indexPath.row].position_name
//            cell.lblsides.text = arrTeam[indexPath.row].sport_name
//            let image = arrTeam[indexPath.row].profile_pic
//            let imgUrlString = imageUrl + "\(image ?? "")"
//            cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
//
//            return cell
//        }else if tableView == teamTable && scrollView.contentOffset.x > 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! TeamSearchCell
//            cell.lblTeamName.text = arrSearchTeam[indexPath.row].team_name
//
//            let image = arrSearchTeam[indexPath.row].team_logo
//            let imgUrlString = imageUrl + "\(image ?? "")"
//            cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
//
//            return cell
//        }
//        return UITableViewCell()
//
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let  vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//
//    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
//        if teamTable.isHidden == false {
//            playerTeamSegment.selectedSegmentIndex = 1
//        }else {
//            playerTeamSegment.selectedSegmentIndex = 0
//        }
//}
