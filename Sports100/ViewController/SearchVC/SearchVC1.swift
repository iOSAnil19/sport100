////
////  SearchVC.swift
////  Sport100
////
////  Created by Vivan Raghuvanshi on 15/02/18.
////  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
////
//
//import UIKit
//
////class SearchVC1: UIViewController,UIScrollViewDelegate {
//
//    //MARK: ------------- VARIABLE/OUTLET ----------------
//    
//    @IBOutlet weak var segment:UISegmentedControl!
//    @IBOutlet weak var playerTable: UITableView!
//    @IBOutlet weak var teamTable: UITableView!
//    
//    @IBOutlet weak var tfSearch: UITextField!
//    
//    @IBOutlet weak var createNewTeam: UIBarButtonItem!
//    
//    var isSearching = false
//    
//    let refreshControl = UIRefreshControl()
//    var selectedFiterType = 0
//
//    
//    var requestUrlString = ""
//    
//    var pageNo = 1
//    var totalPageCount = 1
//    
//    var MonAM = false, TuesAM = false, WednesAM = false, ThursAM = false, FriAM = false, SatAM = false, SunAM = false
//    var MonPM = false, TuesPM = false, WednesPM = false, ThursPM = false, FriPM = false, SatPM = false, SunPM = false
//    var isNickNameFilter = false
//    var selectedPlayerSport:Sport?
//    var selectedPlayerPosition:Position?
//    var selectedContry:String?
//    var selectedStrong:String?
//    
//    
//    var selectedTeamSport:Sport? {
//        didSet {
//            self.allFilteredTeam = self.filterTeamsWithSelectedSport(self.teams)
//            self.teamTable.reloadData()
//        }
//    }
//    
//    var GameMonAM = false, GameTuesAM = false, GameWednesAM = false, GameThursAM = false, GameFriAM = false, GameSatAM = false, GameSunAM = false
//    var GameMonPM = false, GameTuesPM = false, GameWednesPM = false, GameThursPM = false, GameFriPM = false, GameSatPM = false, GameSunPM = false
//    
//    var TrainingMonAM = false, TrainingTuesAM = false, TrainingWednesAM = false, TrainingThursAM = false, TrainingFriAM = false, TrainingSatAM = false, TrainingSunAM = false
//    var TrainingMonPM = false, TrainingTuesPM = false, TrainingWednesPM = false, TrainingThursPM = false, TrainingFriPM = false, TrainingSatPM = false, TrainingSunPM = false
//    
//    var sportTeams = [TeamSport]() {
//        didSet {
//            teams.removeAll()
//            for sportTeam in sportTeams {
//                if let teamsExist = sportTeam.teams, teamsExist.count > 0 {
//                    teams = teams + teamsExist
//                }
//            }
//        }
//    }
//    
//    var delegate:TeamsDelegate?
//    var selectedTeam:Team?
//    
//    var teams = [Team]()
//    var allContacts = [UserData]()
//    
//    var allFilteredTeam = [Team]()
//    var isTeamFiltering = false
//    var isPlayerFiltering = false
//    
//    var searchTeamByPlayer = ""
//
//    @IBOutlet weak var playerView: UIView!
//    @IBOutlet weak var scrolView: UIScrollView!
//    
//    //MARK: ------------- VIEW LIFE CYCLE ----------------
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        scrolView.delegate = self
//    
//        
//        self.navigationItem.leftBarButtonItem = nil
//        self.getGroupedSport()
////        self.teamTable.tableFooterView = UIView(frame: .zero)
//    }
//    
//    
//    func getGroupedSport() {
//        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
//        DispatchQueue.main.async {
//            self.refreshControl.beginRefreshing()
//        }
//        
//        let networkManager = NetworkManager()
//        networkManager.getDataForRequest(Api.getGoupedSports, andParameter: params) { (response, error) in
//            DispatchQueue.main.async {
//                self.refreshControl.endRefreshing()
//                hideHud()
//            }
//            
//            if error == nil {
//                if let result = response as? [String:Any] {
//                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
//                    
//                    if success == 200 {
//                        
//                        if let allGroupedTeams = result["sports"] as? [[String:Any]], allGroupedTeams.count > 0 {
//                            let groupedTeams = allGroupedTeams.map({ (sportInfo) -> TeamSport in
//                                TeamSport(object: sportInfo)
//                            })
//                            self.sportTeams.removeAll()
//                            self.sportTeams = groupedTeams
//                            
//                            if self.teamTable != nil {
//                                self.teamTable.reloadData()
//                            }
//                        }
//                    } else {
//                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
//                        showAlert(ERROR, message: error.capitalized, onView: self)
//                    }
//                } else {
//                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
//                }
//            } else {
//                showAlert(ERROR, message: error?.localizedDescription.capitalized ?? SOMETHING_WRONG, onView: self)
//            }
//        }
//    }
//    override func viewWillAppear(_ animated: Bool) {
//       
//        
//    }
//    @objc func refreshMethod() {
//        self.pageNo = 1
//        self.getGroupedSport()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    //MARK: ------------- UIBUTTON ACTION METHODS ----------------
//    
//    @IBAction func buttonClicked_Filter(_ seder: UIButton) {
//        if selectedFiterType == 0 {
//            if let playerFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayerFilterVC") as? PlayerFilterVC {
//                playerFilterVC.delegate = self
//                self.navigationController?.pushViewController(playerFilterVC, animated: true)
//            }
//        } else {
//            if let teamFilterVC = self.storyboard?.instantiateViewController(withIdentifier: "TeamFilterVC") as? TeamFilterVC {
//                teamFilterVC.delegate = self
//                self.navigationController?.pushViewController(teamFilterVC, animated: true)
//            }
//        }
//        self.resetAllValues()
//    }
//    
//    @IBAction func buttonClicked_Refresh(_ seder: UIButton) {
//        self.resetAllValues()
//    }
//    
//    func resetAllValues() {
//        
//        MonAM = false; TuesAM = false; WednesAM = false; ThursAM = false; FriAM = false; SatAM = false; SunAM = false
//        MonPM = false; TuesPM = false; WednesPM = false; ThursPM = false; FriPM = false; SatPM = false; SunPM = false
//        
//        GameMonAM = false; GameTuesAM = false; GameWednesAM = false; GameThursAM = false; GameFriAM = false; GameSatAM = false; GameSunAM = false
//        GameMonPM = false; GameTuesPM = false; GameWednesPM = false; GameThursPM = false; GameFriPM = false; GameSatPM = false; GameSunPM = false
//        
//        TrainingMonAM = false; TrainingTuesAM = false; TrainingWednesAM = false; TrainingThursAM = false; TrainingFriAM = false; TrainingSatAM = false; TrainingSunAM = false
//        TrainingMonPM = false; TrainingTuesPM = false; TrainingWednesPM = false; TrainingThursPM = false; TrainingFriPM = false; TrainingSatPM = false; TrainingSunPM = false
//        
//        self.selectedTeamSport = nil
//        self.selectedPlayerSport = nil
//        self.isTeamFiltering = false
//        self.isPlayerFiltering = false
//    }
//    
//    @objc func buttonClicked_AddTeam() {
//        if let addTeamVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.AddTeamVC) as? AddTeamVC {
//            addTeamVC.delegate = self
//            self.navigationController?.pushViewController(addTeamVC, animated: true)
//        }
//    }
//    
//    @IBAction func segmentClicked(_ sender:UISegmentedControl) {
//        
//        self.isTeamFiltering = false
//        self.isPlayerFiltering = false
//        self.playerTable.reloadData()
//        self.teamTable.reloadData()
//        
//        self.view.endEditing(true)
//        tfSearch.text = nil
//        tfSearch.placeholder = sender.selectedSegmentIndex == 0 ? "Search Player.." : "Search Team.."
//        selectedFiterType = sender.selectedSegmentIndex
//        self.showPlayersAndHideTeams(sender.selectedSegmentIndex == 0)
//        self.navigationItem.leftBarButtonItem = sender.selectedSegmentIndex == 0 ? nil : self.setCreateTeamButton()
//    }
//    
//    private func setCreateTeamButton() -> UIBarButtonItem {
//        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(buttonClicked_AddTeam))
//        return cancelButton
//    }
//    
//    private func showPlayersAndHideTeams(_ shouldBe: Bool) {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.playerTable.alpha = shouldBe ? 1:0
//            self.teamTable.alpha = shouldBe ? 0:1
//        }) { (done) in }
//    }
//    
//    func setUpView(_ sportId:String) -> (String,UIImage) {
//        var text = ""
//        var img  = #imageLiteral(resourceName: "netball")
//        
//        switch sportId {
//        case "1":
//            text = "Football 5 a side"
//            img = #imageLiteral(resourceName: "football5")
//            break
//        case "2":
//            text = "Football 7 a side"
//            img = #imageLiteral(resourceName: "football7")
//            break
//        case "3":
//            text = "Football 11 a side"
//            img = #imageLiteral(resourceName: "football11")
//            break
//        case "4":
//            text = "Basketball"
//            img = #imageLiteral(resourceName: "basketball")
//            break
//        case "5":
//            text = "Netball"
//            img = #imageLiteral(resourceName: "netball")
//            break
//        default:
//            break
//        }
//        return (text, img)
//    }
//    
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        guard let text = textField.text else { return true }
//        guard let textRange = Range(range, in: text) else { return true }
//        let updatedText = text.replacingCharacters(in: textRange, with: string)
//        
//        if self.playerTable.alpha == 0 {
//            let text = updatedText
//            let isEmpty = text.trimmingCharacters(in: .whitespaces).isEmpty
//            if text == "" || isEmpty == true {
//                self.allFilteredTeam.removeAll()
//                self.isTeamFiltering = false
//                self.teamTable.reloadData()
//            } else {
//                self.isTeamFiltering = true
//                self.searchText(text)
//            }
//        } else {
//            self.filterPlayerWithText(updatedText)
//        }
//        return true
//    }
//    
//    private func filterPlayerWithText(_ name: String) {
//        
//        var params = ["key": Api.key]
//        if let sport = self.selectedPlayerSport {
//            params["sport_id"] = sport.id
//        }
//        if let position = self.selectedPlayerPosition {
//            params["position_id"] = position.id
//        }
//        if isNickNameFilter == true {
//            params["nickname"] = name
//        } else {
//            params["fullName"] = name
//        }
//        params["availability"] = "Yes"
//        if let strong = self.selectedStrong {
//            params["strongest"] = strong
//        }
//        params["avaliablityTime[monam]"] = MonAM ? "AM" : ""
//        params["avaliablityTime[monpm]"] = MonPM ? "PM" : ""
//        params["avaliablityTime[tueam]"] = TuesAM ? "AM" : ""
//        params["avaliablityTime[tuepm]"] = TuesPM ? "PM" : ""
//        params["avaliablityTime[wedam]"] = WednesAM ? "AM" : ""
//        params["avaliablityTime[wedpm]"] = WednesPM ? "PM" : ""
//        params["avaliablityTime[thuam]"] = ThursAM ? "AM" : ""
//        params["avaliablityTime[thupm]"] = ThursPM ? "PM" : ""
//        params["avaliablityTime[friam]"] = FriAM ? "AM" : ""
//        params["avaliablityTime[fripm]"] = FriPM ? "PM" : ""
//        params["avaliablityTime[satam]"] = SatAM ? "AM" : ""
//        params["avaliablityTime[satpm]"] = SatPM ? "PM" : ""
//        params["avaliablityTime[sunam]"] = SunAM ? "AM" : ""
//        params["avaliablityTime[sunpm]"] = SunPM ? "PM" : ""
//        
//        let networkManager = NetworkManager()
//        networkManager.getDataForRequest(Api.searchUser, andParameter: params) { (response, error) in
//            if error == nil {
//                if let result = response as? [String:Any] {
//                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
//                    if success == 200 {
//                        if let allUsers = result["response"] as? [[String:Any]], allUsers.count > 0 {
//                            let users = allUsers.map({ (usersInfo) -> UserData in
//                                UserData.init(usersInfo)
//                            })
//                            self.allContacts = users
//                            self.playerTable.reloadData()
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
//
////MARK: ------------- UITableView DataSource METHODS ----------------
//
//extension SearchVC : UITableViewDataSource {
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//        if tableView == playerTable {
//            return self.allContacts.count
//        } else {
//            return isTeamFiltering ? self.allFilteredTeam.count : self.sportTeams.count
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        var cell: UITableViewCell?
//        
//        if tableView == playerTable {
//            let cellId = "ContactsCell"
//            cell = tableView.dequeueReusableCell(withIdentifier:cellId)
//            
//            let contact = self.allContacts[indexPath.row]
//            
//            if let label = cell?.contentView.viewWithTag(1000) as? UILabel {
//                label.text = contact.firstName + " " + contact.lastName
//            }
//            if let img = cell?.contentView.viewWithTag(1001) as? UIImageView {
//                let imgUrlString = imageUrl + "\(String(describing: contact.userImage))"
//                img.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "place_holder"), options:.continueInBackground, completed: nil)
//            }
//
//        } else {
//            if isTeamFiltering {
//                let cellId = "TeamCell"
//                let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? TeamCell
//                let team = allFilteredTeam[indexPath.row]
//                cell?.setUPTeamCell(team)
//                cell?.selectionStyle = .none
//                return cell ?? UITableViewCell()
//            } else {
//                let cellId = "TeamSportCell"
//                cell = tableView.dequeueReusableCell(withIdentifier:cellId)
//                
//                let sport = self.sportTeams[indexPath.row]
//                
//                if let sportId = sport.iD {
//                    
//                    let (text, img) = self.setUpView(sportId)
//                    
//                    if let lbl = cell?.contentView.viewWithTag(1000) as? UILabel {
//                        lbl.text = text
//                    }
//                    if let imgView = cell?.contentView.viewWithTag(1001) as? UIImageView {
//                        imgView.image = img
//                    }
//                }
//            }
//            
//        }
//        cell?.selectionStyle = .none
//        return cell ?? UITableViewCell()
//    }
//}
//
////MARK: ------------- UITableView Delegate METHODS ----------------
//
//extension SearchVC : UITableViewDelegate {
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == playerTable {
//            return 60 * getScaleFactor()
//        }
//        let height: CGFloat = isTeamFiltering ? 80.0:70.0
//        return height * getScaleFactor()
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//
//        if tableView == playerTable {
//            let contact = self.allContacts[indexPath.row]
//
//            if let profileVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardID.ProfileVC) as? ProfileVC {
//                profileVC.otherUserId = contact.userId
//                //profileVC.userModel = contact
//                self.navigationController?.pushViewController(profileVC, animated: true)
//            }
//        } else {
//            if isTeamFiltering {
//                let team = teams[indexPath.row]
//                let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamDetailViewController", storyboardName:"Main") as! TeamDetailViewController
//                vc.team = team
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc,animated:true)
//            } else {
//                let sport = self.sportTeams[indexPath.row]
//                
//                let vc  = Utility.instantiateViewControllerWithIdentifier(identifier:"TeamsVC", storyboardName:"Main") as! TeamsVC
//                vc.selectedSport = sport
//                vc.updateDelegate = self
//                vc.delegate = self.delegate
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc,animated:true)
//            }
//        }
//    }
//}
//
//extension SearchVC: AddTeamDelegate {
//    func teamEdited(_ team: Team) {
//        
//    }
//    
//    func teamAdded() {
//        self.refreshMethod()
//        
//        if let tabbarVC = self.navigationController?.tabBarController, tabbarVC.viewControllers?.count != 0 {
//            if let feedsNavVC = tabbarVC.viewControllers?.first as? UINavigationController, feedsNavVC.viewControllers.count > 0 {
//                if let feedsVC = feedsNavVC.viewControllers[0] as? FeedsVC {
//                    feedsVC.refreshMethod()
//                    tabbarVC.selectedIndex = 0
//                }
//            }
//        }
//    }
//}
//
//extension SearchVC: TeamUpdateDelegate {
//    func matchAdded() {
//        self.refreshMethod()
//    }
//    
//    func teamEdited() {
//        self.refreshMethod()
//    }
//    
//    func teamRemoved() {
//        self.refreshMethod()
//    }
//}
//
////MARK: ------------- Filter MEthods ----------------
//
//extension SearchVC {
//    
//    func searchText(_ text:String) {
//        
//        if teams.count > 0 {
//            
//            let filterTeams = self.teams.filter({ (team) -> Bool in
//                if let name = team.name {
//                    let lowerName = name.lowercased()
//                    let searchText = text.lowercased()
//                    return lowerName.hasPrefix(searchText)
//                }
//                return false
//            })
//            self.filterFromAllTeamListing(filterTeams)
//        }
//    }
//    
//    func filterFromAllTeam() {
//        self.filterFromAllTeamListing(self.teams)
//    }
//    
//    private func filterFromAllTeamListing(_ t:[Team]) {
//        var allTeams = t
//        allTeams = self.filterTeamsWithSelectedSport(allTeams)
//        allTeams = self.filterTeamsWithGameDay(allTeams)
//        allTeams = self.filterTeamsWithTrainningTime(allTeams)
//        self.allFilteredTeam = allTeams
//        self.teamTable.reloadData()
//    }
//    
//    private func filterTeamsWithSelectedSport(_ teams:[Team]) -> [Team] {
//        var allTeams = teams
//        
//        if let selectedSport = self.selectedTeamSport {
//            self.isTeamFiltering = true
//            let sportViseFilterTeams = teams.filter({ (team) -> Bool in
//                if let sport = team.sport {
//                    if let id = sport.iD {
//                        print(id)
//                        return id == selectedSport.id
//                    }
//                }
//                return false
//            })
//            allTeams = sportViseFilterTeams
//        }
//        return allTeams
//    }
//    
//    private func filterTeamsWithGameDay(_ teams:[Team]) -> [Team] {
//        var filteredTeam = [Team]()
//        filteredTeam = teams
//        
//        if self.GameSunAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sun", "AM")
//        }
//        if self.GameSunPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sun", "PM")
//        }
//        if self.GameMonAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Mon", "AM")
//        }
//        if self.GameMonPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Mon", "PM")
//        }
//        if self.GameTuesAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Tue", "AM")
//        }
//        if self.GameTuesPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Tue", "PM")
//        }
//        if self.GameWednesAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Wed", "AM")
//        }
//        if self.GameWednesPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Wed", "PM")
//        }
//        if self.GameThursAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Thu", "AM")
//        }
//        if self.GameThursPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Thu", "PM")
//        }
//        if self.GameFriAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Fri", "AM")
//        }
//        if self.GameFriPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Fri", "PM")
//        }
//        if self.GameSatAM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sat", "AM")
//        }
//        if self.GameSatPM {
//            filteredTeam = self.checkAvailabilityOfGameDays(filteredTeam, "Sat", "PM")
//        }
//        return filteredTeam
//    }
//    
//    private func checkAvailabilityOfGameDays(_ teams:[Team],_ day:String,_ time:String) -> [Team] {
//        let availableTeams = teams.filter { (team) -> Bool in
//            if let gameDays = team.gameDay, gameDays.count > 0 {
//                let gameDaysAvailable = gameDays.filter({ (gameDay) -> Bool in
//                    if let dayString = gameDay.day {
//                        if let timeString = gameDay.time {
//                            return dayString == day && timeString == time
//                        }
//                    }
//                    return false
//                })
//                if gameDaysAvailable.count > 0 {
//                    return true
//                }
//            }
//            return false
//        }
//        return availableTeams.count > 0 ? availableTeams : teams
//    }
//    
//    private func filterTeamsWithTrainningTime(_ teams:[Team]) -> [Team] {
//        var filteredTeam = [Team]()
//        filteredTeam = teams
//        
//        if self.TrainingSunAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sun", "AM")
//        }
//        if self.TrainingSunPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sun", "PM")
//        }
//        if self.TrainingMonAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Mon", "AM")
//        }
//        if self.TrainingMonPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Mon", "PM")
//        }
//        if self.TrainingTuesAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Tue", "AM")
//        }
//        if self.TrainingTuesPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Tue", "PM")
//        }
//        
//        if self.TrainingWednesAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Wed", "AM")
//        }
//        if self.TrainingWednesPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Wed", "PM")
//        }
//        if self.TrainingThursAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Thu", "AM")
//        }
//        if self.TrainingThursPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Thu", "PM")
//        }
//        
//        if self.TrainingFriAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Fri", "AM")
//        }
//        if self.TrainingFriPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Fri", "PM")
//        }
//        if self.TrainingSatAM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sat", "AM")
//        }
//        if self.TrainingSatPM {
//            filteredTeam = self.checkAvailabilityOfTrainningDays(filteredTeam, "Sat", "PM")
//        }
//        
//        return filteredTeam
//    }
//    
//    private func checkAvailabilityOfTrainningDays(_ teams:[Team],_ day:String,_ time:String) -> [Team] {
//        let availableTeams = teams.filter { (team) -> Bool in
//            if let trainningDays = team.traingTime, trainningDays.count > 0 {
//                let trainningDaysAvailable = trainningDays.filter({ (trainningDayAvailable) -> Bool in
//                    if let trainningday = trainningDayAvailable.day {
//                        if let trainningtime = trainningDayAvailable.time {
//                            return trainningday == day && trainningtime == time
//                        }
//                    }
//                    return false
//                })
//                if trainningDaysAvailable.count > 0 {
//                    return true
//                }
//            }
//            return false
//        }
//        return availableTeams.count > 0 ? availableTeams : teams
//    }
//}
