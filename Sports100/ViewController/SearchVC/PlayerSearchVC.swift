//
//  PlayerSearchVC.swift
//  Sports100
//
//  Created by VeeraJain on 21/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import LocationPickerViewController
import CarbonKit

enum Type {
    case sports
    case postion
    case strongest
    case standard
    case nationality
    case age
    case country
    case gameDay
    case traningDay
    case searchPlayer
    case editTeam
    case team
}

class PlayerSearchVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,GetIndexCount,didSelectdData{
    func indexCount(index: Int, type: Type, id: String, name: String)
    {
        if tfPlayerSport.isFirstResponder || type == .sports {
            if index == 0 {
            tfPlayerSport.text = ""
            }else{
            tfPlayerSport.text = " < \(index)  Entries >"
            }
            sportsID = id
            
        }else  if tfPlayerPosition.isFirstResponder || type == .postion {
            if index == 0 {
            tfPlayerPosition.text = ""
            }else{
            tfPlayerPosition.text = " < \(index)  Entries >"
            }
            
            selectedPosition = id
            
            
        }
        }
    
  
    
    func textField(textfield: String, type: Type, id: String) {
        if tfPlayerStandard.isFirstResponder || type == Type.standard{
            tfPlayerStandard.text = textfield
            standerdID = id
        }else if tfPlayerStrogest.isFirstResponder || type == Type.strongest{
            tfPlayerStrogest.text = textfield
        }
    }
    
    @IBOutlet weak var doneHeight: NSLayoutConstraint!
    @IBOutlet weak var tfPlayerName: UITextField!
    @IBOutlet weak var tfPlayerSport: UITextField!
    @IBOutlet weak var tfPlayerPosition: UITextField!
    @IBOutlet weak var tfPlayerStandard: UITextField!
    @IBOutlet weak var tfPlayerNationality: UITextField!
    @IBOutlet weak var tfPlayerMinHeight: UITextField!
    @IBOutlet weak var tfPlayerMaxHeight: UITextField!
    @IBOutlet weak var tfPlayerMinAge: UITextField!
    @IBOutlet weak var tfPlayerLocation: UITextField!
    @IBOutlet weak var tfplayerAvailability: UITextField!
    @IBOutlet weak var tfPlayerGender: UITextField!
    @IBOutlet weak var tfPlayerMaxAge: UITextField!
    @IBOutlet weak var tfPlayerStrogest: UITextField!
    
    var selectedPosition: String!
    var searchPlayer = DayModel()
    var arrPlayersAvlability = [String]()
    var countrys = [String]()
    var arrTeam = [SearchModel]()
    let  lat = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.latitude))"
    let log = "\(String(describing: locationManeger.shared.locManager.location?.coordinate.longitude))"
    var picker = UIPickerView()
    var teamLocation = ["Within 1 mile","Within 5 mile","Within 10 mile","Within 20 mile","National"]
    var standerdID = ""
    var sportsID = ""
    var countrysName = ""
    var minAge = ["3 ","4 ","5 ","6 ","7 ","8 ","9 ","10 ","11 ","12 ","13 "
        ,"14 ","15 ","16 ","17 ","18 ","19 ","20 ","21 ","22 ","23 ","24 "
        ,"25 ","26 ","27 ","28 ","29 ","30 ","31 ","32 ","33 ","34 ","35 "
        ,"36 ","37 ","38 ","39 ","40 ","41 ","42 ","43 ","44 ","45 ","46 "
        ,"47 ","48 ","49 ","50 ","51 ","52 ","53 ","54 ","55 ","56 ","57 "
        ,"58 ","59 ","60 ","61 ","62 ","63 ","64 ","65 ","66 ","67 ","68 "
        ,"69 ","70 ","71 ","72 ","73 ","74 ","75 ","76 ","77 ","78 ","79 "
        ,"80 ","81 ","82 ","83 ","84 ","85 ","86 ","87 ","88 ","89 ","90 "
        ,"91 ","92 ","93 ","94 ","95 ","96 ","97 ","98 ","99 ","100 "]
    var maxAge = ["3 ","4 ","5 ","6 ","7 ","8 ","9 ","10 ","11 ","12 ","13 "
        ,"14 ","15 ","16 ","17 ","18 ","19 ","20 ","21 ","22 ","23 ","24 "
        ,"25 ","26 ","27 ","28 ","29 ","30 ","31 ","32 ","33 ","34 ","35 "
        ,"36 ","37 ","38 ","39 ","40 ","41 ","42 ","43 ","44 ","45 ","46 "
        ,"47 ","48 ","49 ","50 ","51 ","52 ","53 ","54 ","55 ","56 ","57 "
        ,"58 ","59 ","60 ","61 ","62 ","63 ","64 ","65 ","66 ","67 ","68 "
        ,"69 ","70 ","71 ","72 ","73 ","74 ","75 ","76 ","77 ","78 ","79 "
        ,"80 ","81 ","82 ","83 ","84 ","85 ","86 ","87 ","88 ","89 ","90 "
        ,"91 ","92 ","93 ","94 ","95 ","96 ","97 ","98 ","99 ","100 "]
    var minHeight = ["2\' 0\"","2\' 1\"", "2\' 2\"", "2\' 3\"", "2\' 4\"", "2\' 5\"", "2\' 6\"", "2\' 7\"", "2\' 8\"",
                     "2\' 9\"", "2\' 10\"", "2\' 11\"", "3\' 0\"", "3\' 1\"", "3\' 2\"", "3\' 3\"", "3\' 4\"", "3\' 5\"", "3\' 6\"", "3\' 7\"", "3\' 8\"",
                     "3\' 9\"", "3\' 10\"", "3\' 11\"", "4\' 0\"", "4\' 1\"", "4\' 2\"", "4\' 3\"", "4\' 4\"", "4\' 5\"", "4\' 6\"", "4\' 7\"", "4\' 8\"",
                     "4\' 9\"", "4\' 10\"", "4\' 11\"",
                     "5\' 0\"", "5\' 1\"", "5\' 2\"", "5\' 3\"", "5\' 4\"", "5\' 5\"", "5\' 6\"", "5\' 7\"", "5\' 8\"",
                     "5\' 9\"", "5\' 10\"", "5\' 11\"", "6\' 0\"", "6\' 1\"", "6\' 2\"", "6\' 3\"", "6\' 4\"", "6\' 5\"", "6\' 6\"", "6\' 7\"", "6\' 8\"",
                     "6\' 9\"", "6\' 10\"", "6\' 11\"", "7\' 0\"", "7\' 1\"", "7\' 2\"", "7\' 3\"", "7\' 4\"", "7\' 5\"", "7\' 6\"", "7\' 7\"", "7\' 8\"",
                     "7\' 9\"", "7\' 10\"", "7\' 11\"", "8\' 0\"", "8\' 1\"", "8\' 2\"", "8\' 3\"", "8\' 4\"", "8\' 5\"", "8\' 6\"", "8\' 7\"", "8\' 8\"",
                     "8\' 9\"", "8\' 10\"", "8\' 11\"", "9\' 0\""]
    var maxHeight = ["2\' 0\"","2\' 1\"", "2\' 2\"", "2\' 3\"", "2\' 4\"", "2\' 5\"", "2\' 6\"", "2\' 7\"", "2\' 8\"",
                     "2\' 9\"", "2\' 10\"", "2\' 11\"", "3\' 0\"", "3\' 1\"", "3\' 2\"", "3\' 3\"", "3\' 4\"", "3\' 5\"", "3\' 6\"", "3\' 7\"", "3\' 8\"",
                     "3\' 9\"", "3\' 10\"", "3\' 11\"", "4\' 0\"", "4\' 1\"", "4\' 2\"", "4\' 3\"", "4\' 4\"", "4\' 5\"", "4\' 6\"", "4\' 7\"", "4\' 8\"",
                     "4\' 9\"", "4\' 10\"", "4\' 11\"",
                     "5\' 0\"", "5\' 1\"", "5\' 2\"", "5\' 3\"", "5\' 4\"", "5\' 5\"", "5\' 6\"", "5\' 7\"", "5\' 8\"",
                     "5\' 9\"", "5\' 10\"", "5\' 11\"", "6\' 0\"", "6\' 1\"", "6\' 2\"", "6\' 3\"", "6\' 4\"", "6\' 5\"", "6\' 6\"", "6\' 7\"", "6\' 8\"",
                     "6\' 9\"", "6\' 10\"", "6\' 11\"", "7\' 0\"", "7\' 1\"", "7\' 2\"", "7\' 3\"", "7\' 4\"", "7\' 5\"", "7\' 6\"", "7\' 7\"", "7\' 8\"",
                     "7\' 9\"", "7\' 10\"", "7\' 11\"", "8\' 0\"", "8\' 1\"", "8\' 2\"", "8\' 3\"", "8\' 4\"", "8\' 5\"", "8\' 6\"", "8\' 7\"", "8\' 8\"",
                     "8\' 9\"", "8\' 10\"", "8\' 11\"", "9\' 0\""]
    var arrGender = ["Select", "Male", "Female","Both"]
    @IBOutlet weak var playerTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
            playerTable.delegate = self
            playerTable.dataSource = self
            playerTable.isHidden = true
        
       playerTable.tableFooterView = UIView()
        
        tfplayerAvailability.delegate = self
        picker.delegate = self
        picker.dataSource = self
        tfPlayerSport.delegate = self
        tfPlayerPosition.delegate = self
        tfPlayerStrogest.delegate = self
        tfPlayerNationality.delegate = self
        tfPlayerStandard.delegate = self
        tfPlayerLocation.delegate = self
        tfPlayerName.delegate = self
        
        
        tfPlayerGender.inputView = picker
        
        tfPlayerMinAge.inputView = picker
        tfPlayerMaxAge.inputView = picker
        tfPlayerMinHeight.inputView = picker
        tfPlayerMaxHeight.inputView = picker
        
        tfPlayerLocation.inputView = picker
        
        
        tfPlayerName.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerPosition.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerStrogest.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerStandard.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerNationality.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerMinHeight.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerMaxHeight.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerMinAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerMaxAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfplayerAvailability.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfPlayerLocation.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isToolbarHidden = false
        self.navigationItem.hidesBackButton = false
        
        
        arrPlayersAvlability = searchPlayer.arrName
        let stringRepresentation = arrPlayersAvlability.joined(separator: ",")
        tfplayerAvailability.text = stringRepresentation
        
//        if let parent = self.parent as? SearchScrollerVC {
//            parent.selectedIndex = 0
//        }
    }


    func getUser(){
        showHud("")
        isValid()
        let param = ["key" : Api.key,
                     "sport_id" : sportsID,
                     "nickname" : "",
                     "post_code" : "",
                     "availability" : "",
                     "strongest" : tfPlayerStrogest.text ?? "",
                     "position_id" : selectedPosition ?? "",
                     "town" : "",
                     "avaliablityTime[monam]" : searchPlayer.monAM == true ? "AM" : "",
                     "avaliablityTime[monpm]" : searchPlayer.monPM == true ? "PM" : "",
                     "avaliablityTime[tueam]" : searchPlayer.tusAM == true ? "AM" : "",
                     "avaliablityTime[tuepm]" : searchPlayer.tusPM == true ? "PM" : "",
                     "avaliablityTime[wedam]" : searchPlayer.wedAM == true ? "AM" : "",
                     "avaliablityTime[wedpm]" : searchPlayer.wedPM == true ? "PM" : "",
                     "avaliablityTime[thuam]" : searchPlayer.thrAM == true ? "AM" : "",
                     "avaliablityTime[thupm]" : searchPlayer.thrPM == true ? "PM" : "",
                     "avaliablityTime[friam]" : searchPlayer.friAM == true ? "AM" : "",
                     "avaliablityTime[fripm]" : searchPlayer.friPM == true ? "PM" : "",
                     "avaliablityTime[satam]" : searchPlayer.satAM == true ? "AM" : "",
                     "avaliablityTime[satpm]" : searchPlayer.satPM == true ? "PM" : "",
                     "avaliablityTime[sunam]" : searchPlayer.sunAM == true ? "AM" : "",
                     "avaliablityTime[sunpm]" : searchPlayer.monPM == true ? "AM" : "",
                     "latitude" : lat,
                     "longitude" : log,
                     "distance" : "",
                     "standard" : standerdID,
                     "fullName" : tfPlayerName.text ?? "" ,
                     "nationality" : countrysName 
        ]as [String : Any]
        
        print(param)
        Alamofire.request(url.userSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.error == nil {
                hideHud()
                self.arrTeam.removeAll()
                if let responseObj = response.value as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        hideHud()
                
                let json = JSON(response.value as Any)
                let dic = json["response"].arrayObject
                for data in dic! {
                    let model = SearchModel.init(withDic: data as! [String : Any])
                    self.arrTeam.append(model)
                }
                        if self.arrTeam.count == 0 {
                               self.playerTable.isHidden = true
                            self.doneHeight.constant = 50
                        }
                    }else {
                        hideHud()
                          let message = String("\(String(describing: responseObj[NetworkManager.kMESSAGE] ?? "0"))")
                        self.playerTable.isHidden = true
                        self.doneHeight.constant = 50
                        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        
//                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: ))
                        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (alert) in
                            if let parent = self.parent  {
                                let search = parent as! SearchScrollerVC
                                DispatchQueue.main.async {
                                    search.navigationItem.rightBarButtonItem =  nil
                                }
                                
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                    else {
                    self.playerTable.isHidden = true
                    self.doneHeight.constant = 50
                    let alert = UIAlertController(title: "No Data Found", message: response.error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    if let parent = self.parent  {
                        let search = parent as!SearchScrollerVC
                        DispatchQueue.main.async {
                            search.navigationItem.rightBarButtonItem =  nil
                        }
                        
                    }
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                }
            self.playerTable.reloadData()
        }
    }
    }
    @IBAction func refreshOnPress(_ sender: UIButton) {
        doneHeight.constant = 50
        playerTable.isHidden = true
    }
    
    @IBAction func doneOnPress(_ sender: UIButton) {
//
        doneHeight.constant = 0
        arrTeam.removeAll()
        playerTable.reloadData()
        getUser()
        playerTable.isHidden = false
        if let parent = self.parent  {
            let search = parent as! SearchScrollerVC
            DispatchQueue.main.async {
                search.navigationItem.rightBarButtonItem =  search.filterBtn
            }

        }
//         isValid()
//        let param = ["key" : Api.key,
//                     "sport_id" : sportsID,
//                     "nickname" : "",
//                     "post_code" : "",
//                     "availability" : "",
//                     "strongest" : tfPlayerStrogest.text ?? "",
//                     "position_id" : selectedPosition ?? "",
//                     "town" : "",
//                     "avaliablityTime[monam]" : searchPlayer.monAM == true ? "AM" : "",
//                     "avaliablityTime[monpm]" : searchPlayer.monPM == true ? "PM" : "",
//                     "avaliablityTime[tueam]" : searchPlayer.tusAM == true ? "AM" : "",
//                     "avaliablityTime[tuepm]" : searchPlayer.tusPM == true ? "PM" : "",
//                     "avaliablityTime[wedam]" : searchPlayer.wedAM == true ? "AM" : "",
//                     "avaliablityTime[wedpm]" : searchPlayer.wedPM == true ? "PM" : "",
//                     "avaliablityTime[thuam]" : searchPlayer.thrAM == true ? "AM" : "",
//                     "avaliablityTime[thupm]" : searchPlayer.thrPM == true ? "PM" : "",
//                     "avaliablityTime[friam]" : searchPlayer.friAM == true ? "AM" : "",
//                     "avaliablityTime[fripm]" : searchPlayer.friPM == true ? "PM" : "",
//                     "avaliablityTime[satam]" : searchPlayer.satAM == true ? "AM" : "",
//                     "avaliablityTime[satpm]" : searchPlayer.satPM == true ? "PM" : "",
//                     "avaliablityTime[sunam]" : searchPlayer.sunAM == true ? "AM" : "",
//                     "avaliablityTime[sunpm]" : searchPlayer.monPM == true ? "AM" : "",
//                     "latitude" : lat,
//                     "longitude" : log,
//                     "distance" : "",
//                     "standard" : standerdID,
//                     "fullName" : tfPlayerName.text ?? "" ,
//                     "nationality" : countrysName
//        ] as [String :Any]
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SportsDataTableVC") as! SportsDataTableVC
//        vc.param = param
////        vc.modalPresentationStyle = .overCurrentContext
//        vc.isBool = true
//        self.present(vc, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        
        if textField == tfPlayerSport {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
            vc.tittle = "Sports"
//            vc.isBool = true
            vc.delegate = self
            vc.type = Type.sports
            self.present(vc,animated: true)
        }else if textField == tfPlayerPosition {
            if sportsID == "" {
                    showAlertMessage("First choose sports ")
            }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
            vc.tittle = "Postion"
            vc.type = Type.postion
            vc.sportsID = sportsID
            vc.delegate = self
            self.present(vc,animated: true)
            }
            
        }else if textField == tfPlayerStandard {
                         let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
                            vc.lblHeader = "Standard"
                            vc.type = Type.standard
                            vc.delegate = self
            
                        self.present(vc,animated: true)
            
        }else if textField == tfPlayerNationality{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
            vc.delegate1 = self
            vc.type = Type.nationality
            self.present(vc,animated: true)
            
        }else if textField == tfPlayerStrogest{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.lblHeader = "Strongest Side"
            vc.type = Type.strongest
            vc.delegate = self
            
            self.present(vc,animated: true)
        }else if textField == tfplayerAvailability{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.lblHeader = "Availability"
            vc.type = Type.searchPlayer
            vc.delegate1 = self
            
            self.present(vc,animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrTeam.count
        }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! playerAndTeamCell
        if let name = arrTeam[indexPath.row].first_name {
            cell.lblName.text = name
        }
        
            cell.lblPosition.text = arrTeam[indexPath.row].position_name
            cell.lblsides.text = arrTeam[indexPath.row].sport_name
            let image = arrTeam[indexPath.row].profile_pic
            let imgUrlString = imageUrl + "\(image ?? "")"
            cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
        
            return cell
       
        }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.otherUserId = arrTeam[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if tfPlayerMinHeight.isFirstResponder {
            return minHeight.count
        }else if tfPlayerMinAge.isFirstResponder {
            return minAge.count
        }else if tfPlayerMaxHeight.isFirstResponder {
            return maxHeight.count
        }else if tfPlayerMaxAge.isFirstResponder {
            return maxAge.count
        }else if tfPlayerGender.isFirstResponder {
            return arrGender.count
        }else if tfPlayerLocation.isFirstResponder{
            return teamLocation.count
        }
        return 0
    }
  
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tfPlayerMinHeight.isFirstResponder {
            tfPlayerMinHeight.text = minHeight[row]
        }else if tfPlayerMinAge.isFirstResponder {
            tfPlayerMinAge.text =  minAge[row]
        }else if tfPlayerMaxHeight.isFirstResponder {
            tfPlayerMaxHeight.text =  maxHeight[row]
        }else if tfPlayerMaxAge.isFirstResponder {
            tfPlayerMaxAge.text = maxAge[row]
        }else if tfPlayerGender.isFirstResponder {
            tfPlayerGender.text = arrGender[row]
        }else if tfPlayerLocation.isFirstResponder {
            tfPlayerLocation.text = teamLocation[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if tfPlayerMinHeight.isFirstResponder {
            return minHeight[row]
        }else if tfPlayerMinAge.isFirstResponder {
            return minAge[row]
        }else if tfPlayerMaxHeight.isFirstResponder {
            return maxHeight[row]
        }else if tfPlayerMaxAge.isFirstResponder {
            return maxAge[row]
        }else if tfPlayerGender.isFirstResponder {
            return arrGender[row]
        }else if tfPlayerLocation.isFirstResponder{
            return teamLocation[row]
        }
        return nil
    }
}

extension PlayerSearchVC : SearchAvailability ,GetCountry{
    func countryindexCount(index: Int, type: Type, id: String, name: [String]) {
        if tfPlayerNationality.isFirstResponder || type == .nationality{
            //            tfPlayerNationality.text = name
            if index == 0 {
            tfPlayerNationality.text = ""
            }else{
            tfPlayerNationality.text = " < \(index)  Entries >"
            }
            

        }
        
        countrys = name
        let stringRepresentation = countrys.joined(separator: ",")
        countrysName = stringRepresentation
        
    }
    
    func getAvalability(model: DayModel) {
        searchPlayer = model
        arrPlayersAvlability = model.arrName
    }
    
    func isValid(){
        
        tfPlayerName.text = tfPlayerName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerStrogest.text = tfPlayerStrogest.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerSport.text = tfPlayerSport.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerPosition.text = tfPlayerPosition.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerStandard.text = tfPlayerStandard.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerNationality.text = tfPlayerNationality.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfplayerAvailability.text = tfplayerAvailability.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfPlayerLocation.text = tfPlayerLocation.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
}
