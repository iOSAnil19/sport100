//
//  TeamSearchVC.swift
//  Sports100
//
//  Created by VeeraJain on 21/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import LocationPickerViewController


class TeamSearchVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,UIPickerViewDelegate,UIPickerViewDataSource,GetIndexCount,didSelectdData{
    func indexCount(index: Int, type: Type, id: String, name: String) {
        tfTeamSport.text = " < \(index)  Entries >"
        spostsId = id
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = true
        self.navigationItem.hidesBackButton = true
        arrDay = day.arrName
        let stringRepresentation = arrDay.joined(separator: ",")
        tfTeamGameDay.text = stringRepresentation
        
        arrTraining = teamTraining.arrName
        let string = arrTraining.joined(separator: ",")
        tfTeamTrainingDay.text = string
        
        
//        if let parent = self.parent as? SearchScrollerVC {
//            parent.selectedIndex = 1
//        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if tfTeamGender.isFirstResponder{
            return arrGender.count
            
        }else {
            return teamLocation.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if tfTeamGender.isFirstResponder{
        return arrGender[row]
        }else{
            return teamLocation[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tfTeamGender.isFirstResponder{
        tfTeamGender.text = arrGender[row]
        }else{
            tfTeamLocation.text = teamLocation[row]
        }
    }

    
    func textField(textfield: String, type: Type, id: String) {
        if tfTeamAge.isFirstResponder || type == Type.age {
            tfTeamAge.text = textfield
        } else if tfTeamStandard.isFirstResponder || type == Type.standard {
            tfTeamStandard.text = textfield
            standerdID = id
        }
    }
    
    
    @IBOutlet weak var teamTable: UITableView!
    @IBOutlet weak var doneHeight: NSLayoutConstraint!
    @IBOutlet weak var btnDone2: UIButton!
    @IBOutlet weak var tfTeamName: UITextField!
    
    @IBOutlet weak var tfTeamTrainingDay: UITextField!
    @IBOutlet weak var tfTeamGameDay: UITextField!
 
    @IBOutlet weak var tfTeamGender: UITextField!
    @IBOutlet weak var tfTeamAge: UITextField!
    @IBOutlet weak var tfTeamStandard: UITextField!
    @IBOutlet weak var tfTeamSport: UITextField!

    @IBOutlet weak var tfTeamLocation: UITextField!
    
    var arrDay = [String]()
    var arrTraining = [String]()
    var type : Type?
    var arrGender = ["Select", "Male", "Female","Both"]
    var teamLocation = ["Within 1 mile","Within 5 mile","Within 10 mile","Within 20 mile","National"]
    var arrSearchTeam = [JoinTeamModel]()
    var model: JoinTeamModel?
    var picker = UIPickerView()
    var standerdID = ""
    var day = DayModel()
    var spostsId = ""
    var teamTraining = TeamTraningModel()
    
    let  lat = "\(locationManeger.shared.locManager.location?.coordinate.latitude)"
    let log = "\(locationManeger.shared.locManager.location?.coordinate.longitude)"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
       
        teamTable.tableFooterView = UIView()
        teamTable.isHidden = true
        teamTable.dataSource = self
        teamTable.delegate = self
        
        tfTeamLocation.inputView = picker
        tfTeamSport.delegate = self
        tfTeamStandard.delegate = self
        tfTeamAge.delegate = self
        picker.dataSource = self
        picker.delegate = self
        tfTeamGameDay.delegate = self
        tfTeamTrainingDay.delegate = self
        
        
        tfTeamGender.inputView = picker
//        
        tfTeamName.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamSport.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamStandard.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamAge.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamGender.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "sort-down"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamLocation.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamGameDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
        tfTeamTrainingDay.AddImage(direction: .Right, imageName: #imageLiteral(resourceName: "right-arrow"), Frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 30, height: 30), backgroundColor: .clear)
    }
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfTeamSport {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
            vc.tittle = "Sports"
            vc.delegate = self
            vc.type = Type.sports
            self.present(vc,animated: true)
            
        }else if textField == tfTeamAge {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.lblHeader = "Age"
            vc.type = Type.age
            vc.delegate = self
            self.present(vc,animated: true)
            
        }else if textField == tfTeamStandard {
            let vc = storyboard?.instantiateViewController(withIdentifier: "StanderVC") as! StanderVC
            vc.lblHeader = "Standard"
            vc.type = Type.standard
            vc.delegate = self
            
            self.present(vc,animated: true)
            
        }else if textField == tfTeamGameDay{
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.lblHeader = "Game Day"
            vc.type = Type.gameDay
            vc.delegate = self
            self.present(vc,animated: true)
        }else if textField == tfTeamTrainingDay{
            let vc = storyboard?.instantiateViewController(withIdentifier: "GameDayVC") as! GameDayVC
            vc.lblHeader = "Training Day"
            vc.type = Type.traningDay
            vc.delegate = self
            self.present(vc,animated: true)
        }
        
            

    }

    @IBAction func addOnPress(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateNewTeamVC") as! CreateNewTeamVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func refreshOnPress(_ sender: UIButton) {
        doneHeight.constant = 50
        teamTable.isHidden = true
    }
    @IBAction func doneOnPress(_ sender: UIButton) {
        arrSearchTeam.removeAll()
        teamTable.reloadData()
        getTeam()
        doneHeight.constant = 0
        teamTable.isHidden = false
        if let parent = self.parent  {
            let search = parent as! SearchScrollerVC
            DispatchQueue.main.async {
                search.navigationItem.rightBarButtonItem =  search.filterBtn
            }
            
        }
//        isValid()
//        let param = ["key":Api.key, "sport_id":spostsId,
//                     "name":tfTeamName.text ?? "",
//                     "home_location":tfTeamLocation.text ?? "",
//                     "team_gender": tfTeamGender.text ?? "",
//                     "age_group": tfTeamAge.text ?? "",
//                     "gameDay[monam]":day.monAM == true ? "AM" : "",
//                     "gameDay[monpm]":day.monPM == true ? "PM" : "",
//                     "gameDay[tueam]":day.tusAM == true ? "AM" : "",
//                     "gameDay[tuepm]":day.tusAM == true ? "PM" : "",
//                     "gameDay[wedam]":day.wedAM == true ? "AM" : "",
//                     "gameDay[wedpm]":day.wedPM == true ? "PM" : "",
//                     "gameDay[thuam]":day.thrAM == true ? "AM" : "",
//                     "gameDay[thupm]":day.thrPM == true ? "PM" : "",
//                     "gameDay[friam]":day.friAM == true ? "AM" : "",
//                     "gameDay[fripm]":day.friPM == true ? "PM" : "",
//                     "gameDay[satam]":day.satAM == true ? "AM" : "",
//                     "gameDay[satpm]":day.satPM == true ? "PM" : "",
//                     "gameDay[sunam]":day.sunAM == true ? "AM" : "",
//                     "gameDay[sunpm]":day.sunPM == true ? "PM" : "",
//                     "trainingTime[monam]":teamTraining.monAM == true ? "AM" : "",
//                     "trainingTime[monpm]":teamTraining.monPM == true ? "PM" : "",
//                     "trainingTime[tueam]":teamTraining.tusAM == true ? "AM" : "",
//                     "trainingTime[tuepm]":teamTraining.tusAM == true ? "PM" : "",
//                     "trainingTime[wedam]":teamTraining.wedAM == true ? "AM" : "",
//                     "trainingTime[wedpm]":teamTraining.wedPM == true ? "PM" : "",
//                     "trainingTime[thuam]":teamTraining.thrAM == true ? "AM" : "",
//                     "trainingTime[thupm]":teamTraining.thrPM == true ? "PM" : "",
//                     "trainingTime[friam]":teamTraining.friAM == true ? "AM" : "",
//                     "trainingTime[fripm]":teamTraining.friPM == true ? "PM" : "",
//                     "trainingTime[satam]":teamTraining.satAM == true ? "AM" : "",
//                     "trainingTime[satpm]":teamTraining.satPM == true ? "PM" : "",
//                     "trainingTime[sunam]":teamTraining.sunAM == true ? "AM" : "",
//                     "trainingTime[sunpm]":teamTraining.sunPM == true ? "PM" : "",
//                     "standard":tfTeamStandard.text ?? "",
//                     "latitude":lat,
//                     "longitude":log,
//                     "distance":""] as [String : Any]
//        print(param)
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SportsDataTableVC") as! SportsDataTableVC
//        vc.param = param
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    func getTeam(){
        showHud("")
        isValid()
        if spostsId == "6" {
            spostsId = ""
        }
        let param = ["key":Api.key, "sport_id":spostsId,
                     "name":tfTeamName.text ?? "",
                     "home_location":tfTeamLocation.text ?? "",
                     "team_gender": tfTeamGender.text ?? "",
                     "age_group": tfTeamAge.text ?? "",
                     "gameDay[monam]":day.monAM == true ? "AM" : "",
                     "gameDay[monpm]":day.monPM == true ? "PM" : "",
                     "gameDay[tueam]":day.tusAM == true ? "AM" : "",
                     "gameDay[tuepm]":day.tusAM == true ? "PM" : "",
                     "gameDay[wedam]":day.wedAM == true ? "AM" : "",
                     "gameDay[wedpm]":day.wedPM == true ? "PM" : "",
                     "gameDay[thuam]":day.thrAM == true ? "AM" : "",
                     "gameDay[thupm]":day.thrPM == true ? "PM" : "",
                     "gameDay[friam]":day.friAM == true ? "AM" : "",
                     "gameDay[fripm]":day.friPM == true ? "PM" : "",
                     "gameDay[satam]":day.satAM == true ? "AM" : "",
                     "gameDay[satpm]":day.satPM == true ? "PM" : "",
                     "gameDay[sunam]":day.sunAM == true ? "AM" : "",
                     "gameDay[sunpm]":day.sunPM == true ? "PM" : "",
                     "trainingTime[monam]":teamTraining.monAM == true ? "AM" : "",
                     "trainingTime[monpm]":teamTraining.monPM == true ? "PM" : "",
                     "trainingTime[tueam]":teamTraining.tusAM == true ? "AM" : "",
                     "trainingTime[tuepm]":teamTraining.tusAM == true ? "PM" : "",
                     "trainingTime[wedam]":teamTraining.wedAM == true ? "AM" : "",
                     "trainingTime[wedpm]":teamTraining.wedPM == true ? "PM" : "",
                     "trainingTime[thuam]":teamTraining.thrAM == true ? "AM" : "",
                     "trainingTime[thupm]":teamTraining.thrPM == true ? "PM" : "",
                     "trainingTime[friam]":teamTraining.friAM == true ? "AM" : "",
                     "trainingTime[fripm]":teamTraining.friPM == true ? "PM" : "",
                     "trainingTime[satam]":teamTraining.satAM == true ? "AM" : "",
                     "trainingTime[satpm]":teamTraining.satPM == true ? "PM" : "",
                     "trainingTime[sunam]":teamTraining.sunAM == true ? "AM" : "",
                     "trainingTime[sunpm]":teamTraining.sunPM == true ? "PM" : "",
                     "standard":tfTeamStandard.text ?? "",
                     "latitude":lat,
                     "longitude":log,
                     "distance":""] as [String : Any]
        print(param)
        Alamofire.request(url.teamSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.error == nil {
                self.arrSearchTeam.removeAll()
                if let responseObj = response.value as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        hideHud()
                        let json = JSON(response.value as Any)
                        let dic = json["response"].arrayObject
                        
                        
                        for data in dic! {
                            let model = JoinTeamModel.init(withDic: data as! [String : Any])
                            self.arrSearchTeam.append(model)
                        }
                        if dic == nil {
                            self.teamTable.isHidden = true
                            self.doneHeight.constant = 50
                        }
                    }else {
                        hideHud()
                        let message = String("\(String(describing: responseObj[NetworkManager.kMESSAGE] ?? "0"))")
                        self.teamTable.isHidden = true
                        self.doneHeight.constant = 50
                        let alert = UIAlertController(title: "No Data Found", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (alert) in
                            if let parent = self.parent  {
                                let search = parent as! SearchScrollerVC
                                DispatchQueue.main.async {
                                    search.navigationItem.rightBarButtonItem =  nil
                                }
                                
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                else {
                    hideHud()
                    self.teamTable.isHidden = true
                    self.doneHeight.constant = 50
                    let alert = UIAlertController(title: "No Data Found", message: response.error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                hideHud()
                self.teamTable.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchTeam.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! TeamSearchCell
//        cell.lblTeamName.text = arrSearchTeam[indexPath.row].team_name
        if let name = arrSearchTeam[indexPath.row].team_name {
            cell.lblTeamName.text = name
        }
        let image = arrSearchTeam[indexPath.row].team_logo
        let imgUrlString = imageUrl + "\(image ?? "")"
        cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "Soccer"), options:.continueInBackground, completed: nil)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  vc = storyboard?.instantiateViewController(withIdentifier: "TeamDetailViewController") as! TeamDetailViewController
        vc.teamId = arrSearchTeam[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func isValid(){
        
        tfTeamName.text = tfTeamName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamSport.text = tfTeamSport.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamStandard.text = tfTeamStandard.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamAge.text = tfTeamAge.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamLocation.text = tfTeamLocation.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamGameDay.text = tfTeamGameDay.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        tfTeamTrainingDay.text = tfTeamTrainingDay.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
}


extension TeamSearchVC : DayValue {
    func getTainnigModel(model: TeamTraningModel) {
        teamTraining = model
        arrTraining = model.arrName
        
    }
    func noTrainText(text: String) {
        tfTeamTrainingDay.text = text
    }
    
    func getDayModel(model: DayModel) {
  
        day = model
        arrDay = model.arrName
        
        
    }
   
}
