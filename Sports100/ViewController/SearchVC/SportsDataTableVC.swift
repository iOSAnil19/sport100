
//
//  SportsDataTableVC.swift
//  Sports100
//
//  Created by VeeraJain on 24/08/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class SportsDataTableVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var arrTeam = [SearchModel]()
    var arrSearchTeam = [JoinTeamModel]()
    var param : [String:Any]?
     var filterBtn = UIBarButtonItem()
    let filter  = #imageLiteral(resourceName: "ic-1")
    var isBool:Bool?
    @IBOutlet weak var playerTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        playerTable.tableFooterView = UIView()
        playerTable.dataSource = self
        playerTable.delegate = self
        if isBool == true {
        getUser()
        }else{
            getTeam()
        }
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        filterBtn = UIBarButtonItem(image: filter,  style: .plain, target: self, action: #selector(filterTapped))
        self.navigationItem.rightBarButtonItems = [filterBtn]
    }
    @objc func filterTapped(){
        
        //        hideView.isHidden = false
    }
    @IBAction func nameAscendingOnPress(_ sender: UIButton) {
        nameAsending()
    }
    @IBAction func nameDesendingOnPress(_ sender: UIButton) {
        nameDesending()
    }
    
    @IBAction func distanceAsendingOnPress(_ sender: UIButton) {
        distanseAsending()
    }
    @IBAction func hideOnTapp(_ sender: Any) {
        //        self.hideView.isHidden = true
    }
    @IBAction func distanceDesendingOnPress(_ sender: UIButton) {
        distanceDesending()
    }
    func nameAsending() { // should probably be called sort and not filter
        if let  parentVC = self.presentingViewController {
            let controller = parentVC as! SearchScrollerVC
        
        if controller.scrollView.contentOffset.x == 0.0  {
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.first_name! < $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            //            self.hideView.isHidden = true
            
        }else if controller.scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! < $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        }
        
    }
    func nameDesending() { // should probably be called sort and not filter
        if let  parentVC = self.presentingViewController {
            let controller = parentVC as! SearchScrollerVC
            
        if controller.scrollView.contentOffset.x == 0.0  {
            
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.first_name! > $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            //            self.hideView.isHidden = true
            
        }else if controller.scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! > $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        }
    }
    func distanseAsending() { // should probably be called sort and not filter

        if let  parentVC = self.presentingViewController {
            let controller = parentVC as! SearchScrollerVC
            
        if controller.scrollView.contentOffset.x == 0.0  {
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        else if controller.scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        }
    }
    func distanceDesending() { // should probably be called sort and not filter
        if let  parentVC = self.presentingViewController {
            let controller = parentVC as! SearchScrollerVC
            
        if controller.scrollView.contentOffset.x == 0.0  {
            
            let controller = self.childViewControllers.first as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        else if controller.scrollView.contentOffset.x > 0.0 {
            let controller = self.childViewControllers.first as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            //            self.hideView.isHidden = true
            
        }
        }
    }
    func getUser(){
        showHud("")
        print(param)
        Alamofire.request(url.userSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.error == nil {
                hideHud()
                self.arrTeam.removeAll()
                if let responseObj = response.value as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        hideHud()
                        
                        let json = JSON(response.value as Any)
                        let dic = json["response"].arrayObject
                        for data in dic! {
                            let model = SearchModel.init(withDic: data as! [String : Any])
                            self.arrTeam.append(model)
                        }
                        if self.arrTeam.count == 0 {
                            self.playerTable.isHidden = true
//                            self.doneHeight.constant = 50
                        }
                    }else {
                        hideHud()
                        let message = String("\(String(describing: responseObj[NetworkManager.kMESSAGE] ?? "0"))")
                        self.playerTable.isHidden = true
//                        self.doneHeight.constant = 50
                        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        
                        //                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: ))
                        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (alert) in
                            if let parent = self.parent?.parent  {
                                let search = parent.parent as! SearchScrollerVC
                                DispatchQueue.main.async {
                                    search.navigationItem.rightBarButtonItem =  nil
                                }
                                
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else {
                    self.playerTable.isHidden = true
//                    self.doneHeight.constant = 50
                    let alert = UIAlertController(title: "No Data Found", message: response.error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    if let parent = self.parent?.parent  {
                        let search = parent.parent as!SearchScrollerVC
                        DispatchQueue.main.async {
                            search.navigationItem.rightBarButtonItem =  nil
                        }
                        
                    }
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                self.playerTable.reloadData()
            }
        }
    }
    func getTeam(){
        showHud("")
        
                Alamofire.request(url.teamSearch, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.error == nil {
                self.arrSearchTeam.removeAll()
                if let responseObj = response.value as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        hideHud()
                        let json = JSON(response.value as Any)
                        let dic = json["response"].arrayObject
                        
                        
                        for data in dic! {
                            let model = JoinTeamModel.init(withDic: data as! [String : Any])
                            self.arrSearchTeam.append(model)
                        }
                        if dic == nil {
                            self.playerTable.isHidden = true
//                            self.doneHeight.constant = 50
                        }
                    }else {
                        hideHud()
                        let message = String("\(String(describing: responseObj[NetworkManager.kMESSAGE] ?? "0"))")
                        self.playerTable.isHidden = true
//                        self.doneHeight.constant = 50
                        let alert = UIAlertController(title: "No Data Found", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (alert) in
                            if let parent = self.parent?.parent  {
                                let search = parent.parent as! SearchScrollerVC
                                DispatchQueue.main.async {
                                    search.navigationItem.rightBarButtonItem =  nil
                                }
                                
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                else {
                    hideHud()
                    self.playerTable.isHidden = true
//                    self.doneHeight.constant = 50
                    let alert = UIAlertController(title: "No Data Found", message: response.error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                hideHud()
                self.playerTable.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isBool == true{
            return arrTeam.count }
            else{
        return arrSearchTeam.count
            }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isBool == true{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! SportsDataCell
        if let name = arrTeam[indexPath.row].first_name {
            cell.lblName.text = name
        }
        
        cell.lblPosition.text = arrTeam[indexPath.row].position_name
        cell.lblsides.text = arrTeam[indexPath.row].sport_name
        let image = arrTeam[indexPath.row].profile_pic
        let imgUrlString = imageUrl + "\(image ?? "")"
        cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
        
        return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:   indexPath) as! SportsDataCell
            if let name = arrSearchTeam[indexPath.row].team_name {
                cell.lblName.text = name
            }
            
            cell.lblPosition.isHidden = true
            cell.lblsides.isHidden = true
            let image = arrSearchTeam[indexPath.row].team_logo
            let imgUrlString = imageUrl + "\(image ?? "")"
            cell.imgView?.sd_setImage(with: URL.init(string: imgUrlString), placeholderImage: #imageLiteral(resourceName: "placeholder.png"), options:.continueInBackground, completed: nil)
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isBool == true{
        let  vc = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc.otherUserId = arrTeam[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let  vc = storyboard?.instantiateViewController(withIdentifier: "TeamDetailViewController") as! TeamDetailViewController
            vc.teamId = arrSearchTeam[indexPath.row].id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
    }
}
