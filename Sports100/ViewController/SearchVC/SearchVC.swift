//
//  SearchVC.swift
//  Sports100
//
//  Created by VeeraJain on 14/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import  SwiftyJSON
import Alamofire
//import LocationPickerViewController
import CarbonKit

protocol NavigationButton {
    func navigationItem()
}
class SearchVC: UIViewController{
    
    let button: UIButton = UIButton()
    let items = ["PLAYERS", "TEAMS"] as [Any]
    let editImage    = #imageLiteral(resourceName: "add_team_ic")
    let refresh  = #imageLiteral(resourceName: "ic_reload")
    let filter  = #imageLiteral(resourceName: "ic-1")
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var delegate : NavigationButton?
    var editButton = UIBarButtonItem()
    var filterBtn = UIBarButtonItem()
    var refreshButton  =  UIBarButtonItem()
    var selectedIndex = 0
    
    @IBOutlet weak var containerCarbonView: UIView!
    @IBOutlet var hideView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search"
//        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)

//        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView:containerCarbonView)
////        [segmentedControl setEnabled:NO forSegmentAtIndex:0];
//        carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
//

//        self.style()
//        carbonTabSwipeNavigation.delegate = self
        hideView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height )
        
        view.addSubview(hideView)
        filterBtn.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        hideView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.rightBarButtonItem = nil
        
        
    }
   
    @IBAction func nameAscendingOnPress(_ sender: UIButton) {
        nameAsending()
    }
    @IBAction func nameDesendingOnPress(_ sender: UIButton) {
    nameDesending()
    }
    
    @IBAction func distanceAsendingOnPress(_ sender: UIButton) {
        distanseAsending()
    }
    @IBAction func hideOnTapp(_ sender: Any) {
        self.hideView.isHidden = true
    }
    @IBAction func distanceDesendingOnPress(_ sender: UIButton) {
        distanceDesending()
    }
    func nameAsending() { // should probably be called sort and not filter
        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0 {
           let controller = self.carbonTabSwipeNavigation.viewControllers[0] as! PlayerSearchVC
           let array = controller.arrTeam.sorted(by: { $0.first_name! < $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
        }else if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 1{
            let controller = self.carbonTabSwipeNavigation.viewControllers[1] as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! < $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
        }
    }
    func nameDesending() { // should probably be called sort and not filter
        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0 {
            
            let controller = self.carbonTabSwipeNavigation.viewControllers[0] as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.first_name! > $1.first_name! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
        }else if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 1{
            let controller = self.carbonTabSwipeNavigation.viewControllers[1] as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.team_name! > $1.team_name! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
        }
    }
    func distanseAsending() { // should probably be called sort and not filter
        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0 {
            let controller = self.carbonTabSwipeNavigation.viewControllers[0] as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
        }else if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 1{
            let controller = self.carbonTabSwipeNavigation.viewControllers[1] as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! < $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
        }
    }
    
    func distanceDesending() { // should probably be called sort and not filter
        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0 {
            
            let controller = self.carbonTabSwipeNavigation.viewControllers[0] as! PlayerSearchVC
            let array = controller.arrTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrTeam = array
            controller.playerTable.reloadData()
            self.hideView.isHidden = true
        }else if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 1{
            let controller = self.carbonTabSwipeNavigation.viewControllers[1] as! TeamSearchVC
            let array = controller.arrSearchTeam.sorted(by: { $0.latitude! > $1.latitude! })
            print(array)
            controller.arrSearchTeam = array
            controller.teamTable.reloadData()
            self.hideView.isHidden = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
//        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0{
//            if let img = gradientImage(in: CGRect.init(x: 0, y: 0, width: view.frame.width / 2, height: (carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().frame.size.height)!), colors: [#colorLiteral(red: 0.1411764706, green: 0.4705774283, blue: 0.7843137255, alpha: 1),#colorLiteral(red: 0.431372549, green: 0.7098039216, blue: 0.8784313725, alpha: 1)]){
//                carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().backgroundColor = UIColor(patternImage: img)
//                (carbonTabSwipeNavigation.carbonSegmentedControl?.segments![1] as! UIView).backgroundColor = .clear
//            }
//        }
       
    }
    
    @objc func addTapped(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateNewTeamVC") as! CreateNewTeamVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func filterTapped(){
       
        hideView.isHidden = false
    }
    
    @objc func refreshTapped(){
        if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 0 {

             let controller = self.carbonTabSwipeNavigation.viewControllers[0] as! PlayerSearchVC
            self.navigationItem.rightBarButtonItem = nil
            controller.playerTable.isHidden = true
            controller.doneHeight.constant = 50
            controller.tfPlayerName.text = ""
            controller.tfPlayerSport.text = ""
            controller.tfPlayerPosition.text = ""
            controller.tfPlayerStrogest.text = ""
            controller.tfPlayerStandard.text = ""
            controller.tfPlayerNationality.text = ""
            controller.tfPlayerMinHeight.text = ""
            controller.tfPlayerMinAge.text = ""
            controller.tfPlayerMaxAge.text = ""
            controller.tfPlayerMaxHeight.text = ""
            controller.tfPlayerGender.text = ""
            controller.tfplayerAvailability.text = ""
            controller.tfPlayerLocation.text = ""
            controller.arrTeam.removeAll()
            controller.standerdID.removeAll()
            controller.sportsID.removeAll()
            controller.selectedPosition = ""

            print("done")

        }else if carbonTabSwipeNavigation.carbonSegmentedControl?.selectedSegmentIndex == 1{
            let controller = self.carbonTabSwipeNavigation.viewControllers[1] as! TeamSearchVC
            self.navigationItem.rightBarButtonItem = nil
            controller.teamTable.isHidden = true
            controller.doneHeight.constant = 50
            controller.tfTeamTrainingDay.text = ""
            controller.tfTeamGender.text = ""
            controller.tfTeamAge.text = ""
            controller.tfTeamName.text = ""
            controller.tfTeamSport.text = ""
            controller.tfTeamLocation.text = ""
            controller.tfTeamStandard.text = ""
            controller.tfTeamGameDay.text = ""

            print("perfect")
        }

    }
//    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
//
//        switch index {
//        case 0:
//             carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
//            return self.storyboard?.instantiateViewController(withIdentifier: "PlayerSearchVC") as! PlayerSearchVC
//
//        case 1:
//             carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
//            return self.storyboard?.instantiateViewController(withIdentifier: "TeamSearchVC") as! TeamSearchVC
//
//        default:
//           break
//        }
//        return UIViewController()
//    }
    func style() {
        
        carbonTabSwipeNavigation.setTabBarHeight(60.0)
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 2, forSegmentAt: 1)
        carbonTabSwipeNavigation.setNormalColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1),font: UIFont.systemFont(ofSize: 17.0))
        carbonTabSwipeNavigation.setSelectedColor(.white, font: UIFont.systemFont(ofSize: 17.0))
        
//        var attributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 14)!]
//        attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)]
//        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.orange]
//        navigationController?.navigationBar.titleTextAttributes = attributes
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
         carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
//     self.carbonTabSwipeNavigation(carbonTabSwipeNavigation, viewControllerAt: index)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {


        if index == 0{
            if let img = gradientImage(in: CGRect.init(x: 0, y: 0, width: view.frame.width / 2, height: (carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().frame.size.height)!), colors: [#colorLiteral(red: 0.1411764706, green: 0.4705774283, blue: 0.7843137255, alpha: 1),#colorLiteral(red: 0.431372549, green: 0.7098039216, blue: 0.8784313725, alpha: 1)]){
                carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().backgroundColor = UIColor(patternImage: img)
                (carbonTabSwipeNavigation.carbonSegmentedControl?.segments![1] as! UIView).backgroundColor = .clear
            }
             carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
            navigationItem.leftBarButtonItems = []

            refreshButton = UIBarButtonItem(image: refresh,  style: .plain, target: self, action: #selector(refreshTapped))
              filterBtn = UIBarButtonItem(image: filter,  style: .plain, target: self, action: #selector(filterTapped))
            navigationItem.leftBarButtonItems = [refreshButton]

        }
        else{
            if let img = gradientImage(in: (carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().bounds)!, colors: [#colorLiteral(red: 0.1411764706, green: 0.4705774283, blue: 0.7843137255, alpha: 1),#colorLiteral(red: 0.431372549, green: 0.7098039216, blue: 0.8784313725, alpha: 1)]){
                carbonTabSwipeNavigation.carbonSegmentedControl?.getSelectedSegment().backgroundColor = UIColor(patternImage: img)
                (carbonTabSwipeNavigation.carbonSegmentedControl?.segments![0] as! UIView).backgroundColor = .clear
            }
             carbonTabSwipeNavigation.carbonSegmentedControl?.isUserInteractionEnabled = false
            navigationItem.leftBarButtonItems = []
            editButton   = UIBarButtonItem(image: editImage,  style: .plain, target: self, action: #selector(addTapped))
            filterBtn   = UIBarButtonItem(image: filter,  style: .plain, target: self, action: #selector(filterTapped))
            refreshButton = UIBarButtonItem(image: refresh,  style: .plain, target: self, action: #selector(refreshTapped))
            navigationItem.leftBarButtonItems = [editButton, refreshButton]

        }
    }
   

    
}

