//
//  CountryTable.swift
//  Sports100
//
//  Created by VeeraJain on 20/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol GetIndexCount {
    func indexCount(index : Int,type :Type,id:String,name:String)
}
protocol GetCountry {
    func countryindexCount(index : Int,type :Type,id:String,name:[String])
}
class CountryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
 
    @IBOutlet weak var lblTitel: UILabel!
    @IBOutlet weak var CountryTable: UITableView!
    var countries: [String] = []
    var selectedIndexPaths = NSMutableSet()
    var delegate : GetIndexCount?
    var isBool :Bool?
    var type : Type?
    var arrSport = [ChooseSport]()
    var arrPosition = [PostionModel]()
    var arrImg = [#imageLiteral(resourceName: "football5"),#imageLiteral(resourceName: "football5"),#imageLiteral(resourceName: "football7"),#imageLiteral(resourceName: "football11"),#imageLiteral(resourceName: "basketball"),#imageLiteral(resourceName: "netball")]
    var tittle: String?
    var sportsID = ""
    var PostionID = ""
    var countryName = [String]()
    var delegate1 : GetCountry?
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        if type == Type.sports{
            lblTitel.text = tittle
        }else if type == Type.postion{
             lblTitel.text = tittle
        }
        CountryTable.delegate = self
        CountryTable.dataSource = self
   
    }

    @IBAction func saveOnPress(_ sender: UIButton) {
         if type == Type.sports{
        if let del = delegate{
            del.indexCount(index: selectedIndexPaths.count, type:type!, id: sportsID, name: "")
            self.dismiss(animated: true, completion: nil)
        }
        }else if type == Type.postion{
            if let del = delegate{
                del.indexCount(index: selectedIndexPaths.count, type:type!, id: PostionID, name: "")
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        else {
            if let del = delegate1{
                
//                del.indexCount(index: selectedIndexPaths.count, type:type!, id: "", name: countryName)
                del.countryindexCount(index: selectedIndexPaths.count, type: type!, id: "", name:countryName)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
         if type == Type.sports{
            getSport()
        }else if type == Type.postion{
            getPosition()
        } else{
            CountryTable.delegate = self
            CountryTable.dataSource = self
            countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
                let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
                return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            }
            
            print(countries)
        }
    }
    func getSport() {
        showHud("")
        let networkManager = NetworkManager()
        let params = ["key":Api.key] as [String : Any]
        networkManager.getDataForRequest(Api.getSport, andParameter: params ){ (res, err) in
            if err == nil {
                hideHud()
                let json = JSON(res!)
                let dic = json["Sport"].arrayObject
                
                for data in dic! {
                    let model = ChooseSport.init(withDic: data as! [String : Any])
                    
                    self.arrSport.append(model)
                    
                }
                hideHud()
                self.CountryTable.reloadData()
            }else{
                hideHud()
            }
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    
    func getPosition() {
        showHud("")
        let networkManager = NetworkManager()
        
        let params = ["key":Api.key,"sportID" : sportsID] as [String : Any]
//        networkManager.getDataForRequest("https://apisports100.co.uk/sports100/index.php/users/getPosition", andParameter: params ){ (res, err) in
        Alamofire.request("https://apisports100.co.uk/sports100/index.php/users/getPosition", method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
        
        if response.error == nil {
            hideHud()
                let json = JSON(response.value)
                let dic = json["Position"].arrayObject
            
                
            for data in dic! {
                    let model = PostionModel.init(withDic: data as! [String : Any])
                    self.arrPosition.append(model)
                }
            hideHud()
                self.CountryTable.reloadData()
        }else{
            hideHud()
            }
        }
    }
    @IBAction func cancelOnPress(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if type == Type.sports{
            return arrSport.count
       }else if type == Type.postion{
                return arrPosition.count
       }else{
            return countries.count }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CountryCell
       if type == Type.sports{
        cell.imgWidth.constant = 0
            cell.lblCuntryName.text = arrSport[indexPath.row].name
//        if indexPath.row == 0 {
//            cell.imgSports.isHidden = true
//        }
//           cell.imgSports.image = arrImg[indexPath.row]
        sportsID = arrSport[indexPath.row].id!
//            sportsID = arrSport[indexPath.row].id!
        
         }else if type == Type.postion{
            cell.imgWidth.constant = 0
            cell.lblCuntryName.text = arrPosition[indexPath.row].Position
            PostionID = arrPosition[indexPath.row].id!
        } else{
        
            cell.lblCuntryName.text = countries[indexPath.row]
            cell.imgViewWidth.constant = 0
        
        }
        
        cell.btnCheck.tag = indexPath.row
        configure(cell: cell, forRowAtIndexPath: indexPath as NSIndexPath)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                let cell = tableView.cellForRow(at: indexPath) as! CountryCell
        
        if selectedIndexPaths.contains(indexPath) {
            // deselect
            selectedIndexPaths.remove(indexPath)
        }
        else if type == Type.sports {
            // select
            
            sportsID = arrSport[indexPath.row].id!
            selectedIndexPaths.add(indexPath)
        }else if type == Type.nationality{
          let index = countries[indexPath.row]
            countryName.append(index)
            selectedIndexPaths.add(indexPath)
        }else{
            PostionID = arrPosition[indexPath.row].id!
            selectedIndexPaths.add(indexPath)
        }
        configure(cell: cell, forRowAtIndexPath: indexPath as NSIndexPath)
    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! CountryCell
//
//
//        cell.btnCheck.isSelected = false
//    }
    func configure(cell: CountryCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        cell.textLabel!.text = "Row \(indexPath.row)"
        if selectedIndexPaths.contains(indexPath) {
            // selected
            cell.btnCheck.isSelected = true
        }
        else {
            // not selected
            cell.btnCheck.isSelected = false
        }
    }
}
