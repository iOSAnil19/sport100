//
//  MatchesVC.swift
//  Sports100
//
//  Created by Depex Technologies Pvt. Ltd on 20/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

protocol MatchesDelegate: class {
    func matchAddedDelegate(_ matches:[Matches])
}

class MatchesVC: UIViewController {
    
    //MARK: ------------- IBOutlets/Variables --------------
    
    @IBOutlet weak var table_View:UITableView!
    
    let refreshControl = UIRefreshControl()
    var pageNo = 1
    var totalPageCount = 1
    var matchs = [Matches]()
    
    var selectedTeam:Team?
    var senderVC:UIViewController?
    
    var delegate: MatchesDelegate?
    
    //MARK: ------------- View Life Cycle --------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Matches"
        
        self.table_View.estimatedRowHeight = 87 * getScaleFactor()
        self.table_View.rowHeight = UITableViewAutomaticDimension
        
        table_View.refreshControl = refreshControl
        table_View.tableFooterView = UIView.init(frame: .zero)
        refreshControl.addTarget(self, action: #selector(MatchesVC.refreshMethod), for: .valueChanged)
        self.setCancelButton()
        if selectedTeam?.member == "Yes" {
            self.setAddButton()
        }
     //   showHud("Processing..")
     //   self.getMatchListing()
        if let ms = selectedTeam!.matches {
            matchs = ms.reversed()
            table_View.reloadData()
        }
    }
    
    @objc func refreshMethod() {
        pageNo = 1
        self.getMatchListing()
    }
    
    private func setAddButton() {
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addPressed))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func addPressed() {
        let nibs = Bundle.main.loadNibNamed("AddMatch", owner: self, options: nil)
        let addMatch = nibs?.first as? AddMatch
         addMatch?.delegate = self
        addMatch?.showPopUp(self)
    }
    
    private func setCancelButton() {
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelPressed))
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc func cancelPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getMatchListing() {
        let params = ["key": Api.key, "teamID":selectedTeam?.iD ?? "1"] //
        var requestString = Api.getMatch
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        
        if pageNo != 0 {
            requestString = requestString + "/page:\(pageNo)"
        }
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(requestString, andParameter: params) { (response, error) in
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    self.totalPageCount = Int("\(String(describing: result["totalPage"] ?? "1"))")!
                    
                    if success == 200 {
                        
                        if let allTeams = result["games"] as? [[String:Any]], allTeams.count > 0 {

                            let matchModels = allTeams.map({ (matchInfo) -> Matches in
                                Matches.init(object: matchInfo)
                            })
                            if self.pageNo == 1 {
                                self.matchs = matchModels
                            } else {
                                for m in matchModels {
                                    self.matchs.append(m)
                                }
                            }
                            self.selectedTeam?.matches = self.matchs.reversed()
                            self.delegate?.matchAddedDelegate(self.matchs.reversed())
                            self.table_View.reloadData()
                        }
                        
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
    }
    
    func refresHTeamHomePage() {
        if let nav = senderVC?.navigationController {
            if let tabbarVC = nav.tabBarController, tabbarVC.viewControllers?.count != 0 {
                if let teamNavVC = tabbarVC.viewControllers?[3] as? UINavigationController, teamNavVC.viewControllers.count > 0 {
                    if let teamVC = teamNavVC.viewControllers[0] as? TeamSportsVC {
                        teamVC.refreshMethod()
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MatchesVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.matchs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "MatchCell"
        let cell = tableView.dequeueReusableCell(withIdentifier:cellId) as? MatchCell
        
        let match = self.matchs[indexPath.row]
        cell?.updateMatchCell(match, selectedTeam!)
        cell?.selectionStyle = .none
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let match = self.matchs[indexPath.row]
//            self.removeTeam(match)/
            
            self.matchs.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    func removeTeam(_ team:Team) {
        let params = ["key": Api.key, "userID":userInfo?.userId ?? ""]
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.removeTeam, andParameter: params) { (response, error) in
        }
    }
}

extension MatchesVC : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 63 * getScaleFactor()
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MatchesVC:AddMatchDelegate {
    func addNewMatchWith(_ name:String, _ time:String, _ score:String, _ streeet:String, _ citty:String, _ postalCode:String, _ mycountry:String) {
        
//        "town":citty,
//        "country":mycountry,
//        "postcode":postalCode,
        let params = ["key":Api.key,
            "location":streeet,
            "teamID":selectedTeam!.iD ?? "",
            "teamPlaying":name,
            "score":score,
            "date":time,
            "matchID":"0"]
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
        print(params)
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.addmatch, andParameter: params) { (response, error) in
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    
                    if success == 200 {
                        self.getMatchListing()
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: self)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
            }
        }
        
    }
}




