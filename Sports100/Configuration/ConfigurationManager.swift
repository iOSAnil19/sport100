//
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import UIKit


let kEnvironmentsPlist = "Configuration"
let kConfigurationKey = "ActiveConfiguration"
let kAPIEndpointKey = "APIEndpoint"
let kSocketEndPointKey = "SocketURL"
let kAPIBaseFileURLKey = "BaseFileUrl"
let kLoggingEnabledKey = "LoggingEnabled"
let kAnalyticsTrackingEnabled = "AnalyticsTrackingEnabled"
let kdeepLinkUrl = "DeepUrlLink"


class ConfigurationManager: NSObject {
    
    static let sharedInstance = ConfigurationManager()

    var environment : NSDictionary?
    
    override init() {
        super.init()
        initialize()
    }
    
    // Private method
    
    func initialize ()   {
        
        var environments: NSDictionary?
        if let envsPlistPath = Bundle.main.path(forResource: "Configuration", ofType: "plist") {
            environments = NSDictionary(contentsOfFile: envsPlistPath)
        }
        self.environment = environments!.object(forKey: currentConfiguration()) as? NSDictionary
        if self.environment == nil {
            
            assertionFailure(NSLocalizedString("Unable to load application configuration", comment: "Unable to load application configuration"))
        }
    }
    
    // currentConfiguration
    
    func currentConfiguration () -> String   {
        
        let configuration = Bundle.main.infoDictionary?[kConfigurationKey]
        return configuration! as! String
    }
    
    // APIEndpoint
    
    func APIEndpoint () -> String  {
        let configuration = self.environment![kAPIEndpointKey]
        return (configuration)! as! String
    }
  
    func deepLinkUrl() -> String  {
      let configuration = self.environment![kdeepLinkUrl]
      return (configuration)! as! String
    }
  
    func SocketEndpoint () -> String  {
        
        let configuration = self.environment![kSocketEndPointKey]
        return (configuration)! as! String
    }
  
    func APIFileBaseURL()->String{
        let configuration = self.environment![kAPIBaseFileURLKey]
        return (configuration)! as! String
    }
  
    // isLoggingEnabled
    
    func isLoggingEnabled () -> Bool  {
        
        let configuration = self.environment![kLoggingEnabledKey]
        return configuration as! Bool
    }
    
    // isAnalyticsTrackingEnabled
    
    func isAnalyticsTrackingEnabled () -> Bool  {
        
        let configuration = self.environment![kAnalyticsTrackingEnabled]
        return configuration as! Bool
    }
}




