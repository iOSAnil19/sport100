//
//  LocationManeger.swift
//  Sports100
//
//  Created by VeeraJain on 20/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import CoreLocation

class locationManeger {
    
    static let shared = locationManeger()
    var locManager = CLLocationManager()
    



    func configureLocationManeger(){
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
    }
    
    func getCurrentLoction() -> CLLocation? {
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            return locManager.location
            
        }
        return nil
    }



}
