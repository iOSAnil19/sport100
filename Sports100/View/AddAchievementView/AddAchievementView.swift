//
//  AddAchievementView.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 24/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class AddAchievementView: UIView,UITextViewDelegate {
    
    let CharacterLimit = 60
    @IBOutlet weak var text_View:UITextView!
    @IBOutlet weak var lbl_CharCount:UILabel!
    @IBOutlet weak var tf_Date:UITextField!
    @IBOutlet weak var _view: UIView!
    @IBOutlet weak var date_Picker:UIDatePicker!
    var senderVC : UIViewController?
    var dobSelected:String?
    var selectedID = 0
    
    var delegate:EditBioDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        tf_Date.inputView = date_Picker
    }
    
    func showPopUp(_ sender : UIViewController) {
        senderVC = sender
        self.frame = sender.view.frame
        animateIn(sender)
    }
    
    func animateIn(_ sender : UIViewController) {
        sender.view.addSubview(self)
        self.center = sender.view.center
        _view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
        _view.alpha = 0.5
        
        UIView.animate(withDuration: 0.3) {
            self._view.alpha = 1
            self._view.transform = .identity
        }
        text_View.becomeFirstResponder()
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self._view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
            self._view.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
        text_View.resignFirstResponder()
    }
    
    @IBAction func buttonClicked_HidePopUp(_ sender : UIButton) {
        animateOut()
    }
    
    @IBAction func buttonClicked_Save(_ sender : UIButton) {
        self.addAchievement()
        animateOut()
    }
    
    @IBAction func datePickerChanged(_ picker:UIDatePicker) {
        tf_Date.text = getStringFromDate(picker.date)
        dobSelected = getDateFromDate(picker.date)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        self.lbl_CharCount.text = "\(CharacterLimit - updatedText.count)"
        return updatedText.count <= CharacterLimit
    }

    func addAchievement() {
        
        if dobSelected == nil || text_View.text == "" {
            return
        }
        
        guard let parentVC = self.parentViewController else { return }
        showHud("Processing..")//ApiConstant.achieveId:self.selectedID,
        let params = ["key":Api.key,
                      ApiConstant.userId:userInfo?.userId ?? "",
                      ApiConstant.achieveDescription:text_View.text ?? "",
                      ApiConstant.achieveDate:dobSelected ?? "",
                      "type":"Achievement",
                      "title":"",
                      "post_image": "",
                      "video": ""] as [String : Any]
        print(params)
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.addFeedPost, andParameter: params) { (response, error) in
            hideHud()
            print(response ?? "No response")
            
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        self.delegate?.bioUpdated(self.text_View.text ?? "")
                    } else {
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: parentVC)
                    }
                } else {
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: parentVC)
                }
            } else {
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: parentVC)
            }
        }
    
    }
}


