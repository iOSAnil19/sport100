//
//  KeyboardAccessoryView.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

protocol TRKeyboardAccessoryViewDelegate {
    func doneButtonTapped()
}

class TRKeyboardAccessoryView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var delegate:TRKeyboardAccessoryViewDelegate? = nil

    override func awakeFromNib() {
//        super.awakeFromNib()
//        var rect = self.frame
//        rect.size.height = 33.0
//        self.frame = rect
    }
    
    class func getView() -> TRKeyboardAccessoryView
    {
        return (Bundle.main.loadNibNamed("KeyboardAccessoryView", owner: self, options: nil)!.last! as! TRKeyboardAccessoryView)
    }
    
    func setFrame(frame:CGRect){
        
        self.frame = frame
        toolBar.frame = self.bounds
    }
    
    func setToolBarBackgroundColor(color:UIColor){
        
        toolBar.backgroundColor = color
    }
    
    @IBAction func doneButtonTapped(){
        
        if delegate != nil {
            delegate?.doneButtonTapped()
        }
    }
    
}
