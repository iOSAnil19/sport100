//
//  AddMatch.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 20/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import GooglePlaces

protocol AddMatchDelegate {
    func addNewMatchWith(_ name:String, _ time:String, _ score:String, _ streeet:String, _ citty:String, _ postalCode:String, _ mycountry:String)
}

class AddMatch: UIView,GMSAutocompleteViewControllerDelegate {
    
    
    
    var senderVC : UIViewController?
    var placesClient: GMSPlacesClient = GMSPlacesClient.shared()

    @IBOutlet weak var _view: UIView!
    @IBOutlet weak var tf_TeamName:UITextField!
    @IBOutlet weak var tf_Start:UITextField!
    @IBOutlet weak var tf_Location:UITextField!
    @IBOutlet weak var tf_FinalScore:UITextField!
    var street = ""
    var city = ""
    var postCode = ""
    var country = ""


    var delegate:AddMatchDelegate?
    
    var selectedField:UITextField?
    var selectedTime = ""
    
    func showPopUp(_ sender : UIViewController) {
        senderVC = sender
        self.frame = sender.view.frame
        animateIn(sender)
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(AddMatch.dateChanged), for: .valueChanged)
        tf_Start.inputView = datePicker
    }
    
    func animateIn(_ sender : UIViewController) {
        sender.view.addSubview(self)
        self.center = sender.view.center
        _view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
        _view.alpha = 0.5
        
        UIView.animate(withDuration: 0.3) {
            self._view.alpha = 1
            self._view.transform = .identity
        }
        
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self._view.transform = CGAffineTransform(scaleX:0.7, y:0.7)
            self._view.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    
    
    @objc func dateChanged(_ datePicker:UIDatePicker) {
        if let tf = selectedField {
            if selectedField == tf_Start {
                selectedTime = getPHPDateStringFromDate(datePicker.date)
                tf.text = getDateStringFromDate(datePicker.date)
            }
        }
    }
    
    @IBAction func buttonClicked_HidePopUp(_ sender : UIButton) {
        animateOut()
    }
    
    @IBAction func buttonClicked_Save(_ sender : UIButton) {
        guard let parentVC = self.parentViewController else { return }
        
        if isBlankField(tf_TeamName) {
            showAlert("", message: "Please choose team name", onView: parentVC)
            return
        }
        
        if isBlankField(tf_Start) {
            showAlert("", message: "Please choose match time", onView: parentVC)
            return
        }
        self.delegate?.addNewMatchWith(tf_TeamName.text ?? "", selectedTime, tf_FinalScore.text ?? "", street, city, postCode, country)
        animateOut()
    }
    
    //MARK: ------------- Private Methods --------------
    
    func showLocationPicker(_ textField:UITextField) {
        
        let placePickerController = GMSAutocompleteViewController()
                   placePickerController.delegate = self as GMSAutocompleteViewControllerDelegate
        senderVC?.present(placePickerController, animated: true, completion: nil)
       
    }


func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
     if place.formattedAddress != nil {
//    //            viewController.dismiss(animated: true, completion: nil)
//    //            var zipCode = String()
//
//                self.location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
//                getZipCode(withLocation: self.location!) { (place) in
//
//                    self.textPincode.text = place.postalCode
//                }
//                viewController.dismiss(animated: true, completion: nil)
//            }
//            else{
//    //            MessageView.show(in: viewController.view, withMessage: "Choose valid place")
//                SweetAlert().showAlert("", subTitle: "Choose valid place", style: .success)
//                viewController.dismiss(animated: true, completion: nil)
//            }o

                   DispatchQueue.main.async {
                      // textField.text = place.formattedAddress
                   }
                   if let locationDictionary = place.addressComponents {
                       print(locationDictionary)
//                       if let City = locationDictionary["City"] {
//                           self.city = "\(City)"
//                       }
//                       if let City = locationDictionary["Name"] {
//                           self.street = "\(City)"
//                       }
//                       if let post = locationDictionary["ZIP"] {
//                           self.postCode = "\(post)"
//                       }
//                       if let Country = locationDictionary["Country"] {
//                           self.country = "\(Country)"
//                       }
//
//                       if let address = locationSelected.formattedAddressString {
//                           textField.text = address
//                       }
                   }
//               locationPicker.addBarButtons()
//               let navigationController = UINavigationController(rootViewController: locationPicker)
//               senderVC?.present(navigationController, animated: true, completion: nil)
    }
}

func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
     senderVC?.dismiss(animated: true, completion: nil)
}

func wasCancelled(_ viewController: GMSAutocompleteViewController) {
     senderVC?.dismiss(animated: true, completion: nil)
}
}


extension AddMatch:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedField = textField
        if textField == tf_Start {
            if isBlankField(tf_Start) {
                textField.text = getDateStringFromDate(Date())
                selectedTime = getPHPDateStringFromDate(Date())

            }
        } else if textField == tf_Location {
            showLocationPicker(textField)
            textField.resignFirstResponder()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tf_Start || textField == tf_Location {
            return false
        } else if textField == tf_FinalScore {
            return true
            let text = textField.text ?? ""
            if string == "." || text.contains(find: ".") || string == "" {
                // Payment info is restricted upto two decimal
                return allowUptoTwoDecimalPlace(textField.text ?? "", forRange: range, replacementString: string)
            } else {
                return text.count < 3 ? true : false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
