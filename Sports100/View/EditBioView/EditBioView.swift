//
//  EditBioView.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 24/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol EditBioDelegate : class {
    func bioUpdated(_ text:String)
}

class EditBioView: UIView,UITextViewDelegate, TRKeyboardAccessoryViewDelegate {
    
    let CharacterLimit = 240
    @IBOutlet weak var text_View:UITextView!
    @IBOutlet weak var lbl_CharCount:UILabel!
    @IBOutlet weak var _view: UIView!
    var senderVC : UIViewController?
    weak var delegate:EditBioDelegate?
    
    
    var keyBoardAccessoryView = TRKeyboardAccessoryView.getView()
    
    func showPopUp(_ sender : UIViewController) {
        senderVC = sender
        self.frame = sender.view.frame
        animateIn(sender)
        keyBoardAccessoryView.delegate = self
        IQKeyboardManager.shared.enableAutoToolbar = true
        text_View.inputAccessoryView = keyBoardAccessoryView
        self.lbl_CharCount.text = "\(CharacterLimit - text_View.text.count)"
    }
    
    func doneButtonTapped(){
        if(text_View.text.count == 0){
        }else {
           self.addBio()
        }
        
        animateOut()
        text_View.resignFirstResponder()
    }
    
    func animateIn(_ sender : UIViewController) {
        sender.view.addSubview(self)
        self.center = sender.view.center
        _view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
        _view.alpha = 0.5
        
        UIView.animate(withDuration: 0.3) {
            self._view.alpha = 1
            self._view.transform = .identity
        }
        text_View.becomeFirstResponder()
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self._view.transform = CGAffineTransform(scaleX:0.7, y:0.7)
            self._view.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
        text_View.resignFirstResponder()
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    @IBAction func buttonClicked_HidePopUp(_ sender : UIButton) {
        animateOut()
    }
    
    @IBAction func buttonClicked_Done(_ sender : UIButton) {
        animateOut()
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        self.lbl_CharCount.text = "\(CharacterLimit - updatedText.count)"
        return updatedText.count < CharacterLimit
    }

    
    private func addBio() {
        
        guard let parentVC = self.parentViewController else { return }
        
        showHud("Processing..")
        let params = ["key":Api.key,
                      ApiConstant.userId:userInfo?.userId ?? "",
                      ApiConstant.userBio:text_View.text ?? ""]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.addBio, andParameter: params) { (response, error) in
            hideHud()
            print(response ?? "No response")
            
            if error == nil {
                  hideHud()
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                          hideHud()
                        self.delegate?.bioUpdated(self.text_View.text ?? "")
                    } else {
                          hideHud()
                        let error = "\(String(describing: result[NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                        showAlert(ERROR, message: error, onView: parentVC)
                    }
                } else {
                      hideHud()
                    showAlert(ERROR, message: SOMETHING_WRONG, onView: parentVC)
                }
            } else {
                  hideHud()
                showAlert(ERROR, message: error?.localizedDescription ?? SOMETHING_WRONG, onView: parentVC)
            }
        }
        
    }
}
