//
//  AvailabilityView.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 19/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@objc protocol AvailabilityViewDelegate : class {
    @objc func popHides()
    @objc func setAMAvailability(_ day:String, available:Bool)
    @objc func setPMAvailability(_ day:String, available:Bool)
}

@IBDesignable
class AvailabilityView: UIView {

    @IBOutlet weak var table_View:UITableView!
    let daysArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    @IBOutlet weak var _view: UIView!
    var senderVC : UIViewController?
    
    @IBOutlet weak var delegate:AvailabilityViewDelegate?
    
    func showPopUp(_ sender : UIViewController) {
        senderVC = sender
        self.frame = sender.view.frame
        animateIn(sender)
    }
    
    func animateIn(_ sender : UIViewController) {
        sender.view.addSubview(self)
        self.center = sender.view.center
        _view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
        _view.alpha = 0.5
        
        UIView.animate(withDuration: 0.3) {
            self._view.alpha = 1
            self._view.transform = .identity
        }
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self._view.transform = CGAffineTransform(scaleX:0.3, y:0.3)
            self._view.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    @IBAction func buttonClicked_HidePopUp(_ sender : UIButton) {
        animateOut()
        self.delegate?.popHides()
    }
    
    @IBAction func buttonClicked_Done(_ sender : UIButton) {
        animateOut()
    }
}

extension AvailabilityView:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return daysArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AvailabilityTableCell") as? AvailabilityTableCell {
            cell.lbl_DayName.text = daysArray[indexPath.row]
            cell.parentView = self
            cell.btn_AM.isSelected = false
            cell.btn_PM.isSelected = false
            cell.btn_AM.tag = indexPath.row
            cell.btn_PM.tag = indexPath.row
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
}

extension AvailabilityView:UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0*getScaleFactor()
    }
}

class AvailabilityTableCell:UITableViewCell {
    
    @IBOutlet weak var lbl_DayName:UILabel!
    @IBOutlet weak var btn_AM:UIButton!
    @IBOutlet weak var btn_PM:UIButton!
    
    var parentView:AvailabilityView?
    
    @IBAction func buttonClicked_BtnAM(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if let _view = parentView {
            let dayName = _view.daysArray[sender.tag]
            _view.delegate?.setAMAvailability(dayName, available: sender.isSelected)
        }
    }
    
    @IBAction func buttonClicked_BtnPM(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        if let _view = parentView {
            let dayName = _view.daysArray[sender.tag]
            _view.delegate?.setPMAvailability(dayName, available: sender.isSelected)
        }
    }
}


