//
//  ApiSingleton.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiLocationManager {
    
    typealias serviceCompletion = (_: Any?, _: Error?) -> Void
 static let shared = ApiLocationManager()


    
    func apiManeger(api:String,_ params:[String:Any],_ encoding : ParameterEncoding,withCompletion responseHandler: @escaping serviceCompletion ) {
        Alamofire.request(api, method: .post, parameters: params, encoding: encoding , headers: nil).responseJSON { (response) in
            showHud("Processing")
            print(response.value as Any)
            print(response.error as Any)
            if response.error == nil {
                hideHud()
                responseHandler(response.result.value, response.error)
            }
            
            }
    }
    
    func uploadImage(_ request: String, onPath image:Data?, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
//        var serverPath  = getServerPath()
//
//        let reuestUrl = serverPath + request
//
//        print(reuestUrl)
//
////
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            if let imgExist = image {
//
//
//                    multipartFormData.append(imgExist, withName:ApiConstant.post_photo, fileName: "photo.png", mimeType: "image/png")
//
//            }else {
//                print("error")
//            }
//
//            if let allParams = parameters as? [String:String] {
//                for (key, value) in allParams {
//                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
//                }
//            }}, to: "https://apisports100.co.uk/sports100/index.php/teams/addTeam", method: .post, headers:nil,
//                encodingCompletion: { result in
//                    switch result {
//                    case .success(let upload, _, _):
//
//                        upload.responseJSON { response in
//                            guard response.result.error == nil else {
//                                print("error response")
//                                print(response.result.error ?? "error response")
//                                getResponse(nil,response.result.error)
//                                return
//                            }
//                            if let value = response.result.value {
//                                print(value)
//                                getResponse(value,nil)
//                            }
//                        }
//                    case .failure(let encodingError):
//                        print("error:\(encodingError)")
//                        getResponse(nil, encodingError)
//                    }
//        })
//
        
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")
                
                if let allParams = parameters as? [String:String] {
                                    for (key, value) in allParams {
                                        MultipartFormData.append(value.data(using: .utf8)!, withName: key)
                                        
                                    }
                                }
                if let imgExist = image {
                    if request == Api.uploadVideo {
                        MultipartFormData.append(imgExist, withName:ApiConstant.post_Video, fileName: "video.mov", mimeType: "video/mp4")
                    } else  {
                        MultipartFormData.append(imgExist, withName:ApiConstant.post_photo, fileName: "photo.png", mimeType: "image/png")
                    }
                }
        }, to: request) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    getResponse(response.result.value, response.error)
                    print(response.result.value as Any)
                }
                
            case .failure(let encodingError): break
            print(encodingError)
            }
    }
    

    
}
    
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            if let data = imageData{
                 multipartFormData.append(data, withName:"video", fileName: "video.mov", mimeType: "video/mp4")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response.result.value as Any)
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    func getServerPath() -> String
    {
        let serverPath: String = ConfigurationManager.sharedInstance.APIEndpoint()
        // serverPath.append(MiddlePath)
        print("serverpath",serverPath)
        return serverPath
    }

}
struct url {

    
   static let baseUrl = "https://apisports100.co.uk/sports100/index.php/"
    static let uploadBase = "https://apisports100.co.uk/uploads/"
    
    static let getSport = baseUrl + "users/getSport"
    static let editSport = baseUrl + "teams/editTeam"
    static let teamSearch = baseUrl + "teams/teamSearch"
    static let userSearch = baseUrl + "users/searchUser"
    static let userRegister = baseUrl  + "users/register"
    static let addTeam = baseUrl  + "teams/addTeam"
    static let notificationCount = baseUrl + "messages/getallMesagescount"
    static let readNotificationCount = baseUrl + "messages/clearMessagescount"
    static let uploadVideo = uploadBase + "posts/addPost"
}

