//
//  AgeModel.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchModel: NSObject {
    
    
    var id : String?
    var first_name : String?
    var last_name : String?
    var profile_pic : String?
    var gender : String?
    var dob : String?
    var email : String?
    var nickname : String?
    var street : String?
    var town : String?
    var post_code : String?
    var nationality : String?
    var height : String?
    var weight : String?
    var bio : String?
    var dbs : String?
    var availability : String?
    var strongest : String?
    var sport_name : String?
    var latitude : String?
    var longitude : String?
    var position_name: String?
    var standard: String?
    
    
    
    convenience init(withDic: [String : Any]) {
        self.init()
        let json = JSON(withDic)
        self.id = json["id"].stringValue
        self.first_name = json["first_name"].stringValue
        self.last_name = json["last_name"].stringValue
        self.profile_pic = json["profile_pic"].stringValue
        self.gender = json["gender"].stringValue
        self.dob = json["dob"].stringValue
        self.email = json["email"].stringValue
        self.nickname = json["nickname"].stringValue
        self.street = json["street"].stringValue
        self.town = json["town"].stringValue
        self.post_code = json["post_code"].stringValue
        self.nationality = json["nationality"].stringValue
        self.height = json["height"].stringValue
        self.weight = json["weight"].stringValue
        self.bio = json["bio"].stringValue
        self.dbs = json["dbs"].stringValue
        self.availability = json["availability"].stringValue
        self.strongest = json["strongest"].stringValue
        self.sport_name = json["sport_name"].stringValue
        self.position_name = json["position_name"].stringValue
        self.standard = json["standard"].stringValue
        self.latitude = json["latitude"].stringValue
        self.longitude = json["longitude"].stringValue
        
    }
}
