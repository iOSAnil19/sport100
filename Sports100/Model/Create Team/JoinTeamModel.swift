//
//  AgeModel.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class JoinTeamModel: NSObject {
    
    
    var id : String?
    var sport_id : String?
    var user_id : String?
    var team_name : String?
    var street : String?
    var city : String?
    var home_ground_name : String?
    var post_code : String?
    var home_location : String?
    var league_id : String?
    var profile : String?
    var website : String?
    var cost : String?
    var team_logo : String?
    var location : String?
    var age_group : String?
    var team_gender : String?
    var sport_name : String?
    var standard : String?
    var latitude : String?
    var longitude : String?
    
    
    
    
    convenience init(withDic: [String : Any]) {
        self.init()
        let json = JSON(withDic)
        self.id = json["id"].stringValue
        self.sport_id = json["sport_id"].stringValue
        self.user_id = json["user_id"].stringValue
        self.team_name = json["team_name"].stringValue
        self.street = json["street"].stringValue
        self.city = json["city"].stringValue
        self.home_ground_name = json["home_ground_name"].stringValue
        self.post_code = json["post_code"].stringValue
        self.home_location = json["home_location"].stringValue
        self.league_id = json["league_id"].stringValue
        self.profile = json["profile"].stringValue
        self.website = json["website"].stringValue
        self.cost = json["cost"].stringValue
        self.team_logo = json["team_logo"].stringValue
        self.location = json["location"].stringValue
        self.age_group = json["age_group"].stringValue
        self.team_gender = json["team_gender"].stringValue
        self.sport_name = json["sport_name"].stringValue
        self.standard = json["standard"].stringValue
        self.sport_id = json["sport_id"].stringValue
        self.latitude = json["latitude"].stringValue
        self.longitude = json["longitude"].stringValue
        
    }
}
