//
//  AgeModel.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class AgeModel: NSObject {

    
    var id : String?
    var group : String?
    
    
    
    convenience init(withDic: [String : Any]) {
        self.init()
        let json = JSON(withDic)
        self.id = json["id"].stringValue
        self.group = json["group"].stringValue
    }
}
