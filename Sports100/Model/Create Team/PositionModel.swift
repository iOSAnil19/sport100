//
//  AgeModel.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostionModel: NSObject {
    
    
    var id : String?
    var Position : String?
    
    
    
    convenience init(withDic: [String : Any]) {
        self.init()
        let json = JSON(withDic)
        self.id = json["ID"].stringValue
        self.Position = json["Position"].stringValue
    }
}
