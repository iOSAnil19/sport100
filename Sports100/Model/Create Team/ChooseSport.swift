//
//  ChooseSport.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import  SwiftyJSON

class ChooseSport: NSObject {
    
    var id : String?
    var name : String?
    
    
    
    convenience init(withDic: [String : Any]) {
        self.init()
        let json = JSON(withDic)
        self.id = json["ID"].stringValue
        self.name = json["Name"].stringValue
    }
    
    
    
}
