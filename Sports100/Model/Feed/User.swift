//
//  User.swift
//
//  Created by Vivan Raghuvanshi on 28/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON


public struct User {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let firstName = "FirstName"
    static let profilePic = "ProfilePic"
    static let lastName = "LastName"
    static let userID = "UserId"
  }

  // MARK: Properties
  public var iD: String?
  public var firstName: String?
  public var profilePic: String?
  public var lastName: String?
  public var userID: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    iD = json[SerializationKeys.iD].string
    firstName = json[SerializationKeys.firstName].string
    profilePic = json[SerializationKeys.profilePic].string
    lastName = json[SerializationKeys.lastName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = profilePic { dictionary[SerializationKeys.profilePic] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    return dictionary
  }

}
