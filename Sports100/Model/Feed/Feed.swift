//
//  Feed.swift
//
//  Created by Vivan Raghuvanshi on 28/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Feed {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let feedID = "FeedID"
        static let type = "Type"
        static let feed = "Feed"
        static let user = "User"
        static let isLiked = "isLiked"
        static let created = "Created"
        static let likeCount = "likeCount"
        static let commentCount = "CommentCount"
        static let videoThumb = "videoThumb"
        static let userId = "UserId"
        static let profilePic = "profile_pic"
        
    }
    
    // MARK: Properties
    public var feedID: String?
    public var type: String?
    public var feed: String?
    public var isLiked: Bool?
    public var created: String?
    public var likeCount: String?
    public var commentCount: String?
    public var videoThumb: String?
    public var desc: String?
    public var title: String?
    public var userID: String?
    public var profilePic: String?

    //public var thumbNailImage: UIImage?
    public var user: User?
    
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        feedID = json[SerializationKeys.feedID].string
        type = json[SerializationKeys.type].string
        feed = json[SerializationKeys.feed].string
        if type == "Video" {
            videoThumb = json[SerializationKeys.videoThumb].string
            desc = json["Description"].string
            title = json["VideoTitle"].string
        } else if type == "Image" {
            desc = json["Description"].string
            title = json["ImageTitle"].string
        }else if type == "Team" {
            desc = json["Description"].string
        }
        let liked = json[SerializationKeys.isLiked].string
        userID = json[SerializationKeys.userId].string
        self.profilePic = json[SerializationKeys.profilePic].string
        self.isLiked = liked == "true" ? true: false
        created = json[SerializationKeys.created].string
        likeCount = json[SerializationKeys.likeCount].string
        if likeCount == "" {
            likeCount = "0"
        }
        commentCount = json[SerializationKeys.commentCount].string
        if commentCount == "" {
            commentCount = "0"
        }
        user = User(object: json[SerializationKeys.user])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = feedID { dictionary[SerializationKeys.feedID] = value }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = videoThumb { dictionary[SerializationKeys.videoThumb] = value }
        if let value = feed { dictionary[SerializationKeys.feed] = value }
        if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
        if let value = created { dictionary[SerializationKeys.created] = value }
        if let value = likeCount { dictionary[SerializationKeys.likeCount] = value }
        if let value = commentCount { dictionary[SerializationKeys.commentCount] = value }
        if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
        return dictionary
    }
    
}
