//
//  RecentMsg.swift
//
//  Created by Vivan Raghuvanshi on 17/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct RecentMsg {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let created = "Created"
    static let message = "Message"
    static let messageID = "MessageID"
    static let iD = "UserID"
    static let name = "Name"
    static let image = "Image"

  }

  // MARK: Properties
  public var created: String?
  public var message: String?
  public var messageID: String?
  public var iD: String?
  public var name: String?
  public var image: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    created = json[SerializationKeys.created].string
    message = json[SerializationKeys.message].string
    messageID = json[SerializationKeys.messageID].string
    iD = json[SerializationKeys.iD].string
    name = json[SerializationKeys.name].string
    image = json[SerializationKeys.image].string

    }

}
