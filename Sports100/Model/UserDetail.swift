//
//  UserDetail.swift
//  Sports100
//
//  Created by Nripendra Hudda on 18/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import SwiftyJSON

enum UserType:String {
    case SOCIAL = "social"
    case NORMAL = "normal"
}

var userInfo:UserData? {
    didSet {
        if let info = userInfo {
            if info.availabilityTime.count > 0 {
                let availabilitiesModel = info.availabilityTime.map({ (info) -> Availability in
                    Availability.init(id: "\(String(describing: info["ID"] ?? ""))", day: "\(String(describing: info["Day"] ?? ""))", time: "\(String(describing: info["aTime"] ?? ""))")
                })
                if availabilitiesModel.count > 0 {
                    userInfo?.availabilities = availabilitiesModel
                }
            }
            
            if info.latestAchievements.count > 0 {
                let achievementsModel = info.latestAchievements.map({ (info) -> Achievement in
                    Achievement.init(id: "\(String(describing: info["achievemrntID"] ?? ""))", description: "\(String(describing: info["Description"] ?? ""))", aDate: "\(String(describing: info["Created"] ?? ""))")
                })
                if achievementsModel.count > 0 {
                    userInfo?.userAchievements = achievementsModel
                }
            }
            
            if info.allTeams.count > 0 {
                let teamModel = info.allTeams.map({ (teamInfo) -> UserTeam in
                    UserTeam.init(teamName: "\(String(describing: teamInfo["Name"] ?? ""))", teamId: "\(String(describing: teamInfo["ID"] ?? ""))", teamImage: "\(String(describing: teamInfo["Logo"] ?? ""))", teamGround: "\(String(describing: teamInfo["HomeGround"] ?? ""))")
                })
                if teamModel.count > 0 {
                    userInfo?.teams = teamModel
                }
            }
            
            if info.postImVi.count > 0 {
                let postsModel = info.postImVi.map({ (info) -> Post in
                    
                    let type = "\(String(describing: info["Type"] ?? ""))"
                    
                    
                    if type == "Video" {
                        return Post.init(postImage: "\(String(describing: info["postVideo"] ?? ""))", type: "\(String(describing: info["Type"] ?? ""))", postThumb: "\(String(describing: info["videoThumb"] ?? ""))")
                    } else {
                        return Post.init(postImage: "\(String(describing: info["postImage"] ?? ""))", type: "\(String(describing: info["Type"] ?? ""))", postThumb: "\(String(describing: info["videoThumb"] ?? ""))")
                    }
                })
                if postsModel.count > 0 {
                    userInfo?.userPosts = postsModel
                }
            }
        }
    }
}

var otherUserInfo:UserData? {
    didSet {
        if let info = otherUserInfo {
            setUpModels(info)
        }
    }
}

func setUpModels(_ info:UserData) {
    if info.availabilityTime.count > 0 {
        let availabilitiesModel = info.availabilityTime.map({ (info) -> Availability in
            Availability.init(id: "\(String(describing: info["ID"] ?? ""))", day: "\(String(describing: info["Day"] ?? ""))", time: "\(String(describing: info["aTime"] ?? ""))")
        })
        if availabilitiesModel.count > 0 {
            info.availabilities = availabilitiesModel
        }
    }
    
    if info.allTeams.count > 0 {
        let teamModel = info.allTeams.map({ (teamInfo) -> UserTeam in
            UserTeam.init(teamName: "\(String(describing: teamInfo["Name"] ?? ""))", teamId: "\(String(describing: teamInfo["ID"] ?? ""))", teamImage: "\(String(describing: teamInfo["Logo"] ?? ""))", teamGround: "\(String(describing: teamInfo["HomeGround"] ?? ""))")
        })
        if teamModel.count > 0 {
            otherUserInfo?.teams = teamModel
        }
    }
    
    if info.latestAchievements.count > 0 {
        let achievementsModel = info.latestAchievements.map({ (info) -> Achievement in
            Achievement.init(id: "\(String(describing: info["achievemrntID"] ?? ""))", description: "\(String(describing: info["Description"] ?? ""))", aDate: "\(String(describing: info["Created"] ?? ""))")
        })
        if achievementsModel.count > 0 {
            info.userAchievements = achievementsModel
        }
    }
    
    if info.postImVi.count > 0 {
        let postsModel = info.postImVi.map({ (info) -> Post in
            
            let type = "\(String(describing: info["Type"] ?? ""))"
            
            
            if type == "Video" {
                return Post.init(postImage: "\(String(describing: info["postVideo"] ?? ""))", type: "\(String(describing: info["Type"] ?? ""))", postThumb: "\(String(describing: info["videoThumb"] ?? ""))")
            } else {
                return Post.init(postImage: "\(String(describing: info["postImage"] ?? ""))", type: "\(String(describing: info["Type"] ?? ""))", postThumb: "\(String(describing: info["videoThumb"] ?? ""))")
            }
        })
        if postsModel.count > 0 {
            info.userPosts = postsModel
        }
    }
}

class GetUser {
    
    class func getUserProfileInfo(_ userInfo:@escaping(_: UserData?) -> Void) {
        let networkManager = NetworkManager()
        
        guard let user = UserInfo.sharedInfo.getUserInfo() else { return }
        // "fromID":user.userId Remove By Nripendra 
        let params = ["userID":user.userId, "key":Api.key, "fromID":user.userId,] as [String : Any]
        print(params)
        networkManager.getDataForRequest(Api.getProfile, andParameter:params) { (response, error) in
            hideHud()
            if error == nil {
                
                if let responseObj = response as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        guard let responseExist = responseObj["response"] as? [String : Any] else { return }
                        if let result = responseExist["UserDetails"] as? [String : Any] {
                           let user =  UserInfo.sharedInfo.setUserProfileWithInfo(result)
                           // let user = UserInfo.sharedInfo.getUserInfo()
                            userInfo(user)
                        }
                    } else {
                        userInfo(nil)
                        // showAlert(kLang(key: Error), message:result["result"] as? String ?? kLang(key: Somthing_wrong))
                    }
                }
            } else {
                userInfo(nil)
                //showAlert(kLang(key: Error), message: error?.localizedDescription ?? kLang(key: Somthing_wrong))
            }
        }
    }
    
    class func getOtherUserProfileInfo(_ id:String, _ otherUser:@escaping(_: UserData?) -> Void) {
        let networkManager = NetworkManager()
        
        let params = ["userID":id, "key":Api.key, "fromID":userInfo?.userId ?? ""] as [String : Any]
        
        networkManager.getDataForRequest(Api.getProfile, andParameter:params) { (response, error) in
            hideHud()
            if error == nil {
                
                if let responseObj = response as? [String : Any] {
                    let success = Int("\(String(describing: responseObj[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        guard let responseExist = responseObj["response"] as? [String : Any] else { return }
                        if let result = responseExist["UserDetails"] as? [String : Any] {
                            let user = UserData.init(result)
                            
                            if let friend = responseObj["friend"] as? String {
                                if friend == "Yes" {
                                     user.isFriend = true
                                } else if friend == "Pending" {
                                     user.isPending = true
                                }
                            }
                            otherUser(user)
                        }
                    } else {
                        otherUser(nil)
                        
                        // showAlert(kLang(key: Error), message:result["result"] as? String ?? kLang(key: Somthing_wrong))
                    }
                }
            } else {
                otherUser(nil)
                //showAlert(kLang(key: Error), message: error?.localizedDescription ?? kLang(key: Somthing_wrong))
            }
        }
    }
}

class UserInfo {
    
    class var sharedInfo : UserInfo {
        struct Singleton {
            static let instance = UserInfo()
        }
        return Singleton.instance
    }
    
    func setUserProfileWithInfo(_ userDict : [String: Any]?) -> UserData? {
        let user = UserData.init(userDict)
        self.saveUserInfo(user)
        userInfo = user
        UserDefaults.standard.set(userInfo?.email, forKey: CACHED_MAIL)
        return userInfo
    }
    
    func updateUserProfileWithInfo(_ user : UserData) {
        UserDefaults.standard.set(nil, forKey: "user")
        self.saveUserInfo(user)
    }
    
    private func saveUserInfo(_ user : UserData) {
        let encodedObj = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(encodedObj, forKey: "user")
    }
    
    func getUserInfo() -> UserData? {
        
        if let encodedObj  = UserDefaults.standard.object(forKey: "user") {
            let user = NSKeyedUnarchiver.unarchiveObject(with: encodedObj as! Data) as! UserData
            return user
        }
        return nil
    }
}

class UserData: NSObject ,NSCoding {
    
    /*{
    "device_token" : "123456",
    "Achievement" : [
    {
    "Type" : "Achievement",
    "Created" : "2018-12-23 04:33:50",
    "Description" : "Dhsdhskdhskdskjdhskjhdshdkshdkjhsjdhsjdhskhdkshdksds",
    "achievemrntID" : "20"
    }
    ],
    "AvailabilityTime" : [
    {
    "ID" : "2545",
    "Day" : "Thu",
    "aTime" : "AM"
    }
    ]
    }*/
    

    var userId       = ""
    var fBID        = ""
    var email       = ""
    var lastName    = ""
    var firstName   = ""
    var gender      = ""
    var sportType   = ""
    var sportCode   = ""
    var dob         = ""
    var username    = ""
    var userType    = UserType.NORMAL
    var nickName    = ""
    var city        = ""
    var town        = ""
    var street      = ""
    var country     = ""
    var postCode    = ""
    var nationality = ""
    var status    = ""
    var height    = ""
    var weight    = ""
    var bio       = ""
    var radius    = ""
    var dbs       = ""
    var position  = ""
    var userImage = ""
    var positionID = ""
    var strongest  = ""
    var availability    = ""
    var activated = ""
    var sportId = ""
    var IS_completed = ""
    var isHeight = ""
    var isWeight = ""
    var standard = ""
    var standardID = ""
    var availabilityTime = [[String:Any]]()
    var latestAchievements = [[String:Any]]()
    var postImVi = [[String:Any]]()
    var allTeams = [[String:Any]]()
    var allImg = [[String:Any]]()
    
    var isFriend = false
    var isPending = false


    var availabilities = [Availability]()
    var userAchievements = [Achievement]()
    var userPosts = [Post]()
    var teams = [UserTeam]()

    override init() {
        super.init()
    }
    
    convenience init(_ userDict : Any?) {
        self.init()
        
        print(JSON(userDict))

        if (userDict is NSNull) {return}
        guard let userInfo =  userDict as? Dictionary<String, Any> else { return }
                
        if let id = userInfo["id"], !(id is NSNull) {
            userId = id as? String ?? ""
        }
        
        if let _availabilityTime = userInfo["AvailabilityTime"], !(_availabilityTime is NSNull) {
            if let _availableTime = _availabilityTime as? [[String:Any]] {
                availabilityTime = _availableTime
            }
        }
      
        
        if let _postImage = userInfo["postImage"], !(_postImage is NSNull) {
            if let post = _postImage as? [String:Any] {
                if let _userPost = post["PostImage"] as? [[String:Any]] {
                    postImVi = _userPost
                }
            }
        }
        
        if let _latestAchievements = userInfo["Achievement"], !(_latestAchievements is NSNull) {
                if let _userAchievement = _latestAchievements as? [[String:Any]] {
                    latestAchievements = _userAchievement
                }
        }
        
        if let _Teams = userInfo["Team"], !(_Teams is NSNull) {
                if let _allTeams = _Teams as? [[String:Any]] {
                    allTeams = _allTeams
                }
        }
        if let _PostImage = userInfo["postImage"], !(_PostImage is NSNull) {
            if let _allImg = _PostImage as? [[String:Any]] {
                postImVi = _allImg
            }
        }
        
        
        if let _positionID = userInfo["position_id"], !(_positionID is NSNull) {
            positionID = _positionID as? String ?? ""
        }
        
        if let _sportId = userInfo["sport_id"], !(_sportId is NSNull) {
            sportId = _sportId as? String ?? ""
        }
        
        
        if let _strongest = userInfo["strongest"], !(_strongest is NSNull) {
            strongest = _strongest as? String ?? ""
        }
        
        if let _nickName = userInfo["nickname"], !(_nickName is NSNull) {
            nickName = _nickName as? String ?? ""
        }
        
        if let profile_photo = userInfo["profile_pic"], !(profile_photo is NSNull) {
            userImage = profile_photo as? String ?? ""
        }
        
        if let _city = userInfo["town"], !(_city is NSNull) {
                city = _city as? String ?? ""
        }
        if let _town = userInfo["city"], !(_town is NSNull) {
                  town = _town as? String ?? ""
              }
        
        if let primaryPosition = userInfo["position_name"], !(primaryPosition is NSNull) {
            position = primaryPosition as? String ?? ""
        }
        
        if let _street = userInfo["street"], !(_street is NSNull) {
            street = _street as? String ?? ""
        }
        
        if let _country = userInfo["country"], !(_country is NSNull) {
            country = _country as? String ?? ""
        }
        
        if let _postCode = userInfo["post_code"], !(_postCode is NSNull) {
            postCode = _postCode as? String ?? ""
        }
        
        if let _nationality = userInfo["nationality"], !(_nationality is NSNull) {
            let n = _nationality as? String ?? ""
            nationality = n.capitalized
        }
        
        if let _status = userInfo["status"], !(_status is NSNull) {
            status = _status as? String ?? ""
        }
        
        if let _availability = userInfo["availability"], !(_availability is NSNull) {
            let a = _availability as? String ?? ""
            availability = a == "YES" ? "Available":"Not Available"
        }
        
        if let _height = userInfo["height"], !(_height is NSNull) {
            height = _height as? String ?? ""
        }
        
        if let _heightHide = userInfo["is_hide_weight"], !(_heightHide is NSNull) {
            isHeight = _heightHide as? String ?? ""
        }
        if let _stand = userInfo["standard"], !(_stand is NSNull) {
            standard = _stand as? String ?? ""
        }
        if let _standID = userInfo["standardID"], !(_standID is NSNull) {
            standardID = _standID as? String ?? ""
        }
        
        if let _weight = userInfo["weight"], !(_weight is NSNull) {
            weight = _weight as? String ?? ""
        }
        
        if let _weightHide = userInfo["is_hide_weight"], !(_weightHide is NSNull) {
            isWeight = _weightHide as? String ?? ""
        }
        
        
        if let _bio = userInfo["bio"], !(_bio is NSNull) {
            bio = _bio as? String ?? ""
        }
        
        if let _dbs = userInfo["dbs"], !(_dbs is NSNull) {
            dbs = _dbs as? String ?? ""
        }
        
        if let _FBID = userInfo["fb_id"], !(_FBID is NSNull) {
            fBID = _FBID as? String ?? ""
            if fBID != "" {
                userType = UserType.SOCIAL
            }
        }
        
        if let _email = userInfo["email"], !(_email is NSNull) {
            email = _email as? String  ?? ""
        }
        
        if let user_name = userInfo["username"], !(user_name is NSNull) {
            username = user_name as? String  ?? ""
        }
        
        if let first_name = userInfo["first_name"], !(first_name is NSNull) {
            firstName = first_name as? String ?? ""
        }
        
        if let last_Name = userInfo["last_name"], !(last_Name is NSNull) {
            lastName = last_Name as? String  ?? ""
        }
        
        if let _dob = userInfo["dob"], !(_dob is NSNull) {
            dob = _dob as? String  ?? ""
        }
        
        if let _gender = userInfo["gender"], !(_gender is NSNull) {
            gender = _gender as? String  ?? ""
        }
        
        if let _sportType = userInfo["sport_name"], !(_sportType is NSNull) {
            sportType = _sportType as? String  ?? ""
        }
        
        if let _sportCode = userInfo["sport_id"], !(_sportCode is NSNull) {
            sportCode = _sportCode as? String  ?? ""
        }
        if let _activated = userInfo["activated"], !(_activated is NSNull) {
            activated = _activated as? String  ?? ""
        }
        
        if let _is_completed = userInfo["is_completed"], !( _is_completed is NSNull) {
            IS_completed =  _is_completed as? String ?? ""
        }
        
    }
    
    
    required init(coder decoder: NSCoder) {
        
        self.userId = decoder.decodeObject(forKey: "userId") as! String
        self.position = decoder.decodeObject(forKey: "primaryPosition") as! String
        self.email = decoder.decodeObject(forKey: "email") as! String
        self.lastName = decoder.decodeObject(forKey: "lastName") as! String
        self.firstName = decoder.decodeObject(forKey: "firstName") as! String
        self.gender = decoder.decodeObject(forKey: "gender") as! String
        self.dob = decoder.decodeObject(forKey: "dob") as! String
        self.username = decoder.decodeObject(forKey: "username") as! String
        self.sportType = decoder.decodeObject(forKey: "primarySport") as! String
        self.sportId = decoder.decodeObject(forKey: "sportId") as! String
        self.sportCode = decoder.decodeObject(forKey: "sportCode") as! String
        self.fBID = decoder.decodeObject(forKey: "fBID") as! String
        self.nickName = decoder.decodeObject(forKey: "nickName") as! String
      //  self.city = decoder.decodeObject(forKey: "city") as! String
        self.town = decoder.decodeObject(forKey: "town") as! String
        self.street = decoder.decodeObject(forKey: "street") as! String
        self.country = decoder.decodeObject(forKey: "country") as! String
        self.postCode = decoder.decodeObject(forKey: "postCode") as! String
        self.nationality = decoder.decodeObject(forKey: "nationality") as! String
        self.status = decoder.decodeObject(forKey: "status") as! String
        self.height = decoder.decodeObject(forKey: "height") as! String
        self.weight = decoder.decodeObject(forKey: "weight") as! String
        self.bio = decoder.decodeObject(forKey: "bio") as! String
        self.radius = decoder.decodeObject(forKey: "radius") as! String
        self.dbs = decoder.decodeObject(forKey: "dbs") as! String
        self.userImage = decoder.decodeObject(forKey: "userImage") as! String
        self.positionID = decoder.decodeObject(forKey: "positionID") as! String
        self.strongest = decoder.decodeObject(forKey: "strongest") as! String
        self.activated = decoder.decodeObject(forKey: "activated") as! String
        self.IS_completed = decoder.decodeObject(forKey: "IS_completed") as! String
//        self.isHeight = decoder.decodeObject(forKey: "is_hide_height") as! String
//        self.isWeight = decoder.decodeObject(forKey: "is_hide_weight") as! String
        self.availabilityTime = decoder.decodeObject(forKey: "availabilityTime") as! [[String:Any]]
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(position, forKey: "primaryPosition")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(dob, forKey: "dob")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(sportType, forKey: "primarySport")
        aCoder.encode(sportId, forKey: "sportId")
        aCoder.encode(sportCode, forKey: "sportCode")
        aCoder.encode(fBID, forKey: "fBID")
        aCoder.encode(nickName, forKey: "nickName")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(town, forKey: "town")
        aCoder.encode(street, forKey: "street")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(postCode, forKey: "postCode")
        aCoder.encode(nationality, forKey: "nationality")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(height, forKey: "height")
        aCoder.encode(weight, forKey: "weight")
        aCoder.encode(bio, forKey: "bio")
        aCoder.encode(radius, forKey: "radius")
        aCoder.encode(dbs, forKey: "dbs")
        aCoder.encode(sportType, forKey: "sportType")
        aCoder.encode(userImage, forKey: "userImage")
        aCoder.encode(positionID, forKey: "positionID")
        aCoder.encode(strongest, forKey: "strongest")
        aCoder.encode(activated, forKey: "activated")
        aCoder.encode(IS_completed, forKey: "IS_completed")
//        aCoder.encode(isWeight, forKey: "is_hide_weight")
//        aCoder.encode(isHeight, forKey: "is_hide_height")
        aCoder.encode(availabilityTime, forKey: "availabilityTime")
    }
}


