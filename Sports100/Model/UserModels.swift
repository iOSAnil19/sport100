//
//  UserModels.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 01/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

struct Availability {
    let id:String
    let day:String
    let time:String
}

struct Achievement {
    let id:String
    let description:String
    let aDate:String
}

struct Post {
    let postImage:String
    let type:String
    let postThumb:String
//    let ID : String
}


struct UserTeam {
    let teamName:String
    let teamId:String
    let teamImage:String
    let teamGround:String
}


struct Contact {
    let name:String
    let image:String
    let id:String
}

