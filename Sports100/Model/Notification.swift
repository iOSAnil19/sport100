//
//  Notification.swift
//
//  Created by Vivan Raghuvanshi on 18/03/18
//  Copyright (c) . All rights reserved.
//  993964100738454

import Foundation
import SwiftyJSON

public struct EventNotification {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let created = "Created"
    static let title = "Title"
    static let type = "Type"
    static let eventID = "EventID"
    static let userID = "UserID"
    static let fromID = "FromID"
    static let otheruserName = "otheruserName"
  }

  // MARK: Properties
  public var iD: String?
  public var created: String?
  public var title: String?
  public var type: String?
  public var eventID: String?
  public var userID: String?
  public var fromID: String?
  public var otheruserName: String?


  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    iD = json[SerializationKeys.iD].string
    created = json[SerializationKeys.created].string
    title = json[SerializationKeys.title].string
    type = json[SerializationKeys.type].string
    eventID = json[SerializationKeys.eventID].string
    userID = json[SerializationKeys.userID].string
    fromID = json[SerializationKeys.fromID].string
    otheruserName = json[SerializationKeys.otheruserName].string

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = eventID { dictionary[SerializationKeys.eventID] = value }
    if let value = userID { dictionary[SerializationKeys.userID] = value }
    if let value = fromID { dictionary[SerializationKeys.fromID] = value }
    if let value = otheruserName { dictionary[SerializationKeys.otheruserName] = value }

    return dictionary
  }

}
