//
//  Matches.swift
//
//  Created by Vivan Raghuvanshi on 26/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Matches {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let score = "Score"
    static let location = "Location"
    static let matchDate = "MatchDate"
    static let matchName = "MatchName"
  }

  // MARK: Properties
  public var iD: String?
  public var score: String?
  public var location: String?
  public var matchDate: String?
  public var matchName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    iD = json[SerializationKeys.iD].string
    score = json[SerializationKeys.score].string
    location = json[SerializationKeys.location].string
    matchDate = json[SerializationKeys.matchDate].string
    matchName = json[SerializationKeys.matchName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = score { dictionary[SerializationKeys.score] = value }
    if let value = location { dictionary[SerializationKeys.location] = value }
    if let value = matchDate { dictionary[SerializationKeys.matchDate] = value }
    if let value = matchName { dictionary[SerializationKeys.matchName] = value }
    return dictionary
  }

}
