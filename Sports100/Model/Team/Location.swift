//
//  Location.swift
//
//  Created by Vivan Raghuvanshi on 26/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Location {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let street = "Street"
    static let lat = "Lat"
    static let country = "Country"
    static let lng = "Lng"
    static let iD = "ID"
    static let town = "Town"
    static let postCode = "PostCode"
    static let created = "Created"
    static let updated = "Updated"
  }

  // MARK: Properties
  public var street: String?
  public var lat: String?
  public var country: String?
  public var lng: String?
  public var iD: String?
  public var town: String?
  public var postCode: String?
  public var created: String?
  public var updated: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    street = json[SerializationKeys.street].string
    lat = json[SerializationKeys.lat].string
    country = json[SerializationKeys.country].string
    lng = json[SerializationKeys.lng].string
    iD = json[SerializationKeys.iD].string
    town = json[SerializationKeys.town].string
    postCode = json[SerializationKeys.postCode].string
    created = json[SerializationKeys.created].string
    updated = json[SerializationKeys.updated].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = street { dictionary[SerializationKeys.street] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = country { dictionary[SerializationKeys.country] = value }
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = iD { dictionary[SerializationKeys.iD] = value }
    if let value = town { dictionary[SerializationKeys.town] = value }
    if let value = postCode { dictionary[SerializationKeys.postCode] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = updated { dictionary[SerializationKeys.updated] = value }
    return dictionary
  }

}
