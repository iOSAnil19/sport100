//
//  Sport.swift
//
//  Created by Vivan Raghuvanshi on 26/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TeamSport {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "id"
    static let name = "name"
    static let teams = "Team"
  }

  // MARK: Properties
  public var iD: String?
  public var name: String?
  public var teams: [Team]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    iD = json[SerializationKeys.iD].string
    name = json[SerializationKeys.name].string
    if let allTeams = json[SerializationKeys.teams].array {
        teams = allTeams.map({ (info) -> Team in
            var teamInfo = info.dictionaryValue
            teamInfo["Sport"] = ["ID": iD ?? "", "Name": name ?? ""]
            return Team.init(object: teamInfo)
        })
        //teams = allTeams.map { Team(json: $0) }
    }
  }
}
