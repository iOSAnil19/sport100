//
//  Team.swift
//
//  Created by Vivan Raghuvanshi on 26/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Team {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let name = "Name"
    static let profile = "Profile"
    static let website = "Website"
    static let teamAddress = "TeamAddress"
    static let member = "Member"
    static let traingTime = "TraingTime"
    static let player = "Player"
    static let gameDay = "GameDay"
    static let age = "AgeGroup"
    static let matches = "Matches"
    static let location = "Location"
    static let sport = "Sport"
    static let logo = "Logo"
    static let TeamGender = "TeamGender"
    static let standard = "standard"
    static let standardID = "standardID"
  }

  // MARK: Properties
  public var teamAddress: String?
  public var matches: [Matches]?
  public var iD: String?
  public var member: String?
  public var player: [Player]?
  public var name: String?
  public var gameDay: [GameDay]?
  public var sport: TeamSport?
  public var profile: String?
  public var logo: String?
  public var age: String?
  public var gender: String?
  public var website: String?
  public var traingTime: [TraingTime]?
  public var standard : String?
  public var standardID :String?
    
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    teamAddress = json[SerializationKeys.teamAddress].string
    if let items = json[SerializationKeys.matches].array { matches = items.map { Matches(json: $0) } }
    iD = json[SerializationKeys.iD].string
    if iD == nil {
        iD = json["ID"].string
    }
    member = json[SerializationKeys.member].string
    if let items = json[SerializationKeys.player].array { player = items.map { Player(json: $0) } }
    name = json[SerializationKeys.name].string
 //   location = Location(json: json[SerializationKeys.location])
    if let items = json[SerializationKeys.gameDay].array { gameDay = items.map { GameDay(json: $0) } }
    
    sport = TeamSport(json: json[SerializationKeys.sport])
    profile = json[SerializationKeys.profile].string
    logo = json[SerializationKeys.logo].string
    website = json[SerializationKeys.website].string
    age = json[SerializationKeys.age].string
    standardID = json[SerializationKeys.standardID].string
    standard = json[SerializationKeys.standard].string
    gender = json[SerializationKeys.TeamGender].string
    if let items = json[SerializationKeys.traingTime].array { traingTime = items.map { TraingTime(json: $0) } }
    
  }
}
