//
//  NoTrainCell.swift
//  Sports100
//
//  Created by VeeraJain on 08/07/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class NoTrainCell: UITableViewCell {

    @IBOutlet weak var lblWeDontTrain: UILabel!
    @IBOutlet weak var noTraingView: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
