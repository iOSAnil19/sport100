//
//  Message.swift
//
//  Created by Vivan Raghuvanshi on 17/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Message {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let created = "Created"
    static let from = "From"
    static let message = "Message"
    static let messageID = "MessageID"
    static let to = "To"
  }

  // MARK: Properties
  public var created: String?
  public var message: String?
  public var messageID: String?
  public var receiver: MessageUser?
  public var sender: MessageUser?
  public var datetimestamp = true


  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    created = json[SerializationKeys.created].string
    message = json[SerializationKeys.message].string
    messageID = json[SerializationKeys.messageID].string
    receiver = MessageUser(json: json[SerializationKeys.to])
    sender = MessageUser(json: json[SerializationKeys.from])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = messageID { dictionary[SerializationKeys.messageID] = value }
    if let value = receiver { dictionary[SerializationKeys.to] = value.dictionaryRepresentation() }
    if let value = sender { dictionary[SerializationKeys.from] = value.dictionaryRepresentation() }

    return dictionary
  }

}
