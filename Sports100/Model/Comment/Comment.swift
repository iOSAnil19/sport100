//
//  Comment.swift
//  Sports100
//
//  Created by Nripendra Hudda on 04/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Comment {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let commentID = "commentID"
        static let comment = "comment"
        static let user = "User"
        static let created = "Created"
    }
    
    // MARK: Properties
    public var commentID: String?
    public var comment: String?
    public var created: String?
    public var user: User?
    
    
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        commentID = json[SerializationKeys.commentID].string
        comment = json[SerializationKeys.comment].string
        created = json[SerializationKeys.created].string
        user = User(object: json[SerializationKeys.user])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = commentID { dictionary[SerializationKeys.commentID] = value }
        if let value = comment { dictionary[SerializationKeys.comment] = value }
        if let value = created { dictionary[SerializationKeys.created] = value }
        if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
        return dictionary
    }
    
}
