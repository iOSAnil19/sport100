//
//  TeamMessage.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 21/03/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct TeamMessage {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let userID = "UserID"
        static let created = "Created"
        static let name = "Name"
        static let commentID = "CommentID"
        static let image = "Image"
        static let comment = "Comment"
    }
    
    // MARK: Properties
    public var userID: String?
    public var created: String?
    public var name: String?
    public var commentID: String?
    public var image: String?
    public var comment: String?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        userID = json[SerializationKeys.userID].string
        created = json[SerializationKeys.created].string
        name = json[SerializationKeys.name].string
        commentID = json[SerializationKeys.commentID].string
        image = json[SerializationKeys.image].string
        comment = json[SerializationKeys.comment].string
    }
}
