//
//  CommonFunc.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit
import SVProgressHUD

//*************** Constants *****************

 let SCREEN_SIZE  = UIScreen.main.bounds
 let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
 let STORYBOARD = UIStoryboard.init(name: "Main", bundle: nil)

//***************** Show/Hide Hud ****************//

func showHud(_ title: String?) {
    DispatchQueue.main.async {
        
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
        SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8117647059, alpha: 1))
        SVProgressHUD.setForegroundColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        
        if title == "" || title == nil {
            SVProgressHUD.show()
        } else {
            SVProgressHUD.show(withStatus: title)
        }
    }
}

func hideHud () {
    DispatchQueue.main.async {
        SVProgressHUD.dismiss()
    }
}

func getAllCountries() -> [String] {
    
    var countries = [String]()
    
    for code in NSLocale.isoCountryCodes as [String] {
        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
        countries.append(name)
    }
    countries.append("England")
    countries.append("Scotland")
    countries.append("Wales")
    countries.append("Northern Ireland")
    
    countries = countries.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    return countries
}


//MARK:-******************************** Show Alert ********************************//

func showAlert(_ title: String?, message: String, onView sender:UIViewController) {
    
    DispatchQueue.main.async {
        showAlert(title, message: message, withAction: nil, with: true, andTitle: "Ok", onView: sender)
    }
}

func showAlert(_ title: String?, message: String, withAction addAction:UIAlertAction?, with isCancel:Bool, andTitle cancelTitle:String, onView sender:UIViewController) {
    let alertController = UIAlertController(title:AppTitle.firstUppercased, message: message.firstUppercased, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title:cancelTitle, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        if isCancel == true {
            alertController.addAction(cancelAction)
        }
        
        if let action = addAction {
            alertController.addAction(action)
        }
        
        // Present the controller
        sender.present(alertController, animated: true, completion: nil)
    
}

func showActivityViewToShare(_ sender:UIViewController,_ senderType:UITableViewCell) {
    // text to share
    let text = "The sports social media app that enables you to showcase your sporting achievements, connect with new teams and friends! Available now for free! SPORTS100 - Share. Play. Connect "
        
    // set up activity view controller
    
    guard let urlApp = URL.init(string: "https://itunes.apple.com/us/app/sports100/id1209006813?mt=8") else { return }
    
    let textToShare = [ text, urlApp ] as [Any]
    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = sender.view // so that3 iPads won't crash
    
    // exclude some activity types from the list (optional)
   // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
    
    // present the view controller
    if let popoverController = activityViewController.popoverPresentationController {
        popoverController.sourceView = senderType
    }
     sender.present(activityViewController, animated: true, completion: nil)
}

//MARK:-****************************** Email Validation ******************************//

func isValidEmail(_ testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

//MARK:-****************************** Phone Validation ******************************//

func isValidPhoneNumberFormat(_ string:String) -> Bool {
    //
    //^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$
    //^((\\+)|(00))[0-9]{6,14}$
    let PHONE_REGEX = "([+]?1+[-]?)?+([(]?+([0-9]{3})?+[)]?)?+[-]?+[0-9]{3}+[-]?+[0-9]{4}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: string)
        return result
}

//"^\+?\s?\d{3}\s?\d{3}\s?\d{4}$"

//MARK:-****************************** Limit Textfields ******************************//

func checkValidPhoneNumberLength(_ text:String, forRange range:NSRange, replacementString string: String) -> Bool {
    let charsLimit = 10
    
    let startingLength = text.count
    let lengthToAdd = string.count
    let lengthToReplace =  range.length
    let newLength = startingLength + lengthToAdd - lengthToReplace
    
    return newLength <= charsLimit
}

func limitTextFieldBeforeAndAfterDecimal(_ text:String,forRange range:NSRange, replacementString string: String) -> Bool {
    
    let checkString = text as NSString
    
    let editedString = checkString.replacingCharacters(in: range, with: string)
    if let regex = try? NSRegularExpression(pattern: "^[0-9]{2,0}*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
         return regex.numberOfMatches(in: editedString, options: .reportProgress, range: NSRange(location: 0, length: (editedString as NSString).length)) > 0
    }
    return false
}
import AVFoundation
//func getThumbnailImage(forUrl url: URL) -> UIImage? {
//    let asset: AVAsset = AVAsset(url: url)
//    let imageGenerator = AVAssetImageGenerator(asset: asset)
//
//    do {
//        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
//        var image = UIImage(cgImage: thumbnailImage)
//
//        return image
//    } catch let error {
//        print(error)
//    }
//
//    return nil
//}

func getThumbnailImage(forUrl url: URL) -> UIImage?{
    
    do {
         let asset: AVAsset = AVAsset(url: url)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
        let thumbnail = UIImage(cgImage: cgImage)
        
        return thumbnail
        
    } catch let error {
        
        print("*** Error generating thumbnail: \(error.localizedDescription)")
        return nil
        
    }
}

func allowUptoTwoDecimalPlace(_ text:String, forRange range:NSRange, replacementString string: String)-> Bool {
    
    let checkString = text as NSString
    
    let newText = checkString.replacingCharacters(in: range, with: string)
    if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
        return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
    }
    return false
} //"\\d{0,10}(\\.\\d{0,3})?"

//MARK:-****************************** Blank Checking ******************************//

func isBlank(_ text:String) -> Bool {
    if text.trimmingCharacters(in: .whitespaces).isEmpty {
        return true
    }
    return false
}

func isBlankField(_ textField:UITextField) -> Bool {
    if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
        textField.text = ""
        return true
    }
    return false
}


//MARK:-****************************** Factor of Screen ******************************//

func getScaleFactor() -> CGFloat {
    let screenRect:CGRect = UIScreen.main.bounds
    var screenHeight:CGFloat = screenRect.size.height
    if UIDevice.isIphoneX {
        if screenRect.size.width == 414 {
            screenHeight = 736
        } else {
            screenHeight = 667
        }
    }
    let scalefactor:CGFloat!
    scalefactor = screenHeight / 568.0
    if UIDevice.current.userInterfaceIdiom == .pad {
        return scalefactor
    } else {
        return scalefactor < 1.3 ? scalefactor:1.3
    }
}

//MARK:-****************************** Save and fetch from document directory ******************************//

func saveImageDocumentDirectory(_ image:UIImage?){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.jpg")
    if image == nil {
        do {
            try fileManager.removeItem(atPath: paths)
        } catch {
            print(error)
        }
    } else {
        let imageData = UIImageJPEGRepresentation(image!, 0.8)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
}

func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return "\(documentsDirectory)/apple.jpg"
}

func isIpad() -> Bool {
    let device = UIDevice.current.userInterfaceIdiom
    if device == .pad {
        return true
    }
    return false
}

 func instantiateViewControllerWithIdentifier(identifier:NSString, storyboardName:NSString)-> UIViewController
{
    let storyboard = UIStoryboard(name:storyboardName as String, bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: identifier as String)
    return vc
}

