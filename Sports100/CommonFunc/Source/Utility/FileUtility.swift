//
//  FileUtility.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//


import UIKit

class FileUtility: NSObject {

    // MARK: -
    // MARK: - Get Path local DB
    
    class func getDocumentDirectoryPath() -> String
    {
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        // Get documents folder
        let mediaDataPath: String = documentsDirectory.appending("/items.db")
        //var error: NSError? = nil
        

        if !FileManager.default.fileExists(atPath: mediaDataPath)
        {
            //NSFileManager.defaultManager().createDirectoryAtPath(mediaDataPath, withIntermediateDirectories: false, attributes: nil, error:NSError?)
          //  NSFileManager.defaultManager().createDirectoryAtPath(mediaDataPath, withIntermediateDirectories: false, attributes: nil)
           
            do
            {
               
                try FileManager.default.createDirectory(atPath: mediaDataPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError
            {
                NSLog("\(error.localizedDescription)")
            }
            
        }
        return mediaDataPath
        
    }
    

    // MARK: -
    // MARK: - Save Image in Local Storage 
    
   class func saveImageDocumentDirectory(_ image:UIImage)
    {
        let fileManager = FileManager.default
        
         let imagePAth = (self.createDirectory() as NSString).appendingPathComponent("apple.jpg")
        _ = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.jpg")
         print(imagePAth)
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        fileManager.createFile(atPath: imagePAth as String, contents: imageData, attributes: nil)
    }
    
    
    
    // MARK: -
    // MARK: - Create Temporary Storage
    
    
  class  func createDirectory() -> String
    {
        
        
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage")
        if !fileManager.fileExists(atPath: paths)
        {
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            print("Already dictionary created.")
        }
        return paths
    }
    
    
    // MARK: -
    // MARK: - Get ImageFrom Temporary Storage
    
    class func getImage()->String
    {
        let fileManager = FileManager.default
        let imagePAth  = (self.getDirectoryPath() as NSString).appendingPathComponent("apple.jpg")
        if fileManager.fileExists(atPath: imagePAth)
        {
             return imagePAth
           
        }else
        {
            return ""
        }
        
        

    }
    
    // MARK: -
    // MARK: - Get  Temporary Storag Path
    
    
    class   func getDirectoryPath() -> String
    {
          let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage")
            return paths
    }
    
    // MARK: -
    // MARK: - Delete  Temporary Storage
    
    class func deleteDirectory()
    {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage")
        
        
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        }else{
            print("Something wronge.")
        }
    }

    

    //---------------------
    // tmp directory path
    //---------------------
    
//    class func getTempDirectoryPath() -> String
//    {
//        
//        let tempString = NSString()
//         tempString.appendingPathComponent("tmp")
//        return NSHomeDirectory().stringByAppendingString(tempString as String)
//    }
    //------------------------
    // tmp directory path
    // write to provided path
    //----------------------------
    
    class func writeToDirectoryWithFilePath(filePath: String, andFileData fileData: NSData) -> Bool {
        return fileData.write(toFile: filePath, atomically: true)
    }
    // copy file from one path to other
    
    class func copyFileFromPath(fromFilePath: String, toPath toFilePath: String) -> Bool {
       
        
        
        do {
            
            print("copy")
           
            try
                FileManager.default
                    
                    
                    .copyItem(atPath: fromFilePath, toPath: toFilePath)
                 return true
            
            
        } catch let error as NSError
        {
            // Catch fires here, with an NSError being thrown
            print("error occurred, here are the details:\n \(error)")
        }
         return false
        
    }

    //---------------------
    // Remove file at path
    //---------------------
    
    class func deleteFileAtPath(filePath: String) -> Bool
    {
        if FileManager.default.fileExists(atPath: filePath)
        {
            
            do
            {
                
                print("copy")
                try
                   FileManager.default.removeItem(atPath: filePath)
                   return true
                
                
            } catch let error as NSError
            {
                // Catch fires here, with an NSError being thrown
                print("error occurred, here are the details:\n \(error)")
            }

            

        }
        
        return false
    }
    //---------------------------------
    //  Create flile path  if not exit
    //---------------------------------
    
    class func createNewDirectoryIfNotExistAtPath(path: String, andName name: String) -> Bool
    {
        let fileManager: FileManager = FileManager.default
        
        
        
        
        let newDirectoryWithPath: String = String(format: "%@/%@", path, name)
        
        let created: Bool = false
        if !fileManager.fileExists(atPath: newDirectoryWithPath) {
            do
            {
                
                try FileManager.default.createDirectory(atPath: newDirectoryWithPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError
            {
                NSLog("\(error.localizedDescription)")
            }
        }
        //[newDirectoryWithPath release];
        return created
    }
    
    //---------------------------------
    // Mark Remove File path
    //---------------------------------
    
    class func removeFileFromPath(path: String) -> Bool
    {
        let fileManager: FileManager = FileManager.default
        do
        {
            print("copy")
            try
              fileManager.removeItem(atPath: path)
           
               return true
            
        } catch let error as NSError
        {
            // Catch fires here, with an NSError being thrown
            print("error occurred, here are the details:\n \(error)")
        }
       // var isRemoved: Bool = fileManager.removeItemAtPath(path, error: nil)
        return false
    }
    
    class func copyBundleResourceToTemporaryDirectory(resourceName: String, fileExtension: String) -> NSURL?
    {
        // Get the file path in the bundle
        if let bundleURL = Bundle.main.url(forResource: resourceName, withExtension: fileExtension) {
            
            let tempDirectoryURL = NSURL.fileURL(withPath: NSTemporaryDirectory(), isDirectory: true)
            
            // Create a destination URL.
            let targetURL = tempDirectoryURL.appendingPathComponent("\(resourceName).\(fileExtension)")//URLByAppendingPathComponent("\(resourceName).\(fileExtension)")
            
            // Copy the file.
            do {
                try FileManager.default.copyItem(at: bundleURL, to: targetURL)
                return targetURL as NSURL
            } catch let error {
                NSLog("Unable to copy file: \(error)")
            }
        }
        
        return nil
    }

    
    }



