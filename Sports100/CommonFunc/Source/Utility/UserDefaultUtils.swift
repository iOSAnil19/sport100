//
//  UserDefaultUtils.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import UIKit

class UserDefaultUtils: NSObject {
  
    //-----------------------------------------------
    // Mark save data in NSUserDefaults
    //-----------------------------------------------
    
    class func saveBOOLValue(value: Bool, forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func retriveBOOLValueFroKey(key: String) -> Bool
    {
        return UserDefaults.standard.object(forKey: key as String) as! Bool
        
    }
    
    
    class func saveIntValue(value: Int, forKey key: String)
    {
        UserDefaults.standard.set(Int(value), forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    //Mark :- Save anyObject in UserDefault
    
    class func saveObject(obj: AnyObject?, forKey key: String)
    {
        UserDefaults.standard.set(obj, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    //Mark :- Retrive anyObject in UserDefault
    class func retriveObjectForKey(key: String) -> AnyObject
    {
        return UserDefaults.standard.object(forKey: key)! as AnyObject
    }
    
    //Mark :- Retrive anyObject in UserDefault
    class func retriveAnyObjectForKey(key: String) -> AnyObject?
    {
        
        let defaults = UserDefaults.standard
        
        if let valueString = defaults.string(forKey: key)
        {
            return valueString as AnyObject
        }
        
        return nil
        //return UserDefaults.standard.object(forKey: key)! as AnyObject
    }

    
    
    class func removeSavedObjectForKey(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getDoublePREF(key:NSString) -> Double
    {
        if let val = UserDefaults.standard.object(forKey: key as String) as? Double{
            return val
        }else{
            
            
            return 0
        }
    }
    
    /*!
     @method setPreferenceValueForKey
     @abstract To set the preference value for the key that has been passed
     */
    
    class func setDoublePREF(sValue:Double, key:NSString)
    {
        UserDefaults.standard.setValue(sValue, forKey: key as String)
        UserDefaults.standard.synchronize()
    }
     //------------------------------------------
     //MARK:- set and get preferences for Integer
     //------------------------------------------
    /*!
     @method getPreferenceValueForKey for array for int value
     @abstract To get the preference value for the key that has been passed
     */
    class func getIntPREF(key:NSString) -> Int
    {
        return UserDefaults.standard.object(forKey: key as String) as! Int
    }
    
    
    
    
    
    
    /*!
     @method setPreferenceValueForKey
     @abstract To set the preference value for the key that has been passed
     */
    
    class func setIntPREF(sValue:Int, key:NSString)
    {
        UserDefaults.standard.setValue(sValue, forKey: key as String)
        UserDefaults.standard.synchronize()
    }
    
    //------------------------------------------
    //MARK:- set and get preferences for Integer
    
    //------------------------------------------
    /*!
     @method getPreferenceValueForKey for array for bool value
     @abstract To get the preference value for the key that has been passed
     */
    class func getBoolPREF(key:NSString) -> Bool
    {
        return UserDefaults.standard.bool(forKey: key as String)
    }
    
    /*!
     @method setPreferenceValueForKey
     @abstract To set the preference value for the key that has been passed
     */
    
    class func setBoolPREF(sValue:Bool, key:NSString)
    {
        UserDefaults.standard.set(sValue, forKey: key as String)
        UserDefaults.standard.synchronize()
    }
    
    
    /*!
     @method getPreferenceValueForKey for array for int value
     @abstract To get the preference value for the key that has been passed
     */
    class func getFloatPREF(key:NSString) -> Float
    {
        return UserDefaults.standard.object(forKey: key as String) as! Float
    }
    
    /*!
     @method setPreferenceValueForKey
     @abstract To set the preference value for the key that has been passed
     */
    
    class func setFloatPREF(sValue:Float, key:NSString)
    {
        UserDefaults.standard.setValue(sValue, forKey: key as String)
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- set and get preferences for String


    class func saveSTRINGValue(value: String, forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- setand get preferences for String
    
    
    class func saveAnyObject(value:AnyObject, forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }

    
    class func saveProviderID(value: String, forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    class func getSTRINGValueFroKey(key: String?) -> String?
    {
        let defaults = UserDefaults.standard
      
        if let valueString = defaults.string(forKey: key!)
        {
            return valueString
        }
        
        return ""
    }
    
    
    class func removeUserDefaultForKey(key: String?)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key!)
        userDefaults.synchronize()
    }
    
     //MARK:-
    
    class func getNsNumberPREF(key:NSString) -> NSNumber
    {
        let defaults = UserDefaults.standard
        
        if let valueString = defaults.string(forKey: key as String)
        {
            if let number = Int(valueString)
            {
                let myNumber = NSNumber(value:number)
                
                return myNumber
            }
            else
            {
                return 0
            }
        }
        
        return 0
    }
    
    
    class func setNsNumberPREF(sValue:NSNumber, key:NSString)
    {
        UserDefaults.standard.setValue(sValue, forKey: key as String)
        UserDefaults.standard.synchronize()
    }
}
