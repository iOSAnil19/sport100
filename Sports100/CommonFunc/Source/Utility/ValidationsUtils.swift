//
//  ValidationsUtils.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import UIKit

class ValidationsUtils: NSObject
{
    //-----------------------------------------------
    // Mark  Email validation
    //-----------------------------------------------
    class func validateEmail(emailID: String) -> Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        // NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:
            emailID)
    }
    
    //-----------------------------------------------
    // Mark  number validation
    //-----------------------------------------------
    class func onlyNumberValidation(number: String) -> Bool {
        let nameRegex: String = "^[0-9]+$"
        let nameTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        return nameTest.evaluate(with: number)
    }
    //-----------------------------------------------
    // Mark  enter only alphabets
    //-----------------------------------------------
    class func onlyAlphabetsValidation(alphabet: String) -> Bool {
        let nameRegex: String = "^[a-zA-Z '-]+$"
        ///^[a-zA-Z ]*$/
        let nameTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        return nameTest.evaluate(with: alphabet)
       // return true
    }
    
    class func onlyAlphabetsWithSpaceValidation(alphabet: String) -> Bool {
        let nameRegex: String = "^[a-zA-Z ]+$"
        let nameTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        return nameTest.evaluate(with: alphabet)
    }
    
    //-----------------------------------------------
    // Mark  pass word lenght validation
    //-----------------------------------------------
    class func passwordLength(passwordText: String) -> Bool {
        if passwordText.characters.count < 3 {
            return false
        }
        else if passwordText.characters.count > 25 {
            return false
        }
        
        return true
    }
    
    //-----------------------------------------------
    // Mark  mathch password 
    //-----------------------------------------------
    class func confirmationPasswordValidation(password: String, confirmationPassword: String) -> Bool {
        if (password == confirmationPassword) {
            return true
        }
        else {
            return false
        }
    }
    
    var tableSourceList: [[String]] = [[Int](0..<20).map({ "section 0, cell \($0)" })]
    class func contentView(text: String) -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 375, height: 64))
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let label = UILabel(frame: view.bounds)
        label.frame.origin.x = 10
        label.frame.origin.y = 10
        label.frame.size.width -= label.frame.origin.x
        label.frame.size.height -= label.frame.origin.y
        
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.text = text
        label.numberOfLines = 2
        label.textColor = UIColor.white
        view.addSubview(label)
        
        return view
    }
    
    class func isEmpty(text: String) -> Bool
    {
        if text == "" || text == " " {
            return true
        }
        return false
    }
    
    class func isValidCountPassword(passwordString: String) -> Bool
    {
        if passwordString.characters.count > 7 && passwordString.characters.count <= 16
        {
            return true
        }
        return false
    }
    
    class func isValidPassword(passwordString: String) -> Bool {
        let stricterFilterString = "^(?=.*)(?=.*\\d)(?=.*[$@#()$!%*?&])[a-zA-Z\\d$@#()$!%*?&]{8,}" //eariler
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return passwordTest.evaluate(with: passwordString)
    }
    
    
    class func isValidCountCharacterLimit(textString: String) -> Bool
    {
        if textString.characters.count > 0 && textString.characters.count <= 30 {
            return true
        }
        return false
    }

    
    
    class func replacingCharacterinString(contactStr: String) -> String
    {
        let length: Int = contactStr.characters.count
        var compteString: String = ""
        
        for _ in 0..<length
        {
            compteString = compteString + "●"
        }
        
        
        
        return compteString as String
        
    }
    
    
    class func getStringCharacterCount(string:String) -> Int{
        
        let range = string.startIndex..<string.endIndex
        var length = 0
        string.enumerateSubstrings(in: range, options: NSString.EnumerationOptions.byComposedCharacterSequences) { (substring, substringRange, enclosingRange, stop) -> () in
            length = length + 1
        }
        return length
    }
    

    class func getCharacterCount(completeString: String) -> Int
    {
        let length: Int = self.getStringCharacterCount(string: completeString)
        var totalString: Int? = nil
        
        for _ in 0..<length
        {
            if(length<=140)
            {
              totalString = 140 - length
                
             return  totalString! as Int
                
            }
        }
        
        
        
        return 0
        
    }
    
    
    class func formatPhoneNumber(contactStr: String) -> String
    {
        
        let currentValue: NSString = contactStr as NSString
        let strippedValue: NSString = currentValue.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: NSMakeRange(0, currentValue.length)) as NSString
        
        var formattedString: NSString = ""
        
        if strippedValue.length == 0
        {
            formattedString = "";
        }
        else if strippedValue.length < 3
        {
            formattedString = strippedValue
        }
        else if strippedValue.length == 3
        {
            formattedString = strippedValue
        }
        else if strippedValue.length < 6
        {
            formattedString = "(" + strippedValue.substring(to: 3) + ") " + strippedValue.substring(from: 3) as NSString            }
        else if strippedValue.length == 6
        {
            formattedString = "(" + strippedValue.substring(to: 3) + ") " + strippedValue.substring(from: 3) + " - " as NSString
        }
        else if strippedValue.length <= 10
        {
            formattedString = "(" + strippedValue.substring(to: 3) + ") " + strippedValue.substring(with: NSMakeRange(3, 3)) + " - " + strippedValue.substring(from: 6) as NSString
        }
        else if strippedValue.length >= 11
        {
           // formattedString = "(" + strippedValue.substring(to: 3) + ") " + strippedValue.substring(with: NSMakeRange(3, 3)) + " - " + strippedValue.substring(with: NSMakeRange(6, 4)) + " - " + strippedValue.substring(from: 10) as NSString
        }
        
        //textField.text = formattedString as String
        
        return formattedString as String
        
    }
    
 }
