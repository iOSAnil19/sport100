//
//  Utility.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import Foundation
import UIKit
import Reachability
import SVProgressHUD
let TagNetworkAlertView = 151



var dataDictionary = Dictionary<String, Any>()


extension String {
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.width
    }
}


// UIColor
extension UIColor {
    
    func UIColorMake (_ colorString:String, alpha: CGFloat = 1.0) -> UIColor {
        var cString:String = colorString.trimmingCharacters(in: .whitespaces).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    func DiroThemeColor() -> UIColor {
        return UIColor().UIColorMake("#007AFF")
    }
}


var tabBarController: UITabBarController?

class Utility: NSObject
{
    static let shared = Utility()
    var availableSports: [Sport]?
    
    // MARK: -
    // MARK: - SetNavigationBarTitle
    
    class func addTitleOnNavBar(title: String, subTitle: String?, subTitleColor: UIColor? = nil) -> UIButton
    {
        let titleButton = UIButton(type: .custom)
        titleButton.titleLabel?.textAlignment = .center
        
        let titleFont = UIFont(name: "SanFranciscoTextBold", size: 16.0)!
        let subTitleFont = UIFont(name: "SanFranciscoTextBold", size:ScreenSize.SCREEN_WIDTH == 320 ? 10.0 : 9.0)
        
        let attString = NSMutableAttributedString()
        
        if subTitle != nil && (subTitle?.characters.count)! > 0
        {
            let titleString = NSAttributedString(string: String(format: "%@\n", title.uppercased()), attributes: [NSAttributedStringKey.font: titleFont, NSAttributedStringKey.foregroundColor: UIColor(red: 21.0/255.0, green: 126.0/255.0, blue: 251.0/255.0, alpha: 1.0)])
           // UIColor(red: 21.0/255.0, green: 126.0/255.0, blue: 251.0/255.0, alpha: 1.0)
            attString.append(titleString)
            
            var sColor = subTitleColor
            if sColor == nil {
                
                sColor = UIColor(red: 104.0/255.0, green: 105.0/255.0, blue: 105.0/255.0, alpha: 1.0)
            }
            
            let subTitleString = NSAttributedString(string: String(format: "%@", subTitle!.uppercased()), attributes: [NSAttributedStringKey.font: subTitleFont!, NSAttributedStringKey.foregroundColor: sColor!])
            attString.append(subTitleString)
        }
        else
        {     //uppercased()
            let titleString = NSAttributedString(string: String(format: "%@", title), attributes: [NSAttributedStringKey.font: titleFont, NSAttributedStringKey.foregroundColor: UIColor.black])
           // UIColor(red: 21.0/255.0, green: 126.0/255.0, blue: 251.0/255.0, alpha: 1.0)
            attString.append(titleString)
        }
       // titleButton.addTarget(self, action: #selector(DxUtility.titleAction), for: UIControlEvents.touchUpInside)
        //titleButton.addTarget(self, action: selector(), forControlEvents: UIControlEvents.TouchUpInside)
        titleButton.setAttributedTitle(attString, for: .normal)
        titleButton.titleLabel?.numberOfLines = 1
        titleButton.titleLabel?.lineBreakMode = .byWordWrapping
        titleButton.sizeToFit()
        return titleButton
    }

    
    
    
    //******************************** Show/Hide Hud ********************************//
    
    class func showHud(_ title: String?) {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
            if title == "" || title == nil {
                SVProgressHUD.show()
                SVProgressHUD.setMinimumSize(CGSize(width: 10, height: 10))
               
            } else {
                SVProgressHUD.show(withStatus: title)
            }
        }
    }
    
    class func hideHud () {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: -
    // MARK: - Email Validation Method 
    

    class func validateEmail(emailStr: String) -> Bool
    {
        if(emailStr == "")
        {
            return false
        }
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: emailStr)
    }
    
    // MARK: -
    // MARK: -  Get Device Widht and Height
    
    class func screenWidth() -> CGFloat
    {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize.width;
    }
    
    
    class func screenHeight() -> CGFloat
    {
        let screenSize: CGRect = UIScreen.main.bounds
        return screenSize.height;
    }
    
    // MARK: -
    // MARK: - Number Validation Method
    
    class func validatePhoneNumber(str: String) -> String {
        
        let chars = Set("1234567890".characters)
        return String(str.characters.filter { chars.contains($0) })
    }
    
        // MARK: -
    // MARK: -  StrotyBoard Comman Method
    
    class  func returnController(storyBoard:String , controllerID:String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: controllerID)
        return tabBarController
    }
    
    
     
     // MARK: -
     // MARK: - Function is used to get local time zone name
 
    class func localTimeZoneName() -> String {
        var localTimeZoneName: String { return NSTimeZone.local.identifier }
        
        return localTimeZoneName
    }
    
    
     // MARK: -
     // MARK: -Function is used to get all time zone names
    
    class func allTimeZoneNames() -> NSArray {
        var timeZoneNames: [String] { return NSTimeZone.knownTimeZoneNames }
        return timeZoneNames as NSArray
    }
    
    
    // MARK: -
    // MARK: -  Date functions
    
    class   func getStringFromDate(date:NSDate)->String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm.ss"   // You can also use Long, Medium and No style.
        let inputDate = dateFormatter.string(from: date as Date)
        return inputDate
    }
    
    class func getDateFromStringWithdatefaormat(dateString:String,dateformat:String)->NSDate? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateformat
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        
        let newDate = dateFormatter.date(from: dateString)
        return newDate as NSDate?
    }
    
    
    class func daysDifference(firstDate:NSDate, secondDate:NSDate) -> Int
    {
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let date1 = calendar.startOfDay(for: firstDate as Date)
        let date2 = calendar.startOfDay(for: secondDate as Date)
        let flags = NSCalendar.Unit.day
        let components = calendar.components(flags, from: date1, to: date2, options: [])
        return components.day!
    }
    
    
    class func convertDate(from dateString: String,
                           format: String,
                           covertableFormate a_CovertableFormate: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.date(from: dateString)
        
        //        if a_CovertableFormate.length > 0
        //        {
        //            dateFormatter.dateFormat = a_CovertableFormate
        //        }
        
        let sNewValue = dateFormatter.string(from: date!)
        
        return sNewValue
    }
    
    
   class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
    
    // MARK: -
    // MARK: - Alert Comman Method
   
    
    class  func showAlertController(alert:NSString,message:String ,title:String,okButtonTitle:String,cancelButtonTitle:String,viewController:UIViewController?)
    {
        
        
        var alertController = UIAlertController()
        alertController = UIAlertController(title:"", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: okButtonTitle, style: .default, handler:
            { action in
                switch action.style
                {
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
        }))
        
        
        
        AppDelegate.getDelegate().window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    
 
  
    // MARK: -
    // MARK: - Check Internet Connection Method
    
    class func isNetworkReachable() -> Bool
    {
        Reachability.forInternetConnection().startNotifier()
        
        switch Reachability.forInternetConnection().currentReachabilityStatus()
        {
        case .NotReachable:
            NetworkAlertView.shared.show()
            return false
        case .ReachableViaWiFi:
           NetworkAlertView.shared.hide()
            return true
        case .ReachableViaWWAN:
            NetworkAlertView.shared.hide()
            return true
        }
    }
    
    
    //--------------------------------------------------------------------------
    // ********************===== GCNetworkReachability =====*******************
    //-------------------------------------------------------------------------
    
   class func checkForNetworkRechability()-> Bool
    {
        // ********************===== GCNetworkReachability =====*******************
        let  rechability:Reachability = Reachability(hostName: "www.google.com")
        if (rechability.currentReachabilityStatus().rawValue != 0)
        {
            return true
        } else
        {
            
            return false
        }
    }
    class func showNoNetwork(vwController:UIViewController)
    {
        self.showAlertViewControllertMessage(alertTitle:"", alertmessage:ALERT_NETWORK_NOT_AVAILABLE, okButtonTitle: OK_TITLE, calcelButtonTitle: nil, ViewController:vwController) {_ in
            return;
        }
    }
    
    
    
    // MARK: -
    // MARK: - Get Vender Idetifire If Device token Null

    
    class func getTempDeviceID() -> String
    {
        let tempDeviceID: UUID! = UIDevice.current.identifierForVendor
        let deviceIdStr = tempDeviceID.uuidString
        let thisChSet = NSCharacterSet(charactersIn: " ")
        var sDeviceToken = deviceIdStr.trimmingCharacters(in: thisChSet as CharacterSet!)
        sDeviceToken = sDeviceToken.replacingOccurrences(of: "-", with: "")
        return sDeviceToken
    }

   
    
    // MARK: -
    // MARK: - Instantiate view controller from storyboard
    
    class func instantiateViewControllerWithIdentifier(identifier:NSString, storyboardName:NSString)-> UIViewController
    {
        let storyboard = UIStoryboard(name:storyboardName as String, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: identifier as String) 
        return vc
    }
    
    // MARK: -
    // MARK: - Get User Device Token
    
    class func getuserAuthToken() -> String!
    {
        let access_tokenValue : String = UserDefaultUtils.getSTRINGValueFroKey(key: "access_token")!
        return access_tokenValue
    }

    
    // MARK: -
    // MARK: - Calulate Height of String
    
//    class func calculateHeight(inString:String,stringwidht:CGFloat,font:CGFloat) -> CGFloat {
//        let messageString = inString
//        let attributes : [String : Any] = [NSFontAttributeName.rawValue : UIFont.systemFont(ofSize: font)]
//        
//        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
//        
//        let rect : CGRect = attributedString.boundingRect(with: CGSize(width:stringwidht, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//        
//        let requredSize:CGRect = rect
//        return requredSize.width
//    }
    
    // MARK: -
    // MARK: - Factor of Screen
    
    class func getScaleFactor() -> CGFloat
    {
        let screenRect:CGRect = UIScreen.main.bounds
        let screenWidth:CGFloat = screenRect.size.width
        let scalefactor:CGFloat = screenWidth / 320.0
        
        return scalefactor

    }
    
    class func showAlertViewControllertMessage(alertTitle: String, alertmessage: String, okButtonTitle: String, calcelButtonTitle: String?, ViewController: UIViewController?, and block: @escaping (_ success: Bool) -> Void)
    {
        let alertController = UIAlertController(title:"", message: alertmessage, preferredStyle: .alert)
        
        
        
        var cancelAction = UIAlertAction()
        
        if(calcelButtonTitle != nil)
        {
            cancelAction = UIAlertAction(title: calcelButtonTitle, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            })
        }
        let okAction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action: UIAlertAction!) in
            
            block(true)
        })
        
        alertController.addAction(okAction)
        if(calcelButtonTitle != nil)
        {
            alertController.addAction(cancelAction)
        }
        
        if(ViewController != nil){
          ViewController?.present(alertController, animated: true, completion: nil)
        }
        else{
            AppDelegate.getDelegate().window?.rootViewController?.present(alertController, animated: true, completion: nil)}
    }
    // MARK: -
    // MARK: - Check for existing user
    
   class func isUserAlreadyAdded(email:String, array:NSMutableArray) -> Bool {
        
        let predicate = NSPredicate(format: "email = %@", email)
        let array = array.filtered(using: predicate)
        return (array.count>0)
    }
    
    class func isReferralAlreadyAdded(referralId:String, array:NSMutableArray) -> Bool {
        
        let predicate = NSPredicate(format: "referralId = %@", referralId)
        let array = array.filtered(using: predicate)
        return (array.count>0)
    }
    
    // MARK: -
    // MARK: - Views customization and animation 
    
    class func addVisualEffectOnView(vwController:UIViewController){
        vwController.view.backgroundColor = UIColor.clear;
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        visualEffectView.tag = 999
        visualEffectView.alpha = 0.8
        visualEffectView.frame = (vwController.view.window?.bounds)!
        
        let backgroundBlack = UIView(frame: (vwController.view.window?.bounds)!)
        backgroundBlack.tag = 998
        
        backgroundBlack.backgroundColor = UIColor.black
        visualEffectView.addSubview(backgroundBlack)
        
        vwController.view.addSubview(visualEffectView)
    }
    
    class func addVisualEffectOnWindow(popUpView:UIView) {

        var visualEffectView = AppDelegate.getDelegate().window?.viewWithTag(999) as! UIVisualEffectView?
        if((visualEffectView) == nil){
            visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        }
        
        visualEffectView?.tag = 999
        visualEffectView?.alpha = 0.8
        visualEffectView?.frame = (AppDelegate.getDelegate().window?.bounds)!
        
        var backgroundBlack:UIView? = AppDelegate.getDelegate().window?.viewWithTag(998)
    
        if((backgroundBlack) == nil){
            backgroundBlack = UIView(frame: (AppDelegate.getDelegate().window?.bounds)!)
        }
        backgroundBlack?.tag = 998
        backgroundBlack?.alpha = 0.2
        backgroundBlack?.backgroundColor = UIColor.white
        AppDelegate.getDelegate().window?.addSubview(visualEffectView!)
        AppDelegate.getDelegate().window?.addSubview(backgroundBlack!)
        
        AppDelegate.getDelegate().window?.bringSubview(toFront: visualEffectView!)
        
        
        self .addViewWithBounceAnimationOnWindow(popUp: popUpView);
    }
    
    class func removeVisualEffectView(){
        let visualEffectView: UIVisualEffectView? = AppDelegate.getDelegate().window?.viewWithTag(999) as! UIVisualEffectView?
        let backgroundBlack:UIView? = AppDelegate.getDelegate().window?.viewWithTag(998)
        
        backgroundBlack?.removeFromSuperview()
        visualEffectView?.removeFromSuperview()
    }
    
    class func addViewWithBounceAnimationOnWindow(popUp:UIView){
        
        popUp.backgroundColor = UIColor.clear
        popUp.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        AppDelegate.getDelegate().window?.addSubview(popUp)
        UIView.animate(withDuration: 0.3 / 1.5, animations: {() -> Void in
            popUp.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                popUp.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.3 / 2, animations: {() -> Void in
                    popUp.transform = CGAffineTransform.identity
                    AppDelegate.getDelegate().window?.bringSubview(toFront:popUp)
                })
            })
        })
    }

    
    
    class func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedString()
        
        return base64String!
        
    }
    
    
    // MARK: -
    // MARK: - Set alpha with animation of label
   
    
   class func showAnimation(cellValue:UILabel)
    {
        
        
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            
        }, completion: {(finished: Bool) -> Void in
            //Appear
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                cellValue.alpha = 1.0
            })
        })
        
    }
    
    // MARK: -
    // MARK: - Set alpha with animation of label
   
   class func hideAnimation(cellValue:UILabel)
    {
        
        
        UIView.animate(withDuration: 0.0, animations: {() -> Void in
            
        }, completion: {(finished: Bool) -> Void in
            //Appear
            UIView.animate(withDuration: 0.0, animations: {() -> Void in
                cellValue.alpha = 0.0
            })
        })
        
    }
    
    
    class func showAlertOnViewControllerWithMerge(viewController: UIViewController, title: String?,
                                                 cancelButton: String?,okButton: String?, message: String, completion: @escaping (_ actionCompleted: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: cancelButton, style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        alert.addAction(UIAlertAction(title:okButton, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            print("User click merge button")
            
            completion(true)

            
            
        }))
        
        
          viewController.present(alert, animated: true, completion: nil)
    
 
    }
    
    class func showAlertOnViewController(viewController: UIViewController, title: String?,
                                                  cancelButton: String?,okButton: String?, message: String, completion: @escaping (_ actionCompleted: Bool) -> Void) {
        let alert = UIAlertController(title:title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        alert.addAction(UIAlertAction(title:okButton, style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            print("User click merge button")
            
            completion(true)
            
            
            
        }))
        
        
        viewController.present(alert, animated: true, completion: nil)
        
        
    }

    
    class func getAttributeString(mainString:String,subString1:String,subString2:String,subString3:String) ->NSMutableAttributedString
    {
        let string = mainString as NSString
        let attributedString = NSMutableAttributedString(string: string as String)
        
    
        // 2
//        let attributes = [NSForegroundColorAttributeName: AppBlueColor] as [String : Any]
//       
//        
//        // 3
//        attributedString.addAttributes(attributes, range: string.range(of: subString1))
//        attributedString.addAttributes(attributes, range: string.range(of: subString2))
//        attributedString.addAttributes(attributes, range: string.range(of: subString3))
        
        
        return attributedString
    }
    
    
    class func formattedDateString(dateStr:String,setdateFormatter:String,getFormatter:String)->String{
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = setdateFormatter//"yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateStr)
       // dateFormatter.dateFormat = "MMMM dd, yyyy hh:mm'hrs'"
        dateFormatter.dateFormat = getFormatter//"MMMM dd, yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        let formattedDateString = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    
    class func formattedDateDate(dateStr:String,setdateFormatter:String,getFormatter:String)->Date{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = setdateFormatter//"yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateStr)
        // Get NSDate for the given string
        
        return date!

    }
    
    
    class func savePhoneBook(phoneBook:NSMutableArray?){
        let phoneData = NSKeyedArchiver.archivedData(withRootObject: phoneBook as Any)
        UserDefaults.standard.set(phoneData, forKey: "")
    }
    
    class func removePhoneContact(){
        UserDefaults.standard.removeObject(forKey: "")
//        prefs.removeObjectForKey
    }
    
    class func getPhoneBook()->NSMutableArray?{
        
        var phoneContactsArray:NSMutableArray? = []
        if(UserDefaults.standard.object(forKey: "") != nil){
            let phoneContacts = UserDefaults.standard.object(forKey: "") as? NSData
            
            if let phoneContacts = phoneContacts {
                phoneContactsArray = NSMutableArray(array: NSKeyedUnarchiver.unarchiveObject(with: phoneContacts as Data) as! NSArray)
                print(phoneContactsArray!)
            }
        }
        return phoneContactsArray
    }

    
    
    
    

   class func setTabBarVisible(visible: Bool, animated: Bool,viewController:UIViewController) {
        // hide tab bar
        let frame = tabBarController?.tabBar.frame
        let height = frame?.size.height
        print ("offsetY = \(height)")
    
    
        // zero duration means no animation
        let duration:TimeInterval = (animated ? 0.3 : 0.0)
        
        // animate tabBar
    
        if frame != nil {
            UIView.animate(withDuration: duration) {
                tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: 49)
                viewController.view.frame = CGRect(x: 0, y: 0, width: viewController.view.frame.width, height: viewController.view.frame.height + height!)
                viewController.view.setNeedsDisplay()
                viewController.view.layoutIfNeeded()
                return
            }
        }
    }
    
    
    
    
    
    class func removeWhiteSpace(string:String)->String{
        
        return string.trimmingCharacters(in: .whitespaces)
        
        
    }

    
 class   func setPlaceHolder(placeholder: String)-> String
    {
        
        var text = placeholder
        if text.characters.last! != " " {
            
            //                         define a max size
            
            let maxSize = CGSize(width: UIScreen.main.bounds.size.width - 90, height: 40)
            
            //                        let maxSize = CGSizeMake(self.bounds.size.width - 92, 40)
            // get the size of the text
            let widthText = text.boundingRect( with: maxSize, options: .usesLineFragmentOrigin, attributes:nil, context:nil).size.width
            // get the size of one space
            let widthSpace = " ".boundingRect( with: maxSize, options: .usesLineFragmentOrigin, attributes:nil, context:nil).size.width
            let spaces = floor((maxSize.width - widthText) / widthSpace)
            // add the spaces
            let newText = text + ((Array(repeating: " ", count: Int(spaces)).joined(separator: "")))
            // apply the new text if nescessary
            if newText != text {
                return newText
            }
            
        }
        
        return placeholder;
    }
   
    
}
