//
//  StringUtility.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import UIKit

class StringUtility: NSObject {

    //
    //  StringUtils.m
   
    
//    class func stringByStrippingWhitespace(string: String) -> String {
//        return string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
//    }
    
    class func isDecimalNCRString(str: String) -> Bool {
        let test: NSPredicate = NSPredicate(format: "SELF beginswith %@",str)
        let result: Bool = test.evaluate(with: str)
        return result
    }
    
}
