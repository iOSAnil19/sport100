//
//  NetworkAlertView.swift
//  Sports100
//
//  Created by Nripendra Hudda Inc on 18/01/18.
//  Copyright © 2018 Nripendra Hudda Inc. All rights reserved.
//

import Foundation
import  UIKit

public class NetworkAlertView: UIView
{
    private var networkAlertViewObj: UIView!
    
    // MARK: -
    // MARK: - Share instance 
    
    class var shared: NetworkAlertView
    {
        struct Static
        {
            static let instance: NetworkAlertView = NetworkAlertView()
        }
        return Static.instance
    }
    
    
    // MARK: -
    // MARK: - Call nib
    
    class func instanceFromNib() -> NetworkAlertView {
        
        return UINib(nibName: "NetworkAlertView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NetworkAlertView
    }
    
    
    // MARK: -
    // MARK: - Set NetWork View
    
    private func setup()
    {
        networkAlertViewObj = NetworkAlertView.instanceFromNib()
        networkAlertViewObj.frame = CGRect(x: 0, y: AppDelegate.getDelegate().window!.frame.size.height, width: AppDelegate.getDelegate().window!.frame.size.width, height: networkAlertViewObj.frame.size.height)
    }
    
    // MARK: -
    // MARK: - Show Network View Method
    public func show()
    {
        if networkAlertViewObj == nil
        {
            self.setup()
        }
        else
        {
            self.networkAlertViewObj.removeFromSuperview()

        }
        
        if let window = AppDelegate.getDelegate().window
        {
            window.addSubview(networkAlertViewObj)
            
            UIView.animate(withDuration: 0.5, delay: 0.9, options: .curveEaseOut, animations:
                {
                    self.networkAlertViewObj.frame = CGRect(x: 0, y: (AppDelegate.getDelegate().window!.frame.size.height - self.networkAlertViewObj.frame.size.height), width:AppDelegate.getDelegate().window!.frame.size.width, height:self.networkAlertViewObj.frame.size.height)
                        
                        
                        ///CGRectMake(0,
                    
                    
                }, completion:
                { finished in
            })
        }
    }
    
    
    // MARK: -
    // MARK: - Hide Network View Method
    
    public func hide()
    {
        if networkAlertViewObj != nil
        {
            UIView.animate(withDuration: 0.5, delay: 0.9, options: .curveEaseOut, animations:
                {
                    self.networkAlertViewObj.frame = CGRect(x:0,
                                                            y:AppDelegate.getDelegate().window!.frame.size.height ,
                        width:AppDelegate.getDelegate().window!.frame.size.width,
                        height:self.networkAlertViewObj.frame.size.height)
                    

                    
                }, completion:
                { finished in
                    

                    
            })
            
            

        }
        
    }

}
