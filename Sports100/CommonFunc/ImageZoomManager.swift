//
//  ImageZoomManager.swift
//  Sports100
//
//  Created by Nripendra Hudda on 09/12/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

class ImageZoomManager: NSObject,SKPhotoBrowserDelegate {

   static let sharedimageZoomManager  = ImageZoomManager()
    
    func openPicker(_ sender:UIViewController,_ photoArray:Array<Any>)  {
            let browser = SKPhotoBrowser(photos: createWebPhotos(photoArray))
            browser.initializePageIndex(0)
            browser.delegate = self
            sender.present(browser, animated: true, completion: nil)
            
        }
        
        
        // MARK: - SKPhotoBrowserDelegate
    
        func didDismissAtPageIndex(_ index: Int) {
        }
        
        func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
        }
        
        func removePhoto(index: Int, reload: (() -> Void)) {
            SKCache.sharedCache.removeImageForKey("somekey")
            reload()
        }
        
        // MARK: - private
    func createWebPhotos(_ photoArray:Array<Any>) -> [SKPhotoProtocol] {
            //productInfoDic?.apiProductImages
            return (0..<(photoArray.count)).map { (i: Int) -> SKPhotoProtocol in
                let obj = photoArray[i] as! String
                let photo = SKPhoto.photoWithImageURL(obj)
                photo.shouldCachePhotoURLImage = true
                
                return photo
            }
        }
        
        
        
        
    }
    class CustomImageCache: SKImageCacheable {
        var cache: SDImageCache
        
        init() {
            let cache = SDImageCache(namespace: "com.suzuki.custom.cache")
            self.cache = cache
        }
        
        func imageForKey(_ key: String) -> UIImage? {
            guard let image = cache.imageFromDiskCache(forKey: key) else { return nil }
            
            return image
        }
        
        func setImage(_ image: UIImage, forKey key: String) {
            cache.store(image, forKey: key)
        }
        
        func removeImageForKey(_ key: String) {}
        
        func removeAllImages() {}
        
}
