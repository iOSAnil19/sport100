//
//  SocialManager.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

import UIKit
import FBSDKLoginKit
//import GoogleSignIn

enum SocialLoginType:String {
    case FB_LOGIN = "1"
    case GOOGLE_LOGIN = "2"
}

class SocialManager:NSObject {
    
    let GOOGLE_CLIENT_ID = "1030439621553-itp16ur48mabj8t6aah8n9sfqv4d1u6i.apps.googleusercontent.com" //"1019951584945-7a3u7p65jcfs4uhvhu7aeb3ogt8erg4n.apps.googleusercontent.com"
    let FB_ERROR = "*Unable To Fetch User Infomation From Facebook, Please Try Again"
    
    var socialCompletion:((_: Any?, _: Error?) -> ())?
    var viewController : UIViewController?
    
    //MARK:- ------------------- FACEBOOK LOGIN METHOD ---------------------
    
    // CALL METHOD FOR LOGIN WITH FACEBOOK
    func getFaceboookLoginInfoForView(_ sender:UIViewController,getResponse: @escaping (_: Any?, _: Error?) -> Void) {
        
        let fbLoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: sender) { (result, error) in
            
            if error == nil {
                if result!.isCancelled {
                    
                    // Here we can handle the done button action
                    
                } else {
                    if let token = AccessToken.current {
                        DispatchQueue.main.async {
                            self.returnFacebookUserInformation(token, getResponse: { (reponse, error) in
                                getResponse(reponse, error)
                            })
                        }
                    }
                }
                
            } else {
                getResponse(result, error)
            }
        }
    }
    
    // METHOD TO GET FACEBOOK LOGIN INFORMATION
    private func returnFacebookUserInformation(_ token:AccessToken, getResponse: @escaping (_:Any?, _:Error?) -> Void) {
        
        GraphRequest.init(graphPath: "me", parameters: ["fields": "id, name,first_name,last_name, picture.type(large), email, gender"]).start { (connection, result, error) in
            
        
            
            if error == nil {
                if let fbUser = result as? [String:Any], fbUser["id"] != nil, !(fbUser["id"] is NSNull) {
                    
                    guard fbUser["id"] != nil else { getResponse(self.FB_ERROR,nil); return  }
                    
                    var user = SocialUser()
                    user.setFacebookUserInformation(fbUser)
                    
                    getResponse(user,nil)
                    
                } else {
                    getResponse(self.FB_ERROR,nil)
                }
                
            } else {
                getResponse(nil, error)
            }
        }
    }
    
    //MARK:- ------------------- GOOGLE LOGIN METHOD ---------------------
    
     // CALL METHOD FOR LOGIN WITH GOOGLE
  /*  func getGoogleLoginInfoForView(_ viewController : UIViewController, getResponse : @escaping (_: Any?, _:Error?) -> Void)  {
        
        self.socialCompletion = getResponse
        self.viewController = viewController
        
        GIDSignIn.sharedInstance().signOut()
        let signIn = GIDSignIn.sharedInstance()
        signIn?.shouldFetchBasicProfile = true
        signIn?.delegate = self
        signIn?.uiDelegate = self
        signIn?.clientID  = GOOGLE_CLIENT_ID
        signIn?.signIn()
    }*/
    
     func getTwitterLoginInfoForView(_ viewController : UIViewController, getResponse : @escaping (_: Any?, _:Error?) -> Void)  {
        // Swift
//        Twitter.sharedInstance().logIn(completion: { (session, error) in
//            if (session != nil) {
//                print("signed in as \(session.userName)");
//            } else {
//                print("error: \(error.localizedDescription)");
//            }
//        })
    }
}

/*
extension SocialManager:GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        print("Connected")
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            var socialUser = SocialUser()
            socialUser.setGoogleUserInformation(user)
            self.socialCompletion?(socialUser,nil)
        } else {
           self.socialCompletion?(nil,error)
            print("\(error.localizedDescription)")
        }
    }
    
    // Finished disconnecting |user| from the app successfully if |error| is |nil|.
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        socialCompletion!(user,error)
    }
}

extension SocialManager:GIDSignInUIDelegate {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.viewController!.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.viewController!.dismiss(animated: true, completion: nil)
        
    }
}
*/

struct SocialUser {
    
    var id:String?
    var email:String?
    var firstName:String?
    var lastName:String?
    var profilePic:String?
    var gender:String?
    var accessToken:String?
    var idToken:String?
    
    mutating func setFacebookUserInformation(_ info:[String:Any]) {
       
        self.resetIfAlreadyExistAnyInformation()
        
        print(info)
        
        id = "\(info["id"] ?? "")"
        email = "\(info["email"] ?? "")"
        firstName = "\(info["first_name"] ?? "")"
        lastName = "\(info["last_name"] ?? "")"
        gender = "\(info["gender"] ?? "")"
        if let tokenExist = AccessToken.current?.tokenString {
            accessToken = tokenExist
        }
        
        if let imgInfo = info["picture"] as? [String:Any], imgInfo["data"] != nil, !(imgInfo["data"] is NSNull) {
            if let imgData = imgInfo["data"] as? [String:Any], imgData["url"] != nil, !(imgData["url"] is NSNull) {
                profilePic = "\(String(describing: imgData["url"] ?? ""))"
            }
        }
    }
    
 /*   mutating func setGoogleUserInformation(_ user:GIDGoogleUser) {
        
        self.resetIfAlreadyExistAnyInformation()
        
        id = user.userID ?? ""
        email = user.profile.name ?? ""
        firstName = user.profile.givenName ?? ""
        lastName = user.profile.familyName ?? ""
        email = user.profile.email ?? ""
        accessToken = user.authentication.accessToken
        idToken = user.authentication.idToken
        
       // GmailData.G_mailData.authentication_id_token = user?.authentication.idToken
        if user.profile.hasImage {
            profilePic = user.profile.imageURL(withDimension: 1024).absoluteString
        }
    }*/
    
    mutating func resetIfAlreadyExistAnyInformation() {
        id = nil
        email = nil
        firstName = nil
        lastName = nil
        gender = nil
        profilePic = nil
        accessToken = nil
        idToken = nil
    }
}
