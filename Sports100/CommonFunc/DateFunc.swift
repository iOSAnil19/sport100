//
//  DateFunc.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

func getServerDateFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

//3- Get time
func getTimeStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "hh:mm a"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

func getDateStringFromString(_ dateString : String) -> String {
    if dateString == "" {
        return ""
    }
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: dateString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = "dd MMM yyyy"
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    if serverDate == nil {
        return ""
    }
    
    let localString = date_Fromatter.string(from: serverDate!)
    return localString
}

func getTimeStringFromString(_ dateString : String) -> String {
    if dateString == "" {
        return ""
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: dateString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = "hh:mm a"
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    if serverDate == nil {
        return ""
    }
    
    let localString = date_Fromatter.string(from: serverDate!)
    return localString
}

//2- Get date

func getPHPDateStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

func getDateStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "dd MMM yyyy"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

//2- Get date
func getStringFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "dd/MM/yy"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

func getLocaleDate(_ serverString:String) -> Date {
    if serverString == "" {
        return Date()
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: serverString)
    return serverDate ?? Date()
}

func getLocaleDateString(_ serverString : String) -> String {
    
    if serverString == "" {
        return ""
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: serverString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = " hh:mm a"
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    if serverDate == nil {
        return ""
    }
    
    let localString = date_Fromatter.string(from: serverDate!)
    return localString
}
func geDateString(_ serverString : String,_ format : String, isUTC: Bool = true) -> String {
    
    if serverString == "" {
        return ""
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    if isUTC {
        dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    }
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: serverString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = format
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    if serverDate == nil {
        return ""
    }
    
    let localString = date_Fromatter.string(from: serverDate!)
    return localString
}
func getLocaleDate(_ serverString : String,format : String) -> String {
    
    if serverString == "" {
        return ""
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverDate = dateFromatter.date(from: serverString)
    
    let date_Fromatter = DateFormatter()
    date_Fromatter.dateFormat = format
    date_Fromatter.timeZone = NSTimeZone.local
    date_Fromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    if serverDate == nil {
        return ""
    }
    
    let localString = date_Fromatter.string(from: serverDate!)
    return localString
}


//2- Get date
func getDateFromDate(_ date : Date) -> String {
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "yyyy-MM-dd"
    dateFromatter.timeZone = NSTimeZone.local
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    let serverString = dateFromatter.string(from: date)
    return serverString
}

func getDOBString(_ serverString : String) -> String {
    
    if serverString == "" {
        return ""
    }
    
    let dateFromatter = DateFormatter()
    dateFromatter.dateFormat = "dd-MM-yyyy" 
    dateFromatter.timeZone = TimeZone(abbreviation:"UTC")
    dateFromatter.locale = Locale.init(identifier:"en_US_POSIX")
    
    var formatedStartDate = dateFromatter.date(from: serverString)
    if formatedStartDate == nil {
      dateFromatter.dateFormat = "yyyy-MM-dd"
      formatedStartDate = dateFromatter.date(from: serverString)
      if formatedStartDate == nil {
        return ""
      }
    }
    
    let currentDate = Date()
    let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
    let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
    
    guard let years = differenceOfDate.year else {return ""}
    return "\(years) Years"

}

 func dateInStringFromTimeStamp(timeStamp:Double, formate:String?) -> String{
    
    var resultFormate = formate
    if (resultFormate?.isEmpty)!{
        resultFormate = "mm-DD-yyyy"
    }
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = resultFormate
    
    let date = Date(timeIntervalSince1970: timeStamp/1000)
    let result = dateFormatter.string(from: date as Date)
    return result
}
