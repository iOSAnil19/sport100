//
//  SaveImageInGallery.swift
//  InstantChat
//
//  Created by Nripendra Hudda on 24/11/17.
//  Copyright © 2017 Nikhil Sharma. All rights reserved.
//

import UIKit
import Foundation
import Photos
protocol GetSuccessResponseINProtocol:class {
    func getSuccessImageSaveResponse(status:Bool)
    
}
class SaveImageInGallery: NSObject
{
    static let albumName = "Sports100"
    static let sharedInstance = SaveImageInGallery()
    weak var imageSaveDelegate:GetSuccessResponseINProtocol? = nil
    private var assetCollection: PHAssetCollection!
    
    
    //MARK:
    //MARK:-- Set Init Method
    
    private override init() {
        super.init()
        
        if let assetCollection = fetchAssetCollectionForAlbum()
        {
            self.assetCollection = assetCollection
            
            return
        }
    }
    
    //MARK:
    //MARK:-- Check Authorization
    
    private func checkAuthorizationWithHandler(completion: @escaping ((_ success: Bool) -> Void)) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined
        {
            PHPhotoLibrary.requestAuthorization({ (status) in
                self.checkAuthorizationWithHandler(completion: completion)
            })
        }
        else if PHPhotoLibrary.authorizationStatus() == .authorized
        {
            self.createAlbumIfNeeded()
            
            
            completion(true)
        }
        else {
            completion(false)
        }
    }
    
    //MARK:
    //MARK:-- Create Album
    
    private func createAlbumIfNeeded() {
        if let assetCollection = fetchAssetCollectionForAlbum() {
            // Album already exists
            self.assetCollection = assetCollection
        } else {
//            PHPhotoLibrary.shared().performChanges({
//                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: SaveImageInGallery.albumName)   // create an asset collection with the album name
//            }) { success, error in
//                if success {
//                    self.assetCollection = self.fetchAssetCollectionForAlbum()
//                } else {
//                    // Unable to create album
//                }
//            }
        }
    }
    
   
    //MARK:
    //MARK:-- factch  Album Name is Exit
    
     func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", SaveImageInGallery.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject
        {
            return collection.firstObject
        }
        return nil
    }
    
    
    
    

    //MARK:-- Save Image in Gallery
    
    
    func saveImageInGallery(image: UIImage) {
        self.checkAuthorizationWithHandler { (success) in
            if success {
                if let assetCollection = self.fetchAssetCollectionForAlbum() {
                    // Album already exists
                    
                    self.assetCollection = assetCollection

                    print("image save")
                    PHPhotoLibrary.shared().performChanges({
                        let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                        let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
                        let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                        let enumeration: NSArray = [assetPlaceHolder!]
                        albumChangeRequest!.addAssets(enumeration)
                        
                    }, completionHandler: nil)
                    self.imageSaveDelegate?.getSuccessImageSaveResponse(status:true)
                    
                    
                } else {
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: SaveImageInGallery.albumName)   // create an asset collection with the album name
                    }) { success, error in
                        if success {
                            self.assetCollection = self.fetchAssetCollectionForAlbum()
                            PHPhotoLibrary.shared().performChanges({
                                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                                let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
                                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                                let enumeration: NSArray = [assetPlaceHolder!]
                                albumChangeRequest!.addAssets(enumeration)
                                
                            }, completionHandler: nil)
                            print("image save")
                            self.imageSaveDelegate?.getSuccessImageSaveResponse(status:true)
                        } else
                        {
                            // Unable to create album
                        }
                    }
                }
            }
        }
    }
    
    
    
    //MARK:
    //MARK:-- Fatch Image from custom Folder
    
    
func FetchCustomAlbumPhotos()
    {

       // let imgManager = PHImageManager.default()
         var photosArray = PHFetchResult<PHAsset>()
         var photoAssets = PHFetchResult<AnyObject>()
         let fetchOptions = PHFetchOptions()
          fetchOptions.predicate = NSPredicate(format: "title = %@",SaveImageInGallery.albumName)
          print("fetchOptions",fetchOptions.predicate as Any)
          photoAssets = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions) as! PHFetchResult<AnyObject>

  

        print("photoAssetsphotoAssetsphotoAssets",photoAssets.count)
       photoAssets.enumerateObjects({ (object, count, stop) in
            if object is PHAssetCollection
            {
                let obj:PHAssetCollection = object as! PHAssetCollection
                if (obj.localizedTitle == SaveImageInGallery.albumName)
                {
                      photosArray = PHAsset.fetchAssets(in:obj, options: nil)
                      stop.initialize(to: true)
                }
            }
        })
        
        for item  in 0..<photosArray.count{
            let asset: PHAsset = photosArray[item]
            print(asset)
            self.setImageName(assect:asset)
        }
        
    }
    
  
    func setImageName(assect:PHAsset)
    {
        var  imageType = String()
        let resources = PHAssetResource.assetResources(for: assect)
        if let resource = resources.first
        {
            imageType = resource.originalFilename
            
        }
       
       // imageType = (self.value(forKey: "filename") as? String)!
        
       // let imageArray = imageType.characters.split{$0 == "."}.map(String.init)
       
        print("imageType",imageType)
        
    }
    

    //MARK:
    //MARK:-- downloadVideo
    
    
    
    func downloadVideoLinkAndCreateAssetInGallery(_ videoLink: String) {
        
        // use guard to make sure you have a valid url
        guard let videoURL = URL(string: videoLink) else { return }
        
        guard let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        // check if the file already exist at the destination folder if you don't want to download it twice
        if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
            
            // set up your download task
            URLSession.shared.downloadTask(with: videoURL) { (location, response, error) -> Void in
                
                // use guard to unwrap your optional url
                guard let location = location else { return }
                
                // create a deatination url with the server response suggested file name
                let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? videoURL.lastPathComponent)
                
                do {
                    
                    try FileManager.default.moveItem(at: location, to:destinationURL)
                    
                     self.saveVideoIngallery(destinationURL)
                
                } catch { print(error) }
                
                }.resume()
            
        } else {
            print("File already exists at destination url")
        
        }
        
    }
    
    //MARK:
    //MARK:-- Save Video in gallery
    
    func saveVideoIngallery(_ videoLink: URL) {
        

        self.checkAuthorizationWithHandler { (success) in
            if success {
                if let assetCollection = self.fetchAssetCollectionForAlbum() {
                    // Album already exists
                    
                    self.assetCollection = assetCollection
                    PHPhotoLibrary.shared().performChanges({
                        let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoLink)
                        let assetPlaceHolder = assetChangeRequest?.placeholderForCreatedAsset
                        let albumChangeRequest = PHAssetCollectionChangeRequest(for:self.assetCollection)
                        let enumeration: NSArray = [assetPlaceHolder!]
                        albumChangeRequest!.addAssets(enumeration)
                        
                    }, completionHandler: nil)
                    
                    print("save VideoIn gallery")
                    self.imageSaveDelegate?.getSuccessImageSaveResponse(status:true)
                    
                    
                } else {
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: SaveImageInGallery.albumName)   // create an asset collection with the album name
                    }) { success, error in
                        if success {
                            self.assetCollection = self.fetchAssetCollectionForAlbum()
                            PHPhotoLibrary.shared().performChanges({
                                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoLink)
                                let assetPlaceHolder = assetChangeRequest?.placeholderForCreatedAsset
                                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                                let enumeration: NSArray = [assetPlaceHolder!]
                                albumChangeRequest!.addAssets(enumeration)
                                
                            }, completionHandler: nil)
                            print("save Video Ingallery")
                        
                         self.imageSaveDelegate?.getSuccessImageSaveResponse(status:true)
                            
                        } else
                        {
                            // Unable to create album
                        }
                    }
                }
            }
        }
       
    }
}


