//
//  NetworkManager.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


typealias serviceCompletion = (_: Any?, _: Error?) -> Void
typealias imageCompletion = (_: UIImage?) -> Void

class NetworkManager: NSObject {
    
    static let kSUCCESS = 1
    static let kRESPONSE = "response"
    static let kMESSAGE = "message"
    static let KRESPONSECODE = "responsecode"
    static let Error  = "error"
    static let Success  = "success"
    
    func getDataForRequest(_ request: String, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
        var serverPath  = ""
        if(request.contains(find:".php")) {
            serverPath = Api.base
        }
        else {
            serverPath = getServerPath()
        }
        
        let reuestUrl = serverPath + request
         print("---Complete Path----",reuestUrl)
        var encodingFormat: ParameterEncoding = URLEncoding.default
            if request == "" {
               encodingFormat = JSONEncoding.default
            }
       // let headers = ["Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(reuestUrl, method: .post, parameters: parameters, encoding: encodingFormat, headers: nil).responseJSON { (responseData) in
            
            if responseData.result.isSuccess {
                if((responseData.result.value) != nil) {
                    // When result is not nil
                    let swiftyJsonVar = JSON(responseData.result.value!)
            
                    print(swiftyJsonVar)
                    let result = swiftyJsonVar.dictionaryObject
                    if let responseCode = result?[NetworkManager.KRESPONSECODE] as? Int
                    {
                        if(responseCode == 200){
                             hideHud()
                            getResponse(result ?? "", nil)
                            
                        }else {
                             hideHud()
                            let error = "\(String(describing: result![NetworkManager.kMESSAGE] ?? INTERNAL_ERROR_MESSAGE))"
                            //showAlert(ERROR, message: error.capitalized, onView: (APPDELEGATE.window?.rootViewController)!)
                            getResponse(result ?? "", nil)
                        }
                    }else {
                          hideHud()
                    }
                   
                }
                else {
                     hideHud()
                    print(responseData.result)
                }
            }
            else {
                if let error = responseData.error, error.code == 4 {
                    let err = CustomError.init(description:INTERNAL_ERROR_MESSAGE)
                    getResponse(nil,err)
                    return;
                }
                 hideHud()
                print(responseData.error)
                getResponse(nil, responseData.error)
            }
        }
    }
    
    func getImageForRequest(_ request: String, andParameter parameters: [String:String]?, withCompletion getResponse: @escaping imageCompletion) {
        
        let reuestUrl = Api.base + request
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession.init(configuration: sessionConfig, delegate: nil, delegateQueue:OperationQueue.main)
        
        var request = URLRequest.init(url: URL.init(string: reuestUrl)!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
        request.httpMethod = "POST"

        request.setBodyContent(parameters!)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     //   request.addValue("application/json", forHTTPHeaderField: "Accept")

        // Create DataTask
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil && data != nil {
                if let imgExist = UIImage.init(data: data!) {
                    saveImageDocumentDirectory(imgExist)
                    getResponse(imgExist)
                    return;
                }
                getResponse(nil)
            }
             getResponse(nil)
        }
        dataTask.resume()
    }
    
    func uploadImage(_ request: String, onPath image:Data?, placeholderImage: Data?,_ imgParam : String, andParameter parameters: [String:Any]?, withCompletion getResponse: @escaping serviceCompletion) {
        
        var serverPath  = getServerPath()
        
        let reuestUrl = serverPath + request

        print(reuestUrl)
        print(parameters)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imgExist = image {
                
                if request == Api.uploadVideo {
                    multipartFormData.append(imgExist, withName:imgParam, fileName: "video.mov", mimeType: "video/mp4")
//                    if let placeImage = placeholderImage {
//                        multipartFormData.append(placeImage, withName:"post_image", fileName: "photo.png", mimeType: "image/png")
//                    }
                } else  {
                    multipartFormData.append(imgExist, withName:imgParam, fileName: "photo.png", mimeType: "image/png")
                }
            }
            var param =  [String:String]()
            if let params = parameters{
                param = params as! [String : String]
            }
            if let allParams = param as? [String:String] {
                    for (key, value) in allParams {
                        multipartFormData.append(value.data(using: .utf8)!, withName: key)
                    }
            }}, to: reuestUrl, method: .post, headers:nil,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            guard response.result.error == nil else {
                                print("error response")
                                print(response.result.error ?? "error response")
                                getResponse(nil,response.result.error)
                                return
                            }
                            if let value = response.result.value {
                                print(value)
                                getResponse(value,nil)
                            }
                        }
                    case .failure(let encodingError):
                        print("error:\(encodingError)")
                        getResponse(nil, encodingError)
                    }
        })
    }
    
  
    //MARK: - Server path
    
    func getServerPath() -> String
    {
        let serverPath: String = ConfigurationManager.sharedInstance.APIEndpoint()
        // serverPath.append(MiddlePath)
        print("serverpath",serverPath)
        return serverPath
    }
    
   
    //MARK: - Server Complete Path
    
    func getFullPath(path: String) -> String {
        var fullPath: String = self.getServerPath()
        fullPath.append("/")
        fullPath.append(path)
        let escapedAddress: String = fullPath.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return escapedAddress
    }
    
    var baseUrl: String {
        return getServerPath()
    }
    
    
    
}

class CustomError:Error {
    
    var localizedDescription: String { return _description }
    
    private var _description: String
    
    init(description: String) {
        self._description = description
    }
}

