//
//  UIButtonX.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 10/09/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UIImageViewX: UIImageView {
    @IBInspectable var shadow_Color : UIColor = UIColor.clear {
        didSet {
            addShadow()
        }
    }
    
    func addShadow(shadowOffset: CGSize = CGSize(width: 0.25, height: 0.5),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadow_Color.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }

}

@IBDesignable class UIButtonX: UIButton {
    
    @IBInspectable var corner_Radius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var FirstColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var SecondColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    private func updateView () {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
        layer.locations = [0.3]
    }
}

@IBDesignable class NavigationBarX: UINavigationBar {

    @IBInspectable var FirstColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var SecondColor : UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    
    
    private func updateView () {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [FirstColor.cgColor, SecondColor.cgColor]
        layer.locations = [0.3]
    }
    
}



//@IBDesignable class NavigationControllerX: UINavigationController {
//
//    @IBInspectable var barImage : UIImage = #imageLiteral(resourceName: "Navbar.PNG") {
//        didSet {
//            setImage()
//        }
//    }
//
//    private func setImage() {
//        let imageView = UIImageView(image:barImage)
//        self.navigationItem.titleView = imageView
//    }
//}



