//
//  UITextFieldX.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import UIKit

@IBDesignable class UITextFieldX: UITextField {
    
    //4- Set left/right padding
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        //  return CGRect(x: bounds.origin.x + leftPadding, y: leftPadding, bounds.origin.y, width: bounds.size.width - leftPadding - rightPadding, height: bounds.size.height)
        //return bounds.insetBy(dx: leftPadding, dy: leftPadding)
        return CGRect(x: bounds.origin.x + leftPadding, y: bounds.origin.y, width: bounds.size.width - leftPadding - rightPadding, height: bounds.size.height)
        
        
        
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    
    //5- Set left image
    @IBInspectable var leftImage: UIImage? {
        didSet {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = leftImage
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = placeholderColor
            leftView = imageView
        }
    }
    
    //5- Set left image
    @IBInspectable var rightImage: UIImage? {
        didSet {
            rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 100, y: 0, width: 20, height: 20))
            imageView.image = rightImage
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = placeholderColor
            rightView = imageView
        }
    }
    
    //6- Set placeholder color
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeholderColor])
        }
    }
    
    @IBInspectable var shadow_Color : UIColor = UIColor.clear {
        didSet {
            addShadow()
        }
    }
    
    func addShadow(shadowOffset: CGSize = CGSize(width: 0.25, height: 0.5),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadow_Color.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    
    @IBInspectable public var underLineWidth: CGFloat = 0.0 {
        didSet {
            updateUnderLineFrame()
        }
    }
    
    /// Sets the underline color
    @IBInspectable public var underLineColor: UIColor = .groupTableViewBackground {
        didSet {
            updateUnderLineUI()
        }
    }
    
    // MARK: - init methods
    override public init(frame: CGRect) {
        super.init(frame: frame)
        applyStyles()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyStyles()
    }
    
    // MARK: - Layout
    override public func layoutSubviews() {
        super.layoutSubviews()
        updateUnderLineFrame()
    }
    
    // MARK: - Styles
    private func applyStyles() {
        applyUnderLine()
    }
    
    // MARK: - Underline
    private var underLineLayer = CALayer()
    private func applyUnderLine() {
        // Apply underline only if the text view's has no borders
        if borderStyle == UITextBorderStyle.none {
            underLineLayer.removeFromSuperlayer()
            updateUnderLineFrame()
            updateUnderLineUI()
            layer.addSublayer(underLineLayer)
            layer.masksToBounds = true
        }
    }
    
    private func updateUnderLineFrame() {
        var rect = bounds
        rect.origin.y = bounds.height - underLineWidth
        rect.size.height = underLineWidth
        underLineLayer.frame = rect
    }
    
    private func updateUnderLineUI() {
        underLineLayer.backgroundColor = underLineColor.cgColor
    }
}

