//
//  ImageManager.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 15/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit

class ImageManager:NSObject {
    
    private let picker = UIImagePickerController()
    
    var imgSelected:((_:UIImage?)->Void)?
    var viewSender:UIViewController?
    
    override init() {
        super.init()
        picker.allowsEditing = true
        picker.delegate = self
    }
    
    func showImagePicker(_ sender:UIViewController,_ sourceType:UIButton,getImage:@escaping (_:UIImage?)->Void) {
        
        viewSender = sender
        imgSelected = getImage
        
        let alert = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (alert) in
            self.takeImageFrom(.camera)
        }
        
        let gallery = UIAlertAction.init(title: "Gallery", style: .default) { (alert) in
            if(UIImagePickerController.isSourceTypeAvailable(.photoLibrary)){
                self.takeImageFrom(.savedPhotosAlbum)
            }
        }
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sourceType
            popoverController.sourceRect = sourceType.bounds
        }
        viewSender?.present(alert, animated: true, completion: nil)
    }
    
   
    
    private func takeImageFrom(_ source:UIImagePickerControllerSourceType) {
        if let viewController = UIApplication.shared.windows.first?.rootViewController as UIViewController? {
            if (UIImagePickerController.isSourceTypeAvailable(source)) {
                self.picker.sourceType = source
                viewController.present(self.picker, animated: true, completion: nil)
            }
        }
    }
}

extension ImageManager:UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imgSelected!(nil)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pic = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgSelected!(pic)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ImageManager:UINavigationControllerDelegate {
    
}
