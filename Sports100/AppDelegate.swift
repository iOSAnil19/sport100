//
//  AppDelegate.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//  mailto: info@sports100.com

import UIKit
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import UserNotifications
import GoogleMaps
import GooglePlaces


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var currentIndex = 0
    var previousIndex = 0
    var appDeviceToken = "123456"
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        locationManeger.shared.configureLocationManeger()
        
        self.window?.backgroundColor = #colorLiteral(red: 0.9332515597, green: 0.9333856702, blue: 0.9332222342, alpha: 1)
        
        GMSServices.provideAPIKey(GooglePlaceAPIKey)
        GMSPlacesClient.provideAPIKey(GooglePlaceAPIKey)
        
        IQKeyboardManager.shared.enable = true
        
        Fabric.with([Crashlytics.self])
        // TODO: Move this to where you establish a user session
        self.logUser()
        
        // Push Notification Settings
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            application.registerUserNotificationSettings(pushNotificationSettings)
            application.registerForRemoteNotifications()
        }
        
        DispatchQueue.main.async {
            self.getAllTheSports()
        }
        
        
        if let user = UserInfo.sharedInfo.getUserInfo(), user.positionID != "" {
            userInfo = user
            self.goToFeedView()
        } else if UserDefaults.standard.value(forKey:onboardSeen) != nil {
            self.goToLoginView()
        }

        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("reporting@sports100.com")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Sports100")
    }
    
    private func goToLoginView() {
        if let navigationController = STORYBOARD.instantiateViewController(withIdentifier:"RootNavigation") as? UINavigationController {
            // let navVC = UINavigationController.init(rootViewController: tabBarVC)
            let loginVC = STORYBOARD.instantiateViewController(withIdentifier: StoryboardID.LoginVC)
            navigationController.viewControllers = [loginVC]
            self.window?.rootViewController = navigationController
        }
    }
    
    private func goToFeedView() {
        if let tabBarVC = STORYBOARD.instantiateViewController(withIdentifier:StoryboardID.TabBarVC) as? TabBarVC {
           // let navVC = UINavigationController.init(rootViewController: tabBarVC)
            self.window?.rootViewController = tabBarVC
        }
    }


    class func getDelegate () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
  //MARK:-
  //MARK:- Open URL
  func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    return true
  }
    
    //MARK:-
    //MARK:- Push Notification Delegate
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("PUSH ERROR: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var newToken = deviceToken.description
        newToken = newToken.trimmingCharacters(in: CharacterSet.init(charactersIn: "<>"))//CharacterSet(charactersInString: "<>"))
        newToken = newToken.replacingOccurrences(of: " ", with: "")
        
        
        let   tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceToken" ,tokenString)
        
        appDeviceToken = tokenString
        var sDeviceToken: String = "\(deviceToken)".trimmingCharacters(in: CharacterSet.whitespaces)
        sDeviceToken = sDeviceToken.replacingOccurrences(of: "", with: "")
        
        print("deviceToken : \(sDeviceToken)")
        //        UserDefaults.standard.set(tokenString, forKey: "deviceToken")
        //        UserDefaults.standard.synchronize()
        //        var sDeviceToken: String = "\(deviceToken)".trimmingCharacters(in: CharacterSet.whitespaces)
        //        sDeviceToken = sDeviceToken.replacingOccurrences(of: "", with: "")
        //        sDeviceToken = sDeviceToken.lowercased()
        //        print("deviceToken : \(sDeviceToken)")
        
        appDeviceToken = tokenString
        UserDefaults.standard.setValue(tokenString,forKey:Notification_APNS_TOKEN)
        UserDefaults.standard.synchronize()
        //        if(AppDelegate.getDelegate().DeviceToken == "")
        //        {
        //            AppDelegate.getDelegate().DeviceToken = Utility.getTempDeviceID()
        //        }
        
    }
    
    
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Called to let your app know which action was selected by the user for a given notification.
        //        NSLog("Userinfo \(response.notification.request.content.userInfo)")
        
        var pushMessage : NSString = "";
        if let aps = response.notification.request.content.userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let message = alert["message"] as? NSString {
                    //Do stuff
                    pushMessage = message
                }
            } else if let alert = aps["alert"] as? NSString {
                //Do stuff
                pushMessage = alert
            }
        }
        
        
        
        
        
        // self.navigationController(navigationVC, animated: false, completion: nil)
        
        NSLog("pushMessaging %@", pushMessage)
        //        let alert = UIAlertController(title:"", message: pushMessage as String, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
        //            print("Pressed OK")
        //
        //        }))
        //        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
        let state: UIApplicationState = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
        
        if state == .background {
            
            // background
            MoveTonext()
        }
        else if state == .active {
            
            // foreground
        }
        else if state == .inactive {
            
            // foreground
           // perform(#selector(MoveTonext), with: nil, afterDelay: 1)
        }
    }
    
    func MoveTonext()
    {
//        if self.UserData()
//        {
//            let vc = Utility.instantiateViewControllerWithIdentifier(identifier:"NotificationListViewController", storyboardName:StoryBoadName.Media_STORYBOARD as NSString)
//            if let viewpresented = topViewController(){
//                let navigationVC = UINavigationController(rootViewController: vc)
//                viewpresented.present(navigationVC, animated: false, completion: nil)
//            }
//        }
    }
    
    
    // MARK: - Received push notifications in foreground
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        print(#function)
        
        NSLog("Userinfo \(notification.request.content.userInfo)")
        
        if let aps = notification.request.content.userInfo ["aps"] as? NSDictionary
        {
            if let alert = aps["alert"] as? NSDictionary {
                
                
            }
        }
        
        NSLog("Userinfo \(notification.request.content.categoryIdentifier)")
        NSLog("Userinfo \(notification.request.content)")
        
    
        //  NSLog("pushMessaging %@", pushMessage);
        
        // INSNotificationManager.sharedInstance.showNotification(userInfo:notification.request.content.userInfo as NSDictionary )
        ///self.showNotification(userInfo: notification.request.content.userInfo as NSDictionary)
        
        completionHandler([.sound,.alert])
    }
    
    // MARK: - Handle data of notification APP in BackGround

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        print(#function)
        
        // Print full message.
        completionHandler(.newData)
        //=========
        
    }
    
    func topViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            return topController
            // topController should now be your topmost view controller
            //topController.present(alertController, animated: true, completion: nil)
        }
        return nil
        
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
         UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    fileprivate func getAllTheSports() {
        
        let params = ["key": Api.key]
        
        let networkManager = NetworkManager()
        networkManager.getDataForRequest(Api.getSport, andParameter: params) { (response, error) in
            if error == nil {
                if let result = response as? [String:Any] {
                    let success = Int("\(String(describing: result[NetworkManager.KRESPONSECODE] ?? "0"))")
                    if success == 200 {
                        if let sportsArray = result["Sport"] as? [[String:Any]] {
                            let sportsModels = sportsArray.map({ (sportInfo) -> Sport in
                                return Sport(name: "\(String(describing: sportInfo["Name"] ?? ""))", id: "\(String(describing: sportInfo["ID"] ?? ""))")
                            })
                            if sportsModels.count > 0 {
                                Utility.shared.availableSports = sportsModels
                            }
                        }
                    } else {
                        // error occured
                    }
                } else {
                    // showAlert(ERROR, message: SOMETHING_WRONG, onView: self)
                }
            } else {
                // error occured
            }
        }
    }


}

