//
//  Constant.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

//com.sport100.app

let termsConditionsURL = "https://apisports100.co.uk/app/webroot/WebappT&Cs.pdf"
let privacyURL = "https://apisports100.co.uk/app/webroot/Privacy&CookiePolicy.pdf"

//let imageUrl = "https://apisports100.co.uk"
//let imageUrl = "http://apogeesoftwares.com/sports100/users"
let imageUrl = "https://apisports100.co.uk/sports100/"

//************************ Alert titles/messages **************************

let INVALID_EMAIL = "*Please enter a valid email"
let FILL_ALL_FIELDS = "*Please fill all the required fields"
let ACCEPT_TERM = "*Please accept terms and conditions"
let ACCEPT_PRIVACY = "*Please accept privacy policy"
let ACCEPT_COOKIE = "*Please accept cookie policy"
let SERVER_ERROR = "*Server error"
let SOMETHING_WRONG = "*Something went wrong, Please try again"
let EMPTY_STRING = ""
let FAILED = "FAILED"
let INTERNAL_ERROR = "INTERNAL_ERROR"
let INTERNAL_ERROR_MESSAGE = "It seems to be a slow day. Please try after some time"
let ERROR = "Error"
let AppTitle = "Sports100"
let Ok = "OK"
let Cancel = "Cancel"
let forgotPwdMessage = "Password reset link has been emailed to you."


// CONSTANT -----
let ALERT_NETWORK_NOT_AVAILABLE = "Network is not available. Please try again."
let APP_TITLE = "Sports100"
let OK_TITLE = "Ok"
let CANCEL_TITLE = "Cancel"
let CACHED_MAIL = "Email"

let Notification_APNS_TOKEN =  "Notification_APNS_TOKEN"
//let GooglePlaceAPIKey = "AIzaSyB613QcqkaLJyxOGXkYTMceulEmVRMYYOQ"
//let GooglePlaceAPIKey = "AIzaSyBaJadBpkQFod9_canxzr0-BpzO2p1gKAk"
//let GooglePlaceAPIKey = "AIzaSyDVC-SWoRYnHksy4dRmyUQNA6TQeLN2KNs"
let GooglePlaceAPIKey = "AIzaSyDVC-SWoRYnHksy4dRmyUQNA6TQeLN2KNs"





// MARK :-- User ARCIVE KEY

let onboardSeen = "OnboardSeen"
let loginUserKey = "loginUserKey"
let profileUpdated = "profileUpdated"

let SCALE_FACTOR = Utility.getScaleFactor()

// MARK : Check Device

public struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

public struct DeviceType {
    
    //320 x 480 points    320 x 568 points    375 x 667 points    414 x 736 points
    static let IS_IPHONE            = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_7          = IS_IPHONE_6
    static let IS_IPHONE_7P         = IS_IPHONE_6P
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO_9_7      = IS_IPAD
    static let IS_IPAD_PRO_12_9     = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    
}

public struct Version {
    static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
    static let iOS9 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
    static let iOS10 = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
}



