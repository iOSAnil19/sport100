//
//  Api.swift
//  Sports100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation


struct Api {
    static let key = "50X14l25ul7gDGG27dYiL2NQsbFtt897"
    //static let base = "http://sports100.dev-zpwebsites.com/api/"
//    static let base = "https://apisports100.co.uk/api/"
    static let base = "https://apisports100.co.uk/sports100/index.php/"
    
   // static let base = "http://apogeesoftwares.com/sports100/"  Set From Configuration File
    
    //static let uploadBase = "http://sports100.dev-zpwebsites.com/uploads/"
    
    static let uploadBase = "https://apisports100.co.uk/uploads/"

    
    // MARK : - UPDATED SERVICE METHOD --------------
    
   
    ///  POST API Method-------------
    static let addPostText = "posts/addPostText"
    static let addAchievment = "posts/addAchievment"
    static let saveImage = "posts/saveImage"
    static let uploadVideo = "posts/addPost"
    static let addFeedPost = "posts/addPost"
    ///  End -------------
    
    static let signin = "users/signin"
    static let userRegister = "users/register"
    static let saveProfile = "users/saveProfile"
    static let forgotPassword = "users/forgotPassword"
    static let getProfile = "users/getProfile"
    static let facebookLogin = "users/facebookLogin"
    static let addBio = "users/addBio"
    static let changePassword = "users/changeUserPassword"
    static let getAllUser = "users/getAllUser"
    static let logout = "users/signout"
    static let deactivateAccount = "users/deactivateAccount"
    static let getPosition = "users/getPosition"
    
    static let searchUser = "users/searchUser"

    
    static let getAllFeed = "posts/getUserFeed"
    static let getAllUserFeed = "posts/getAllUserFeed"
    static let likeUnlikePost = "posts/likeUnlikePost"
    static let deletePost = "posts/deletePost"
    static let reportPost = "posts/report"
    static let writeComment = "posts/comment"
    static let getComment = "posts/getComment"
    static let isView = "posts/isView"
    static let viewLikeUser = "posts/viewLikeUser"
    static let getAllTeam = "teams/getSportAllTeam"
    static let getLeague  = "teams/getLeague"
    static let addTeam    = "teams/addteam"
    static let editTeam   = "teams/editTeam"
    static let removeTeam = "teams/removeTeam"
    static let teamNotification = "teams/teamNotification"
    static let requestTeamMember = "teams/requestTeamMember"
    static let acceptTeamRequest = "teams/acceptTeamRequest"
    static let declineTeamRequest = "teams/declineTeamRequest"
    static let editTrainingTime = "teams/editTrainingTime"
    static let addmatch = "teams/addmatch"
    static let getMatch = "teams/getMatch"
    static let getTeamMessage = "teams/getTeamMessage"
    static let sendteamMessage = "teams/teamMessage"
    static let leaveTeam = "teams/leaveTeam"
    static let getTeam = "teams/getTeam"
    static let getAllUserTeam = "teams/getAllUserTeam"
    static let getSport = "users/getSport"
    static let getGoupedSports = "teams/getSportAllTeam"
    static let getStanderd = "users/getStandard"
    static let getAge = "teams/getTeamAgeGroup"
    static let searchTeam = "teams/teamSearch"


    static let getMessageThred = "messages/getMessageThred"
    static let sendMessage = "messages/sendMessage"
    static let getAllMessage = "messages/getAllMessage"
    static let removeMessageThread = "messages/removeMessageThread"
    
    
    static let sendRequest = "friends/sendRequest"
    static let contactList = "friends/contactList"
    static let allContactList = "users/getAllUser"
    static let cancelReqst = "friends/cancelContactRequest"
    static let resendReqst = "friends/resendContactRequest"

    static let acceptContactRequest = "friends/acceptContactRequest"
    static let declineContactRequest = "friends/declineContactRequest"
    static let deleteContactRequest = "friends/deleteContactRequest"
    static let sendFeedback = "friends/sendEmail"
    static let sendReport = "posts/report"
    
    static let searchResult = "sports/searhResult"

}

struct ApiConstant
{
    
    static let userToken   = "deviceToken"

    //MARK :-- User Login
    
    static let userEmail      = "email"
    static let userPassword   = "password"
    
    //MARK :-- User Registration

    static let userFirstname  = "first_name"
    static let userLastname   = "last_name"
    static let userGender     = "gender"
    static let userDob        = "dob"
    static let sportTyppe     = "sports_type"
    static let deviceType     = "device_type"
    static let fBId           = "fb_id"
    static let standard           = "standard"
    
    //MARK :-- Complete Profile Page
    
    static let userProfilePic   = ""
    static let usernNickname    = ""
    static let choosePosition   = ""
    static let nationality      = ""
    static let availability     = ""
    static let availabilityTime = ""
    static let primaryHand      = ""
    static let userHeight       = ""
    static let userWeight       = ""
    static let userDBSNo        = ""
    static let userLocation     = ""
    static let userBio          = "bio"
    
    //MARK :-- Setting Page
    
    static let oldPassword     = "oldPassword"
    static let newPassword     = "newPassword"


    
    
    //MARK :-- Add Achievement
    
    static let userId = "userID"
    static let achieveDescription = "description"
    static let achieveId = "selectedID"
    static let achieveDate = "aDate"

    //MARK :-- Post Image
    static let post_image = "profile_photo"
    static let profile_photo = "profile_photo"
    static let post_Video = "video"
    static let post_photo = "post_image"
    
}

enum APIEndpoint {
    
    case none
    
}
