//
//  ButtonExtension.swift
//  Cult
//
//  Created by Mukesh Muteja on 02/01/19.
//  Copyright © 2019 Han. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func addGradientBorder(_ startColor : UIColor, _ endColor : UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        let shape = CAShapeLayer()
        shape.lineWidth = 2.0
        shape.path = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: 6.0).cgPath
        
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func changeTextColor(_ startColor : UIColor, _ endColor : UIColor) {
        if let layers = self.layer.sublayers {
            for sublayer in layers {
                if let layers = sublayer.sublayers {
                    for minSub in layers {
                        if (minSub is CAGradientLayer) {
                            return
                        }
                    }
                }
            }
        }
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        //    let alpha: Float = angle / 360
        //    let startPointX = powf(
        //        sinf(2 * Float.pi * ((alpha + 0.75) / 2)),
        //        2
        //    )
        //    let startPointY = powf(
        //        sinf(2 * Float.pi * ((alpha + 0) / 2)),
        //        2
        //    )
        //    let endPointX = powf(
        //        sinf(2 * Float.pi * ((alpha + 0.25) / 2)),
        //        2
        //    )
        //    let endPointY = powf(
        //        sinf(2 * Float.pi * ((alpha + 0.5) / 2)),
        //        2
        //    )
        
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        self.layer.sublayers?.removeAll()
        let overlayView = UIView(frame: bounds)
        overlayView.isUserInteractionEnabled = false
        overlayView.layer.insertSublayer(gradient, at: 0)
        overlayView.mask = titleLabel
        addSubview(overlayView)
    }
    
    func setGradientBackground(_ startColor : UIColor, _ endColor : UIColor) {
        
        if let image = gradientImage(in: self.frame, colors: [startColor.cgColor , endColor.cgColor]) {
            self.setBackgroundImage(image, for: .normal)
        }
    }
    
    func removeCALayer() {
        if let layers = self.layer.sublayers {
            for sublayer in layers {
                if let layers = sublayer.sublayers {
                    for minSub in layers {
                        if (minSub is CAGradientLayer) {
                            minSub.removeFromSuperlayer()
                        }
                    }
                }
            }
        }
    }
}

