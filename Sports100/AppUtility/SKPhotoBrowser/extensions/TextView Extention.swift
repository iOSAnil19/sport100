//
//  TextViewExtension.swift
//  Cult
//
//  Created by Mukesh Muteja on 03/01/19.
//  Copyright © 2019 Han. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func changeTextColor(_ startColor : UIColor, _ endColor : UIColor) {
        if let color = drawGradientColor(in: self.frame, colors: [startColor.cgColor , endColor.cgColor]) {
            self.textColor = color
        }
        
    }
    
}

extension UITextView {
    func changeTextColor(_ startColor : UIColor, _ endColor : UIColor) {
        if let color = drawGradientColor(in: self.frame, colors: [startColor.cgColor , endColor.cgColor]) {
            self.textColor = color
        }
        
    }
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            
            attributedString.addAttribute(kCTKernAttributeName as NSAttributedStringKey,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(kCTKernAttributeName as NSAttributedStringKey, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
    
}



extension UILabel {
    func changeTextColor(_ startColor : UIColor, _ endColor : UIColor) {
        //    let gradient = CAGradientLayer()
        //    gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        //    gradient.colors = [startColor.cgColor, endColor.cgColor]
        //    gradient.startPoint = CGPoint(x: 0, y: 1)
        //    gradient.endPoint = CGPoint(x: 1, y: 1)
        if let color = drawGradientColor(in: self.frame, colors: [startColor.cgColor , endColor.cgColor]) {
            self.textColor = color
        }
        //    let overlayView = UIView(frame: bounds)
        //    overlayView.isUserInteractionEnabled = false
        //    overlayView.layer.insertSublayer(gradient, at: 0)
        //    overlayView.mask = self
        //
        //    addSubview(overlayView)
    }
    
    func applyGradientWith(startColor: UIColor, endColor: UIColor) -> Bool {
        
        var startColorRed:CGFloat = 0
        var startColorGreen:CGFloat = 0
        var startColorBlue:CGFloat = 0
        var startAlpha:CGFloat = 0
        
        if !startColor.getRed(&startColorRed, green: &startColorGreen, blue: &startColorBlue, alpha: &startAlpha) {
            return false
        }
        
        var endColorRed:CGFloat = 0
        var endColorGreen:CGFloat = 0
        var endColorBlue:CGFloat = 0
        var endAlpha:CGFloat = 0
        
        if !endColor.getRed(&endColorRed, green: &endColorGreen, blue: &endColorBlue, alpha: &endAlpha) {
            return false
        }
        
        let gradientText = self.text ?? ""
        
        let name:String = NSAttributedStringKey.font.rawValue
        let textSize: CGSize = gradientText.size(withAttributes: [NSAttributedString.Key(rawValue: name):self.font])
        let width:CGFloat = textSize.width
        let height:CGFloat = textSize.height
        
        UIGraphicsBeginImageContext(CGSize(width: width, height: height))
        
        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return false
        }
        
        UIGraphicsPushContext(context)
        
        let glossGradient:CGGradient?
        let rgbColorspace:CGColorSpace?
        let num_locations:size_t = 2
        let locations:[CGFloat] = [ 0.0, 1.0 ]
        let components:[CGFloat] = [startColorRed, startColorGreen, startColorBlue, startAlpha, endColorRed, endColorGreen, endColorBlue, endAlpha]
        rgbColorspace = CGColorSpaceCreateDeviceRGB()
        glossGradient = CGGradient(colorSpace: rgbColorspace!, colorComponents: components, locations: locations, count: num_locations)
        let topCenter = CGPoint.zero
        let bottomCenter = CGPoint(x: 0, y: textSize.height)
        context.drawLinearGradient(glossGradient!, start: topCenter, end: bottomCenter, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
        
        UIGraphicsPopContext()
        
        guard let gradientImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return false
        }
        
        UIGraphicsEndImageContext()
        
        self.textColor = UIColor(patternImage: gradientImage)
        
        return true
    }
}

fileprivate func drawGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
    if let image = gradientImage(in: rect, colors: colors) {
        return UIColor(patternImage: image)
    } else {
        return nil
    }
}


func gradientImage(in rect: CGRect, colors: [CGColor]) -> UIImage? {
    let currentContext = UIGraphicsGetCurrentContext()
    currentContext?.saveGState()
    defer { currentContext?.restoreGState() }
    
    let size = rect.size
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                    colors: colors as CFArray,
                                    locations: nil) else { return nil }
    
    let context = UIGraphicsGetCurrentContext()
    context?.drawLinearGradient(gradient,
                                start: CGPoint(x: 0, y: 1),
                                end: CGPoint(x: size.width, y: 1),
                                options: [])
    let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    guard let image = gradientImage else { return nil }
    return image
    
}
