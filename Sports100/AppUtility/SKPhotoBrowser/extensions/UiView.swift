//
//  UiView.swift
//  Sports100
//
//  Created by VeeraJain on 23/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

class UiView : UIView {
    
    func gradientImage(in rect: CGRect, colors: [CGColor]) -> UIImage? {
        let currentContext = UIGraphicsGetCurrentContext()
        currentContext?.saveGState()
        defer { currentContext?.restoreGState() }
        
        let size = rect.size
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                        colors: colors as CFArray,
                                        locations: nil) else { return nil }
        
        let context = UIGraphicsGetCurrentContext()
        context?.drawLinearGradient(gradient,
                                    start: CGPoint(x:0, y: 0),
                                    end: CGPoint(x: 1, y:size.height),
                                    options: [])
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = gradientImage else { return nil }
        return image
        
    }
}
