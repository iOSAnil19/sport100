
//
//  TextFeild.swift
//  Sports100
//
//  Created by VeeraJain on 10/05/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

extension UITextField {
    enum Direction
    {
        case Left
        case Right
    }
    
    func addRightLabel(direction:Direction,text:String,Frame:CGRect) -> UILabel
    {
        let View = UIView(frame: Frame)
        View.backgroundColor = UIColor.clear
        
        let label = UILabel()
        label.font = UIFont.init(name: "OpenSans-Bold", size: 15)
        label.frame = View.bounds
        label.text = text
        label.textColor = UIColor.white
        label.minimumScaleFactor = 0.5
        
        View.addSubview(label)
        
        if Direction.Left == direction
        {
            self.leftViewMode = .always
            self.leftView = View
        }
        else
        {
            self.rightViewMode = .always
            self.rightView = View
        }
        return label
    }
func AddImage(direction:Direction,imageName:UIImage,Frame:CGRect,backgroundColor:UIColor)
{
    let View = UIView(frame: Frame)
    View.backgroundColor = backgroundColor
    
    let imageView = UIImageView(frame: CGRect.init(x: 0, y: 10, width: 10   , height: 10))
    imageView.image = imageName
    imageView.contentMode = .scaleAspectFit
    View.addSubview(imageView)
    
    if Direction.Left == direction
    {
        self.leftViewMode = .always
        self.leftView = View
    }
    else
    {
        self.rightViewMode = .always
        self.rightView = View
    }
}
}
