//
//  UIViewcontrollerExtension.swift
//  Sports100
//
//  Created by VeeraJain on 19/06/19.
//  Copyright © 2019 Vivan Raghuvanshi. All rights reserved.
//


import UIKit

extension UIViewController {
func showAlertMessage(_ message: String, andWithTitle title: String? = nil) {
    let alertVC = UIAlertController(
        title: title,
        message: message,
        preferredStyle: .alert)
    let okAction = UIAlertAction(
        title: "OK",
        style:.default,
        handler: nil)
    alertVC.addAction(okAction)
    present(
        alertVC,
        animated: true,
        completion: nil)
}
}
