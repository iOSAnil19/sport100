//
//  StoryboardID.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 14/02/18.
//  Copyright © 2018 Vivan Raghuvanshi. All rights reserved.
//

import Foundation

struct StoryboardID {
    static let TabBarVC = "TabBarVC"
    static let LoginVC = "LoginVC"
    static let SignUpVC = "SignUpVC"
    static let TermsAndConditionVC = "TermsAndConditionVC"
    static let AddMoreInfoVC = "AddMoreInfoVC"
    static let CompleteRegisterVC = "CompleteRegisterVC"
    static let MessagesVC = "MessagesVC"
    static let FeedsVC = "FeedsVC"
    static let MyFeedVC = "MyFeedVC"
    static let SearchVC = "SearchVC"
    static let TeamsVC = "TeamsVC"
    static let ProfileVC = "ProfileVC"
    static let SettingsVC = "SettingsVC"
    static let ContactsVC = "ContactsVC"
    static let CameraVC = "CustomCameraViewController"
    static let PhotoPreviewVC = "PhotoPreviewVC"
    static let VideoPreviewVC = "VideoPreviewVC"
    
    static let CommentViewController = "CommentViewController"
    static let AddTeamVC = "AddTeamVC"
    static let NotificationVC = "NotificationVC"
    static let PlayersVC = "PlayersVC"
    static let MatchesVC = "MatchesVC"
    static let TeamMessageVC = "TeamMessageVC"
    static let TeamMenuViewController = "TeamMenuViewController"
    static let MyTeamsVC = "MyTeamsVC"
    static let ReportVC = "ReportVC"
    static let FeedbackVC = "FeedbackVC"
    static let ViewsVC = "ViewsVC"
    static let TeamSportsVC = "TeamSportsVC"
    static let CameraNavigationVC = "CameraNavigationVC"
    static let CreateNewTeam = "CreateNewTeamVC"
}


