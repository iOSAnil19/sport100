//
//  training.swift
//  Sport100
//
//  Created by Zachary Powell on 26/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation
import UIKit

class training {
    var date: String
    var startTime: String
    var endTime: String
    var cost: String
    var ID:Int
    
    init (Date: String, StartTime: String, EndTime: String, Cost: String, ID: Int) {
        self.date = Date
        self.startTime = StartTime
        self.endTime = EndTime
        self.cost = Cost
        self.ID = ID
    }
    
    init(){
        self.date = ""
        self.startTime = ""
        self.endTime = ""
        self.cost = ""

        ID = 0
    }
}
