//
//  EditTeamViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 26/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ImagePicker
import LocationPickerViewController
import ActionSheetPicker_3_0
import Fusuma

class EditTeamViewController: UIViewController, ImagePickerDelegate, FusumaDelegate {
    @IBOutlet weak var searchBar: UIView!

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var location: UIButton!
    @IBOutlet weak var homeName: UITextField!
    @IBOutlet weak var homeLocation: UIButton!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var website: UITextField!
    @IBOutlet weak var setSport: UIButton!
    @IBOutlet weak var setLeague: UIButton!
    @IBOutlet weak var save: UIButton!
    
    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var bottombar: UIView!
    
    var profilePictureChanged = false
    
    var nameText = ""
    var street = ""
    var town = ""
    var postcode = ""
    var homeText = ""
    var hStreet = ""
    var hTown = ""
    var hPostcode = ""
    var bioText = ""
    var websiteText = ""
    var sport = ""
    var league = ""
    
    var sports: [String] = []
    var leagues: [String] = []
    
    var teamID = 0
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    
    @IBAction func locationTouch(_ sender: Any) {
        let locationPicker = LocationPicker()
        locationPicker.pickCompletion = { (pickedLocationItem) in
            
            self.location.setTitle("Team set: " + pickedLocationItem.name, for: UIControlState.normal)
            let locationDictionary = pickedLocationItem.addressDictionary
            
            for (key, value) in locationDictionary! {
                if (key.description == "Street"){
                    self.street = value as! String
                }else if (key.description == "City"){
                    self.town = value as! String
                }else if (key.description == "ZIP"){
                    self.postcode = value as! String
                }
            }
            
            print(self.postcode)
        }
        locationPicker.addBarButtons()
        
        let navigationController = UINavigationController(rootViewController: locationPicker)
        present(navigationController, animated: true, completion: nil)
        
        
    }
    @IBAction func homeLocationTouch(_ sender: Any) {
        let locationPicker = LocationPicker()
        locationPicker.pickCompletion = { (pickedLocationItem) in
            
            self.homeLocation.setTitle("Home set: " + pickedLocationItem.name, for: UIControlState.normal)
            let locationDictionary = pickedLocationItem.addressDictionary
            
            for (key, value) in locationDictionary! {
                if (key.description == "Street"){
                    self.hStreet = value as! String
                }else if (key.description == "City"){
                    self.hTown = value as! String
                }else if (key.description == "ZIP"){
                    self.hPostcode = value as! String
                }
            }
            
            print(self.postcode)
        }
        locationPicker.addBarButtons()
        
        let navigationController = UINavigationController(rootViewController: locationPicker)
        present(navigationController, animated: true, completion: nil)
    }
    @IBAction func setSportTouch(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select the Team sport", rows: sports, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.setSport.setTitle("Team Sport: " + picked, for: UIControlState.normal)
                self.sport = picked
                
                Just.post(
                    API.getLeagues,
                    data: ["key": API.key, "sport":picked]
                ) { r in
                    if r.ok {
                        let json = JSON(data: r.content!);
                        let success = json["success"].intValue
                        
                        print(json)
                        
                        if (success == 1){
                            if let items = json["leagues"].array {
                                for item in items {
                                    let sport = item["Name"].stringValue
                                    
                                    self.leagues.append(sport)
                                }
                            }
                            
                        }else{
                            
                        }
                        
                    }
                }
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func setLeagueTouch(_ sender: Any) {
        if (sport != ""){
            ActionSheetStringPicker.show(withTitle: "Select the Team League", rows: leagues, initialSelection: 0, doneBlock: {
                picker, value, index in
                
                if let picked = index as? String {
                    self.setLeague.setTitle("Team League: " + picked, for: UIControlState.normal)
                    self.league = picked
                    
                } else {
                    print("Error") // Was not a string
                }
                
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        }else{
            showAlert("Error", message:"Select Sport first.", onView: self)
        }
    }
    @IBAction func save(_ sender: Any) {
        
        
        nameText = name.text!
        homeText = homeName.text!
        bioText = bio.text!
        websiteText = website.text!
        
        if (nameText == ""){
            showAlert("Error", message:"Please enter your Team Name.", onView: self)
            return
        }
        
        if (homeText == ""){
            showAlert("Error", message:"Please enter your Home Ground Name.", onView: self)
            return
        }
        
        if (bioText == ""){
            showAlert("Error", message:"Please enter your Team bio.", onView: self)
            return
        }
        
        SwiftSpinner.show("Saving Team")
        
        
        let defaults = UserDefaults.standard
        
        var dataDictionary = [String: Any]()
        
        dataDictionary["key"] = API.key
        dataDictionary["street"] = street
        dataDictionary["city"] = town
        dataDictionary["postcode"] = postcode
        dataDictionary["homeStreet"] = hStreet
        dataDictionary["homeCity"] = hTown
        dataDictionary["homePostcode"] = hPostcode
        dataDictionary["name"] = nameText
        dataDictionary["homeGroundName"] = homeText
        dataDictionary["league"] = league
        dataDictionary["profile"] = bioText
        dataDictionary["website"] = websiteText
        dataDictionary["sport"] = sport
        dataDictionary["teamID"] = teamID
        dataDictionary["userID"] = defaults.integer(forKey: "ID")
        
        var fileDictionary = [String: HTTPFile]()
        if(profilePictureChanged){
            let imageData = UIImageJPEGRepresentation(self.profileImage.image!, 0.7)
            
            fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        }
        
        Just.post(
            API.updateTeam,
            data: dataDictionary,
            files: fileDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.performSegue(withIdentifier: "editToTeam", sender: nil)
                        SwiftSpinner.hide()
                        
                    }
                    
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        SwiftSpinner.hide()
                        
                    }
                    
                }
                
            }
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        topbar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottombar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        name.layer.cornerRadius = 15
        name.clipsToBounds = true
        
        location.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        location.layer.cornerRadius = 15
        location.clipsToBounds = true
        
        self.profileImage.layer.borderWidth = 4
        self.profileImage.layer.masksToBounds = false
        self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
        self.profileImage.clipsToBounds = true
        
        homeName.layer.cornerRadius = 15
        homeName.clipsToBounds = true
        
        homeLocation.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        homeLocation.layer.cornerRadius = 15
        homeLocation.clipsToBounds = true
        
        bio.layer.cornerRadius = 15
        bio.clipsToBounds = true
        
        website.layer.cornerRadius = 15
        website.clipsToBounds = true
        
        setSport.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        setSport.layer.cornerRadius = 15
        setSport.clipsToBounds = true
        
        setLeague.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        setLeague.layer.cornerRadius = 15
        setLeague.clipsToBounds = true
        
        save.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        save.layer.cornerRadius = 15
        save.clipsToBounds = true
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        
        Just.post(
            API.getAllSports,
            data: ["key": API.key]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["sports"].array {
                        for item in items {
                            let sport = item["Name"].stringValue
                            
                            self.sports.append(sport)
                        }
                    }
                    
                }else{
                    
                }
                
            }
        }

        // editteamToSearch
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "editteamToSearch", sender: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        SwiftSpinner.show("Loading Team")
        
        Just.post(
            API.getTeam,
            data: ["key": API.key, "teamID" : teamID]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                
                print(json)
                if (success == 1){
                    
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.name.text = json["team"]["Name"].stringValue
                        
                        let url = URL(string:API.uploadBase +  json["team"]["File"].stringValue)
                        self.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                        
                        self.profileImage.layer.borderWidth = 0
                        self.profileImage.layer.masksToBounds = false
                        self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
                        self.profileImage.clipsToBounds = true
                        
                        self.homeName.text = json["team"]["HomeGroundName"].stringValue
                        self.bio.text = json["team"]["Profile"].stringValue
                        self.website.text = json["team"]["Website"].stringValue

                        SwiftSpinner.hide()
                        
                    }
                    
                    
                }else{
                    SwiftSpinner.hide()
                    
                }
                
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let imagePickerController = ImagePickerController()
        imagePickerController.imageLimit = 1
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        profileImage.image = images[0]
        profilePictureChanged = true
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "editteamToSearch"){
            
        }else{
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = teamID
        }

    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
}
