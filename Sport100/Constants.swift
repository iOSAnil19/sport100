//
//  Constants.swift
//
//  Copyright © 2017 Sports100. All rights reserved.
//

import UIKit


struct API {
    static let key = "50X14l25ul7gDGG27dYiL2NQsbFtt897"
    //static let base = "http://sports100.dev-zpwebsites.com/api/"
    static let base = "https://apisports100.co.uk/api/"
    
    //static let uploadBase = "http://sports100.dev-zpwebsites.com/uploads/"
    
    static let uploadBase = "https://apisports100.co.uk/uploads/"
    
    static let login = base+"login.php";
    static let fblogin = base+"fblogin.php";
    static let reset = base+"forgottenPassword.php"
    static let getAllSports = base+"getAllSports.php"
    static let getLeagues = base+"getLeagues.php"
    static let newTeam = base+"addTeam.php"
    static let updateTeam = base+"updateTeam.php"
    static let getTeams = base+"getTeams.php"
    static let getTeam = base+"getTeam.php"
    static let getPlayers = base+"getPlayers.php"
    static let register = base+"register.php"
    static let getProfileInfo = base+"getProfileDetails.php"
    static let saveProfileInfo = base+"saveProfileDetails.php"
    static let saveBio = base+"saveBio.php"
    static let uploadImage = base+"saveImage.php"
    static let deleteTeamPlayer = base+"removeTeamPlayer.php"
    static let saveMatch = base+"saveMatch.php"
    static let getMatches = base+"getMatches.php"
    static let saveTraining = base+"saveTraining.php"
    static let getTraining = base+"getTraining.php"
    static let deleteTeamTraining = base+"deleteTeamTraining.php"
    static let uploadVideo = base+"uploadVideo.php"
    static let getFeed = base+"getFeed.php"
    static let getPositions = base+"getAllPositions.php"
    
    static let getVideo = base+"getVideo.php"
    static let getFeedProfile = base+"getFeedProfile.php"
    static let getFeedImage = base+"getFeedImage.php"
    static let getFeedMatch = base+"getFeedMatch.php"
    static let getFeedHistory = base+"getFeedHistory.php"
    static let getFeedAchievement = base+"getFeedAchievement.php"


    static let search = base+"search.php"

    static let contacts = base+"getContacts.php"
    
    static let addContact = base+"addContact.php"
    static let deleteContact = base+"deleteContact.php"

    static let getMessageThread = base+"getMessageThread.php"
    static let getMessageThreads = base+"getMessageThreads.php"
    static let sendMessage = base+"sendMessage.php"
    static let deleteMessageThread = base+"deleteThread.php"
    
    static let getHistory = base+"getHistory.php"
    static let deleteHistory = base+"deleteHistory.php"
    static let saveHistory = base+"saveHistory.php"
    
    static let getAchievement = base+"getAchievement.php"
    static let deleteAchievement = base+"deleteAchievement.php"
    static let saveAchievement = base+"addAchievement.php"
    
    static let deleteVideo = base+"deleteVideo.php"

}

struct sportColour {
    static let basketball = UIColor(red:0.40, green:0.14, blue:0.51, alpha:1.0)
    static let football = UIColor(red:0.20, green:0.40, blue:0.80, alpha:1.0)
    static let netball = UIColor(red:0.00, green:0.60, blue:0.60, alpha:1.0)
    static let football5 = UIColor(red:0.20, green:0.60, blue:0.80, alpha:1.0)
    static let football7 = UIColor(red:0.00, green:0.40, blue:0.80, alpha:1.0)
    static let football11 = UIColor(red:0.30, green:0.51, blue:0.73, alpha:1.0)
    
    static let basketballGradient = UIColor(red:0.64, green:0.10, blue:0.36, alpha:1.0)
    static let footballGradient = UIColor(red:0.20, green:0.20, blue:0.80, alpha:1.0)
    static let netballGradient = UIColor(red:0.00, green:0.46, blue:0.60, alpha:1.0)
    static let football5Gradient = UIColor(red:0.20, green:0.39, blue:0.80, alpha:1.0)
    static let football7Gradient = UIColor(red:0.00, green:0.23, blue:0.80, alpha:1.0)
    static let football11Gradient = UIColor(red:0.06, green:0.23, blue:0.53, alpha:1.0)


}
