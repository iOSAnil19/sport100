//
//  ViewTeamsViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 11/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma

class TeamViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var sportView: UIImageView!
    @IBOutlet weak var cellView: UIView!
}

class ViewTeamsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate {

    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var bottombar: UIView!
    @IBOutlet weak var createTeam: UIButton!
    
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var teamTable: UITableView!
    var teams = [team]()
    
    var selectedTeam = team()
    
    var userid = 0
    var own = true
    
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        topbar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottombar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        createTeam.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)

        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        createTeam.layer.cornerRadius = 15
        createTeam.clipsToBounds = true
        
        teamTable.rowHeight = 70

        SwiftSpinner.show("Loading Teams")
                
        if (userid == 0){
            let defaults = UserDefaults.standard
            userid = defaults.integer(forKey: "ID")
            own = true
        }else{
            own = false
            createTeam.isHidden = true
        }
        
        Just.post(
            API.getTeams,
            data: ["key": API.key, "userID" : userid]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    if let items = json["leagues"].array {
                        for item in items {
                            self.teams.append(team.init(Name: item["Name"].stringValue, ID: item["ID"].intValue, sportID: item["SportID"].intValue))
                        }
                    }
                    print(self.teams)
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.teamTable.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
        }

        //teamsToSearch
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "teamsToSearch", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = teams[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Team", for: indexPath) as! TeamViewCell
        
        cell.backgroundColor = UIColor(white: 1, alpha: 0)
        cell.label.text = element.name
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        switch element.sportID {
        case 5:
        
            cell.cellView.applyGradient(withColours: [sportColour.netball, sportColour.netballGradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "netball");

        case 4:
            cell.cellView.applyGradient(withColours: [sportColour.basketball, sportColour.basketballGradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "basketball");

        case 3:
            cell.cellView.applyGradient(withColours: [sportColour.football11, sportColour.football11Gradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "11aside");

        case 2:
            cell.cellView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "7aside");
        case  1:
            cell.cellView.applyGradient(withColours: [sportColour.football5, sportColour.football5Gradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "fiveasideSM");
        default:
            cell.cellView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
            cell.sportView.image = UIImage(named: "7aside");
        }

        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var element = teams[indexPath.row]
        selectedTeam = element
        self.performSegue(withIdentifier: "teamsToTeam", sender: self)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "teamsToTeam" {
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = selectedTeam.ID
            
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
    
}
