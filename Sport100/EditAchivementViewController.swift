//
//  EditAchivementViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 11/07/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import SwipeCellKit
import ActionSheetPicker_3_0

class EditAchivementViewCell: SwipeTableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
}

class EditAchivementViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {

    var tableData = [achivement]()

    var dataDictionary = [String: Any]()
    var userid = 0
    
    @IBOutlet weak var threadTable: UITableView!
    
    @IBOutlet weak var create: UIButton!
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var aDate: UIButton!
    
    @IBOutlet weak var achievementCount: UILabel!
    var aday = ""
    var amonth = ""
    var ayear = ""
    
    var selectedID = 0
    
   
    @IBAction func editingChanged(_ sender: Any) {
        let achivementtext = String(40 - (descriptionField.text?.count)!)
        achievementCount.text = achivementtext
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let achivementtext = String(40 - textView.text.count)
        achievementCount.text = achivementtext
    }
    
    @IBAction func selectDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Achivement Date:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let date : Date = value as! Date
            
            let calendar = Calendar.current
            
            let dateDay = calendar.component(.day, from: date)
            let dateMonth = calendar.component(.month, from: date)
            let dateYear = calendar.component(.year, from: date)
            
            self.aDate.setTitle("Date: " + String(dateDay) + "-" + String(dateMonth) + "-" + String(dateYear), for: UIControlState.normal)
            
            self.aday = String(dateDay)
            self.amonth = String(dateMonth)
            self.ayear = String(dateYear)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as! UIButton).superview!.superview)
        
        datePicker?.show()
    }
    
    @IBAction func saveNew(_ sender: Any) {
        
        let descriptionText = descriptionField.text ?? ""
        
        if (descriptionText == ""){
            showAlert("Error", message:"Please enter your achivement description", onView: self)
            return
        }
        
        if (descriptionText.count > 40) {
            
            showAlert("Error", message:"Achivement description cant be more than 40 characters", onView: self)
            return
        }
        
        let date = ayear+"-"+amonth+"-"+aday
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        dataDictionary["description"] = descriptionText
        dataDictionary["aDate"] = date
        dataDictionary["selectedID"] = selectedID
        
        self.editView.isHidden = true
        
        Just.post(
            API.saveAchievement,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                
                print(json)
                
                if (success == 1){
                    
                    let defaults = UserDefaults.standard
                    self.userid = defaults.integer(forKey: "ID")
                    
                    self.dataDictionary["key"] = API.key
                    self.dataDictionary["userID"] = self.userid
                    
                    Just.post(
                        API.getAchievement,
                        data: self.dataDictionary
                    ) { r in
                        if r.ok {
                            let json = JSON(data: r.content!);
                            let success = json["success"].intValue
                            
                            print(json)
                            
                            
                            if (success == 1){
                                self.tableData.removeAll()
                                
                                if let items = json["achievement"].array {
                                    for item in items {
                                        let newAchivement = achivement.init(ID: item["ID"].intValue, description: item["Description"].stringValue, date: item["aDate"].stringValue)
                                        
                                        self.tableData.append(newAchivement)
                                    }
                                }
                                DispatchQueue.main.async { [unowned self] in
                                    
                                    self.threadTable.reloadData()
                                }
                            }
                            SwiftSpinner.hide()
                            
                        }
                    }
                }
                SwiftSpinner.hide()
                
            }
        }
        
    }
    
    @IBAction func createNew(_ sender: Any) {
        editView.isHidden = false
        selectedID = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        create.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        create.layer.cornerRadius = 15
        create.clipsToBounds = true
        
        editView.layer.cornerRadius = 15
        editView.clipsToBounds = true
        editView.isHidden = true
        
        save.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        save.layer.cornerRadius = 15
        save.clipsToBounds = true

        aDate.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        aDate.layer.cornerRadius = 15
        aDate.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        SwiftSpinner.show("Loading Achivement")
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        
        Just.post(
            API.getAchievement,
            data: self.dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                
                if (success == 1){
                    self.tableData.removeAll()
                    
                    if let items = json["achievement"].array {
                        for item in items {
                            let newAchivement = achivement.init(ID: item["ID"].intValue, description: item["Description"].stringValue, date: item["aDate"].stringValue)
                            
                            self.tableData.append(newAchivement)
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.threadTable.reloadData()
                    }
                }
                SwiftSpinner.hide()
                
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = tableData[indexPath.row]
        
        editView.isHidden = false
        selectedID = element.ID
        
        descriptionField.text = element.description
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = tableData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "achivement", for: indexPath) as! EditAchivementViewCell
        cell.descriptionLabel.text = element.description
        cell.dateLabel.text = element.date
        
        cell.delegate = self
        
        return cell
    }


    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            let element = self.tableData[indexPath.row]
            
            Just.post(
                API.deleteAchievement,
                data: ["key": API.key, "achievementID" : element.ID ]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    
                    if (success == 1){
                        Just.post(
                            API.getAchievement,
                            data: self.dataDictionary
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                print(json)
                                
                                
                                if (success == 1){
                                    self.tableData.removeAll()
                                    
                                    if let items = json["achievement"].array {
                                        for item in items {
                                            let newAchivement = achivement.init(ID: item["ID"].intValue, description: item["Description"].stringValue, date: item["aDate"].stringValue)
                                            
                                            self.tableData.append(newAchivement)
                                        }
                                    }
                                    DispatchQueue.main.async { [unowned self] in
                                        
                                        self.threadTable.reloadData()
                                    }
                                }
                                SwiftSpinner.hide()
                                
                            }
                        }
                    }else{
                        showAlert("Error", message: "Failed to remove achivement", onView: self)
                    }
                    
                }
                
            }
        }
        
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction]
    }

}
