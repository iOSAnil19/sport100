//
//  ContactsViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 16/05/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma
import SwipeCellKit

class ContactViewCell: SwipeTableViewCell {
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactTitle: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class ContactsViewController: UIViewController, FusumaDelegate, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
   
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var contactTable: UITableView!
    let fusuma = FusumaViewController()
    
    var tableData = [contact]()
    var indexOfNumbers = [String]()
    
    var contactDictionary: [String:[contact]] = [:]
    
    var dataDictionary = [String: Any]()
    var userid = 0

    var selectedID = 0
    
    var name = ""
    
    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactTable.backgroundColor = UIColor.clear

        let indexNumbers = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
        indexOfNumbers = indexNumbers.components(separatedBy: " ")

        fusuma.delegate = self
        fusuma.hasVideo = true
        topView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        contactDictionary["A"] = Array<contact>()
        contactDictionary["B"] = Array<contact>()
        contactDictionary["C"] = Array<contact>()
        contactDictionary["D"] = Array<contact>()
        contactDictionary["E"] = Array<contact>()
        contactDictionary["F"] = Array<contact>()
        contactDictionary["G"] = Array<contact>()
        contactDictionary["H"] = Array<contact>()
        contactDictionary["I"] = Array<contact>()
        contactDictionary["J"] = Array<contact>()
        contactDictionary["K"] = Array<contact>()
        contactDictionary["L"] = Array<contact>()
        contactDictionary["M"] = Array<contact>()
        contactDictionary["N"] = Array<contact>()
        contactDictionary["O"] = Array<contact>()
        contactDictionary["P"] = Array<contact>()
        contactDictionary["Q"] = Array<contact>()
        contactDictionary["R"] = Array<contact>()
        contactDictionary["S"] = Array<contact>()
        contactDictionary["T"] = Array<contact>()
        contactDictionary["U"] = Array<contact>()
        contactDictionary["V"] = Array<contact>()
        contactDictionary["W"] = Array<contact>()
        contactDictionary["X"] = Array<contact>()
        contactDictionary["Y"] = Array<contact>()
        contactDictionary["Z"] = Array<contact>()

        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "contactToSearch", sender: nil)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        SwiftSpinner.show("Loading Contacts")
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        
        
        Just.post(
            API.contacts,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    self.tableData.removeAll()


                    if let items = json["result"].array {
                        for item in items {
                            let newcontact = contact.init(Name: item["FirstName"].stringValue + " " + item["LastName"].stringValue , ID: item["OtherUserID"].intValue, Picture: item["File"].stringValue)
                            
                            self.tableData.append(newcontact)
                        }
                    }
                    
                    self.tableData.sort { $0.name < $1.name }
                    

                    for contactinfo in self.tableData {
                        var capital = contactinfo.name.capitalized
                        
                        var letterArray = self.contactDictionary[String(describing: capital.characters.first!)] ?? Array<contact>()
                        letterArray.append(contactinfo)

                        self.contactDictionary[String(describing: capital.characters.first!)] = letterArray
                    
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.contactTable.reloadData()
                    }
                }
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
         }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }
    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexString = indexOfNumbers[indexPath.section]
        let section = contactDictionary[indexString]
        let element = section?[indexPath.row]
        
        selectedID = element?.ID ?? 0
        self.performSegue(withIdentifier: "contactToProfile", sender: self)

    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let indexString = indexOfNumbers[section]

        let count = contactDictionary[indexString]?.count ?? 0
        print(indexString)
        print(count)
        return count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return indexOfNumbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath) as! ContactViewCell
        
        cell.backgroundColor = UIColor.clear
        
        cell.cellView.backgroundColor = UIColor.clear
        
        let indexString = indexOfNumbers[indexPath.section]
        let section = contactDictionary[indexString]
        let element = section?[indexPath.row]
                
        cell.contactImage.layer.borderWidth = 3
        cell.contactImage.layer.masksToBounds = false
        cell.contactImage.layer.borderColor = sportColour.football.cgColor
        cell.contactImage.layer.cornerRadius = cell.contactImage.frame.height/2
        cell.contactImage.clipsToBounds = true
                
        cell.contactTitle.text = element?.name
        let url = URL(string:API.uploadBase +  (element?.picture)!)
        cell.contactImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
        
        cell.delegate = self

        return cell
    }
    

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
         return indexOfNumbers
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return indexOfNumbers.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return indexOfNumbers[section]
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        let temp = indexOfNumbers as NSArray
        return temp.index(of: title)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        // Background color
        view.tintColor = UIColor.clear
        
        // Text Color
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = UIColor(red:0.23, green:0.33, blue:0.62, alpha:1.0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
       if segue.identifier == "contactToProfile" {
            let yourNextViewController = (segue.destination as! ProfileViewController)
            yourNextViewController.userID = selectedID
            
        
        }else if segue.identifier == "profileToMessage"{
                let yourNextViewController = (segue.destination as! SendNewMessageViewController)
                yourNextViewController.pickedID = selectedID
                yourNextViewController.name = name
                
        }

    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            let indexString = self.indexOfNumbers[indexPath.section]
            var section = self.contactDictionary[indexString]
            
            let element = section?[indexPath.row]
            
            let refreshAlert = UIAlertController(title: "Delete", message: "Are you sure you want to remove this contact?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                Just.post(
                    API.deleteContact,
                    data: ["key": API.key, "userID" : self.userid, "otherID":element?.ID ?? 0]
                ) { r in
                    if r.ok {
                        let json = JSON(data: r.content!);
                        let success = json["success"].intValue
                        
                        if (success == 1){
                            Just.post(
                                API.contacts,
                                data: self.dataDictionary
                            ) { r in
                                if r.ok {
                                    let json = JSON(data: r.content!);
                                    let success = json["success"].intValue
                                    
                                    self.tableData.removeAll()
                                    self.contactDictionary.removeAll()
                                    
                                    SwiftSpinner.hide()
                                    
                                    if (success == 1){
                                        
                                        if let items = json["result"].array {
                                            for item in items {
                                                let newcontact = contact.init(Name: item["FirstName"].stringValue + " " + item["LastName"].stringValue , ID: item["OtherUserID"].intValue, Picture: item["File"].stringValue)
                                                
                                                self.tableData.append(newcontact)
                                            }
                                        }
                                        
                                        self.tableData.sort { $0.name < $1.name }
                                        
                                        
                                        for contactinfo in self.tableData {
                                            var capital = contactinfo.name.capitalized
                                            
                                            var letterArray = self.contactDictionary[String(describing: capital.first!)] ?? Array<contact>()
                                            letterArray.append(contactinfo)
                                            
                                            self.contactDictionary[String(describing: capital.first!)] = letterArray
                                            
                                        }
                                        
                                        DispatchQueue.main.async { [unowned self] in
                                            
                                            self.contactTable.reloadData()
                                        }
                                    }
                                }
                            }
                        }else{
                            showAlert("Error", message: "Failed to remove contact", onView: self)
                        }
                        
                    }
                    
                }
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
            
            
        }
        
        let flag = SwipeAction(style: .default, title: "Message", handler: { action, indexPath in
            
            let indexString = self.indexOfNumbers[indexPath.section]
            let section = self.contactDictionary[indexString]
            let element = section?[indexPath.row]
            
            self.selectedID = element?.ID ?? 0
            self.name = element?.name ?? ""
            
            self.performSegue(withIdentifier: "profileToMessage", sender: self)
        
            }
            
        )
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        //flag.image = UIImage(named: "message")
        flag.backgroundColor = sportColour.football5
        
        return [deleteAction, flag]
    }
}
