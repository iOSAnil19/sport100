//
//  searchResult.swift
//  Sport100
//
//  Created by Zachary Powell on 10/05/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class searchResult {
    var Name: String
    var Image: String
    var ID:Int
    
    
    init (Name: String, Image: String, ID: Int) {
        self.Name = Name
        self.Image = Image
        self.ID = ID
    }
    
    init(){
        self.Name = ""
        self.Image = ""
        ID = 0
    }
}
