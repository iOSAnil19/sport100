//
//  history.swift
//  Sport100
//
//  Created by Zachary Powell on 11/07/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class history {
    var ID: Int = 0
    var sport: String
    var level: String
    var club: String
    var joined: String
    var left: String
    var league: String

    
    init (ID: Int, sport: String, level: String, club: String, joined: String, left: String, league: String) {
        self.sport = sport
        self.level = level
        self.club = club
        self.joined = joined
        self.left = left
        self.league = league
        self.ID = ID
    }
    
    init(){
        sport = ""
        level = ""
        club = ""
        joined = ""
        left = ""
        league = ""
    }
    
}
