//
//  match.swift
//  Sport100
//
//  Created by Zachary Powell on 26/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation
import UIKit

class match {
    var teams: String
    var date: String
    var score: String
    var ID:Int
    
    init (Teams: String, Date: String, Score: String, ID: Int) {
        self.teams = Teams
        self.date = Date
        self.score = Score
        self.ID = ID
    }
    
    init(){
        teams = ""
        date = ""
        score = ""
        ID = 0
    }
}
