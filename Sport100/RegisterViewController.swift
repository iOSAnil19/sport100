//
//  RegisterViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 20/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Navajo_Swift
import SafariServices

class RegisterViewController: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var male: UISwitch!
    @IBOutlet weak var female: UISwitch!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var confirmEmail: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var tcSwitch: UISwitch!
 
    @IBOutlet weak var firstSport: UIButton!
    @IBOutlet weak var dobButton: UIButton!
    
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var strength: UILabel!
    var sports: [String] = []
    
    var first = ""
    var day = ""
    var month = ""
    var year = ""
    
    var emailText = ""
    var firstNameText = ""
    var lastNameText = ""
    var fbid = ""
    
    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 44, height: 44))
        titleView.addSubview(UIImageView.init(image: #imageLiteral(resourceName: "logo.png")))
        
        self.navigationItem.titleView = titleView
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)        ], gradientOrientation: .vertical)
        
        dobButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        dobButton.layer.cornerRadius = 15
        dobButton.clipsToBounds = true
       
        
        if (emailText != ""){
            email.text = emailText
            confirmEmail.text = emailText
        }
        
        if (firstNameText != ""){
            firstName.text = firstNameText
        }
        
        if (lastNameText != ""){
            lastName.text = lastNameText
            
        }
        
        Just.post(
            API.getAllSports,
            data: ["key": API.key]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["sports"].array {
                        for item in items {
                            let sport = item["Name"].stringValue
                            
                            self.sports.append(sport)
                        }
                    }
                    
                }else{
                    
                }
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func passwordChanged(_ sender: Any) {
        let password = self.password.text ?? ""
        let strengthPassword = Navajo.strength(of: password)
        
        strength.text = Navajo.localizedString(for: strengthPassword)
    }
    
    @IBAction func textChanged(_ sender: Any) {
        
        
        
    }
    
    @IBAction func dobClicked(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Birthday:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let maxTime: TimeInterval = 13 * 52 * 7 * 24 * 60 * 60;
            let maxDate = NSDate(timeInterval: -maxTime, since: Date()) as Date!
            
            
            var date : Date = value as! Date
            
            print(maxDate!.years(from: date))
            
            if(maxDate!.years(from: date) < 0){
                self.sendAlert(message: "You must be 13 or over to use SPORTS100")
                return
            }
            
            let calendar = Calendar.current
            
            let dateDay = calendar.component(.day, from: date)
            let dateMonth = calendar.component(.month, from: date)
            let dateYear = calendar.component(.year, from: date)
            
            self.dobButton.setTitle("Birthday: " + String(dateDay) + "-" + String(dateMonth) + "-" + String(dateYear), for: UIControlState.normal)
            
            self.day = String(dateDay)
            self.month = String(dateMonth)
            self.year = String(dateYear)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as! UIButton).superview!.superview)
        let minTime: TimeInterval = 110 * 52 * 7 * 24 * 60 * 60;

        datePicker?.minimumDate = NSDate(timeInterval: -minTime, since: Date()) as Date!
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func firstSportClicked(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your first sport", rows: sports, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.firstSport.setTitle("First Sport: " + picked, for: UIControlState.normal)
                self.first = picked
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func maleChanged(_ sender: Any) {
        if (male.isOn){
            female.setOn(true, animated: true)
            male.setOn(false, animated: true)
        }else{
            male.setOn(true, animated: true)
            female.setOn(false, animated: true)
        }

    }
    
    @IBAction func femaleChanged(_ sender: Any) {
        if (female.isOn){
            male.setOn(true, animated: true)
            female.setOn(false, animated: true)
        }else{
            female.setOn(true, animated: true)
            male.setOn(false, animated: true)
        }
    }
    
    @IBAction func buttonClicked_TermsCondition(_ sender: Any) {
       
        if let url = URL.init(string:"http://sports100.com/app/terms.html") {
            let svc = SFSafariViewController.init(url: url)
            self.present(svc, animated: true, completion: nil)
        }
    }
    
    func sendAlert(message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: Any) {
        
        let textFirstName = firstName.text ?? ""
        let textLastName = lastName.text ?? ""
        let textEmail = email.text ?? ""
        let textConfirmEmail = confirmEmail.text ?? ""
        let textPassword = password.text ?? ""
        let textConfirmPassword = confirmPassword.text ?? ""
        
        let strengthPassword = Navajo.strength(of: textPassword)
        
        let decimalCharacters = CharacterSet.decimalDigits
        let firstdecimalRange = textFirstName.rangeOfCharacter(from: decimalCharacters)
        let seconddecimalRange = textLastName.rangeOfCharacter(from: decimalCharacters)

        if firstdecimalRange != nil {
            sendAlert(message: "First Name can not include numbers")
            return
        }
        
        if seconddecimalRange != nil {
            sendAlert(message: "Second Name can not include numbers")
            return
        }
        
        if (textFirstName == ""){
            sendAlert(message: "Please enter your First Name.")
            return
        }
        if (textLastName == ""){
            sendAlert(message: "Please enter your Last Name.")
            return
        }
        if (day == ""){
            sendAlert(message: "Please enter your date of birth.")
            return
        }
        if (month == ""){
            sendAlert(message: "Please enter your date of birth.")
            return
        }
        if (year == ""){
            sendAlert(message: "Please enter your date of birth.")
            return
        }
        if (textEmail == ""){
            sendAlert(message: "Please enter your email.")
            return
        }
        if (textConfirmEmail == ""){
            sendAlert(message: "Please enter your confirm your email.")
            return
        }
        if (textPassword == ""){
            sendAlert(message: "Please enter your password.")
            return
        }
        if (textConfirmPassword == ""){
            sendAlert(message: "Please enter your confirm your password.")
            return
        }
        if (first == ""){
            sendAlert(message: "Please enter your main Sport.")
            return
        }
        if (textEmail != textConfirmEmail){
            sendAlert(message: "Your emails do not match.")
            return
        }
        if (textPassword != textConfirmPassword){
            sendAlert(message: "Your passwords do not match.")
            return
        }
        if (strengthPassword == .weak || strengthPassword == .veryWeak){
            sendAlert(message: "Password must be at least Reasonable stength.")
            return
        }
        if (!isValidEmail(textEmail)){
            sendAlert(message: "Email is not valid.")
            return
        }
        if (textFirstName.count < 2 || textFirstName.count > 20){
            sendAlert(message: "Insure first name is at least 2 character long and not more than 20")
            return
        }
        if (textLastName.count < 2 || textLastName.count > 20){
            sendAlert(message: "Insure last name is at least 2 character long and not more than 20")
            return
        }
        if (!tcSwitch.isOn){
            sendAlert(message: "You must accept the terms and conditions to sign up")
            return
        }
        
        
        SwiftSpinner.show("Registering your account...")
        var gender = ""
        if (male.isOn){
            gender = "Male"
        }else{
            gender = "Female"
        }
        
        let dob = year+"-"+month+"-"+day
        
        Just.post(
            API.register,
            data: ["key": API.key, "firstname": textFirstName, "lastname": textLastName, "gender":gender, "dob":dob, "email":textEmail, "password":textPassword, "psport":first, "ssport":"", "fb":fbid ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                print(r)
                print(json)
                
                if (success == 1){
                    
                    SwiftSpinner.hide()
                    DispatchQueue.main.async { [unowned self] in
                        if let completeRegisterViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompleteRegisterViewController") {
                        self.navigationController?.pushViewController(completeRegisterViewController, animated: true)
                        }
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    SwiftSpinner.hide()
                    
                    
                    return
                }
                
            }
        }

    }
}
