//
//  EditTrainingTimeViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 26/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Fusuma


class editTrainingViewCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var delete: UIButton!

    @IBOutlet weak var cellView: UIView!
}

class EditTrainingTimeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate {
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var bottomBar: UIView!

    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var endTimeText: UIButton!
    @IBOutlet weak var startTimeText: UIButton!
    @IBOutlet weak var dayText: UIButton!
    @IBOutlet weak var add: UIButton!
    @IBOutlet weak var trainingTable: UITableView!
    var teamID = 0
    var trainingID = 0
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") as NSString
        let newText = text.replacingCharacters(in: range, with: string)
        if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
            return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
        }
        return false
    }
    
    @IBOutlet weak var addView: UIView!
    var trainingArray = [training]()
    
    var daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    @IBAction func saveTraining(_ sender: Any) {
        SwiftSpinner.show("Saving Training Time")
        
        Just.post(
            API.saveTraining,
            data: ["key": API.key, "teamID" : teamID, "trainingID":trainingID, "price":price.text ?? "", "endTime":endTimeText.title(for: UIControlState.normal) ?? "", "startTime":startTimeText.title(for: UIControlState.normal) ?? "", "day":dayText.title(for: UIControlState.normal) ?? ""]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(self.teamID)
                print(json)
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.price.text = ""
                        self.endTimeText.setTitle("End Time", for: UIControlState.normal)
                        self.startTimeText.setTitle("Start Time", for: UIControlState.normal)
                        self.dayText.setTitle("Day of the week", for: UIControlState.normal)
                        self.addView.isHidden = true
                        self.trainingID=0
                        
                        SwiftSpinner.show("Loading Training Times")
                        
                        Just.post(
                            API.getTraining,
                            data: ["key": API.key, "teamID" : self.teamID]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                
                                SwiftSpinner.hide()
                                
                                if (success == 1){
                                    self.trainingArray.removeAll()
                                    if let items = json["times"].array {
                                        for item in items {
                                            
                                            self.trainingArray.append(training.init(Date: item["tDate"].stringValue, StartTime: item["tStart"].stringValue, EndTime: item["tEnd"].stringValue, Cost: item["Cost"].stringValue, ID: item["ID"].intValue))
                                        }
                                    }
                                    
                                    DispatchQueue.main.async { [unowned self] in
                                        self.trainingTable.reloadData()
                                    }
                                    
                                }else{
                                    
                                }
                                
                            }
                        }
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
            }
        }

    }
    @IBAction func setEnd(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "End Time:", datePickerMode: UIDatePickerMode.time, selectedDate: Date(),  doneBlock: {
            picker, value, index in
            
            let date = value as! Date
            let dateFormatter = DateFormatter()
            //the "M/d/yy, H:mm" is put together from the Symbol Table
            dateFormatter.dateFormat = "hh:mm a"
            self.endTimeText.setTitle(dateFormatter.string(from: date), for: UIControlState.normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        datePicker?.show()

    }

    
    @IBAction func setStart(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Start Time:", datePickerMode: UIDatePickerMode.time, selectedDate: Date(),  doneBlock: {
            picker, value, index in
            
            let date = value as! Date
            let dateFormatter = DateFormatter()
            //the "M/d/yy, H:mm" is put together from the Symbol Table
            dateFormatter.dateFormat = "hh:mm a"
            self.startTimeText.setTitle(dateFormatter.string(from: date), for: UIControlState.normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        datePicker?.show()
    }
   
    @IBAction func setDay(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Pick day of the week", rows: daysOfWeek, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            self.dayText.setTitle(self.daysOfWeek[value], for: UIControlState.normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        self.view.endEditing(true)

    }

    
    @IBAction func close(_ sender: Any) {
        addView.isHidden = true
        self.price.text = ""
        self.endTimeText.setTitle("End Time", for: UIControlState.normal)
        self.startTimeText.setTitle("Start Time", for: UIControlState.normal)
        self.dayText.setTitle("Day of the week", for: UIControlState.normal)
        self.trainingID=0

    }
    
    @IBAction func addTime(_ sender: Any) {
        addView.isHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addView.isHidden = true

        fusuma.delegate = self
        fusuma.hasVideo = true
        topBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        
        add.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        add.layer.cornerRadius = 15
        add.clipsToBounds = true

        addView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        addView.layer.cornerRadius = 15
        addView.clipsToBounds = true
        addView.isHidden = true
        
        closeButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        closeButton.layer.cornerRadius = 15
        closeButton.clipsToBounds = true
        
        saveButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        saveButton.layer.cornerRadius = 15
        saveButton.clipsToBounds = true
        
        self.dayText.layer.cornerRadius = 5
        self.startTimeText.layer.cornerRadius = 5
        self.endTimeText.layer.cornerRadius = 5
        
        self.dayText.clipsToBounds = true
        self.startTimeText.clipsToBounds = true
        self.endTimeText.clipsToBounds = true
        
        SwiftSpinner.show("Loading Training Times")
        
        Just.post(
            API.getTraining,
            data: ["key": API.key, "teamID" : self.teamID]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    self.trainingArray.removeAll()
                    if let items = json["times"].array {
                        for item in items {
                            
                            self.trainingArray.append(training.init(Date: item["tDate"].stringValue, StartTime: item["tStart"].stringValue, EndTime: item["tEnd"].stringValue, Cost: item["Cost"].stringValue, ID: item["ID"].intValue))
                        }
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.trainingTable.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
        }
        //trainingToSearch
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "trainingToSearch", sender: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToTeam" {
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = teamID
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = trainingArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Training", for: indexPath) as! editTrainingViewCell
        
        cell.delete.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        cell.delete.tag = indexPath.row
        
        cell.backgroundColor = UIColor(white: 1, alpha: 0)
        cell.cost.text = "£ "+element.cost
        cell.date.text = element.date
        cell.startTime.text = element.startTime
        cell.endTime.text = element.endTime
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        cell.cellView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = trainingArray[indexPath.row]
        
        self.dayText.setTitle(element.date, for: UIControlState.normal)
        self.trainingID = element.ID
        self.startTimeText.setTitle(element.startTime, for: UIControlState.normal)
        self.endTimeText.setTitle(element.endTime, for: UIControlState.normal)
        self.price.text = element.cost
        
        addView.isHidden = false
    }

    
    func cancelTapped(sender: UIButton){
        let buttonTag = sender.tag
        let selected = trainingArray[buttonTag]
        
        Just.post(
            API.deleteTeamTraining,
            data: ["key": API.key, "teamID" : teamID, "trainingID":selected.ID]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                
                if (success == 1){
                    SwiftSpinner.show("Loading Training Times")
                    
                    Just.post(
                        API.getTraining,
                        data: ["key": API.key, "teamID" : self.teamID]
                    ) { r in
                        if r.ok {
                            let json = JSON(data: r.content!);
                            let success = json["success"].intValue
                            
                            
                            SwiftSpinner.hide()
                            
                            if (success == 1){
                                self.trainingArray.removeAll()
                                if let items = json["times"].array {
                                    for item in items {
                                        
                                        self.trainingArray.append(training.init(Date: item["tDate"].stringValue, StartTime: item["tStart"].stringValue, EndTime: item["tEnd"].stringValue, Cost: item["Cost"].stringValue, ID: item["ID"].intValue))
                                    }
                                }
                                
                                DispatchQueue.main.async { [unowned self] in
                                    self.trainingTable.reloadData()
                                }
                                
                            }else{
                                
                            }
                            
                        }
                    }

                }else{
                    showAlert("Error", message:"Failed to remove training", onView: self)
                }
                
            }
            
        }
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
}
