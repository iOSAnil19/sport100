//
//  GalleryViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 17/04/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ScalingCarousel

class Cell: ScalingCarouselCell {}

class GalleryViewController: UIViewController {

    var userid = 0
    let itemsPerRow: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)

    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var carousel:ScalingCarouselView!
    
    var pictures: [String] = []
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        closeButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        closeButton.layer.cornerRadius = 15
        closeButton.clipsToBounds = true

        Just.post(
            API.getProfileInfo,
            data: ["key": API.key, "userID": userid ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                         self.pictures.removeAll()
                         if let items = json["pictures"].array {
                            for item in items {
                                self.pictures.append(item["File"].stringValue)
                            }
                         }
                        
                        self.carousel.reloadData()
                        
                    }

                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    return
                }
                
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

typealias CarouselDatasource = GalleryViewController
extension CarouselDatasource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let url = URL(string:API.uploadBase +  pictures[indexPath.row])

        if let scalingCell = cell as? ScalingCarouselCell {
            let imageview = scalingCell.mainView as! UIImageView
            
            imageview.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
        }
        
        return cell
    }
}

typealias CarouselDelegate = GalleryViewController
extension GalleryViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()
    }
}
