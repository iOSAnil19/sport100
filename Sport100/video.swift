//
//  video.swift
//  Sport100
//
//  Created by Zachary Powell on 04/09/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation
import UIKit

class video {
    var videoURL: URL
    var imageURL: URL
    var ID: String
    
    init (videoUrl: URL, imageURL: URL, id: String) {
        self.videoURL = videoUrl
        self.imageURL = imageURL
        self.ID = id
    }
    
}
