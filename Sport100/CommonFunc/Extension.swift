//
//  Extension.swift
//  SmartTrucking
//
//  Created by Vivan Raghuvanshi on 19/08/17.
//  Copyright © 2017 Vivan Raghuvanshi. All rights reserved.
//

import Foundation
import UIKit

let aiFrame = CGRect.init(x:(SCREEN_WIDTH - 52), y:33.5, width: 37, height: 37)

extension UIViewController : UITextFieldDelegate {

    func startIndicator(_ sender:UIViewController, forFrame activityFrame:CGRect?, withCenter center:CGPoint?) {
        let activity = UIActivityIndicatorView()
        activity.tag = 1000
        activity.activityIndicatorViewStyle = .gray
        activity.hidesWhenStopped = true
        if activityFrame == nil {
            activity.frame = aiFrame
        } else {
            activity.frame = activityFrame!
        }
        sender.view.addSubview(activity)
        sender.view.bringSubview(toFront: activity)
        if let c = center {
            activity.center = c
        }
        activity.startAnimating()
    }

    func stopIndicator(_ sender:UIViewController) {
        DispatchQueue.main.async {
            if let activity = sender.view.viewWithTag(1000) as? UIActivityIndicatorView {
                activity.stopAnimating()
            }
        }
    }


    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true;
    }
}

//extension UILabel {
//
//    override open func awakeFromNib() {
//         super.awakeFromNib()
//        self.setup()
//    }
//
//    func setup() {
//        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!*getScaleFactor())
//    }
//}
//
//extension UITextField {
//
//    override open func awakeFromNib() {
//        super.awakeFromNib()
//        self.setup()
//    }
//
//    func setup() {
//        self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)!*getScaleFactor())
//    }
//}

extension String {
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: String.CompareOptions.caseInsensitive) != nil
    }
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    /// Percent escape value to be added to a HTTP request
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "*".
    /// This will also replace spaces with the "+" character as outlined in the application/x-www-form-urlencoded spec:
    ///
    /// http://www.w3.org/TR/html5/forms.html#application/x-www-form-urlencoded-encoding-algorithm
    ///
    /// - returns: Return percent escaped string.
    
    func addingPercentEncodingForQuery() -> String? {
        let allowed = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._* ")
        
        return addingPercentEncoding(withAllowedCharacters: allowed)?.replacingOccurrences(of: " ", with: "+")
    }
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: generalDelimitersToEncode + subDelimitersToEncode)
        
        return addingPercentEncoding(withAllowedCharacters: allowed)
    }
}


extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// - returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    public func stringFromHttpParameters() -> String {
        let parameterArray = map { key, value -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}

extension URLRequest {
    
    /// Populate the HTTPBody of `application/x-www-form-urlencoded` request
    ///
    /// - parameter parameters:   A dictionary of keys and values to be added to the request
    
    mutating func setBodyContent(_ parameters: [String : String]) {
        let parameterArray = parameters.map { (key, value) -> String in
            return "\(key.addingPercentEncodingForQuery()!)=\(value.addingPercentEncodingForQuery()!)"
        }
        httpBody = parameterArray.joined(separator: "&").data(using: .utf8)
    }
}




extension UIView {
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    
    public func jitter() {
        let animation = CABasicAnimation(keyPath:"position")
        animation.duration = 0.05
        animation.repeatCount = 2
        animation.autoreverses = true
        
        let fromPoint = CGPoint.init(x: self.center.x - 5.0, y: self.center.y)
        let toPoint = CGPoint.init(x: self.center.x + 5.0, y: self.center.y)
        
        animation.fromValue = NSValue(cgPoint:fromPoint)
        animation.toValue = NSValue(cgPoint:toPoint)
        
        layer.add(animation, forKey:"position")
        
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    public func flashView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.alpha = 1
        }) { (animationCompleted) in
            if animationCompleted == true {
                UIView.animate(withDuration: 0.3, delay: 3.0, options: .curveEaseOut, animations: {
                    self.alpha = 0
                }, completion: nil)
            }
        }
    }
    
    public func roundCorner() {
        let height = self.bounds.height
        self.layer.cornerRadius = height/2
        self.clipsToBounds = true
    }
}

extension UIImage {
    
    func scaleImage(_ image:UIImage)->UIImage {
        
        let maxSize = 500.0 as CGFloat
        let width = image.size.width
        let height = image.size.height
        
        var newWidth = width
        var newHeight = height
        
        if (width > maxSize || height > maxSize) {
            if (width > height) {
                newWidth = maxSize;
                newHeight = (height*maxSize)/width;
            } else {
                newHeight = maxSize;
                newWidth = (width*maxSize)/height;
            }
            
            
            let newSize = CGSize.init(width: newWidth, height: newHeight)
            
            UIGraphicsBeginImageContext(newSize)
            
            image.draw(in: CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            return newImage!
        }
        return image
    }
}


//extension UIButton {
//    
//    override open func awakeFromNib() {
//        super.awakeFromNib()
//        self.setup()
//    }
//    
//    /*override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.isUserInteractionEnabled = false
//        self.perform(#selector(enableUserInteraction), with: self, afterDelay: 0.25)
//    }
//    
//    @objc private func enableUserInteraction() {
//        self.isUserInteractionEnabled = true
//    }*/
//    
//    func setup() {
//        self.titleLabel?.font = UIFont(name: (self.titleLabel?.font?.fontName)!, size: (self.titleLabel?.font?.pointSize)!*getScaleFactor())
//    }
//}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

