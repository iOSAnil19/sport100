//
//  achivement.swift
//  Sport100
//
//  Created by Zachary Powell on 21/07/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class achivement {
    var ID: Int = 0
    var description: String
    var date: String
    
    
    init (ID: Int, description: String, date: String) {
        self.description = description
        self.date = date
        self.ID = ID
    }
    
    init(){
        date = ""
        description = ""
    }
    
}
