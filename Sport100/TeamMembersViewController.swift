//
//  TeamMembersViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 19/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma

class PlayerViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var remove: UIButton!
    @IBOutlet weak var cellView: UIView!
}

class TeamMembersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate {

    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomVIew: UIView!
    @IBOutlet weak var playerList: UITableView!
    
    var teamid:Int!
    var sportid:Int!
    var adminid:Int!
    var userID = 0
    
    var players = [player]()
    var selectedPlayer = player()
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        topView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomVIew.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        let defaults = UserDefaults.standard
         userID = defaults.integer(forKey: "ID")
        
        playerList.rowHeight = 60
        
        SwiftSpinner.show("Loading Players")
        
        Just.post(
            API.getPlayers,
            data: ["key": API.key, "teamID" : teamid]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(self.teamid)
                print(json)
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    if let items = json["players"].array {
                        for item in items {
                            self.players.append(player.init(Name: item["FirstName"].stringValue + " " + item["LastName"].stringValue, ID: item["UserID"].intValue, number: item["tNumber"].intValue))
                        }
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.playerList.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
        }
        
        // membersToSearch
    
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "membersToSearch", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cancelTapped(sender: UIButton){
        let buttonTag = sender.tag
        var selected = players[buttonTag]
        
        Just.post(
            API.deleteTeamPlayer,
            data: ["key": API.key, "teamID" : teamid, "userID":selected.ID]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                
                if (success == 1){
                    SwiftSpinner.show("Loading Players")
                    
                    self.players.removeAll()
                    
                    Just.post(
                        API.getPlayers,
                        data: ["key": API.key, "teamID" : self.teamid]
                    ) { r in
                        if r.ok {
                            let json = JSON(data: r.content!);
                            let success = json["success"].intValue
                            
                            print(self.teamid)
                            print(json)
                            
                            SwiftSpinner.hide()
                            
                            if (success == 1){
                                if let items = json["players"].array {
                                    for item in items {
                                        self.players.append(player.init(Name: item["FirstName"].stringValue + " " + item["LastName"].stringValue, ID: item["UserID"].intValue, number: item["tNumber"].intValue))
                                    }
                                }
                                
                                DispatchQueue.main.async { [unowned self] in
                                    self.playerList.reloadData()
                                }
                                
                            }else{
                                
                            }
                            
                        }
                    }
                }else{
                    showAlert("Error", message: "Failed to remove player", onView: self)
                }
                
            }

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = players[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Player", for: indexPath) as! PlayerViewCell
        
        cell.backgroundColor = UIColor(white: 1, alpha: 0)
        cell.name.text = element.name
        cell.number.text = String(element.number)
        
        if (adminid == userID){
            cell.remove.isHidden = false
        }else{
            cell.remove.isHidden = true
        }

        if (element.ID == userID){
            cell.remove.isHidden = true
        }
        
        cell.remove.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        cell.remove.tag = indexPath.row
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        switch sportid {
        case 5:
            
            cell.cellView.applyGradient(withColours: [sportColour.netball, sportColour.netballGradient], gradientOrientation: .horizontal)
            
        case 4:
            cell.cellView.applyGradient(withColours: [sportColour.basketball, sportColour.basketballGradient], gradientOrientation: .horizontal)
            
        case 3:
            cell.cellView.applyGradient(withColours: [sportColour.football11, sportColour.football11Gradient], gradientOrientation: .horizontal)
            
        case 2:
            cell.cellView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
        case  1:
            cell.cellView.applyGradient(withColours: [sportColour.football5, sportColour.football5Gradient], gradientOrientation: .horizontal)
        default:
            cell.cellView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var element = players[indexPath.row]
        selectedPlayer = element
        self.performSegue(withIdentifier: "playersToProfile", sender: self)
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "playersToProfile" {
            if (selectedPlayer.ID != userID){
                let yourNextViewController = (segue.destination as! ProfileViewController)
                yourNextViewController.userID = selectedPlayer.ID
            }
 
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }
    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }

}
