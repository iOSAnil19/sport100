//
//  SearchResultViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 09/05/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit

class SearchViewCell: UITableViewCell {
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var searchTitle: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class SearchResultViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var noSearchFound: UILabel!
    var searchPlayers = true
    var searchNetball = true
    var searchBasketball = true
    var searchfootball5 = true
    var searchfootball7 = true
    var searchfootball11 = true
    var start = 0
    var userid = 0
    
    var searchresults = [searchResult]()

    var dataDictionary = [String: Any]()
    
    var searchText=""
    
    var selectedID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        topBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer
            = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        SwiftSpinner.show("Loading Search Results")
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        dataDictionary["start"] = start
        
        if (searchPlayers){
            dataDictionary["table"]="Profile"
        }else{
            dataDictionary["table"]="Team"
        }
        
        dataDictionary["basketball"]=searchNetball
        dataDictionary["netball"]=searchBasketball
        dataDictionary["football5"]=searchfootball5
        dataDictionary["football7"]=searchfootball7
        dataDictionary["football11"]=searchfootball11
        
        dataDictionary["search"]=searchText
        
        Just.post(
            API.search,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.searchTable.isHidden = false
                        self.noSearchFound.isHidden = true
                    }
                    if let items = json["results"].array {
                        print("HERE")
                        for item in items {
                            print("THERE")
                            self.searchresults.append(searchResult.init(Name: item["Name"].stringValue, Image: item["File"].stringValue, ID: item["ID"].intValue))
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        self.searchTable.reloadData()
                    }
                    self.start += 20
                }else{
                    DispatchQueue.main.async { [unowned self] in
                        self.searchTable.isHidden = true
                        self.noSearchFound.isHidden = false
                    }
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (searchresults.count - indexPath.row) == 5 {
            dataDictionary["start"] = start

            Just.post(
                API.search,
                data: dataDictionary
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    SwiftSpinner.hide()
                    
                    if (success == 1){
                        self.searchTable.isHidden = false
                        self.noSearchFound.isHidden = true
                        if let items = json["results"].array {
                            for item in items {
                                self.searchresults.append(searchResult.init(Name: item["Name"].stringValue, Image: item["File"].stringValue, ID: item["ID"].intValue))
                            }
                        }
                        print(self.searchresults.count)
                        DispatchQueue.main.async { [unowned self] in
                            self.searchTable.reloadData()
                        }
                        self.start += 20
                    }else{
                        self.searchTable.isHidden = true
                        self.noSearchFound.isHidden = false
                        
                    }
                    
                }
            }

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(searchresults.count)
        return searchresults.count
    }


    func tableCellView(cellSearch: searchResult, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResult", for: indexPath) as! SearchViewCell
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
        
        cell.searchImage.layer.borderWidth = 3
        cell.searchImage.layer.masksToBounds = false
        cell.searchImage.layer.borderColor = colourID.cgColor
        cell.searchImage.layer.cornerRadius = cell.searchImage.frame.height/2
        cell.searchImage.clipsToBounds = true
        
        cell.cellView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .horizontal)
                
        cell.searchTitle.text = cellSearch.Name
        let url = URL(string:API.uploadBase +  cellSearch.Image)
        cell.searchImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                
        
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = searchresults[indexPath.row]
        print(element)
        return tableCellView(cellSearch: element, indexPath: indexPath, tableView: tableView)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = searchresults[indexPath.row]
        self.selectedID = element.ID
        
        if (searchPlayers){
            self.performSegue(withIdentifier: "searchToProfile", sender: self)

        }else{
            self.performSegue(withIdentifier: "searchToTeam", sender: self)

        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "searchToTeam" {
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = selectedID
            
        }else if segue.identifier == "searchToProfile" {
            let yourNextViewController = (segue.destination as! ProfileViewController)
            yourNextViewController.userID = selectedID
            
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
