//
//  TeamViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 11/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import SDWebImage
import Fusuma

class TeamViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate  {

    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var bottombar: UIView!
    
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamPicture: UIImageView!
    
    @IBOutlet weak var viewPlayers: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var postcode: UILabel!
    @IBOutlet weak var hStreet: UILabel!
    @IBOutlet weak var hCity: UILabel!
    @IBOutlet weak var hPostcode: UILabel!
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var scoreResult: UILabel!
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var messageTeam: UIButton!
    @IBOutlet weak var leaveTeam: UIButton!
    @IBOutlet weak var viewMatches: UIButton!
    @IBOutlet weak var editTraining: UIButton!
    
    @IBOutlet var trainingTable: UITableView!
    var teamid:Int!
    var adminid = 0
    var sportid = 0
    
    var trainingArray = [training]()
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func leaveTeam(_ sender: Any) {
        let defaults = UserDefaults.standard

        if (adminid == defaults.integer(forKey: "ID")){
            showAlert("Error", message:"Unable to leave a team you are an admin of", onView: self)
        }else{
            Just.post(
                API.deleteTeamPlayer,
                data: ["key": API.key, "teamID" : teamid, "userID":defaults.integer(forKey: "ID")]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
        
                    if (success == 1){
                        DispatchQueue.main.async { [unowned self] in
                            self.performSegue(withIdentifier: "teamToTeams", sender: nil)
                        }
                        
                        showAlert("Success", message:"Player had been removed", onView: self)
                    } else {
                        showAlert("Error", message:"Failed to remove player", onView: self)
                    }
                    
                }
                
            }
        }
        
    }
    @IBAction func edit(_ sender: Any) {
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        SwiftSpinner.show("Loading Team")
        let defaults = UserDefaults.standard
        
        Just.post(
            API.getTraining,
            data: ["key": API.key, "teamID" : self.teamid]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    self.trainingArray.removeAll()
                    if let items = json["times"].array {
                        for item in items {
                            
                            self.trainingArray.append(training.init(Date: item["tDate"].stringValue, StartTime: item["tStart"].stringValue, EndTime: item["tEnd"].stringValue, Cost: item["Cost"].stringValue, ID: item["ID"].intValue))
                        }
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.trainingTable.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
        }
        
        Just.post(
            API.getTeam,
            data: ["key": API.key, "teamID" : teamid]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                
                print(json)
                if (success == 1){
                    
                    DispatchQueue.main.async { [unowned self] in
                        
                    self.teamName.text = json["team"]["Name"].stringValue
                    
                        let url = URL(string:"http://77.68.84.28/uploads/" +  json["team"]["File"].stringValue)
                        self.teamPicture.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                        
                        self.teamPicture.layer.borderWidth = 0
                        self.teamPicture.layer.masksToBounds = false
                        self.teamPicture.layer.cornerRadius = self.teamPicture.frame.height/2
                        self.teamPicture.clipsToBounds = true

                        self.leagueName.text = json["league"]["Name"].stringValue
                        
                        self.street.text = json["location"]["Street"].stringValue
                        self.city.text = json["location"]["Town"].stringValue
                        self.postcode.text = json["location"]["PostCode"].stringValue
                        
                        self.hStreet.text = json["homeLocation"]["Street"].stringValue
                        self.hCity.text = json["homeLocation"]["Town"].stringValue
                        self.hPostcode.text = json["homeLocation"]["PostCode"].stringValue
                        
                        self.sportid = json["team"]["SportID"].intValue
                        self.adminid = json["team"]["AdminID"].intValue

                        self.scoreResult.text = json["game"]["Score"].stringValue

                    switch json["team"]["SportID"].stringValue {
                    case "5":
                        
                        self.infoView.applyGradient(withColours: [sportColour.netball, sportColour.netballGradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "netball");
                        
                    case "4":
                        self.infoView.applyGradient(withColours: [sportColour.basketball, sportColour.basketballGradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "basketball");
                        
                    case "3":
                        self.infoView.applyGradient(withColours: [sportColour.football11, sportColour.football11Gradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "11aside");
                        
                    case "2":
                        self.infoView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "7aside");
                    case  "1":
                        self.infoView.applyGradient(withColours: [sportColour.football5, sportColour.football5Gradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "fiveasideSM");
                    default:
                        self.infoView.applyGradient(withColours: [sportColour.football7, sportColour.football7Gradient], gradientOrientation: .horizontal)
                        self.sportImage.image = UIImage(named: "7aside");
                    }
                        
                        if (self.adminid == defaults.integer(forKey: "ID")){
                            self.edit.isHidden = false
                            self.editTraining.isHidden = false
                        }else{
                            self.edit.isHidden = true
                            self.editTraining.isHidden = true

                        }
                        
                    SwiftSpinner.hide()

                }
                    

                }else{
                    SwiftSpinner.hide()

                }
                
            }
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        topbar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottombar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        leaveTeam.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        leaveTeam.layer.cornerRadius = 15
        leaveTeam.clipsToBounds = true
        
        viewPlayers.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        viewPlayers.layer.cornerRadius = 15
        viewPlayers.clipsToBounds = true
        
        messageTeam.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        messageTeam.layer.cornerRadius = 15
        messageTeam.clipsToBounds = true
        
        editTraining.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        editTraining.layer.cornerRadius = 15
        editTraining.clipsToBounds = true
        
        infoView.layer.cornerRadius = 15
        infoView.clipsToBounds = true

        viewMatches.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        viewMatches.layer.cornerRadius = 15
        viewMatches.clipsToBounds = true

        //teamToSearch
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "teamToSearch", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = trainingArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Training", for: indexPath) as! editTrainingViewCell
        
        cell.backgroundColor = UIColor(white: 1, alpha: 0)
        cell.cost.text = "£ "+element.cost
        cell.date.text = element.date
        cell.startTime.text = element.startTime
        cell.endTime.text = element.endTime
        
        //cell.cellView.backgroundColor = UIColor(white: 1, alpha: 0)

        
        
        
        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "teamToPlayers" {
            let yourNextViewController = (segue.destination as! TeamMembersViewController)
            yourNextViewController.teamid = teamid
            yourNextViewController.adminid = adminid
            yourNextViewController.sportid = sportid
        }else if segue.identifier == "teamToMatch" {
            let yourNextViewController = (segue.destination as! MatchesViewController)
            yourNextViewController.teamid = teamid
            yourNextViewController.adminid = adminid

        }else if segue.identifier == "teamToEdit" {
            let yourNextViewController = (segue.destination as! EditTeamViewController)
            yourNextViewController.teamID = teamid
            
        }else if segue.identifier == "teamToTraining" {
            let yourNextViewController = (segue.destination as! EditTrainingTimeViewController)
            yourNextViewController.teamID = teamid
            
        }
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }

}

