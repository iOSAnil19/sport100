//
//  MessageThreadViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 07/06/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma

class MessageViewCell: UITableViewCell {
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var messageDate: UILabel!
}

class MessageThreadViewController: UIViewController, FusumaDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var threadTable: UITableView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textArea: UITextView!
    let fusuma = FusumaViewController()
    
    var threadID = 0
    var userID = 0
    var toID = 0
    
    var tableData = [message]()
    var dataDictionary = [String: Any]()


    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)
        
    }
    @IBAction func sendMessage(_ sender: Any) {
        if (textArea.text != ""){
            
            dataDictionary.removeAll()
            dataDictionary["fromID"] = userID
            dataDictionary["toID"] = toID
            dataDictionary["message"] = textArea.text
            dataDictionary["key"] = API.key

            Just.post(
                API.sendMessage,
                data: dataDictionary
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    
                    if (success == 1){
                        
                        self.tableData.removeAll()
                        
                        self.dataDictionary.removeAll()

                        self.dataDictionary["key"] = API.key
                        self.dataDictionary["messageID"] = self.threadID
                        
                        
                        Just.post(
                            API.getMessageThread,
                            data: self.dataDictionary
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                if (success == 1){
                                    if let items = json["messages"].array {
                                        for item in items {
                                            
                                            let newMessage = message.init(message: item["Message"].stringValue, ID: item["ID"].intValue, messageDate: item["Created"].stringValue, fromContact: contact.init(Name: item["From"]["Name"].stringValue, ID: item["From"]["ID"].intValue, Picture: item["From"]["Image"].stringValue), toContact: contact.init(Name: item["To"]["Name"].stringValue, ID: item["To"]["ID"].intValue, Picture: item["To"]["Image"].stringValue))
                                            
                                            self.tableData.append(newMessage)
                                            
                                        }
                                    }
                                    
                                    if self.tableData[0].toContact.ID == self.userID{
                                        self.toID = self.tableData[0].fromContact.ID
                                    }else{
                                        self.toID = self.tableData[0].toContact.ID
                                    }
                                    
                                    DispatchQueue.main.async { [unowned self] in
                                        self.textArea.text = ""
                                        self.textArea.endEditing(true)
                                        self.threadTable.reloadData()
                                    }
                                }
                                
                            }
                        }

                    }
                }
            }

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        userID = defaults.integer(forKey: "ID")

        fusuma.delegate = self
        fusuma.hasVideo = true
        topView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        sendButton.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        sendButton.layer.cornerRadius = 15
        sendButton.clipsToBounds = true
        
        textArea.layer.cornerRadius = 15
        textArea.clipsToBounds = true
        
        threadTable.rowHeight = UITableViewAutomaticDimension
        threadTable.estimatedRowHeight = 140
        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "messageToSearch", sender: nil)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        SwiftSpinner.show("Loading Messages")
        
        dataDictionary["key"] = API.key
        dataDictionary["messageID"] = threadID
        
        Just.post(
            API.getMessageThread,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                
                if (success == 1){
                    if let items = json["messages"].array {
                        for item in items {
                            
                            let newMessage = message.init(message: item["Message"].stringValue, ID: item["ID"].intValue, messageDate: item["Created"].stringValue, fromContact: contact.init(Name: item["From"]["Name"].stringValue, ID: item["From"]["ID"].intValue, Picture: item["From"]["Image"].stringValue), toContact: contact.init(Name: item["To"]["Name"].stringValue, ID: item["To"]["ID"].intValue, Picture: item["To"]["Image"].stringValue))
                            
                            self.tableData.append(newMessage)
                            
                        }
                    }
                    
                    if self.tableData[0].toContact.ID == self.userID{
                        self.toID = self.tableData[0].fromContact.ID
                    }else{
                        self.toID = self.tableData[0].toContact.ID
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.threadTable.reloadData()
                    }
                }
                
            }
        }
        SwiftSpinner.hide()

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }
    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = tableData[indexPath.row]
        
        print(element.fromContact.ID)
        print(userID)
        
        var cell: MessageViewCell
        if element.fromContact.ID == userID{
             cell = tableView.dequeueReusableCell(withIdentifier: "ownMessage", for: indexPath) as! MessageViewCell
        }else{
             cell = tableView.dequeueReusableCell(withIdentifier: "otherMessage", for: indexPath) as! MessageViewCell
        }
        
        
        let colourID = UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0)
        
        cell.contactImage.layer.borderWidth = 2
        cell.contactImage.layer.masksToBounds = false
        cell.contactImage.layer.borderColor = colourID.cgColor
        cell.contactImage.layer.cornerRadius = cell.contactImage.frame.height/2
        cell.contactImage.clipsToBounds = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: element.messageDate)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        dateFormatter1.doesRelativeDateFormatting = true
        
        let nowString = dateFormatter1.string(from: date!)
        
        cell.messageDate.text = nowString
        cell.message.text = element.message

        let url = URL(string:API.uploadBase +  element.fromContact.picture)
        cell.contactImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = tableData[indexPath.row]
        
        
    }

    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }


}
