//
//  EditHistoryViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 11/07/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import SwipeCellKit
import ActionSheetPicker_3_0

class EditHistoryViewCell: SwipeTableViewCell {
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    @IBOutlet weak var leftLabel: UILabel!

}

class EditHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    
    var tableData = [history]()
    
    var sports: [String] = []
    var leagues: [String] = []
    
    var dataDictionary = [String: Any]()
    var userid = 0
    
    @IBOutlet weak var threadTable: UITableView!

    @IBOutlet weak var create: UIButton!
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var sportButton: UIButton!
    @IBOutlet weak var leagueButton: UIButton!
    @IBOutlet weak var league: UITextField!
    @IBOutlet weak var club: UITextField!
    @IBOutlet weak var joinDate: UIButton!
    @IBOutlet weak var leftDate: UIButton!
    @IBOutlet weak var levelButton: UIButton!
    

    var sport = ""
    var leagueString = ""
    var levelString = ""
    
    var lday = ""
    var lmonth = ""
    var lyear = ""
    
    var jday = ""
    var jmonth = ""
    var jyear = ""

    var selectedID = 0
    
    var levelArray = ["School","College","University","Work","Recreational","Amateur","Semi Professional","Professional"]
    
    @IBAction func selectLevel(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Level", rows: levelArray, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.levelButton.setTitle("Set Level: " + picked, for: UIControlState.normal)
                self.levelString = picked
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        

    }
    @IBAction func selectLeftDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Left Date:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let date : Date = value as! Date
            
            let calendar = Calendar.current
            
            let dateDay = calendar.component(.day, from: date)
            let dateMonth = calendar.component(.month, from: date)
            let dateYear = calendar.component(.year, from: date)
            
            self.leftDate.setTitle("Date Left: " + String(dateDay) + "-" + String(dateMonth) + "-" + String(dateYear), for: UIControlState.normal)
            
            self.lday = String(dateDay)
            self.lmonth = String(dateMonth)
            self.lyear = String(dateYear)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as! UIButton).superview!.superview)
        
        datePicker?.show()
    }
    @IBAction func selectJoinDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Join Date:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let date : Date = value as! Date
            
            let calendar = Calendar.current
            
            let dateDay = calendar.component(.day, from: date)
            let dateMonth = calendar.component(.month, from: date)
            let dateYear = calendar.component(.year, from: date)
            
            self.joinDate.setTitle("Date Joined: " + String(dateDay) + "-" + String(dateMonth) + "-" + String(dateYear), for: UIControlState.normal)
            
            self.jday = String(dateDay)
            self.jmonth = String(dateMonth)
            self.jyear = String(dateYear)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as! UIButton).superview!.superview)
        
        datePicker?.show()

    }
    @IBAction func selectLeague(_ sender: Any) {
        if (sport != ""){
            ActionSheetStringPicker.show(withTitle: "Select the Team League", rows: leagues, initialSelection: 0, doneBlock: {
                picker, value, index in
                
                if let picked = index as? String {
                    
                    self.league.text = picked
                    self.leagueString = picked
                    
                } else {
                    print("Error") // Was not a string
                }
                
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        }else {
            showAlert("Error", message:"Select Sport first.", onView: self)
        }

    }
    @IBAction func selectSport(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select the Sport", rows: sports, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.sportButton.setTitle("Sport: " + picked, for: UIControlState.normal)
                self.sport = picked
                
                Just.post(
                    API.getLeagues,
                    data: ["key": API.key, "sport":picked]
                ) { r in
                    if r.ok {
                        let json = JSON(data: r.content!);
                        let success = json["success"].intValue
                        
                        print(json)
                        
                        if (success == 1){
                            if let items = json["leagues"].array {
                                for item in items {
                                    let sport = item["Name"].stringValue
                                    
                                    self.leagues.append(sport)
                                }
                            }
                            
                        }else{
                            
                        }
                        
                    }
                }
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func saveNew(_ sender: Any) {
        
        let levelText = levelString
        let clubText = club.text ?? ""
        let leagueText = league.text ?? ""
        
        if (levelText == ""){
            showAlert("Error", message:"Please enter your level", onView: self)
            return
        }
        
        if (clubText == ""){
            showAlert("Error", message:"Please enter your club", onView: self)
            return
        }
        
        if (clubText.count > 20){
            showAlert("Error", message:"Club can't be more than 20 characters long", onView: self)
            return
        }
        
        if (leagueText == ""){
            showAlert("Error", message:"Please enter your league", onView: self)
            return
        }
        
        if (leagueText.count > 20){
            showAlert("Error", message:"League can't be more than 20 characters long", onView: self)
            return
        }
        
        if (sport == ""){
            showAlert("Error", message:"Please enter your sport", onView: self)
            return
        }

        let start = jyear+"-"+jmonth+"-"+jday
        let end = lyear+"-"+lmonth+"-"+lday

        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        dataDictionary["level"] = levelText
        dataDictionary["club"] = clubText
        dataDictionary["joined"] = start
        dataDictionary["left"] = end
        dataDictionary["league"] = leagueText
        dataDictionary["sport"] = sport
        dataDictionary["selectedID"] = selectedID
        
        Just.post(
            API.saveHistory,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                self.editView.isHidden = true

                print(json)
                
                if (success == 1){
                    
                    Just.post(
                        API.getHistory,
                        data: self.dataDictionary
                    ) { r in
                        if r.ok {
                            let json = JSON(data: r.content!);
                            let success = json["success"].intValue
                            
                            print(json)
                            
                            
                            if (success == 1){
                                self.tableData.removeAll()
                                
                                if let items = json["history"].array {
                                    for item in items {
                                        let newHistory = history.init(ID:item["ID"].intValue , sport: item["Sportsname"].stringValue, level: item["Level"].stringValue, club: item["Club"].stringValue, joined: item["Joined"].stringValue, left: item["hLeft"].stringValue, league: item["LeagueName"].stringValue)
                                        
                                        self.tableData.append(newHistory)
                                    }
                                }
                                DispatchQueue.main.async { [unowned self] in
                                    
                                    self.threadTable.reloadData()
                                }
                            }
                            SwiftSpinner.hide()
                            
                        }
                    }
                }
                SwiftSpinner.hide()
                
            }
        }
        
    }
    @IBAction func createNew(_ sender: Any) {
        editView.isHidden = false
        selectedID = 0
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        create.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        create.layer.cornerRadius = 15
        create.clipsToBounds = true
        
        editView.layer.cornerRadius = 15
        editView.clipsToBounds = true
        editView.isHidden = true
        
        sportButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        sportButton.layer.cornerRadius = 15
        sportButton.clipsToBounds = true
        
        leagueButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        leagueButton.layer.cornerRadius = 15
        leagueButton.clipsToBounds = true
        
        levelButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        levelButton.layer.cornerRadius = 15
        levelButton.clipsToBounds = true

        save.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        save.layer.cornerRadius = 15
        save.clipsToBounds = true
        
        joinDate.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        joinDate.layer.cornerRadius = 15
        joinDate.clipsToBounds = true
        
        leftDate.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        leftDate.layer.cornerRadius = 15
        leftDate.clipsToBounds = true
        
        Just.post(
            API.getAllSports,
            data: ["key": API.key]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["sports"].array {
                        for item in items {
                            let sport = item["Name"].stringValue
                            
                            self.sports.append(sport)
                        }
                    }
                    
                }else{
                    
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        SwiftSpinner.show("Loading History")
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userid
        
        Just.post(
            API.getHistory,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["history"].array {
                        for item in items {
                            let newHistory = history.init(ID:item["ID"].intValue , sport: item["Sportsname"].stringValue, level: item["Level"].stringValue, club: item["Club"].stringValue, joined: item["Joined"].stringValue, left: item["hLeft"].stringValue, league: item["LeagueName"].stringValue)
                            
                            self.tableData.append(newHistory)
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.threadTable.reloadData()
                    }
                }
                SwiftSpinner.hide()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = tableData[indexPath.row]
        
        editView.isHidden = false
        selectedID = element.ID
        
        levelButton.setTitle("Set Level: " + element.league, for: UIControlState.normal)
        
        levelString = element.league
        
        club.text = element.club
        league.text = element.league
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = tableData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "history", for: indexPath) as! EditHistoryViewCell
        cell.clubLabel.text = element.club
        cell.joinedLabel.text = element.joined
        cell.leftLabel.text = element.left
        cell.levelLabel.text = element.level
        
        cell.delegate = self
        
        return cell
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            let element = self.tableData[indexPath.row]
            
            Just.post(
                API.deleteHistory,
                data: ["key": API.key, "historyID" : element.ID ]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    
                    if (success == 1){
                        Just.post(
                            API.getHistory,
                            data: self.dataDictionary
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                print(json)
                                
                                
                                if (success == 1){
                                    self.tableData.removeAll()

                                    if let items = json["history"].array {
                                        for item in items {
                                            let newHistory = history.init(ID:item["ID"].intValue , sport: item["Sportsname"].stringValue, level: item["Level"].stringValue, club: item["Club"].stringValue, joined: item["Joined"].stringValue, left: item["hLeft"].stringValue, league: item["LeagueName"].stringValue)
                                            
                                            self.tableData.append(newHistory)
                                        }
                                    }
                                    DispatchQueue.main.async { [unowned self] in
                                        
                                        self.threadTable.reloadData()
                                    }
                                }
                                SwiftSpinner.hide()
                                
                            }
                        }
                    }else{
                        showAlert("Error", message: "Failed to remove player", onView: self)
                    }
                    
                }
                
            }
        }
        
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction]
    }


}
