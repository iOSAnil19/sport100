//
//  LoginViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 20/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import FBSDKLoginKit

//com.sport100.app

class LoginViewController: UIViewController  {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------

    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var register: UIButton!
    
    @IBOutlet weak var fbloginButton: UIButton!
    
    var emailAddress = ""
    var firstName = ""
    var lastName = ""
    var fbid = ""
    
    let socialManager = SocialManager()
    
    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            ], gradientOrientation: .vertical)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let defaults = UserDefaults.standard
        
        let isSetUp = defaults.bool(forKey: "logged")
        let isFirst = defaults.bool(forKey: "first")
        
        if (isSetUp){
            if (isFirst){
                self.goToEditProfile()
            }else{
                self.goToFeedView()
            }
        }
        
        self.email.text = defaults.string(forKey: "email")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func buttonClicked_ForgotPassword(_ sender:UIButton) {
        if let forgottenPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgottenPasswordViewController") {
            self.present(forgottenPasswordViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonClicked_Register(_ sender:UIButton) {
        if let registerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") {
            self.navigationController?.pushViewController(registerViewController, animated: true)
        }
    }
    
    @IBAction func buttonClicked_Facebook(_ sender: Any) {
        
        socialManager.getFaceboookLoginInfoForView(self) { (response, error) in
            self.handleReponseAndError(response,error)
        }
    }
    
    @IBAction func buttonClicked_Login(_ sender: Any) {
        if (password.text == ""){
            showAlert("Error", message: "Please enter your password.", withAction: nil, with: true, andTitle: "Close", onView: self)
            return
        }
        if (email.text == ""){
            showAlert("Error", message: "Please enter your Email.", withAction: nil, with: true, andTitle: "Close", onView: self)
            return
        }
        
        if (!isValidEmail(email.text!)){
            showAlert("Error", message: "Email is not valid.", withAction: nil, with: true, andTitle: "Close", onView: self)
            return
        }
        
        SwiftSpinner.show("Logging in...")
        
        Just.post(
            API.login,
            data: ["key": API.key, "email": email.text!, "password": password.text! ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                let first = json["first"].intValue
                
                
                if (success == 1){
                    let defaults = UserDefaults.standard
                    defaults.setValue(true, forKey: "logged")
                    defaults.setValue(self.email.text!, forKey: "email")
                    defaults.setValue(json["UserID"].intValue, forKey: "ID")
                    
                    if (first == 1){
                        self.goToEditProfile()
                        defaults.setValue(true, forKey: "first")
                        
                    } else{
                        self.goToFeedView()
                        defaults.setValue(false, forKey: "first")
                    }
                    
                    defaults.synchronize()
                    SwiftSpinner.hide()
                    
                } else {
                    showAlert("Error", message: json["error"].stringValue, withAction: nil, with: true, andTitle: "Close", onView: self)
                    SwiftSpinner.hide()
                    return
                }
            }
        }
    }
    
    //MARK: ---------- Private Methods --------------
    
    // Handle the response after Facebook and Google login
    private func handleReponseAndError(_ result:Any?, _ error:Error?) {
        
        if error == nil {
            if let user = result as? SocialUser {
                self.loginWithSocialUser(user)
                
            } else {
                showAlert("", message: "\(String(describing: result ?? SOMETHING_WRONG))", onView: self)
            }
        } else {
            if let code = error?.code, code == 1 {
                showAlert("", message:"The user canceled the sign-in flow", onView: self)
            }
            showAlert("", message: error?.localizedDescription ?? SOMETHING_WRONG, onView: self)
        }
    }
    
    func loginWithSocialUser(_ user:SocialUser){
                    
        SwiftSpinner.show("Loging in...")
        
        self.emailAddress = user.email ?? ""
        self.firstName = user.firstName ?? ""
        self.lastName = user.lastName ?? ""
        self.fbid = user.id ?? ""
        
        Just.post(
            API.fblogin,
            data: ["key": API.key, "fb": self.fbid ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                let first = json["first"].intValue

                if (success == 1){
                    let defaults = UserDefaults.standard
                    defaults.setValue(true, forKey: "logged")
                    
                    defaults.setValue(json["UserID"].intValue, forKey: "ID")
                    
                    if (first == 1){
                        self.goToEditProfile()
                        defaults.setValue(true, forKey: "first")
                        
                    }else{
                        self.goToFeedView()
                        defaults.setValue(false, forKey: "first")
                        
                    }
                    defaults.synchronize()
                    SwiftSpinner.hide()
                } else {
                    
                    let registerAction = UIAlertAction(title: "Register", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in
                            if let registerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
                                registerViewController.emailText = self.emailAddress
                                registerViewController.firstNameText = self.firstName
                                registerViewController.lastNameText = self.lastName
                                registerViewController.fbid = self.fbid
                            self.navigationController?.pushViewController(registerViewController, animated: true)}
                    })
                    
                    showAlert("Not Registered", message: "You have not registered yet", withAction: registerAction, with: true, andTitle: "Close", onView: self)
                    SwiftSpinner.hide()
                    return
                }
            }
        }
    }
    
    private func goToFeedView() {
        
        if let feedViewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedViewController") {
            self.navigationController?.pushViewController(feedViewController, animated: true)
        }
    }
    
    private func goToEditProfile() {
        
        if let editProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") {
            self.navigationController?.pushViewController(editProfileViewController, animated: true)
        }
    }
}
