//
//  SettingsVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 30/01/18.
//  Copyright © 2018 Zachary Powell. All rights reserved.
//

import UIKit
import MessageUI

class SettingsVC: UIViewController {
    
    //MARK: ----------- VIEW LIFE CYCLE ------------

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)        ], gradientOrientation: .vertical)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------

    @IBAction func buttonClicked_Logout(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.setValue(false, forKey: "logged")
        defaults.setValue(true, forKey: "first")
        
        defaults.setValue(0, forKey: "ID")
        defaults.synchronize()
        
        if let navVC = self.storyboard?.instantiateViewController(withIdentifier: "RootNavigation") {
            APPDELEGATE.window?.rootViewController = navVC
        }
    }
    
    @IBAction func buttonClicked_Rate(_ sender: Any) {
        if let url  = URL(string: "itms-apps://itunes.apple.com/app/id1024941703") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func buttonClicked_SendFeedBack(_ sender: Any) {
        self.sendMailFor("Feedback")
    }
    
    @IBAction func buttonClicked_SendBugReport(_ sender: Any) {
        self.sendMailFor("Bug report")
    }
    
    //MARK: ----------- PRIVATE METHODS ------------
    
    private func sendMailFor(_ subject:String) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["feedback@sports100.com"])
            mail.setSubject("Sports100 \(subject)")
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
}

extension SettingsVC:MFMailComposeViewControllerDelegate {
    
}
