//
//  ProfileVC.swift
//  Sport100
//
//  Created by Vivan Raghuvanshi on 30/01/18.
//  Copyright © 2018 Zachary Powell. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Settings(_ sender: UIButton) {
        if let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC"){
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }

}
