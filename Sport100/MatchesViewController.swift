//
//  MatchesViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 25/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma

class matchViewCell: UITableViewCell {
    @IBOutlet weak var teams: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class MatchesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate{

    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var matches: UITableView!
    @IBOutlet weak var addmatch: UIButton!
    
    @IBOutlet weak var finalScore: UITextField!
    @IBOutlet weak var teamPlaying: UITextField!
    @IBOutlet weak var addView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var time: UIDatePicker!
    var teamid:Int!
    var adminid = 0
    var matchID = 0
    var userID = 0
    
    var matchs = [match]()
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    @IBAction func close(_ sender: Any) {
        addView.isHidden = true
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        if(Date().timeIntervalSince(time.date) < 0){
            finalScore.isHidden = true
        }else{
            finalScore.isHidden = false
        }
    }
    
    @IBAction func saveMatch(_ sender: Any) {
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:MM:SS"
        let dateString = dayTimePeriodFormatter.string(from: time.date)

        SwiftSpinner.show("Saving Team")

        Just.post(
            API.saveMatch,
            data: ["key": API.key, "teamID" : teamid, "matchID":matchID, "score":finalScore.text ?? "", "teamPlaying":teamPlaying.text ?? "", "date":dateString]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(self.teamid)
                print(json)
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.finalScore.text = ""
                        self.teamPlaying.text = ""
                        self.addView.isHidden = true
                        self.matchID=0
                        
                        SwiftSpinner.show("Loading Matches")

                        Just.post(
                            API.getMatches,
                            data: ["key": API.key, "teamID" : self.teamid]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                         
                                
                                SwiftSpinner.hide()
                                
                                if (success == 1){
                                    self.matchs.removeAll()
                                    if let items = json["games"].array {
                                        for item in items {
                                            self.matchs.append(match.init(Teams: json["team"]["Name"].stringValue + " v " + item["Away"].stringValue, Date: item["mDate"].stringValue, Score: item["Score"].stringValue, ID: item["ID"].intValue))
                                            
                                        }
                                    }
                                    
                                    DispatchQueue.main.async { [unowned self] in
                                        self.matches.reloadData()
                                    }
                                    
                                }else{
                                    
                                }
                                
                            }
                        }
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }

                }
                
            }
        }
    }
    
    @IBAction func add(_ sender: Any) {
        addView.isHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        topBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        addView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        addView.layer.cornerRadius = 15
        addView.clipsToBounds = true
        addView.isHidden = true
        
        closeButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        closeButton.layer.cornerRadius = 15
        closeButton.clipsToBounds = true
        
        save.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        save.layer.cornerRadius = 15
        save.clipsToBounds = true
        
        addmatch.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        addmatch.layer.cornerRadius = 15
        addmatch.clipsToBounds = true

        let defaults = UserDefaults.standard
        userID = defaults.integer(forKey: "ID")
        
        if (adminid == userID){
            addmatch.isHidden = false
        }else{
            addmatch.isHidden = true
        }

        
        SwiftSpinner.show("Loading Matches")
        
        Just.post(
            API.getMatches,
            data: ["key": API.key, "teamID" : teamid]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(self.teamid)
                print(json)
                
                SwiftSpinner.hide()
                
                if (success == 1){
                    if let items = json["games"].array {
                        for item in items {
                            self.matchs.append(match.init(Teams: json["team"]["Name"].stringValue + " v " + item["Away"].stringValue, Date: item["mDate"].stringValue, Score: item["Score"].stringValue, ID: item["ID"].intValue))

                        }
                    }
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.matches.reloadData()
                    }
                    
                }else{
                    
                }
                
            }
        }

        //matchToSearch
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "matchToSearch", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "matchToTeam" {
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = teamid
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = matchs[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "match", for: indexPath) as! matchViewCell
        
        cell.backgroundColor = UIColor(white: 1, alpha: 0)
        cell.teams.text = element.teams
        cell.date.text = element.date
        cell.score.text = element.score
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        cell.cellView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var element = matchs[indexPath.row]
        
        if (adminid == userID){
            let myDate = element.date.toDateFormattedWith(format: "YYYY-MM-dd HH:MM:SS")
            
            let parts = element.teams.components(separatedBy: " v ")

            
            self.time.date = myDate
            self.matchID = element.ID
            self.teamPlaying.text = parts[1]
            self.finalScore.text = element.score
            addView.isHidden = false
        }
        
        //selectedPlayer = element
        //self.performSegue(withIdentifier: "playersToProfile", sender: self)
        
    }


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
}
