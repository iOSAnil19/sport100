//
//  CompleteRegisterViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 20/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit

class CompleteRegisterViewController: UIViewController {

    @IBOutlet weak var login: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        login.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
