//
//  SendNewMessageViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 02/06/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma
import ActionSheetPicker_3_0

class SendNewMessageViewController: UIViewController, FusumaDelegate, UITextViewDelegate {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------
    
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    let fusuma = FusumaViewController()

    @IBOutlet weak var sendbutton: UIButton!
    @IBOutlet weak var tobar: UIButton!
    @IBOutlet weak var message: UITextView!
    
    var contacts: [String] = []
    var contactIDs: [Int] = []

    var pickedID = 0
    var name = ""
    
    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fusuma.delegate = self
        fusuma.hasVideo = true
        topView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        message.layer.cornerRadius = 15
        message.clipsToBounds = true
        
        tobar.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        tobar.layer.cornerRadius = 15
        tobar.clipsToBounds = true
        
        sendbutton.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        sendbutton.layer.cornerRadius = 15
        sendbutton.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        let defaults = UserDefaults.standard
        var userid = defaults.integer(forKey: "ID")
        
        if (name != ""){
            self.contacts.append(name)
            self.contactIDs.append(pickedID)
            
            self.tobar.setTitle("To: " + name, for: UIControlState.normal)
            
        }else{
            var dataDictionary = [String: Any]()
            
            dataDictionary["key"] = API.key
            dataDictionary["userID"] = userid
            
            
            Just.post(
                API.contacts,
                data: dataDictionary
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    SwiftSpinner.hide()
                    
                    if (success == 1){
                        
                        if let items = json["result"].array {
                            for item in items {
                                self.contacts.append(item["FirstName"].stringValue + " " + item["LastName"].stringValue)
                                self.contactIDs.append(item["OtherUserID"].intValue)
                            }
                        }
                        
                    }
                }
            }
        }
        
        
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
        
        self.message.delegate = self
    }
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func uploadMedia(_ sender: Any) {
        
        self.present(fusuma, animated: true, completion: nil)
    }
    
    @IBAction func setTo(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select Contact", rows: contacts, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.tobar.setTitle("To: " + picked, for: UIControlState.normal)
                let index = self.contacts.index(of: picked)
                self.pickedID = self.contactIDs[index!]
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func send(_ sender: Any) {
        
        if (message.text == ""){
            sendAlert(message: "Please enter your message.")
            return
        }
        
        if (pickedID == 0){
            sendAlert(message: "Please select a contact.")
            return
        }
        
        let defaults = UserDefaults.standard
        var userid = defaults.integer(forKey: "ID")
        
        var dataDictionary = [String: Any]()
        
        dataDictionary["key"] = API.key
        dataDictionary["fromID"] = userid
        dataDictionary["toID"] = pickedID
        dataDictionary["message"] = message.text

        
        Just.post(
            API.sendMessage,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.performSegue(withIdentifier: "newToMessages", sender: nil)
                    }
                                        
                }
            }
        }
    }
    
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "messageToSearch", sender: nil)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }

    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }

    func sendAlert(message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
