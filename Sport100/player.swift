//
//  player.swift
//  Sport100
//
//  Created by Zachary Powell on 20/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation
import UIKit

class player {
    var name: String
    var ID: Int = 0
    var number: Int = 0
    
    init (Name: String, ID: Int, number: Int) {
        self.name = Name
        self.ID = ID
        self.number = number
    }
    
    init(){
        name = ""
        
    }
}
