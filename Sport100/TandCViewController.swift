//
//  TandCViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 22/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit

class TandCViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    lazy var activityIndicator:UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.center = self.view.center
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

//        topBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
//
//            ], gradientOrientation: .vertical)

        webview.loadRequest(URLRequest(url: URL(string: "http://sports100.com/app/terms.html")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TandCViewController:UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.view.addSubview(activityIndicator)
        self.view.bringSubview(toFront: activityIndicator)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.removeFromSuperview()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator.removeFromSuperview()
    }
}
