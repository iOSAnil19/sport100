//
//  SearchViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 03/05/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Fusuma

class SearchViewController: UIViewController, FusumaDelegate {
    @IBOutlet weak var searchBar: UIView!

    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var bottomBar: UIView!
    
    @IBOutlet weak var playersFilter: UIButton!
    @IBOutlet weak var teamsFilter: UIButton!
    
    @IBOutlet weak var searchButton: UIButton!
    
    let fusuma = FusumaViewController()
    
    @IBOutlet weak var football11Button: UIButton!
    @IBOutlet weak var football7Button: UIButton!
    @IBOutlet weak var football5Button: UIButton!
    @IBOutlet weak var basketballButton: UIButton!
    @IBOutlet weak var netballButton: UIButton!
    var searchPlayers = true
    var searchNetball = true
    var searchBasketball = true
    var searchfootball5 = true
    var searchfootball7 = true
    var searchfootball11 = true

    @IBAction func basketballClick(_ sender: Any) {
        if (searchBasketball){
            searchBasketball = false
            basketballButton.setImage(UIImage(named: "basketball-gray.png"), for: UIControlState.normal)
        }else{
            searchBasketball = true
            basketballButton.setImage(UIImage(named: "basketball-colour.png"), for: UIControlState.normal)
        }
    }
    @IBAction func netballClick(_ sender: Any) {
        if (searchNetball){
            searchNetball = false
            netballButton.setImage(UIImage(named: "netball-gray.png"), for: UIControlState.normal)
        }else{
            searchNetball = true
            netballButton.setImage(UIImage(named: "netball-colour.png"), for: UIControlState.normal)
        }
    }
    @IBAction func football5Click(_ sender: Any) {
        if (searchfootball5){
            searchfootball5 = false
            football5Button.setImage(UIImage(named: "Football5-gray.png"), for: UIControlState.normal)
        }else{
            searchfootball5 = true
            football5Button.setImage(UIImage(named: "Football5-colour.png"), for: UIControlState.normal)
        }
    }
    @IBAction func football7Click(_ sender: Any) {
        if (searchfootball7){
            searchfootball7 = false
            football7Button.setImage(UIImage(named: "football7-gray.png"), for: UIControlState.normal)
        }else{
            searchfootball7 = true
            football7Button.setImage(UIImage(named: "football7-colour.png"), for: UIControlState.normal)
        }
    }
    @IBAction func football11Clicked(_ sender: Any) {
        if (searchfootball11){
            searchfootball11 = false
            football11Button.setImage(UIImage(named: "football11-gray.png"), for: UIControlState.normal)
        }else{
            searchfootball11 = true
            football11Button.setImage(UIImage(named: "football11-colour.png"), for: UIControlState.normal)
        }
    }
    @IBAction func search(_ sender: Any) {
        
        if (searchText.text == ""){
            showAlert("Error", message:"Please enter a search term.", onView: self)
            return
        }
        
        self.performSegue(withIdentifier: "searchToResult", sender: self)

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "searchToResult"){
            let yourNextViewController = (segue.destination as! SearchResultViewController)
            yourNextViewController.searchPlayers = searchPlayers
            yourNextViewController.searchNetball = searchNetball
            yourNextViewController.searchBasketball = searchBasketball
            yourNextViewController.searchfootball5 = searchfootball5
            yourNextViewController.searchfootball7 = searchfootball7
            yourNextViewController.searchfootball11 = searchfootball11
            yourNextViewController.searchText = searchText.text!
        }
    }
    
    @IBAction func playerSearchClick(_ sender: Any) {
        if (!searchPlayers){
            searchPlayers = true
            
            print("CLICKED")
            
            playersFilter.isSelected = true
            teamsFilter.isSelected = false
        }
    }
    
    @IBAction func teamSearchClick(_ sender: Any) {
        if (searchPlayers){
            searchPlayers = false
            print("HIT")
            
            teamsFilter.isSelected = true
            playersFilter.isSelected = false
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func uploadMedi(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fusuma.delegate = self
        fusuma.hasVideo = true
        topBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        bottomBar.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        
        searchButton.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)], gradientOrientation: .vertical)
        searchButton.layer.cornerRadius = 15
        searchButton.clipsToBounds = true
        
        playersFilter.setBackgroundColor(color: UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), forState: .normal)
        teamsFilter.setBackgroundColor(color: UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), forState: .normal)
        
        playersFilter.setBackgroundColor(color: UIColor(red:0.34, green:0.54, blue:0.91, alpha:1.0), forState: .selected)
        teamsFilter.setBackgroundColor(color: UIColor(red:0.34, green:0.54, blue:0.91, alpha:1.0), forState: .selected)

        playersFilter.isSelected = true
        teamsFilter.isSelected = false
        // Do any additional setup after loading the view.
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
        
        self.searchText.delegate = self;
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }

}
