//
//  team.swift
//  Sport100
//
//  Created by Zachary Powell on 19/03/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

import UIKit

class team {
    var name: String
    var ID: Int = 0
    var sportID: Int = 0
    
    init (Name: String, ID: Int, sportID: Int) {
        self.name = Name
        self.ID = ID
        self.sportID = sportID
    }
    
    init(){
        name = ""
        
    }
}
