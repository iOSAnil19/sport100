//
//  EditProfileViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 28/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ImagePicker
import ActionSheetPicker_3_0
import LocationPickerViewController
import CoreLocation
import MapKit

class EditProfileViewController: UIViewController, ImagePickerDelegate {

    @IBOutlet weak var nickname: UITextField!
    @IBOutlet weak var primarySport: UIButton!
    @IBOutlet weak var position: UIButton!
    @IBOutlet weak var position2: UIButton!
    @IBOutlet weak var secondaryPosition: UIButton!
    @IBOutlet weak var secondaryPosition2: UIButton!
    @IBOutlet weak var availablity: UIButton!
    @IBOutlet weak var times: UIButton!
    @IBOutlet weak var dbs: UITextField!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var strongestButton: UIButton!
    
    
    @IBOutlet weak var heightButton: UIButton!
    @IBOutlet weak var weightButton: UIButton!
    
    @IBOutlet weak var nationalityButton: UIButton!
    
    @IBOutlet weak var secondarySport: UIButton!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var monAM: UISwitch!
    @IBOutlet weak var monPM: UISwitch!
    @IBOutlet weak var TuesAM: UISwitch!
    @IBOutlet weak var tuesPM: UISwitch!
    @IBOutlet weak var wedAM: UISwitch!
    @IBOutlet weak var wedPM: UISwitch!
    @IBOutlet weak var thursAM: UISwitch!
    @IBOutlet weak var thursPM: UISwitch!
    @IBOutlet weak var friAM: UISwitch!
    @IBOutlet weak var friPM: UISwitch!
    @IBOutlet weak var satAM: UISwitch!
    @IBOutlet weak var satPM: UISwitch!
    @IBOutlet weak var sunAM: UISwitch!
    @IBOutlet weak var sunPM: UISwitch!
    
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var done: UIButton!
    var nicknameText = ""
    var primarySportText = ""
    var secondarySportText = ""

    var dbsText = ""
    var availabilityText = "Available"
    var positionText = ""
    var position2Text = ""
    var secondarypositionText = ""
    var secondaryposition2Text = ""
    var heightText = ""
    var weightText = ""
    var nationalityText = ""
    
    var street = ""
    var city = ""
    var postcode = ""
    
    var strongest = ""
    
    var profilePictureChanged = false
    var sports: [String] = []
    var sport1Positions: [String] = []
    var sport2Positions: [String] = []

    var sportid: [Int] = []

    var sport1Positionsid: [Int] = []
    var sport2Positionsid: [Int] = []
    
    var countries: [String] = []
    var strongestArray = ["left foot", "right foot", "left and right foot", "left hand", "right hand", "left and right hand"]
    
    var heightArray = ["100", "101", "102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207","208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","247","248","249","250","251","252","253","254","255","256","257","258","259","260","261","262","263","264","265","266","267","268","269","270"]
    
    var weightArray = ["50", "51", "52", "53","54", "55","56", "57","58", "59","60", "61","62", "63","64", "65","66", "67","68", "69","70", "71","72", "73","74", "75","76", "77","78", "79","80", "81","82", "83","84", "85","86", "87","88", "89","90", "91","92", "93","94", "95","96", "97","98", "99", "100", "101", "102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144","145","146","147","148","149","150","151","152","153","154","155","156","157","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174","175","176","177","178","179","180","181","182","183","184","185","186","187","188","189","190","191","192","193","194","195","196","197","198","199","200","201","202","203","204","205","206","207","208","209","210","211","212","213","214","215","216","217","218","219","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","247","248","249","250","251","252","253","254","255","256","257","258","259","260","261","262","263","264","265","266","267","268","269","270"]

    var sport1PrimaryPosistion = 0
    var sport1SecondaryPosistion = 0
    var sport2PrimaryPosistion = 0
    var sport2SecondaryPosistion = 0
    
    @IBAction func selectWeight(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Weight (in KG)", rows: weightArray, initialSelection: 100, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.weightButton.setTitle("Weight: " + picked + "kg", for: UIControlState.normal)
                self.weightText = picked
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }

    @IBAction func selectHeight(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Height (in CM)", rows: heightArray, initialSelection: 70, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.heightButton.setTitle("Height: " + picked + "cm", for: UIControlState.normal)
                self.heightText = picked
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }

    @IBAction func selectStrongest(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Primary Hand/Foot", rows: strongestArray, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.strongestButton.setTitle("Primary Hand/Foot: " + picked, for: UIControlState.normal)
                self.strongest = picked
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    @IBAction func selectPosition4(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Secondary Position", rows: sport2Positions, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.secondaryPosition2.setTitle("Secondary Position: " + picked, for: UIControlState.normal)
                let positionindex = self.sport2Positions.index(of: picked)
                self.sport2SecondaryPosistion = self.sport2Positionsid[positionindex!]
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    @IBAction func selectPosition3(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Primary Position", rows: sport2Positions, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.secondaryPosition.setTitle("Primary Position: " + picked, for: UIControlState.normal)
                let positionindex = self.sport2Positions.index(of: picked)
                self.sport2PrimaryPosistion = self.sport2Positionsid[positionindex!]
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    @IBAction func selectPosition2(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Secondary Position", rows: sport1Positions, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.position2.setTitle("Secondary Position: " + picked, for: UIControlState.normal)
                let positionindex = self.sport1Positions.index(of: picked)
                self.sport1SecondaryPosistion = self.sport1Positionsid[positionindex!]
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func selectPosition1(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your Primary Position", rows: sport1Positions, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.position.setTitle("Primary Position: " + picked, for: UIControlState.normal)
                let positionindex = self.sport1Positions.index(of: picked)
                self.sport1PrimaryPosistion = self.sport1Positionsid[positionindex!]
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    @IBAction func selectSecondarySport(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your second sport", rows: sports, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.secondarySport.setTitle("Secondary Sport: " + picked, for: UIControlState.normal)
                self.secondarySportText = picked
                
                let sportindex = self.sports.index(of: picked)
                let sportid = self.sportid[sportindex!]
                
                Just.post(
                    API.getPositions,
                    data: ["key": API.key, "sportid":sportid]
                ) { r in
                    if r.ok {
                        let json = JSON(data: r.content!);
                        let success = json["success"].intValue
                        
                        print(json)
                        
                        self.sport2Positions.removeAll()
                        
                        if (success == 1){
                            if let items = json["positions"].array {
                                for item in items {
                                    let position = item["Position"].stringValue
                                    let positionid = item["ID"].intValue
                                    self.sport2Positions.append(position)
                                    self.sport2Positionsid.append(positionid)
                                }
                            }
                            
                        }else{
                            
                        }
                        
                    }
                }
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func dbsQuestion(_ sender: Any) {
        let alert = UIAlertController(title: "DBS", message: "The Disclosure and Barring Service (DBS) helps employers make safer recruitment decisions and prevent unsuitable people from working with vulnerable groups, including children. It replaces the Criminal Records Bureau (CRB) and Independent Safeguarding Authority (ISA).", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectNationality(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your country", rows: countries, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.nationalityButton.setTitle("Nationality: " + picked, for: UIControlState.normal)
                self.nationalityText = picked
                
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    @IBAction func selectPrimarySport(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select your first sport", rows: sports, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.primarySport.setTitle("Primary Sport: " + picked, for: UIControlState.normal)
                self.primarySportText = picked
                
                let sportindex = self.sports.index(of: picked)
                let sportid = self.sportid[sportindex!]
                
                Just.post(
                    API.getPositions,
                    data: ["key": API.key, "sportid":sportid]
                ) { r in
                    if r.ok {
                        let json = JSON(data: r.content!);
                        let success = json["success"].intValue
                        
                        print(json)
                        
                        self.sport1Positions.removeAll()
                        
                        if (success == 1){
                            if let items = json["positions"].array {
                                for item in items {
                                    let position = item["Position"].stringValue
                                    let positionid = item["ID"].intValue
                                    self.sport1Positions.append(position)
                                    self.sport1Positionsid.append(positionid)
                                }
                            }
                            
                        }else{
                            
                        }
                        
                    }
                }

            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    @IBAction func selectAvailablity(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Set your Availablity", rows: ["Available", "Not Available"], initialSelection: 0, doneBlock: {
            picker, value, index in
            
            if let picked = index as? String {
                self.availablity.setTitle(picked, for: UIControlState.normal)
                self.availabilityText = picked
                
                if (self.availabilityText == "Available"){
                    self.times.isHidden = false
                }else{
                    self.times.isHidden = true
                }
            } else {
                print("Error") // Was not a string
            }
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func selectTimes(_ sender: Any) {
        timeView.isHidden = false
    }
    @IBAction func selectLocation(_ sender: Any) {
        let locationPicker = LocationPicker()
        locationPicker.pickCompletion = { (pickedLocationItem) in
            
            self.locationButton.setTitle("Location set: " + pickedLocationItem.name, for: UIControlState.normal)
            let locationDictionary = pickedLocationItem.addressDictionary
            
            for (key, value) in locationDictionary! {
                if (key.description == "Street"){
                        self.street = value as! String
                }else if (key.description == "City"){
                        self.city = value as! String
                }else if (key.description == "ZIP"){
                        self.postcode = value as! String
                }
            }
            
            print(self.postcode)
        }
        locationPicker.addBarButtons()
        
        let navigationController = UINavigationController(rootViewController: locationPicker)
        present(navigationController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        
        nicknameText = nickname.text!
        dbsText = dbs.text!
        
        if (nicknameText != ""){
            if (nicknameText.count > 20){
                sendAlert(message: "Nickname can't be longer than 20 characters.")
                return
            }
        }
        if (primarySportText == ""){
            sendAlert(message: "Please enter your Primary Sport.")
            return
        }
        if (availabilityText == ""){
            sendAlert(message: "Please set your availability.")
            return
        }
        if (positionText == "0"){
            sendAlert(message: "Please enter your primary position.")
            return
        }
        
        if (dbsText.count > 0){
            if (!(dbsText.count > 10 && dbsText.count < 14)){
                sendAlert(message: "DBS number must be between 11 and 13 characters long")
                return
            }
            
            let letters = CharacterSet.letters
            let digits = CharacterSet.decimalDigits
            
            if (!letters.contains(dbsText.unicodeScalars.first!)){
                sendAlert(message: "DBS number must start with a letter")
                return
            }
            let dbsNumbers = dbsText.unicodeScalars.dropFirst()
            for uni in dbsNumbers {
                if !(digits.contains(uni)) {
                    showAlert("Error", message: "DBS number only contain numbers after the first letter", onView: self)
                    sendAlert(message: "DBS number only contain numbers after the first letter")
                    return
                }
            }
        }
        
        SwiftSpinner.show("Saving Profile")

        let defaults = UserDefaults.standard

        var dataDictionary = [String: Any]()
        
        dataDictionary["key"] = API.key
        dataDictionary["monAM"] = self.monAM.isOn
        dataDictionary["monPM"] = self.monPM.isOn
        dataDictionary["tuesAM"] = self.TuesAM.isOn
        dataDictionary["tuesPM"] = self.tuesPM.isOn
        dataDictionary["wedAM"] = self.wedAM.isOn
        dataDictionary["wedPM"] = self.wedPM.isOn
        dataDictionary["thursAM"] = self.thursAM.isOn
        dataDictionary["thursPM"] = self.thursPM.isOn
        dataDictionary["friAM"] = self.friAM.isOn
        dataDictionary["friPM"] = self.friPM.isOn
        dataDictionary["satAM"] = self.satAM.isOn
        dataDictionary["satPM"] = self.satPM.isOn
        dataDictionary["sunAM"] = self.sunAM.isOn
        dataDictionary["sunPM"] = self.sunPM.isOn
        dataDictionary["nickname"] = nicknameText
        dataDictionary["primarySport"] = primarySportText
        dataDictionary["secondarySport"] = secondarySportText
        dataDictionary["dbs"] = dbsText
        dataDictionary["availability"] = availabilityText
        
        dataDictionary["positionID"] = sport1PrimaryPosistion
        dataDictionary["position2ID"] = sport1SecondaryPosistion
        dataDictionary["secondarypositionID"] = sport2PrimaryPosistion
        dataDictionary["secondaryposition2ID"] = sport2SecondaryPosistion
        
        dataDictionary["height"] = heightText
        dataDictionary["weight"] = weightText
        dataDictionary["nationality"] = nationalityText
        dataDictionary["street"] = street
        dataDictionary["city"] = city
        dataDictionary["postcode"] = postcode
        dataDictionary["userID"] = defaults.integer(forKey: "ID")
        
        dataDictionary["strongest"] = strongest

        var fileDictionary = [String: HTTPFile]()
        if(profilePictureChanged){
            let imageData = UIImageJPEGRepresentation(self.profileImage.image!, 0.7)

            fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        }
        
        Just.post(
            API.saveProfileInfo,
            data: dataDictionary,
            files: fileDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    let defaults = UserDefaults.standard
                    defaults.setValue(false, forKey: "first")
                    defaults.synchronize()
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.performSegue(withIdentifier: "editToProfile", sender: nil)
                    }
                    
                    SwiftSpinner.hide()
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    SwiftSpinner.hide()
                }
                
            }
        }
    }
    @IBAction func timeDone(_ sender: Any) {
        timeView.isHidden = true
    }
    
    func setLayout(){
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        nickname.layer.cornerRadius = 15
        nickname.clipsToBounds = true
        
        primarySport.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        primarySport.layer.cornerRadius = 15
        primarySport.clipsToBounds = true
        
                secondarySport.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        secondarySport.layer.cornerRadius = 15
        secondarySport.clipsToBounds = true
        
        
        nationalityButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        nationalityButton.layer.cornerRadius = 15
        nationalityButton.clipsToBounds = true
        
        position.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        position.layer.cornerRadius = 15
        position.clipsToBounds = true
        
        position2.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        position2.layer.cornerRadius = 15
        position2.clipsToBounds = true
        
        strongestButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        strongestButton.layer.cornerRadius = 15
        strongestButton.clipsToBounds = true
        
        secondaryPosition.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        secondaryPosition.layer.cornerRadius = 15
        secondaryPosition.clipsToBounds = true
        
        secondaryPosition2.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        secondaryPosition2.layer.cornerRadius = 15
        secondaryPosition2.clipsToBounds = true
        
        availablity.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        availablity.layer.cornerRadius = 15
        availablity.clipsToBounds = true

        times.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        times.layer.cornerRadius = 15
        times.clipsToBounds = true
        
        done.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        done.layer.cornerRadius = 15
        done.clipsToBounds = true
        
        heightButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        heightButton.layer.cornerRadius = 15
        heightButton.clipsToBounds = true
        
        weightButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        weightButton.layer.cornerRadius = 15
        weightButton.clipsToBounds = true
        
        dbs.layer.cornerRadius = 15
        dbs.clipsToBounds = true
        
        locationButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        locationButton.layer.cornerRadius = 15
        locationButton.clipsToBounds = true
        
        cancel.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        cancel.layer.cornerRadius = 15
        cancel.clipsToBounds = true
        
        save.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        save.layer.cornerRadius = 15
        save.clipsToBounds = true
        
        timeView.layer.cornerRadius = 15
        timeView.clipsToBounds = true

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }
        
        countries = self.countries.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }

        
        timeView.isHidden = true

        let defaults = UserDefaults.standard
        let userID = defaults.integer(forKey: "ID")
        let isFirst = defaults.bool(forKey: "first")
        
        if (isFirst){
            cancel.isHidden = true;
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        
        Just.post(
            API.getProfileInfo,
            data: ["key": API.key, "userID": userID ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.nicknameText = json["profile"]["Nickname"].stringValue
                        self.nickname.text = self.nicknameText
                        
                        self.primarySportText = json["primarySport"]["Name"].stringValue
                        self.primarySport.setTitle("Primary Sport: " + self.primarySportText, for: UIControlState.normal)
                        
                        self.secondarySportText = json["secondarySport"]["Name"].stringValue
                        self.secondarySport.setTitle("Secondary Sport: " + self.secondarySportText, for: UIControlState.normal)
                        
                        self.position.setTitle("Primary Position: " + json["firstposition"]["Position"].stringValue, for: UIControlState.normal)
                        self.position2.setTitle("Secondary Position: " + json["secondposition"]["Position"].stringValue, for: UIControlState.normal)
                        self.secondaryPosition.setTitle("Primary Position: " + json["secondaryfirstposition"]["Position"].stringValue, for: UIControlState.normal)
                        self.secondaryPosition2.setTitle("Secondary Position: " + json["secondarysecondposition"]["Position"].stringValue, for: UIControlState.normal)

                        
                        self.sport1PrimaryPosistion = json["firstposition"]["PositionID"].intValue
                        self.sport1SecondaryPosistion = json["secondposition"]["PositionID"].intValue
                        self.sport2PrimaryPosistion = json["secondaryfirstposition"]["PositionID"].intValue
                        self.sport2SecondaryPosistion = json["secondarysecondposition"]["PositionID"].intValue

                        self.strongest = json["profile"]["Strongest"].stringValue
                        self.strongestButton.setTitle("Primary Hand/Foot: " + self.strongest, for: UIControlState.normal)
                        
                        
                        Just.post(
                            API.getPositions,
                            data: ["key": API.key, "sportid":json["primarySport"]["SportID"].intValue]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                self.sport1Positions.removeAll()
                                
                                if (success == 1){
                                    if let items = json["positions"].array {
                                        for item in items {
                                            let position = item["Position"].stringValue
                                            let positionid = item["ID"].intValue
                                            self.sport1Positions.append(position)
                                            self.sport1Positionsid.append(positionid)
                                        }
                                    }
                                    
                                }else{
                                    
                                }
                                
                            }
                        }
                        
                        Just.post(
                            API.getPositions,
                            data: ["key": API.key, "sportid":json["secondarySport"]["SportID"].intValue]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                print(json)
                                
                                self.sport2Positions.removeAll()
                                
                                if (success == 1){
                                    if let items = json["positions"].array {
                                        for item in items {
                                            let position = item["Position"].stringValue
                                            let positionid = item["ID"].intValue
                                            self.sport2Positions.append(position)
                                            self.sport2Positionsid.append(positionid)
                                        }
                                    }
                                    
                                }else{
                                    
                                }
                                
                            }
                        }


                        
                        self.profileImage.layer.borderWidth = 4
                        self.profileImage.layer.masksToBounds = false
                        self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
                        self.profileImage.clipsToBounds = true
                        self.profileImage.layer.borderColor = UIColor.white.cgColor

                        
                        let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                        self.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                        
                        
                        self.dbsText = json["profile"]["DBS"].stringValue
                        self.dbs.text = self.dbsText
                        
                        self.nationalityText = json["profile"]["Nationality"].stringValue
                        self.nationalityButton.setTitle("Nationality: " + self.nationalityText, for: UIControlState.normal)
                        
                        self.availabilityText = json["profile"]["Availability"].stringValue
                        
                        if (self.availabilityText == "YES"){
                            self.availablity.setTitle("Available", for: UIControlState.normal)
                            self.times.isHidden = false
                        }else{
                            self.availablity.setTitle("Not Available", for: UIControlState.normal)
                            self.times.isHidden = true
                        }
                        
                        
                        self.heightText = json["profile"]["Height"].stringValue
                        self.weightText = json["profile"]["Weight"].stringValue
                        
                        self.weightButton.setTitle("Weight: " + self.weightText + "kg", for: UIControlState.normal)
                        self.heightButton.setTitle("height: " + self.heightText + "cm", for: UIControlState.normal)
                        

                        if let items = json["availability"].array {
                            for item in items {
                                
                                switch item["Day"].stringValue {
                                case "Mon":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.monPM.isOn = true;
                                    }else{
                                        self.monAM.isOn = true;
                                    }
                                case "Tues":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.tuesPM.isOn = true
                                    }else{
                                        self.TuesAM.isOn = true
                                    }
                                case "Wed":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.wedPM.isOn = true
                                    }else{
                                        self.wedAM.isOn = true
                                    }
                                case "Thurs":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.thursPM.isOn = true
                                    }else{
                                        self.thursAM.isOn = true
                                    }
                                case "Fri":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.friPM.isOn = true
                                    }else{
                                        self.friAM.isOn = true
                                    }
                                case "Sat":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.satPM.isOn = true
                                    }else{
                                        self.satAM.isOn = true
                                    }
                                case "Sun":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.sunPM.isOn = true
                                    }else{
                                        self.sunAM.isOn = true
                                    }
                                default:
                                    if (item["aTime"].stringValue == "PM"){
                                        self.satPM.isOn = true
                                    }else{
                                        self.satAM.isOn = true
                                    }
                                }
                            }
                        }
                        
                        
                    }
                }
            }
            }
        
        Just.post(
            API.getAllSports,
            data: ["key": API.key]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["sports"].array {
                        for item in items {
                            let sport = item["Name"].stringValue
                            
                            self.sports.append(sport)
                            self.sportid.append(item["ID"].intValue)
                        }
                    }
                    
                }else{
                    
                }
                
            }
        }


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let imagePickerController = ImagePickerController()
        imagePickerController.imageLimit = 1
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }

    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        profileImage.image = images[0]
        profilePictureChanged = true
    }
    
    func sendAlert(message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
