//
//  VideoViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 31/08/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import ScalingCarousel
import MediaPlayer
import AVKit
import AVFoundation

class VideoCell: ScalingCarouselCell {}

class VideoViewController: UIViewController {

    var userid: String = ""
    @IBOutlet weak var close: UIButton!
    @IBOutlet weak var carousel:ScalingCarouselView!

    var videosArray = [video]()
    
    var moviePlayer:MPMoviePlayerController!

    
    let itemsPerRow: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    
    
    @IBAction func closeClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        close.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        close.layer.cornerRadius = 15
        close.clipsToBounds = true

        Just.post(
            API.getProfileInfo,
            data: ["key": API.key, "userID": userid, "ownID":"0"  ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        if let videos = json["videos"].array {
                            for videostring in videos {
                                if let url = videostring["File"].string {
                                    self.videosArray.append(video(videoUrl: URL(string: API.uploadBase +  url)!, imageURL: URL(string: API.uploadBase +  videostring["thumb"].string!)!, id: videostring["MediaID"].string!))
                                    
                                }
                            }
                            self.carousel.reloadData()
                        }
                    }
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func deleteVideo(_ sender: Any) {
        let touchPoint = carousel.convert(CGPoint.zero, from: sender as! UIView)
        if let indexPath = carousel.indexPathForItem(at: touchPoint) {
            let videoFromArray = videosArray[indexPath.row]
            Just.post(
                API.deleteVideo,
                data: ["key": API.key, "mediaID" : videoFromArray.ID ]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    if (success == 1){
                        self.videosArray.removeAll()
                        Just.post(
                            API.getProfileInfo,
                            data: ["key": API.key, "userID": self.userid, "ownID":"0"  ]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                if (success == 1){
                                    DispatchQueue.main.async { [unowned self] in
                                        if let videos = json["videos"].array {
                                            for videostring in videos {
                                                if let url = videostring["File"].string {
                                                    self.videosArray.append(video(videoUrl: URL(string: API.uploadBase +  url)!, imageURL: URL(string: API.uploadBase +  videostring["thumb"].string!)!, id: videostring["MediaID"].string!))
                                                    
                                                }
                                            }
                                            self.carousel.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        showAlert("Error", message: "Failed to remove contact", onView: self)
                    }
                    
                }
                
            }
        }
        
    }
    

}

typealias VideoCarouselDatasource = VideoViewController
extension VideoCarouselDatasource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let url = videosArray[indexPath.row].imageURL
        
        if let scalingCell = cell as? ScalingCarouselCell {
            let imageview = scalingCell.mainView as! UIImageView
            
            imageview.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let url = videosArray[indexPath.row].videoURL
        
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
}

typealias VideoCarouselDelegate = VideoViewController
extension VideoViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()
    }
}
