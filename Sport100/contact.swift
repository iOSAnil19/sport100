//
//  contact.swift
//  Sport100
//
//  Created by Zachary Powell on 21/05/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class contact {
    var name: String
    var ID: Int = 0
    var picture: String
    
    init (Name: String, ID: Int, Picture: String) {
        self.name = Name
        self.ID = ID
        self.picture = Picture
    }
    
    init(){
        name = ""
        picture = ""
    }
    
    public var description: String { return  "Name: " + name + " picture:" + picture }

}
