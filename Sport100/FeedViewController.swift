//
//  FeedViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 20/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import Player
import Fusuma
import AVKit
import AVFoundation
import ESPullToRefresh

class PostViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var sportView: UIImageView!
    @IBOutlet weak var cellView: UIView!
}

class BioViewCell: UITableViewCell {
    @IBOutlet weak var bio: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
}

class ImageViewCell: UITableViewCell {
    @IBOutlet weak var imageUploaded: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
}


class NewMatchViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var matchDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class NewTeamViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class TeamUpdatedViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class UpdatedMatchViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var awaysportImage: UIImageView!
    @IBOutlet weak var awayteamName: UILabel!
    @IBOutlet weak var matchDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class VideoUploadedViewCell: UITableViewCell {
    @IBOutlet weak var profileText: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var videoView: UIView!
}


class ProfileUpdatedViewCell: UITableViewCell {
    @IBOutlet weak var profileText: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
}

class TrainingAddedViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class TrainingUpdatedViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var teamDetails: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class HistoryViewCell: UITableViewCell {
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var historyText: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class AchievementViewCell: UITableViewCell {
    @IBOutlet weak var achievementText: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cellView: UIView!
}

class FeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FusumaDelegate {

    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var bottom: UIView!
    @IBOutlet weak var feedTable: UITableView!
    
    @IBOutlet weak var emptyFeed: UILabel!
    var userid = 0
    var start = 0
    var updateTime = Date()
    
    var posts = [post]()

    var feeduserid = 0
    var feedteamid = 0
    
    var videoIndex = 1
    
    var videos = [AVPlayer]()
    
    let fusuma = FusumaViewController()

    @IBAction func uploadMedia(_ sender: Any) {
        self.present(fusuma, animated: true, completion: nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fusuma.delegate = self
        fusuma.hasVideo = true
        feedTable.estimatedRowHeight = 68.0
        feedTable.rowHeight = UITableViewAutomaticDimension
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        topView.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        bottom.applyGradient(withColours: [UIColor(red:0.44, green:0.64, blue:0.81, alpha:1.0), UIColor(red:0.30, green:0.52, blue:0.80, alpha:1.0)
            
            ], gradientOrientation: .vertical)

        self.feedTable.es_addPullToRefresh {
            [unowned self] in
            Just.post(
                API.getFeed,
                data: ["key": API.key, "userID" : self.userid, "date" : self.updateTime.timeIntervalSince1970]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        if let items = json["result"].array {
                            for item in items {
                                var newPosts = [post]()
                                newPosts.append(post.init(Date: item["Created"].stringValue, Event: item["Event"].stringValue, TeamID: item["TeamID"].intValue, UserID: item["UserID"].intValue, Data: item["Data"].stringValue, ID: item["ID"].intValue, video: 0))
                                
                                self.posts.insert(contentsOf: newPosts, at: 0)
                                
                            }
                        }
                        self.updateTime = Date()

                        DispatchQueue.main.async { [unowned self] in
                            self.videos.removeAll()
                            self.feedTable.reloadData()
                            self.feedTable.es_stopPullToRefresh()
                        }
                    }else{
                        
                    }
                    
                }
            }

        }
        
        self.feedTable.es_addInfiniteScrolling {
            [unowned self] in
            Just.post(
                API.getFeed,
                data: ["key": API.key, "userID" : self.userid, "start" : self.start]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        if let items = json["result"].array {
                            for item in items {
                                self.posts.append(post.init(Date: item["Created"].stringValue, Event: item["Event"].stringValue, TeamID: item["TeamID"].intValue, UserID: item["UserID"].intValue, Data: item["Data"].stringValue, ID: item["ID"].intValue, video: 0))
                                
                            }
                        }
                        self.start = self.start + 10
                        DispatchQueue.main.async { [unowned self] in
                            self.videos.removeAll()
                            self.feedTable.reloadData()
                            self.feedTable.es_stopLoadingMore()
                        }
                    }else{
                        
                    }
                    
                }
            }
            
        }
 
        /*self.feedTable.es_addInfiniteScrolling {
            [weak self] in
            SwiftSpinner.show("Loading Feed")

            Just.post(
                API.getFeed,
                data: ["key": API.key, "userID" : self?.userid, "start" : self?.start]
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
 
                    SwiftSpinner.hide()
 
                    if (success == 1){
                        if let items = json["result"].array {
                            for item in items {
                                self?.posts.append(post.init(Date: item["Created"].stringValue, Event: item["Event"].stringValue, TeamID: item["TeamID"].intValue, UserID: item["UserID"].intValue, Data: item["Data"].stringValue, ID: item["ID"].intValue))
                            }
                        }
                        self?.start = (self?.start)! + 10
                        self?.feedTable.reloadData()
                        
                    }else{
                        
                    }
                    
                }
            }
            self?.feedTable.es_stopLoadingMore()
            /// If no more data
            //self?.feedTable.es_noticeNoMoreData()
        }*/
        
        SwiftSpinner.show("Loading Feed")
        
        let defaults = UserDefaults.standard
        userid = defaults.integer(forKey: "ID")
        
        
        Just.post(
            API.getFeed,
            data: ["key": API.key, "userID" : userid, "start" : start]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                SwiftSpinner.hide()
                
                if (success == 1){
                    if let items = json["result"].array {
                        for item in items {
                            self.posts.append(post.init(Date: item["Created"].stringValue, Event: item["Event"].stringValue, TeamID: item["TeamID"].intValue, UserID: item["UserID"].intValue, Data: item["Data"].stringValue, ID: item["ID"].intValue, video: 0))
                        }
                    }
                    self.updateTime = Date()
                    self.start = self.start + 10
                    DispatchQueue.main.async { [unowned self] in
                        self.feedTable.reloadData()
                        if (self.posts.count > 0){
                            self.feedTable.isHidden = false

                            self.emptyFeed.isHidden = true
                        }else{
                            self.feedTable.isHidden = true
                            self.emptyFeed.isHidden = false

                        }
                    }
                }else{
                    
                }
                
            }
        }
        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "feedToSearch", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "logout"){
            let defaults = UserDefaults.standard
            defaults.setValue(false, forKey: "logged")
            defaults.setValue(true, forKey: "first")
            
            defaults.setValue(0, forKey: "ID")
            defaults.synchronize()
        }else if segue.identifier == "feedToTeam" {
            let yourNextViewController = (segue.destination as! TeamViewController)
            yourNextViewController.teamid = feedteamid
            
        }else if segue.identifier == "feedToUser" {
                let yourNextViewController = (segue.destination as! ProfileViewController)
                yourNextViewController.userID = feeduserid
            
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func getPrettyDate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let unformattedDate = dateFormatter.date(from: date)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        dateFormatter1.doesRelativeDateFormatting = true
        return dateFormatter1.string(from: unformattedDate!)
    }
    
    func UpdatedMatchCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpdatedMatch", for: indexPath) as! UpdatedMatchViewCell
        
        
        cell.time.text = getPrettyDate(date: cellPost.Date)
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamName.text = json["team"]["Name"].stringValue
                cell.awayteamName.text = json["match"]["Away"].stringValue
                cell.sportImage.image = UIImage(named: sportImage)
                cell.awaysportImage.image = UIImage(named: sportImage)

                cell.matchDetails.text = json["match"]["Score"].stringValue
                
            }
        }
        
        return cell
    }

    
    func NewMatchCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMatch", for: indexPath) as! NewMatchViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
    
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamName.text = json["team"]["Name"].stringValue
                cell.sportImage.image = UIImage(named: sportImage)
                cell.matchDetails.text = "New game added: " + json["match"]["mDate"].stringValue + " against " + json["match"]["Away"].stringValue


            }
        }
        
        return cell
    }
    
    func TrainingAddedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingAdded", for: indexPath) as! TrainingAddedViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamDetails.text = "The team " + json["team"]["Name"].stringValue + " has added a new training time."
                cell.sportImage.image = UIImage(named: sportImage)
                
            }
        }
        
        return cell
    }
    
    func TrainingUpdatedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingUpdated", for: indexPath) as! TrainingUpdatedViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamDetails.text = "The team " + json["team"]["Name"].stringValue + " has updated a training time."
                cell.sportImage.image = UIImage(named: sportImage)
                
            }
        }
        
        return cell
    }

    func TeamCreatedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewTeam", for: indexPath) as! NewTeamViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamDetails.text = "The new team " + json["team"]["Name"].stringValue + " has been created."
                cell.sportImage.image = UIImage(named: sportImage)
                
            }
        }
        
        return cell
    }

    func TeamUpdatedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamUpdated", for: indexPath) as! TeamUpdatedViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        
        let r = Just.post(
            API.getFeedMatch,
            data: ["key": API.key, "teamID" : cellPost.TeamID, "matchID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            print(json)
            if (success == 1){
                
                var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                
                var sportImage = ""
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                cell.teamDetails.text = "The team " + json["team"]["Name"].stringValue + " has been updated."
                cell.sportImage.image = UIImage(named: sportImage)
                
            }
        }
        
        return cell
    }

    
    func imageCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Image", for: indexPath) as! ImageViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        
        
        let r = Just.post(
            API.getFeedImage,
            data: ["key": API.key, "userID" : cellPost.UserID, "mediaID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            if (success == 1){
                
                switch json["primarySport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                case "Basketball":
                    colourID = sportColour.basketball
                case "Football 11 a side":
                    colourID = sportColour.football11
                case "Football 7 a side":
                    colourID = sportColour.football7
                case "Football 5 a side":
                    colourID = sportColour.football5
                default:
                    colourID = sportColour.netball
                }
                
                cell.profileImage.layer.borderWidth = 3
                cell.profileImage.layer.masksToBounds = false
                cell.profileImage.layer.borderColor = colourID.cgColor
                cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
                cell.profileImage.clipsToBounds = true
                
                cell.profileName.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " uploaded an image:"
                let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                cell.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                
                let url2 = URL(string:API.uploadBase +  json["image"]["File"].stringValue)
                cell.imageUploaded.sd_setImage(with: url2, placeholderImage: UIImage(named: "default-profile-pic"))
            }
        }
        
        return cell
    }

    
    func bioCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Bio", for: indexPath) as! BioViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)
        cell.bio.text = cellPost.Data
        
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        
    
        let r = Just.post(
            API.getFeedProfile,
            data: ["key": API.key, "userID" : cellPost.UserID]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            if (success == 1){
                
                switch json["primarySport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                case "Basketball":
                    colourID = sportColour.basketball
                case "Football 11 a side":
                    colourID = sportColour.football11
                case "Football 7 a side":
                    colourID = sportColour.football7
                case "Football 5 a side":
                    colourID = sportColour.football5
                default:
                    colourID = sportColour.netball
                }

                cell.profileImage.layer.borderWidth = 3
                cell.profileImage.layer.masksToBounds = false
                cell.profileImage.layer.borderColor = colourID.cgColor
                cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
                cell.profileImage.clipsToBounds = true
                
                cell.profileName.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " has updated his bio:"
                let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                cell.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))

            }
        }
        
        return cell
    }
    
    func historyCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "History", for: indexPath) as! HistoryViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        var sportImage = ""
        var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        
        let r = Just.post(
            API.getFeedHistory,
            data: ["key": API.key, "historyID" : cellPost.Data]
        )

        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            if (success == 1){
                
                switch json["sport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                    gradientColourID = sportColour.netballGradient
                    sportImage = "netball"
                case "Basketball":
                    sportImage = "basketball"
                    colourID = sportColour.basketball
                    gradientColourID = sportColour.basketballGradient
                case "Football 11 a side":
                    sportImage = "11aside"
                    colourID = sportColour.football11
                    gradientColourID = sportColour.football11Gradient
                case "Football 7 a side":
                    sportImage = "7aside"
                    colourID = sportColour.football7
                    gradientColourID = sportColour.football7Gradient
                case "Football 5 a side":
                    sportImage = "fiveasideSM"
                    colourID = sportColour.football5
                    gradientColourID = sportColour.football5Gradient
                default:
                    sportImage = "netball"
                    colourID = sportColour.netball
                }
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                
                cell.sportImage.image = UIImage(named: sportImage)
                
                cell.historyText.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " Has added their history at the club " + json["history"]["Clubs"].stringValue
                
                
            }
        }
        
        return cell
    }
    
    func profileUpdatedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileUpdated", for: indexPath) as! ProfileUpdatedViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        
        let r = Just.post(
            API.getFeedProfile,
            data: ["key": API.key, "userID" : cellPost.UserID]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            
            if (success == 1){
                
                switch json["primarySport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                case "Basketball":
                    colourID = sportColour.basketball
                case "Football 11 a side":
                    colourID = sportColour.football11
                case "Football 7 a side":
                    colourID = sportColour.football7
                case "Football 5 a side":
                    colourID = sportColour.football5
                default:
                    colourID = sportColour.netball
                }
                
                cell.profileImage.layer.borderWidth = 3
                cell.profileImage.layer.masksToBounds = false
                cell.profileImage.layer.borderColor = colourID.cgColor
                cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
                cell.profileImage.clipsToBounds = true
                
                cell.profileText.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " has updated profile."
                let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                cell.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                
            }
        }
        
        return cell
    }

    func VideoUploadedCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoUploaded", for: indexPath) as! VideoUploadedViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
        
        let r = Just.post(
            API.getVideo,
            data: ["key": API.key, "userID" : cellPost.UserID, "mediaID" : cellPost.Data]
        )
        
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            if (success == 1){
                
                switch json["primarySport"]["Name"].stringValue {
                case "Netball":
                    colourID = sportColour.netball
                case "Basketball":
                    colourID = sportColour.basketball
                case "Football 11 a side":
                    colourID = sportColour.football11
                case "Football 7 a side":
                    colourID = sportColour.football7
                case "Football 5 a side":
                    colourID = sportColour.football5
                default:
                    colourID = sportColour.netball
                }
                
                cell.profileImage.layer.borderWidth = 3
                cell.profileImage.layer.masksToBounds = false
                cell.profileImage.layer.borderColor = colourID.cgColor
                cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
                cell.profileImage.clipsToBounds = true
                
                cell.profileText.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " has uploaded a video."
                let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                cell.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                
                let url2 = URL(string:API.uploadBase +  json["video"]["File"].stringValue)
                let player = AVPlayer(url: url2!)
                
                videos.append(player)
                
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = cell.videoView.bounds
                
                cell.videoView.layer.addSublayer(playerLayer)
                
                
                
            }
        }
        
        return cell
    }

    func achievementCellView(cellPost: post, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Achievement", for: indexPath) as! AchievementViewCell
        cell.time.text = getPrettyDate(date: cellPost.Date)

        cell.cellView.layer.cornerRadius = 15
        cell.cellView.clipsToBounds = true
        
        let colourID = UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0)
        let gradientColourID = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
        
        let r = Just.post(
            API.getFeedAchievement,
            data: ["key": API.key, "achievementID" : cellPost.Data]
        )
        if r.ok {
            let json = JSON(data: r.content!);
            let success = json["success"].intValue
            if (success == 1){
                
                cell.cellView.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                
                cell.achievementText.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue + " Has added their achievement: " + json["achievement"]["Description"].stringValue
                
                
            }
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let element = posts[indexPath.row]
        
        switch element.Event {
            case "History Added":
                return historyCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Achievement Added":
                return achievementCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Bio Updated":
                return bioCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Image Uploaded":
                return imageCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Match Added":
                return NewMatchCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Match Updated":
                return UpdatedMatchCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Profile Updated":
                return profileUpdatedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Team Created":
                return TeamCreatedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Team Updated":
                return TeamUpdatedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Training Time Added":
                return TrainingAddedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Training Time Updated":
                return TrainingUpdatedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            case "Video Uploaded":
                posts[indexPath.row].video = videoIndex
                videoIndex += 1
                let element = posts[indexPath.row]
                return VideoUploadedCellView(cellPost: element, indexPath: indexPath, tableView: tableView)
            default:
                return tableView.dequeueReusableCell(withIdentifier: "Post", for: indexPath) as! PostViewCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = posts[indexPath.row]
    
        if (element.video == 0){
            if element.TeamID != 0{
                self.feedteamid = element.TeamID
                self.performSegue(withIdentifier: "feedToTeam", sender: self)
            }else{
                self.feeduserid = element.UserID
                self.performSegue(withIdentifier: "feedToUser", sender: self)
            }

        }else{
            let player = videos[element.video-1]
            player.play()
        }
        
        
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard
        
        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    /*func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            print("size of image in MB: %f ", (imageSize / 1024)/1024 )
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    print(json)
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }*/
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }

}
