//
//  ProfileViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 28/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit
import Fusuma
import Gallery
import ImageIO
import Player

class ProfileHistoryViewCell: UITableViewCell {
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var clubLabel: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var sporticon: UIImageView!
    @IBOutlet weak var league: UILabel!
}

class ProfileAchievementViewCell: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}


class ProfileViewController: UIViewController, FusumaDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var searchBar: UIView!

    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var sendMessage: UIButton!
    
    @IBOutlet weak var editAchivement: UIButton!
    @IBOutlet weak var editHistory: UIButton!
    
    @IBOutlet weak var availableText: UILabel!
    @IBOutlet weak var availableImage: UIImageView!
    
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var dbs: UILabel!
    
    @IBOutlet weak var sportBox: UIView!
    @IBOutlet weak var weightBox: UIView!
    @IBOutlet weak var timeBox: UIView!
    
    
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var sportText: UILabel!
    @IBOutlet weak var firstPosition: UILabel!
    @IBOutlet weak var secondPosition: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var sportText2: UILabel!
    
    @IBOutlet weak var secondPosition2: UILabel!
    @IBOutlet weak var firstPosition2: UILabel!
    @IBOutlet weak var sportImage2: UIImageView!
    @IBOutlet weak var amMon: UIImageView!
    @IBOutlet weak var amTues: UIImageView!
    @IBOutlet weak var amWed: UIImageView!
    @IBOutlet weak var amThrus: UIImageView!
    @IBOutlet weak var amFri: UIImageView!
    @IBOutlet weak var amSat: UIImageView!
    @IBOutlet weak var amSun: UIImageView!
    @IBOutlet weak var pmMon: UIImageView!
    @IBOutlet weak var pmTues: UIImageView!
    @IBOutlet weak var pmWed: UIImageView!
    @IBOutlet weak var pmThurs: UIImageView!
    @IBOutlet weak var pmFri: UIImageView!
    @IBOutlet weak var pmSat: UIImageView!
    @IBOutlet weak var pmSun: UIImageView!
    
    @IBOutlet weak var bioText: UILabel!
    
    @IBOutlet weak var strongest: UILabel!
    @IBOutlet weak var flag: UIImageView!
    
    @IBOutlet weak var mediaBar: UIView!
    @IBOutlet weak var inviteButon: UIButton!
    
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var editbioButton: UIButton!
    
    @IBOutlet weak var editBioView: UIView!
    @IBOutlet weak var editBioText: UITextView!
    @IBOutlet weak var topBar: UIView!

    @IBOutlet weak var bottombar: UIView!
    var userID = 0
    var ownprofile = false;
    
    let fusuma = FusumaViewController()
    
    var videoURL = URL(string: "")

    var player = Player()

    @IBOutlet weak var bioCount: UILabel!
    @IBOutlet weak var firstSportEnd: UIView!
    @IBOutlet weak var secondSportEnd: UIView!
    //@IBOutlet weak var heightEnd: UIView!

    @IBOutlet weak var achivementView: UIView!
    @IBOutlet weak var historyView: UIView!
    
    var showTab = 0
    
    var selectedAttrs = [
        NSUnderlineStyleAttributeName : 1]
    
    var unSelectedAttrs = [
        NSUnderlineStyleAttributeName : 0]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    //HISTORY
    var historyTableData = [history]()
    @IBOutlet weak var historyTable: UITableView!

    var achievementTableData = [achivement]()
    @IBOutlet weak var achievementTable: UITableView!
    
    @IBAction func backSearchButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    func textViewDidChange(_ textView: UITextView) {
       let biotext = String(250 - textView.text.count)
       bioCount.text = biotext
    }
    @IBAction func messageContact(_ sender: Any) {
        self.performSegue(withIdentifier: "profileToMessage", sender: self)
    }
    
    @IBOutlet weak var bioView: UIView!
    @IBAction func editBio(_ sender: Any) {
        if (showTab == 0){
            if (editBioView.isHidden){
                editBioView.isHidden = false
            }else{
                editBioView.isHidden = true
            }
        }else if (showTab == 1){
            self.performSegue(withIdentifier: "profileToEditAchivement", sender: self)
        }else if (showTab == 2){
            self.performSegue(withIdentifier: "profileToEditHistory", sender: self)
        }
        
    }
    @IBAction func uploadVideo(_ sender: Any) {
    
        self.present(fusuma, animated: true, completion: nil)
    }
   
    @IBAction func addContact(_ sender: Any) {
        SwiftSpinner.show("Adding Contact")
        let defaults = UserDefaults.standard
        let ownID = defaults.integer(forKey: "ID")

        Just.post(
            API.addContact,
            data: ["key": API.key, "userID" : ownID, "otherID" : userID]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                SwiftSpinner.hide()

                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        self.contactButton.isHidden = true
                        
                        let alert = UIAlertController(title: "Success", message: "Player has been added to your contacts", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    DispatchQueue.main.async { [unowned self] in
                        
                        let alert = UIAlertController(title: "Error", message: "Failed to add, please try again", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
    }
    
    @IBAction func viewVideos(_ sender: Any) {
        /*self.player.view.frame = self.view.bounds
        
        self.player.url = videoURL
        
        self.addChildViewController(self.player)
        self.view.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        //self.player.fillMode = FillMode.resizeAspectFit.rawValue

        self.player.playbackLoops = true

        self.player.playFromBeginning()
        
        
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.player.view.addGestureRecognizer(tapGestureRecognizer)*/
        
        performSegue(withIdentifier: "profileToVideo", sender: self)

       
    }
    
    func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        self.player.stop()
        self.player.willMove(toParentViewController: self)
        self.player.view.removeFromSuperview()
        self.player.removeFromParentViewController()
    }
    
    @IBAction func saveBio(_ sender: Any) {
        
        
        if (editBioText.text.count > 250){
            let alert = UIAlertController(title: "Error", message: "Bio limited too 250 characters.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            

            return
        }
        
        SwiftSpinner.show("Saving Bio")

        Just.post(
            API.saveBio,
            data: ["key": API.key, "userID" : userID, "bio" : editBioText.text]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in

                    self.bioText.text = self.editBioText.text
                    
                    self.editBioView.isHidden = true
                    SwiftSpinner.hide()
                    }
                }else{
                    SwiftSpinner.hide()
                }
                
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
        super.viewDidAppear(animated)
        
        SwiftSpinner.show("Loading Profile")

        let defaults = UserDefaults.standard
        
        editBioView.isHidden = true
        if (userID == 0){
            userID = defaults.integer(forKey: "ID")
            ownprofile = true
        }
        
        let ownID = defaults.integer(forKey: "ID")
        
        if (ownprofile){
            editProfile.isHidden = false
            editbioButton.isHidden = false

            editAchivement.isHidden = false
            editHistory.isHidden = false
            
            sendMessage.isHidden = true
            inviteButon.isHidden = true
            contactButton.isHidden = true
            searchButton.isHidden = true
            
        }else{
            editbioButton.isHidden = true
            editProfile.isHidden = true
            
            editAchivement.isHidden = true
            editHistory.isHidden = true
        }
        Just.post(
            API.getProfileInfo,
            data: ["key": API.key, "userID": userID, "ownID":ownID  ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                if (success == 1){
                    DispatchQueue.main.async { [unowned self] in
                        
                        if (json["isContact"].boolValue){
                            self.contactButton.isHidden = true
                        }
                        var colourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                        var gradientColourID = UIColor(red:0.00, green:0.20, blue:0.80, alpha:1.0)
                        
                        var sportImage = ""
                        var sportImage2 = ""
                        
                        switch json["primarySport"]["Name"].stringValue {
                        case "Netball":
                            colourID = sportColour.netball
                            gradientColourID = sportColour.netballGradient
                            sportImage = "netball"
                        case "Basketball":
                            sportImage = "basketball"
                            colourID = sportColour.basketball
                            gradientColourID = sportColour.basketballGradient
                        case "Football 11 a side":
                            sportImage = "11aside"
                            colourID = sportColour.football11
                            gradientColourID = sportColour.football11Gradient
                        case "Football 7 a side":
                            sportImage = "7aside"
                            colourID = sportColour.football7
                            gradientColourID = sportColour.football7Gradient
                        case "Football 5 a side":
                            sportImage = "fiveasideSM"
                            colourID = sportColour.football5
                            gradientColourID = sportColour.football5Gradient
                        default:
                            sportImage = "netball"
                            colourID = sportColour.netball
                        }
                        
                        switch json["secondarySport"]["Name"].stringValue {
                        case "Netball":
                            sportImage2 = "netball"
                        case "Basketball":
                            sportImage2 = "basketball"
                        case "Football 11 a side":
                            sportImage2 = "11aside"
                        case "Football 7 a side":
                            sportImage2 = "7aside"
                        case "Football 5 a side":
                            sportImage2 = "fiveasideSM"
                        default:
                            sportImage2 = "netball"
                        }
                        
                        self.profilePicture.layer.borderWidth = 4
                        self.profilePicture.layer.masksToBounds = false
                        self.profilePicture.layer.borderColor = colourID.cgColor
                        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height/2
                        self.profilePicture.clipsToBounds = true
                        
                        let url = URL(string:API.uploadBase +  json["profileImage"]["File"].stringValue)
                        self.profilePicture.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile-pic"))
                        
                        self.sportImage.image = UIImage(named: sportImage);
                        self.sportImage2.image = UIImage(named: sportImage2);
                        
                        self.sportBox.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .topRightBottomLeft)
                        self.weightBox.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .topRightBottomLeft)
                        self.timeBox.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .topRightBottomLeft)
                        
                        self.topBar.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .vertical)
                        self.bottombar.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .vertical)

                        
                        self.mediaBar.applyGradient(withColours: [colourID, gradientColourID], gradientOrientation: .horizontal)
                        
                        self.name.text = json["user"]["FirstName"].stringValue + " " + json["user"]["LastName"].stringValue
                        self.nickname.text = json["profile"]["Nickname"].stringValue
                        self.dbs.text = json["profile"]["DBS"].stringValue
                        
                        self.strongest.text = json["profile"]["Strongest"].stringValue
                        
                        if (json["profile"]["Availability"].stringValue == "YES"){
                            self.availableText.text = "Available"
                            self.availableImage.image = UIImage(named: "avaible-on")
                        }
                        
                        self.sportText2.text = json["secondarySport"]["Name"].stringValue
                        
                        self.sportText.text = json["primarySport"]["Name"].stringValue
                        self.height.text = json["profile"]["Height"].stringValue + "cm"
                        self.weight.text = json["profile"]["Weight"].stringValue + "kg"
                        self.location.text = json["location"]["Town"].stringValue
                        
                        self.firstPosition.text = json["firstposition"]["Position"].stringValue
                        self.secondPosition.text = json["secondposition"]["Position"].stringValue
                        
                        self.firstPosition2.text = json["secondaryfirstposition"]["Position"].stringValue
                        self.secondPosition2.text = json["secondarysecondposition"]["Position"].stringValue
                        
                        self.bioText.text = json["profile"]["Bio"].stringValue
                        self.editBioText.text = json["profile"]["Bio"].stringValue
                        
                        var nationalityString = json["profile"]["Nationality"].stringValue
                        nationalityString = nationalityString.replacingOccurrences(of: " ", with: "-")
                        
                        self.videoURL = URL(string: API.uploadBase +  json["videos"][0]["File"].stringValue)
                        self.flag.image = UIImage(named: nationalityString)
                        
                        /*self.pictures.removeAll()
                        if let items = json["pictures"].array {
                            for item in items {
                                self.pictures.append(item["File"].stringValue)
                            }
                            
                        }*/

                        
                        if let items = json["availability"].array {
                            for item in items {
                                
                                switch item["Day"].stringValue {
                                case "Mon":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmMon.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amMon.image = UIImage(named: "avaible-on")
                                    }
                                case "Tues":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmTues.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amTues.image = UIImage(named: "avaible-on")
                                    }
                                case "Wed":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmWed.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amWed.image = UIImage(named: "avaible-on")
                                    }
                                case "Thurs":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmThurs.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amThrus.image = UIImage(named: "avaible-on")
                                    }
                                case "Fri":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmFri.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amFri.image = UIImage(named: "avaible-on")
                                    }
                                case "Sat":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmSat.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amSat.image = UIImage(named: "avaible-on")
                                    }
                                case "Sun":
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmSun.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amSun.image = UIImage(named: "avaible-on")
                                    }
                                default:
                                    if (item["aTime"].stringValue == "PM"){
                                        self.pmMon.image = UIImage(named: "avaible-on")
                                    }else{
                                        self.amMon.image = UIImage(named: "avaible-on")
                                    }
                                }
                                
                            }
                            self.view.isHidden = false

                            SwiftSpinner.hide()
                        }
                        
                        
                        
                    }
                    
                    
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    SwiftSpinner.hide()
                    
                    
                    return
                }
                
            }
        }

        searchBar.layer.cornerRadius = 15
        searchBar.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.search (_:)))
        self.searchBar.addGestureRecognizer(gesture)
        
        
        var dataDictionary = [String: Any]()

        dataDictionary["key"] = API.key
        dataDictionary["userID"] = userID

        Just.post(
            API.getHistory,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                if (success == 1){
                    if let items = json["history"].array {
                        for item in items {
                            let newHistory = history.init(ID:item["ID"].intValue , sport: item["Sportsname"].stringValue, level: item["Level"].stringValue, club: item["Club"].stringValue, joined: item["Joined"].stringValue, left: item["hLeft"].stringValue, league: item["LeagueName"].stringValue)
                            
                            self.historyTableData.append(newHistory)
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.historyTable.reloadData()
                    }
                }
                SwiftSpinner.hide()
                
            }
        }

        Just.post(
            API.getAchievement,
            data: dataDictionary
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                
                if (success == 1){
                    if let items = json["achievement"].array {
                        for item in items {
                            let newAchievement = achivement.init(ID: item["ID"].intValue, description: item["Description"].stringValue, date: item["aDate"].stringValue)
                            self.achievementTableData.append(newAchievement)
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        
                        self.achievementTable.reloadData()
                    }
                }
                SwiftSpinner.hide()
                
            }
        }

        
        // Do any additional setup after loading the view.
    }
    
    func search(_ sender:UITapGestureRecognizer){
        self.performSegue(withIdentifier: "profileToSearch", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.isHidden = true
        
        fusuma.delegate = self
        fusuma.hasVideo = true
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
        
        sportBox.layer.cornerRadius = 15
        sportBox.clipsToBounds = true
        weightBox.layer.cornerRadius = 15
        weightBox.clipsToBounds = true
        timeBox.layer.cornerRadius = 15
        timeBox.clipsToBounds = true
        
        bioView.layer.cornerRadius = 15
        bioView.clipsToBounds = true
        
        achivementView.layer.cornerRadius = 15
        achivementView.clipsToBounds = true
        
        historyView.layer.cornerRadius = 15
        historyView.clipsToBounds = true
        
        mediaBar.layer.cornerRadius = 15
        mediaBar.clipsToBounds = true
        
        sendMessage.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        sendMessage.layer.cornerRadius = 15
        sendMessage.clipsToBounds = true
        
        inviteButon.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        inviteButon.layer.cornerRadius = 15
        inviteButon.clipsToBounds = true
        
        searchButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        searchButton.layer.cornerRadius = 15
        searchButton.clipsToBounds = true
        
        contactButton.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)  ], gradientOrientation: .vertical)
        contactButton.layer.cornerRadius = 15
        contactButton
            .clipsToBounds = true
        
        
        var recognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(swipeLeft))
        recognizer.direction = .left
        self.view.addGestureRecognizer(recognizer)
        
        recognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }
    
    func swipeLeft(recognizer : UISwipeGestureRecognizer)
    {
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func swipeRight(recognizer : UISwipeGestureRecognizer)
    {
        self.performSegue(withIdentifier: "backhome", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        fusuma.dismiss(animated: true, completion: nil)
        
        var fileDictionary = [String: HTTPFile]()
        let imageData = UIImageJPEGRepresentation(image, 0.7)
        fileDictionary["profile_photo"] = .data("test", imageData!, "jpeg")
        
        let defaults = UserDefaults.standard

        Just.post(
            API.uploadImage,
            data: ["key": API.key, "userID" : defaults.integer(forKey: "ID")],
            files: fileDictionary
            
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                if (success == 1){
                    
                    let alert = UIAlertController(title: "Success", message: "Image Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    Just.post(
                        API.getProfileInfo,
                        data: ["key": API.key, "userID": self.userID ]
                    ) { r in
                        if r.ok {
                            let json = JSON(data: r.content!);
                            let success = json["success"].intValue
                            
                            if (success == 1){
                              
                                
                                
                            }else{
                                let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                                DispatchQueue.main.async { [unowned self] in
                                    self.present(alert, animated: true, completion: nil)
                                }
                                
                                
                                return
                            }
                        }
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        }
        
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        var movieData: NSData?
        do {
            movieData = try NSData(contentsOfFile: (fileURL.relativePath), options: NSData.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        let alertController = UIAlertController(title: "Upload Video", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Upload", style: .default, handler: {
            alert -> Void in
            
            let title = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            
            let imageSize: Int = movieData!.length
            
            var fileDictionary = [String: HTTPFile]()
            fileDictionary["video"] = .data("video", movieData! as Data, "mov")
            
            let defaults = UserDefaults.standard
            
            Just.post(
                API.uploadVideo,
                data: ["key": API.key, "userID" : defaults.integer(forKey: "ID"), "videoTitle" : title.text ?? "", "videoDescription":secondTextField.text ?? ""],
                files: fileDictionary
                
            ) { r in
                if r.ok {
                    let json = JSON(data: r.content!);
                    let success = json["success"].intValue
                    
                    if (success == 1){
                        
                        let alert = UIAlertController(title: "Success", message: "Video Uploaded!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        Just.post(
                            API.getProfileInfo,
                            data: ["key": API.key, "userID": self.userID ]
                        ) { r in
                            if r.ok {
                                let json = JSON(data: r.content!);
                                let success = json["success"].intValue
                                
                                print(json)
                                
                                if (success == 1){
                                    
                                    self.videoURL = URL(string: API.uploadBase +  json["videos"][0]["File"].stringValue)
                                    
                                    
                                }else{
                                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                                    DispatchQueue.main.async { [unowned self] in
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    
                                    
                                    return
                                }
                            }
                        }
                        
                    }else{
                        let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                        DispatchQueue.main.async { [unowned self] in
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Title"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Video Description"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async { [unowned self] in
            self.present(alertController, animated: true, completion: nil)
        }
        
        //self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    
    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
        
        let alert = UIAlertController(title: "Access Requested", message: "Saving image needs to access your photo album", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) -> Void in
            
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fusumaClosed() {
        print("Called when the FusumaViewController disappeared")
    }
    
    func fusumaWillClosed() {
        print("Called when the close button is pressed")
    }
 
    //profileToTeams

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileToTeams" {
            print(userID)
            let yourNextViewController = (segue.destination as! ViewTeamsViewController)
            yourNextViewController.userid = userID
            
        }else if segue.identifier == "profileToGallery"{
            let yourNextViewController = (segue.destination as! GalleryViewController)
            yourNextViewController.userid = userID

        }else if segue.identifier == "profileToMessage"{
            let yourNextViewController = (segue.destination as! SendNewMessageViewController)
            yourNextViewController.pickedID = userID
            yourNextViewController.name = name.text!
            
        }else if segue.identifier == "profileToVideo"{
            let yourNextViewController = (segue.destination as! VideoViewController)
            yourNextViewController.userid = String(userID)
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        print("ALL")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.achievementTable){
            print(achievementTableData.count)
            return achievementTableData.count
        }else{
            return historyTableData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        if (tableView == self.achievementTable){
            let element = achievementTableData[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "achievement", for: indexPath) as! ProfileAchievementViewCell
            
            cell.descriptionLabel.text = element.description
            cell.dateLabel.text = element.date
            print("ACHIVEMENT CELL")
            return cell
            
        }else{
            let element = historyTableData[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "history", for: indexPath) as! ProfileHistoryViewCell
            
            
            cell.clubLabel.text = element.club
            cell.joinedLabel.text = element.joined
            cell.leftLabel.text = element.left
            cell.levelLabel.text = element.level
            cell.league.text = element.league
            
            var sportImage = ""
            
            switch element.sport {
            case "Netball":
                sportImage = "netball-colour"
            case "Basketball":
                sportImage = "basketball-colour"
            case "Football 11 a side":
                sportImage = "football11-colour"
            case "Football 7 a side":
                sportImage = "football7-colour"
            case "Football 5 a side":
                sportImage = "Football5-colour"
            default:
                sportImage = "netball"
            }
            
            cell.sporticon.image = UIImage(named: sportImage)
                       
            return cell
        }
    }

    
}


