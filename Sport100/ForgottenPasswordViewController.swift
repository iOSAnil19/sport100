//
//  ForgottenPasswordViewController.swift
//  Sport100
//
//  Created by Zachary Powell on 20/02/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import UIKit

class ForgottenPasswordViewController: UIViewController {
    
    //MARK: ----------- OUTLETS/VARIABLES ------------

    @IBOutlet weak var email: UITextField!
    
    //MARK: ----------- VIEW LIFE CYCLE ------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.applyGradient(withColours: [UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
            
            ], gradientOrientation: .vertical)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: ----------- BUTTON ACTION METHODS ------------
    
    @IBAction func buttonClicked_Login(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func resetPassword(_ sender: Any) {
        
        if (email.text == ""){
            let alert = UIAlertController(title: "Error", message: "Please enter your Email.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        self.view.endEditing(true)
        
        SwiftSpinner.show("Sending Reset Request...")
        
        Just.post(
            API.reset,
            data: ["key": API.key, "email": email.text! ]
        ) { r in
            if r.ok {
                let json = JSON(data: r.content!);
                let success = json["success"].intValue
                
                print(json)
                if (success == 1){
                    let alert = UIAlertController(title: "Success", message: "A link to reset your password has been emailed to you.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        self.performSegue(withIdentifier: "resetToLogin", sender: nil)
                    }
                    
                    SwiftSpinner.hide()
                    
                    
                } else{
                    let alert = UIAlertController(title: "Error", message: json["error"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    DispatchQueue.main.async { [unowned self] in
                        self.present(alert, animated: true, completion: nil)
                        self.performSegue(withIdentifier: "resetToLogin", sender: nil)

                    }
                    
                    SwiftSpinner.hide()
                    return
                }
                
            }
        }
    }
}
