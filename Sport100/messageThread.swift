//
//  messageThread.swift
//  Sport100
//
//  Created by Zachary Powell on 02/06/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class messageThread {
    var name: String
    var ID: Int = 0
    var picture: String
    var lastmessageDate: String
    
    init (Name: String, ID: Int, Picture: String, lastmessageDate: String) {
        self.name = Name
        self.ID = ID
        self.picture = Picture
        self.lastmessageDate = lastmessageDate
    }
    
    init(){
        name = ""
        picture = ""
        lastmessageDate = ""
    }
    
    public var description: String { return  "Name: " + name + " picture:" + picture }
    
}
