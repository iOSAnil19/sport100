//
//  message.swift
//  Sport100
//
//  Created by Zachary Powell on 07/06/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation

class message {
    var ID: Int = 0
    var message: String
    var messageDate: String
    
    var fromContact: contact
    var toContact: contact

    
    init (message: String, ID: Int, messageDate: String, fromContact: contact, toContact: contact) {
        self.message = message
        self.ID = ID
        self.messageDate = messageDate
        self.fromContact = fromContact
        self.toContact = toContact
    }
    
    init(){
        message = ""
        messageDate = ""
        fromContact = contact.init()
        toContact = contact.init()
    }
    
}
