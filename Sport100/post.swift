//
//  post.swift
//  Sport100
//
//  Created by Zachary Powell on 19/04/2017.
//  Copyright © 2017 Zachary Powell. All rights reserved.
//

import Foundation
import UIKit

class post {
    var Date: String
    var Event: String
    var TeamID: Int
    var UserID: Int
    var Data: String
    var ID:Int
    var video:Int

    
    init (Date: String, Event: String, TeamID: Int, UserID: Int, Data: String, ID: Int, video: Int) {
        self.Date = Date
        self.Event = Event
        self.TeamID = TeamID
        self.UserID = UserID
        self.Data = Data
        self.ID = ID
        self.video = video
    }
    
    init(){
        self.Date = ""
        self.Event = ""
        self.Data = ""
        self.TeamID = 0
        self.UserID = 0
        ID = 0
        video=0
    }
}
